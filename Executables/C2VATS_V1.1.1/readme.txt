C2VATS V 1.1.1

Installation Guide:

- Unzip C2VatsSetup.zip file
- Double click on result file C2VatsSetup.msi
- Follow instructions and click yes to all
- A shortcut C2ValidatorATS.exe will be created in your desktop

The installation folder is C:\Program Files (x86)\SuzoHapp\C2VATS\C2VATS_Vx.x\

Uninstalling C2ValidatorATS

- Click on setup file C2VatsSetup.msi
- Choose option "Remove Application"

--------------------------About this Release------------------------
ONLY diff between version 1.1 and 1.1.1 is the cx2.dll
The rest is exactly the same

Test Cases included in this release:

- Material Sensor Test, No Pulse  

- Sorter Gates Test                

- Validator Door Test

- Sorter Door Test				   

- Lever Status Test

- Light Sensor Test

- Coin Position Sensor Test

- HMI Key Status Test   	

- HMI Display Test       New in this release


New Features implemented:

- Able to save logs in file, optional via quick button or menu.

- Animation to indicate the user to take action in some test cases.

- Logger Implementation cont. Several testcase using logger for test.

- Debug Mode test partially implemented.

- Possible for the user to switch between AT Mode and Debug Mode via Menu.

- Hints when hoving the mouse over the buttons.

- The installation create a folder with the version, making it possible to have different versions installed.

- Bug fixes.

- 7 Debug test included.


Other Notes:

