C2VATS V 1.0

Installation Guide:

- Unzip C2VatsSetup.zip file
- Double click on result file C2VatsSetup.msi
- Follow instructions and click yes to all
- A shortcut C2ValidatorATS.exe will be created in your desktop

The installation folder is C:\Program Files (x86)\SuzoHapp\C2VATS

Uninstalling C2ValidatorATS

- Click on setup file C2VatsSetup.msi
- Choose option "Remove Application"

--------------------------About this Release------------------------
Test Cases included in this release:

- Material Sensor Test, No Pulse  

- Sorter Gates Test                

- Validator Door Test

- Sorter Door Test				   

- Lever Status Test

- Light Sensor Test

- Coin Position Sensor Test

- HMI Key Status Test   	New in this releas


New Features implemented:

- Test Session Indicating the status of the whole session

- Animation to indicate the user to take action in some test cases

- Logger Implementation. Several testcase using logger for test

- Log level hard coded to INFO

- Debug Mode test partially implemented.

- Possible for the user to switch between AT Mode and Debug Mode via quick buttons(No possible yet via Menu)

- About box showing version of the application and company.

- SW architecture enhances.

- Bug fixes


Other Notes:

