C2VATS V 1.3

Installation Guide:

- Unzip C2VatsSetup.zip file
- Double click on result file C2VatsSetup.msi
- Follow instructions and click Yes to all
- A shortcut C2ValidatorATS.exe will be created in your desktop

The installation folder is C:\Program Files (x86)\SuzoHapp\C2VATS\C2VATS_Vx.x\

Uninstalling C2VATS

- Click on setup file C2VatsSetup.msi
- Choose option "Remove Application"


--------------------------About this Release------------------------

Test Cases included in this release:

- Material Sensor Test, No Pulse  

- Sorter Gates Test                

- Validator Door Test

- Sorter Door Test				   

- Lever Status Test

- Light Sensor Test

- Coin Position Sensor Test

- HMI Key Status Test   	

- HMI Display Test      

- Coin Insertion. 1 Eur Coin Test   

- RIM Sensor test *New in this release 


Debug Cases (ONLY the one included in 1.2 are listed here). See other release notes for total overview

- LS1 Stability Test

- LS2 Stability Test

- LS3 Stability Test



New Features implemented:

- RIM Sensor availability selectable by customer during the Acceptance Test Mode. 

- HMI Door only test feature added. Selectable by customer via Configuration menu. 

- "Fail" test case button. Used when test cases requiring using interaction does not react or incorrectly react to user actions.

- All test cases requiring user interaction shows indicative pictures.

- HMI and Key board always detetected by the application. Thus showing Test Cases corresponding to the configuration under test.

- More Coins added to test in Debug test "Coin Drop Evaluation test"

- Light Sensor test does not fail if datablock is not present.

- Bug fixes.

- Minor improvements.

Other Notes:


