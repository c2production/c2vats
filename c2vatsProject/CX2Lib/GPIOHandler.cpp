
#include "stdafx.h"
#include "GPIOHandler.h"
#include "C_CX2.h"
#include "DeviceUtils.h"
#include <vector>

/* Command strings */
#define CMD_GPIO_SET		"gpio set %d\r"
#define CMD_GPIO_CLEAR		"gpio clear %d\r"
#define CMD_GPIO_READ		"gpio read %d\r"

/* Command len */
#define CMD_GPIO_SET_LEN	11
#define CMD_GPIO_CLEAR_LEN	13
#define CMD_GPIO_READ_LEN	12

/* Expected response len */
#define RESP_GPIO_READ_LEN	(CMD_GPIO_READ_LEN + 5)

/* Static members definition and initialization */
HANDLE GPIOHandler::m_hComPort = INVALID_HANDLE_VALUE;
char GPIOHandler::m_cmdBuffer[32] = { 0x00 };
char GPIOHandler::m_responseBuffer[32] = { 0x00 };
char GPIOHandler::m_response[32] = { 0x00 };
unsigned char GPIOHandler::m_DIOState[GPIO_MAX_NUM] = { GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN };
unsigned char GPIOHandler::m_DIOStateSaved[GPIO_MAX_NUM] = { GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN, GPIO_UNKNOWN };

extern C_CX2 **pSDL;

bool GPIOHandler::Open(void)
{
	bool retCode = false, deviceFound = false;
	DWORD cmdLen, bytesNum;
	std::vector<UINT> ports;
	std::vector<std::wstring> friendlyNames;
	wchar_t portName[32];

	/* If device is not open */
	if (m_hComPort == INVALID_HANDLE_VALUE)
	{
		/* Obtain che list of COM devices */
		if (CEnumerateSerial::UsingSetupAPI2(ports, friendlyNames))
		{
			/* Scan the found devices searching for the GPIO module */
			for (unsigned int devCnt = 0; devCnt < ports.size(); devCnt++)
			{
				if ((friendlyNames[devCnt].find(L"Numato Lab") != std::string::npos) &&
					(friendlyNames[devCnt].find(L"USB GPIO Module") != std::string::npos))
				{
					/* Build COM port name */
					wsprintf(portName, L"\\\\.\\COM%d", ports[devCnt]);
					deviceFound = true;
					break;
				}
			}
		}

		if (deviceFound)
		{
			/* Obtain the serial handle */
			if ((m_hComPort = CreateFile(portName, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0)) != INVALID_HANDLE_VALUE)
			{
				/* Set timeout properties */
				COMMTIMEOUTS timeouts;
				timeouts.ReadIntervalTimeout = 100;
				timeouts.ReadTotalTimeoutMultiplier = 0;
				timeouts.ReadTotalTimeoutConstant = 100;
				timeouts.WriteTotalTimeoutMultiplier = 0;
				timeouts.WriteTotalTimeoutConstant = 100;
				SetCommTimeouts(m_hComPort, &timeouts);

				/* Flush the Serial port's RX buffer */
				PurgeComm(m_hComPort, PURGE_RXCLEAR | PURGE_RXABORT);

				/* Write a Carriage Return to make sure that any partial commands or junk
				   data left in the command buffer is cleared */
				m_cmdBuffer[0] = '\r';
				cmdLen = 1;
				if ((WriteFile(m_hComPort, m_cmdBuffer, cmdLen, &bytesNum, NULL)) && (bytesNum == cmdLen))
				{
					retCode = true;
				}
			}
		}
	}

	return retCode;
}

bool GPIOHandler::IsOpen(void)
{
	return ((m_hComPort != INVALID_HANDLE_VALUE) || (SDL_PTR != NULL));
}

bool GPIOHandler::Close(void)
{
	/* Close the serial handle */
	if (m_hComPort != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_hComPort);
		m_hComPort = INVALID_HANDLE_VALUE;

		return true;
	}

	return false;
}

bool GPIOHandler::HardResetSet(unsigned char state)
{
	return(Set(GPIO_RESET, state));
}

bool GPIOHandler::StartPrintSet(unsigned char state)
{
	if ((m_hComPort == INVALID_HANDLE_VALUE) && (SDL_PTR != NULL))
	{
		// invert logic on SDL
		return(Set(GPIO_START_PRINT, (state == GPIO_ON ? GPIO_OFF : GPIO_ON)));
	}
	else
	{
		return(Set(GPIO_START_PRINT, state));
	}
}

bool GPIOHandler::HardReset(void)
{
	/* Drive the external RESET pin to reset all the
	Currenza boards */
	bool retCode = Set(GPIO_RESET, GPIO_OFF);
	if (retCode)
	{
		Sleep(500);
		retCode = Set(GPIO_RESET, GPIO_ON);
	}

	return retCode;
}

bool GPIOHandler::StartPrint(void)
{
	/* Drive the external StartPrint pin to start the audit print */
	bool retCode = StartPrintSet(GPIO_OFF);
	if (retCode)
	{
		Sleep(250);
		retCode = StartPrintSet(GPIO_ON);
	}

	return retCode;
}

bool GPIOHandler::MasterReqSet(unsigned char state)
{
	return(Set(GPIO_MASTERREQ, state));
}

bool GPIOHandler::GwFlashSet(unsigned char state)
{
	return(Set(GPIO_GWFLASH, state));
}

bool GPIOHandler::EmpFlashSet(unsigned char state)
{
	return(Set(GPIO_EMPFLASH, state));
}

bool GPIOHandler::PowerOn(void)
{
	bool retCode = false;

	if ((m_hComPort == INVALID_HANDLE_VALUE) && (SDL_PTR != NULL))
	{
		retCode = SDL_PTR->SetOutPin(GPIO_POWER);
	}

	return retCode;
}

bool GPIOHandler::MDBPowerOn(void)
{
	bool retCode = false;

	if ((m_hComPort == INVALID_HANDLE_VALUE) && (SDL_PTR != NULL))
	{
		retCode = SDL_PTR->SetOutPin(GPIO_MDB_POWER);
	}

	return retCode;
}

bool GPIOHandler::MDBPowerOff(void)
{
	bool retCode = false;

	if ((m_hComPort == INVALID_HANDLE_VALUE) && (SDL_PTR != NULL))
	{
		retCode = SDL_PTR->ResetOutPin(GPIO_MDB_POWER);
	}

	return retCode;
}


bool GPIOHandler::PowerOff(void)
{
	bool retCode = false;

	if ((m_hComPort == INVALID_HANDLE_VALUE) && (SDL_PTR != NULL))
	{
		retCode = SDL_PTR->ResetOutPin(GPIO_POWER);
	}

	return retCode;
}

bool GPIOHandler::SetCmd(unsigned char id)
{
	bool retCode = false;
	DWORD bytesNum;

	/* If device is open */
	if (m_hComPort != INVALID_HANDLE_VALUE)
	{
		/* Flush the Serial port's RX buffer */
		PurgeComm(m_hComPort, PURGE_RXCLEAR | PURGE_RXABORT);

		/* Send gpio set command */
		sprintf_s(m_cmdBuffer, CMD_GPIO_SET, id);
		if ((WriteFile(m_hComPort, m_cmdBuffer, CMD_GPIO_SET_LEN, &bytesNum, NULL)) && (bytesNum == CMD_GPIO_SET_LEN))
		{
			retCode = true;
		}
	}
	else
	{
		if (SDL_PTR != NULL)
		{
			SDL_PTR->SDLPurgeRx();
			retCode = SDL_PTR->SetOutPin(id);
		}
	}

	if (retCode)
	{
		m_DIOState[id] = GPIO_ON;
	}

	return retCode;
}

bool GPIOHandler::ClearCmd(unsigned char id)
{
	bool retCode = false;
	DWORD bytesNum;

	/* If device is open */
	if (m_hComPort != INVALID_HANDLE_VALUE)
	{
		/* Flush the Serial port's RX buffer */
		PurgeComm(m_hComPort, PURGE_RXCLEAR | PURGE_RXABORT);

		/* Send gpio clear command */
		sprintf_s(m_cmdBuffer, CMD_GPIO_CLEAR, id);
		if ((WriteFile(m_hComPort, m_cmdBuffer, CMD_GPIO_CLEAR_LEN, &bytesNum, NULL)) && (bytesNum == CMD_GPIO_CLEAR_LEN))
		{
			retCode = true;
		}
	}
	else
	{
		if (SDL_PTR != NULL)
		{
			SDL_PTR->SDLPurgeRx();
			retCode = SDL_PTR->ResetOutPin(id);
		}
	}

	if (retCode)
	{
		m_DIOState[id] = GPIO_OFF;
	}

	return retCode;
}

bool GPIOHandler::ReadCmd(unsigned char id, unsigned char *state)
{
	bool retCode = false;
	DWORD bytesNum;

	/* If device is open */
	if (m_hComPort != INVALID_HANDLE_VALUE)
	{
		/* Flush the Serial port's RX buffer */
		PurgeComm(m_hComPort, PURGE_RXCLEAR | PURGE_RXABORT);

		/* Send gpio read command */
		sprintf_s(m_cmdBuffer, CMD_GPIO_READ, id);
		if ((WriteFile(m_hComPort, m_cmdBuffer, CMD_GPIO_READ_LEN, &bytesNum, NULL)) && (bytesNum == CMD_GPIO_READ_LEN))
		{
			/* Read the response */
			if ((ReadFile(m_hComPort, m_responseBuffer, RESP_GPIO_READ_LEN, &bytesNum, NULL)) && (bytesNum == RESP_GPIO_READ_LEN))
			{
				/* Add terminator to the returned string */
				m_responseBuffer[bytesNum] = '\0';

				/* Extract the response */
				char *respPtr = ExtractResponse(m_cmdBuffer, m_responseBuffer);

				/* On or off? */
				if (*respPtr == '0')
				{
					*state = GPIO_OFF;
					m_DIOState[id] = GPIO_OFF;
					retCode = true;
				}
				else if (*respPtr == '1')
				{
					*state = GPIO_ON;
					m_DIOState[id] = GPIO_ON;
					retCode = true;
				}
			}
		}
	}
	else
	{
		if (SDL_PTR != NULL)
		{
			retCode = SDL_PTR->GetOutPin(&id);

			if (retCode)
			{
				if (id == 1)
				{
					*state = GPIO_ON;
					m_DIOState[id] = GPIO_ON;
				}
				else
				{
					*state = GPIO_OFF;
					m_DIOState[id] = GPIO_OFF;
				}
			}
		}
	}

	return retCode;
}

char *GPIOHandler::ExtractResponse(char *request, char *response)
{
	char *ret = NULL;

	/* Clear output */
	memset(m_response, 0x00, sizeof(m_response));

	/* Skip "\n" and ">" characters */
	for (unsigned char cnt = 0, respCnt = 0; cnt < strlen(response); cnt++)
	{
		if ((response[cnt] != '\n') && (response[cnt] != '>'))
		{
			m_response[respCnt++] = response[cnt];
		}
	}

	/* Locate the response after the request */
	if (strstr(m_response, request) != NULL)
	{
		ret = &m_response[strlen(request)];
	}

	return ret;
}

bool GPIOHandler::Set(unsigned char id, unsigned char state)
{
	bool retCode = false;

	/* Is Id valis? */
	if (id < GPIO_MAX_NUM)
	{
		/* Launch the specific command to set the state */
		if (state == GPIO_ON)
		{
			/* Set command */
			retCode = SetCmd(id);
		}
		else if (state == GPIO_OFF)
		{
			/* Clear command */
			retCode = ClearCmd(id);
		}
		else // state == GPIO_UNKNOWN
		{
			retCode = true;
		}
	}

	return retCode;
}

bool GPIOHandler::SaveConfig(void)
{
	bool retCode = false;

	/* If device is open */
	if ((m_hComPort != INVALID_HANDLE_VALUE) || (SDL_PTR != NULL))
	{
		memcpy(m_DIOStateSaved, m_DIOState, sizeof(m_DIOStateSaved));
		retCode = true;
	}

	return retCode;
}

bool GPIOHandler::RestoreConfig(void)
{
	bool retCode = false;

	/* If device is open */
	if ((m_hComPort != INVALID_HANDLE_VALUE) || (SDL_PTR != NULL))
	{
		/* For all saved DIO */
		retCode = true;
		for (unsigned char cnt = 0; cnt < GPIO_MAX_NUM; cnt++)
		{
			/* Restore the saved DIO configuration */
			if (!Set(cnt, m_DIOStateSaved[cnt]))
			{
				retCode = false;
			}
		}
	}

	return retCode;
}
