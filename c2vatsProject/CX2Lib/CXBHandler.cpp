
#include "stdafx.h"
#include <fstream>
#include "C_CX2.h"
#include "CX2Lib.h"
#include "CX2utils.h"
#include "GPIOHandler.h"

#define CXB_BOOT_TIMEOUT_MSEC			500
#define CXB_INTRA_MSG_TIMEOUT_MSEC		20
#define CXB_RETRY_NUM					5

typedef enum _CXB_state_t
{
	CXB_STATE_INIT,
	CXB_STATE_OPEN_FILE,
	CXB_STATE_START_BOOTLOADER,
	CXB_STATE_IDENTIFY_BOOTLOADER,
	CXB_STATE_TRANSFER_DATA,
	CXB_STATE_SW_RESET_BOARD,
	CXB_STATE_HW_RESET_BOARD,
	CXB_STATE_RESET_COMPLETED,
	CXB_STATE_ERROR
} CXB_state_t;

static const char *bootloaderIdStr = "cxloader";

void CXBHandlerThread(LPVOID pThisP)
{
	bool running = true;
	C_CX2 *CX2 = (C_CX2 *)pThisP;
	CXB_state_t CXB_state, CXB_nextState;
	CX2_RC CX2retCode;
	unsigned char CX2data[128];
	unsigned char CX2dataLen;
	std::ifstream fwFile;
	std::streamoff filesize;

	/* Set external state: FW Upload in progress */
	CX2->SetFwUploadState(CXB_RC_UPLOAD_IN_PROGRESS);	// Already done in StartSession
	CX2->SetFwUploadPercentage(0);

	/* Machine state: INIT */
	CXB_state = CXB_STATE_INIT;

	while (running)
	{
		/* According to machine state */
		switch (CXB_state)
		{
			case CXB_STATE_INIT:
				/* Set GPIO for CXB data transfer */
				if (GPIOHandler::IsOpen())
				{
					GPIOHandler::SaveConfig();				// Save current configuration
					GPIOHandler::EmpFlashSet(GPIO_ON);
					GPIOHandler::GwFlashSet(GPIO_ON);
					GPIOHandler::MasterReqSet(GPIO_OFF);	// Become master on CX2
					CX2->C_CX2Purge();						// Purge local communication channel
				}
				CXB_nextState = CXB_STATE_OPEN_FILE;
				break;

			case CXB_STATE_OPEN_FILE:
				fwFile.open(CX2->GetFwUploadFilename(), std::ios::in);
				if (fwFile.is_open() == NULL)
				{
					/* Set external state: FILE_NOT_FOUND */
					CX2->SetFwUploadState(CXB_RC_FILE_NOT_FOUND);
					CXB_nextState = CXB_STATE_ERROR;
				}
				else
				{
					/* Calculate file size */
					filesize = fwFile.tellg();
					fwFile.seekg(0, std::ios::end);
					filesize = fwFile.tellg() - filesize;
					fwFile.seekg(0, std::ios::beg);

					/* Go to next state */
					CXB_nextState = CXB_STATE_START_BOOTLOADER;
				}
				break;

			case CXB_STATE_START_BOOTLOADER:
				/* SW Reset device */
				CX2->ResetDevice((char *)&CX2retCode);
				Sleep(CXB_BOOT_TIMEOUT_MSEC);

				/* Start bootloader */
				CX2data[0] = 0x01;		// Cmd ID : Start bootloader
				CX2dataLen = 0x01;		// Len: 1 byte
				if (CX2->FwUpdateCommand((char *)&CX2retCode, (char *)CX2data, (unsigned char *)&CX2dataLen))
				{
					/* Give time to jump to bootloader */
					Sleep(CXB_BOOT_TIMEOUT_MSEC);
					CXB_nextState = CXB_STATE_IDENTIFY_BOOTLOADER;
				}
				else
				{
					/* Set external state: COMM_FAILURE */
					CX2->SetFwUploadState(CXB_RC_COMM_FAILURE);
					CXB_nextState = CXB_STATE_ERROR;
				}
				break;

			case CXB_STATE_IDENTIFY_BOOTLOADER:
				/* Bootloader should reply "cxloader" to IDENTIFICATION->ID-String command */
				CX2data[0] = 0x02;		// Info No : ID-String
				CX2dataLen = 0x01;		// Len: 1 byte
				if (CX2->GetIdInfo((char *)&CX2retCode, (char *)CX2data, (unsigned char *)&CX2dataLen))
				{
					if (CX2retCode != RC_ACK)
					{
						/* Set external state: BOARD_UNREACHABLE */
						CX2->SetFwUploadState(CXB_RC_BOARD_UNREACHABLE);
						CXB_nextState = CXB_STATE_ERROR;
					}
					else
					{
						/* Check bootloader response */
						if ((CX2dataLen != strlen(bootloaderIdStr)) ||
							(strncmp(bootloaderIdStr, (const char *)CX2data, CX2dataLen) != 0))
						{
							/* Bootloader not started or not available */
							/* Set external state: BOOTLOADER_NOT_FOUND */
							CX2->SetFwUploadState(CXB_RC_BOOTLOADER_NOT_FOUND);
							CXB_nextState = CXB_STATE_ERROR;
						}
						else
						{
							/* Bootloader detected */
							Sleep(CXB_INTRA_MSG_TIMEOUT_MSEC);
							CXB_nextState = CXB_STATE_TRANSFER_DATA;
						}
					}
				}
				else
				{
					/* Set external state: BOOTLOADER_NOT_FOUND */
					CX2->SetFwUploadState(CXB_RC_BOOTLOADER_NOT_FOUND);
					CXB_nextState = CXB_STATE_ERROR;
				}
				break;

			case CXB_STATE_TRANSFER_DATA:
			{
				char fwData[256];
				unsigned char fwDataBin[128];
				BOOL lineIsValid, baudrateIncreased = FALSE;
				unsigned char lineLen;
				unsigned char *cxb_cmd, *cxb_len, *cxb_dat, *cxb_chk;
				CXB_RC fileTransferStatus = CXB_RC_SUCCESS;
				long originalSerialTimeout;
				CX2_utils utils;

				if (CX2->m_RS232Handler->isSDL())
				{
					//SDL_PTR->ChangeIntraMsgDelay(10);
					CX2->ChangeIntraMsgDelay(10);
				}

				/* Only for Interface board, change the baudrate to 115200 */
				if (CX2->m_Device == CX2_DEV_INTERFACE)
				{
					CX2data[0] = 0xA7;		// Cmd ID : Change baudrate
					CX2data[1] = 0x04;		// Data   : 115200
					CX2dataLen = 0x02;		// Len: 2 bytes
					CX2->FwUpdateCommand((char *)&CX2retCode, (char *)CX2data, (unsigned char *)&CX2dataLen);
					Sleep(CXB_INTRA_MSG_TIMEOUT_MSEC);

					/* Also locally change the baudrate to 115200 */
					if (CX2->m_RS232Handler->isSDL())
					{
						baudrateIncreased = CX2->ChangeBaudRate(C_RS232Handler::Baud115200);
					}
					else
					{
						baudrateIncreased = CX2->m_RS232Handler->ChangeBaudRate(C_RS232Handler::Baud115200);
					}
				}

				/* To increase the reliability, during FW upload operation
				 * set the response timeout to 5 seconds */
				originalSerialTimeout = CX2->m_RS232Handler->GetReadTimeoutConstant();
				CX2->m_RS232Handler->SetReadTimeoutConstant(5000);

				/* Read FW file line by line */
				while (fwFile.getline((char *)fwData, sizeof(fwData)) &&
					   fileTransferStatus == CXB_RC_SUCCESS)
				{
					/* Percentage */
					std::streamoff readBytes = fwFile.tellg();
					unsigned char percentage = (unsigned char)(readBytes * 100 / filesize);
					CX2->SetFwUploadPercentage(percentage);

					/* Filter empty and comment lines */
					if ((fwData[0] != '#') && (fwData[0] != '\n'))
					{
						/* Convert from hex to bin (at least 3 bytes expected) */
						lineLen = utils.hex2bin(fwData, (char *)fwDataBin);

						cxb_cmd = &fwDataBin[0];				// Command Id
						cxb_len = &fwDataBin[1];				// Command Len
						cxb_dat = &fwDataBin[2];				// Command data
						cxb_chk = &fwDataBin[*cxb_len + 1];		// Checksum

						/* Consistency check on read line */
						lineIsValid = FALSE;
						if (lineLen > *cxb_len)
						{
							/* Checksum calculation */
							unsigned char lineChecksum = 0;
							for (unsigned char *src = cxb_cmd; src <= cxb_chk; src++)
							{
								lineChecksum += *src;
							}
							lineIsValid = (lineChecksum == 0x00);
						}

						if (lineIsValid)
						{
							/* Parse the command */
							if (*cxb_cmd == 0xF1)
							{
								/*
								* [F1] [len] [id] <var> [chk] additional Header Information
								* F1 03 00 nn chk file type nn
								* F1 06 01 mm mm rr rr chk file version (m = main, r = revision) [packed bcd]
								* F1 05 02 dd mm yy chk file date (d/m/y = day/month/year) [packed bcd]
								* F1 04 03 cc cc chk file checksum (c = crc16 msb/lsb)
								*/
							}
							else if (*cxb_cmd == 0xF2)
							{
								unsigned int retry = 0;
								do
								{
									/*
									* [F2] [len] <var> [chk] Commandline for transmission to cxloader
									* Transmit <var> directly to cxloader
									*/
									if (*cxb_len > 1)
									{
										if (CX2->m_RS232Handler->isSDL() == false)
										{
											Sleep(CXB_INTRA_MSG_TIMEOUT_MSEC);
										}

										/* Send command over CX2 */
										CX2dataLen = *cxb_len - 1;
										memcpy(CX2data, cxb_dat, CX2dataLen);
										if (CX2->FwUpdateCommand((char *)&CX2retCode, (char *)CX2data, (unsigned char *)&CX2dataLen))
										{
											if (CX2retCode != RC_ACK)
											{
												fileTransferStatus = CXB_RC_FILE_TRANSFER_ERROR;
											}
											else
											{
												/* Command correctly transferred and acknowledged */
												fileTransferStatus = CXB_RC_SUCCESS;
												break;
											}
										}
										else
										{
											fileTransferStatus = CXB_RC_COMM_FAILURE;
										}
										retry++;
									}
								} while (retry < CXB_RETRY_NUM);
							}
							else if (*cxb_cmd == 0xF8)
							{
								/*
								* [F8] [len] [id] <var> [chk] additional Footer Information
								* F8 01 07 end of file
								*/
								fileTransferStatus = CXB_RC_SUCCESS;
								if (CX2->m_RS232Handler->isSDL() == false)
								{
									Sleep(CXB_INTRA_MSG_TIMEOUT_MSEC);
								}
							}
						}
						else
						{
							/* Consistency error on input file */
							fileTransferStatus = CXB_RC_FILE_MALFORMED;
							break;
						}
					}
				}

				/* Restore the read timeout */
				CX2->m_RS232Handler->SetReadTimeoutConstant(originalSerialTimeout);

				if (CX2->m_RS232Handler->isSDL())
				{
					CX2->ChangeIntraMsgDelay(0); //restore intra-msg delay to 0ms
				}

				/* Restore the baudrate */
				if (baudrateIncreased)
				{
					Sleep(CXB_INTRA_MSG_TIMEOUT_MSEC);

					CX2data[0] = 0xA7;		// Cmd ID : Change baudrate
					CX2data[1] = 0x02;		// Data   : 38400
					CX2dataLen = 0x02;		// Len: 2 bytes
					CX2->FwUpdateCommand((char *)&CX2retCode, (char *)CX2data, (unsigned char *)&CX2dataLen);
					Sleep(CXB_INTRA_MSG_TIMEOUT_MSEC);

					/* Also locally restore the baudrate to 38400 */
					if (CX2->m_RS232Handler->isSDL())
					{
						CX2->ChangeBaudRate(C_RS232Handler::Baud38400);
					}
					else
					{
						CX2->m_RS232Handler->ChangeBaudRate(C_RS232Handler::Baud38400);
					}
				}

				/* Set external state */
				if (fileTransferStatus != CXB_RC_SUCCESS)
				{
					CX2->SetFwUploadState(fileTransferStatus);
					CXB_nextState = CXB_STATE_ERROR;
				}
				else
				{
					CX2->SetFwUploadPercentage(100);
					CXB_nextState = CXB_STATE_SW_RESET_BOARD;
				}
				break;
			}

			case CXB_STATE_SW_RESET_BOARD:
				/* SW Reset for the uploaded device */
				CX2->ResetDevice((char *)&CX2retCode);

				/* Job completed */
				if (GPIOHandler::IsOpen())
				{
					CXB_nextState = CXB_STATE_HW_RESET_BOARD;
				}
				else
				{
					CXB_nextState = CXB_STATE_RESET_COMPLETED;
				}
				break;

			case CXB_STATE_HW_RESET_BOARD:
				Sleep(CXB_BOOT_TIMEOUT_MSEC);
				/* Restore GPIO configuration */
				GPIOHandler::RestoreConfig();
				/* Reset whole Currenza */
				GPIOHandler::HardReset();

				/* Job completed */
				CXB_nextState = CXB_STATE_RESET_COMPLETED;
				break;

			case CXB_STATE_RESET_COMPLETED:
				Sleep(CXB_BOOT_TIMEOUT_MSEC);
				/* Set external state: COMPLETED */
				CX2->SetFwUploadState(CXB_RC_COMPLETED);
				/* Exit with success */
				running = false;
				break;

			case CXB_STATE_ERROR:
				/* If GPIO module available */
				if (GPIOHandler::IsOpen())
				{
					/* Restore GPIO configuration */
					GPIOHandler::RestoreConfig();
					/* Reset whole Currenza */
					GPIOHandler::HardReset();
				}
				/* Exit with error */
				running = false;
				break;
		}

		/* Change state if needed */
		if (CXB_nextState != CXB_state)
		{
			CXB_state = CXB_nextState;
		}
	}

	/* Make sure to close the fw file after the operation */
	if (fwFile.is_open())
	{
		fwFile.close();
	}

	/* Thread exit */
	AfxEndThread(0);
}
