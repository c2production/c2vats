#include "stdafx.h"
#include <fstream>
#include "CX2utils.h"

/**** CX2_utils ****/

CX2_utils::CX2_utils(void)
{

}

CX2_utils::~CX2_utils(void)
{

}

int CX2_utils::char2int(char input)
{
	if (input >= '0' && input <= '9')
		return input - '0';
	if (input >= 'A' && input <= 'F')
		return input - 'A' + 10;
	if (input >= 'a' && input <= 'f')
		return input - 'a' + 10;

	return 0;
}

unsigned char CX2_utils::hex2bin(char *src, char *target)
{
	unsigned char numBytes = 0;

	while (*src && src[1])
	{
		*(target++) = char2int(*src) * 16 + char2int(src[1]);
		src += 2;
		numBytes++;
	}

	return numBytes;
}

/**** CX2_flashpage ****/

CX2_flashpage::CX2_flashpage(unsigned int pageSize, unsigned char filler, address_t startAddress)
{
	m_startAddress = startAddress / pageSize * pageSize;		// Make sure the address is pageSize aligned
	m_pageSize = pageSize;
	m_filler = filler;

	/* Allocate space for page data and fill it */
	m_data = (unsigned char *)malloc(pageSize);
	memset(m_data, filler, m_pageSize);
	m_empty = true;
}

CX2_flashpage::~CX2_flashpage(void)
{
	/* Free resources */
	if (m_data != NULL)
	{
		free(m_data);
	}
}

bool CX2_flashpage::SetData(address_t address, unsigned char *data)
{
	bool retCode = false;

	if ((address >= m_startAddress) && (address < m_startAddress + m_pageSize))
	{
		m_data[address - m_startAddress] = *data;
		if (*data != m_filler)
		{
			m_empty = false;
		}
		retCode = true;
	}
	return retCode;
}

bool CX2_flashpage::GetPageData(unsigned char *data)
{
	memcpy(data, m_data, m_pageSize);
	return(m_empty);
}

address_t CX2_flashpage::GetStartAddress(void)
{
	return m_startAddress;
}

/**** CX2_srecord ****/

CX2_srecord::CX2_srecord(CString filename, unsigned int pageSize, unsigned char filler)
{
	/* Initialize local members */
	m_baseAddress = 0;
	m_pagesCnt = 0;

	/* Save constructor arguments */
	m_pageSize = pageSize;
	m_filler = filler;

	/* Parse the specified file */
	ParseFile(filename);
}

CX2_srecord::~CX2_srecord(void)
{
	/* The items containted in the list are implicitally called
	   by list destructor */
}

CX2_SREC_RC CX2_srecord::GetParseResult(void)
{
	/* Return the status of the parsing */
	return m_parseResult;
}

bool CX2_srecord::GetPageData(bool isFirst, address_t *address, unsigned char *data, bool *empty, unsigned int *pageNum, unsigned int *totalPageNum)
{
	bool noMoreItem;

	/* If first request, point to the beginning of the list */
	if (isFirst)
	{
		m_pagesIt = m_pages.begin();
		m_pagesCnt = 1;
	}
	else if (m_pagesIt != m_pages.end())
	{
		/* Move to next list item */
		m_pagesIt++;
		m_pagesCnt++;
	}

	if (m_pagesIt != m_pages.end())
	{
		/* Copy data from current read position to specified destination */
		*empty = m_pagesIt->GetPageData(data);
		*address = m_pagesIt->GetStartAddress();
		noMoreItem = false;
	}
	else
	{
		/* No more item in the list */
		noMoreItem = true;
	}

	*pageNum = (!noMoreItem) ? m_pagesCnt : 0;
	*totalPageNum = m_pages.size();
	return noMoreItem;
}

void CX2_srecord::ParseFile(CString filename)
{
	m_parseResult = CX2_SREC_RC_GENERIC_ERROR;
	std::ifstream fwFile;
	char filedata[256];
	unsigned char filedataBin[128];
	CX2_utils utils;

	/* Open the file */
	fwFile.open(filename, std::ios::in);
	if (fwFile.is_open() != NULL)
	{
		/* Read all file line by line */
		while (fwFile.getline(filedata, sizeof(filedata)))
		{
			char *srecType = filedata;

			/* S-Record shall start with 'S' */
			if (*srecType != 'S')
			{
				m_parseResult = CX2_SREC_RC_FILE_IS_MALFORMED;
				break;
			}

			/* Which Srecord? */
			srecType++;
			if ((*srecType == '0') || (*srecType == '5'))
			{
				/* S0: Header or S5: Record count. Skip them */
			}
			else if (*srecType == '2')
			{
				/* S2: Data with address of 3 bytes */
				unsigned char lineLen = utils.hex2bin(&filedata[2], (char *)filedataBin);
				unsigned char lineChecksum = 0;

				/* Consistency checks: calculate and verify the checksum */
				for (unsigned char *src = &filedataBin[0]; src < &filedataBin[lineLen-1]; src++)
				{
					lineChecksum += *src;
				}
				lineChecksum = ~lineChecksum;
				if (lineChecksum != filedataBin[lineLen - 1])
				{
					m_parseResult = CX2_SREC_RC_FILE_IS_MALFORMED;
					break;
				}

				/* Extract address and data */
				unsigned char dataLen = filedataBin[0] - 4;
				address_t address = (address_t)filedataBin[1] * 0x10000 + 
					                (address_t)filedataBin[2] * 0x100 +
									(address_t)filedataBin[3];
				unsigned char *data = &filedataBin[4];

				/* The first address is the base address (logic assumption) */
				if (m_baseAddress == 0)
				{
					m_baseAddress = address;
				}

				/* For all data bytes */
				for (unsigned char cnt = 0; cnt < dataLen; cnt++, address++, data++)
				{
					unsigned int destPageNum = (address - m_baseAddress) / m_pageSize;
					unsigned int pageBaseAddress = address / m_pageSize * m_pageSize;
					unsigned int pagesToBeCreated = destPageNum + 1 - m_pages.size();

					/* Create all the needed pages */
					for (unsigned int pageCnt = pagesToBeCreated; pageCnt > 0; pageCnt--)
					{
						CX2_flashpage *newPage = new CX2_flashpage(m_pageSize, m_filler, pageBaseAddress - (pageCnt - 1) * m_pageSize);
						m_pages.push_back(*newPage);
					}

					/* Set the byte inside the correct page */
					m_pages.back().SetData(address, data);
				}
			}
			else if (*srecType == '8')
			{
				/* S8: End of block */
				m_parseResult = CX2_SREC_RC_OK;
			}
			else
			{
				/* Not supported */
				m_parseResult = CX2_SREC_RC_FILE_NOT_SUPPORTED;
				break;
			}
		}
		fwFile.close();
	}
	else
	{
		m_parseResult = CX2_SREC_RC_FILE_NOT_FOUND;
	}
}


