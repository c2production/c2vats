#pragma once

#include "C_RS232Handler.h"

#define CX2_BUFFER_DIMENSION 255
#define SDL_PTR (*pSDL)

class C_CX2
{
public:

	C_CX2(void);				// Constructor
	~C_CX2(void);				// Destructor

	/* CX2 Initialization/deinitialization */
	bool C_CX2Init(C_RS232Handler* a_RS232Handler);
	void C_CX2Stop(void);
	void C_CX2Purge(void);

	/* CX2 raw send/receive */
	bool RawSend(char* a_Buffer, unsigned long a_BufferSize);
	bool RawReceive(char* a_Buffer, unsigned long& a_BufferSize);

	/* CX2 commands */
	bool PingDevice(char* pa_RetCode);
	bool GetIdInfo(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen);
	bool ResetDevice(char* pa_RetCode);
	bool PollDevice(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen);
	bool GetValue(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen);
	bool SetValue(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen);
	bool GetAccLevel(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen);
	bool SetAccLevel(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen);
	bool ExecFunction(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen);
	bool ReadTable(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen);
	bool WriteTable(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen);
	bool FwUpdateCommand(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen);

	/* CX2 SDL commands */
	bool SetOutPin(unsigned char pin);
	bool ResetOutPin(unsigned char pin);
	bool GetOutPin(unsigned char *pin);
	bool ChangeBaudRate(char br);
	bool ChangeIntraMsgDelay(char ms_delay);
	bool RxDiscard(char status);
	void SDLPurgeRx();

	/* CXB FW upload commands */
	bool FwUpload_StartSession(CString a_filename, unsigned char* pa_RetCode);
	bool FwUpload_GetStatus(unsigned char *pa_Percentage, unsigned char* pa_RetCode);
	bool FwUpload_EndSession(void);

	/* Public attributes for RS232 and semaphore management */
	C_RS232Handler*         m_RS232Handler;
	bool					m_RS232Owner;
	HANDLE                  m_Semaphore;
	unsigned char			m_Device;
	unsigned char           m_DeviceAddress;
	unsigned char           m_SourceAddress;

private:

	char					m_uchTXbuffer[CX2_BUFFER_DIMENSION];	// TX buffer
	char					m_uchRXbuffer[CX2_BUFFER_DIMENSION];	// RX buffer
	unsigned char           m_RetryNumber;							// Number of retry for a single command

	bool bCheckAnswer(unsigned char *pa_uchBuff, unsigned char a_uchNumBytesRX);
	bool uchCommand(unsigned char a_uchNumDataBytes, unsigned char a_uchCommand, char* pa_uchData);
	bool GetSemaphore(void);
	bool RelSemaphore(void);

	/* FW update management */
	friend void CXBHandlerThread(LPVOID pThisP);
	friend void RBLHandlerThread(LPVOID pThisP);

	unsigned char m_fwUploadState;
	unsigned char GetFwUploadState(void);
	void SetFwUploadState(unsigned char state);

	CString m_fwUploadFilename;
	CString GetFwUploadFilename(void);
	void SetFwUploadFilename(CString filename);

	unsigned char m_fwUploadPercentage;
	unsigned char GetFwUploadPercentage(void);
	void SetFwUploadPercentage(unsigned char percentage);
};

extern C_CX2 **pSDL;
