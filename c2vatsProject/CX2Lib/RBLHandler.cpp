
#include "stdafx.h"
#include <fstream>
#include <iostream>
#include "C_CX2.h"
#include "CX2Lib.h"
#include "CX2utils.h"
#include "GPIOHandler.h"
using namespace std;

/* Machine state definitions */
typedef enum _RBL_state_t
{
	RBL_STATE_INIT,
	RBL_STATE_OPEN_FILE,
	RBL_STATE_START_BOOTLOADER,
	RBL_STATE_CONFIGURE_BOOTLOADER,
	RBL_STATE_DUMP_FLASH_CONTENT,			// Only used for debug
	RBL_STATE_TRANSFER_DATA,
	RBL_STATE_RESET_BOARD,
	RBL_STATE_RESET_COMPLETED,
	RBL_STATE_ERROR
} RBL_state_t;

/* RBL protocol types */
typedef unsigned char RBL_CMD;
typedef unsigned char RBL_DATA;
typedef unsigned int RBL_ADDR;

/* SRD Status register definition */
typedef struct _RBL_SRD_t
{
	union {
		struct {
			RBL_DATA res1 : 4;
			RBL_DATA program_status : 1;
			RBL_DATA erase_status : 1;
			RBL_DATA res0 : 1;
			RBL_DATA sequencer_status : 1;
		} SRD;
		RBL_DATA high;
	};
	union {
		struct {
			RBL_DATA res1 : 2;
			RBL_DATA ID_check_bits : 2;
			RBL_DATA res0 : 4;
		} SRD1;
		RBL_DATA low;
	};
} RBL_SRD_t;

/* Renesas Bootloader control commands */
#define RBL_CMD_PAGE_READ			((RBL_CMD)0xFF)
#define RBL_CMD_PAGE_PROGRAM		((RBL_CMD)0x41)
#define RBL_CMD_UNIT_PROGRAM		((RBL_CMD)0x49)
#define RBL_CMD_BLOCK_ERASE			((RBL_CMD)0x20)
#define RBL_CMD_ERASE_ALL_BLOCKS	((RBL_CMD)0xA7)
#define RBL_CMD_READ_STATUS_REG		((RBL_CMD)0x70)
#define RBL_CMD_CLEAR_STATUS_REG	((RBL_CMD)0x50)
#define RBL_CMD_ID_DATA_CHECK		((RBL_CMD)0xF5)
#define RBL_CMD_VERSION_INFO		((RBL_CMD)0xFB)
#define RBL_CMD_BIT_RATE_9600		((RBL_CMD)0xB0)
#define RBL_CMD_BIT_RATE_19200		((RBL_CMD)0xB1)
#define RBL_CMD_BIT_RATE_38400		((RBL_CMD)0xB2)
#define RBL_CMD_BIT_RATE_57600		((RBL_CMD)0xB3)
#define RBL_CMD_BIT_RATE_115200		((RBL_CMD)0xB4)
#define RBL_CMD_BIT_RATE_SETTING	((RBL_CMD)0xB5)
#define RBL_CMD_STANDARD_TIME_DATA	((RBL_CMD)0x00)

#define RBL_PAGE_SIZE				256
#define RBL_INTRA_MSG_TIMEOUT_MSEC	30
#define RBL_BOOT_TIMEOUT_MSEC		500
#define RBL_ID_DATA_ADR_LEN			3
#define RBL_ID_DATA_LEN				7
#define RBL_ID_DATA_FLASH_UNLOCKED	0x3

/* ID DATA addresses per device (see hardware user manual) */
static const RBL_DATA IdDataAddress_HMI[RBL_ID_DATA_ADR_LEN] =			{ 0xDF, 0xFF, 0x00 };
static const RBL_DATA IdDataAddress_ccTalk[RBL_ID_DATA_ADR_LEN] =		{ 0xDF, 0xFF, 0x00 };
static const RBL_DATA IdDataAddress_Interface[RBL_ID_DATA_ADR_LEN] =	{ 0xDF, 0xFF, 0x0F };

/* ID DATA per device (see firmware linker file, .id) */
static const RBL_DATA IdData_HMI[RBL_ID_DATA_LEN] =			{ 0x4E, 0x52, 0x49, 0x48, 0x49, 0x63, 0x32 };	// "NRIHIc2"
static const RBL_DATA IdData_ccTalk[RBL_ID_DATA_LEN] =		{ 0x4E, 0x52, 0x49, 0x43, 0x54, 0x63, 0x32 };	// "NRICTc2"
static const RBL_DATA IdData_Interface[RBL_ID_DATA_LEN] =	{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };	// Not used
static const RBL_DATA IdData_Default[RBL_ID_DATA_LEN] =		{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

/* Send 1-byte command */
static void RBL_CmdSend(C_CX2 *CX2, RBL_CMD cmd)
{
	RBL_CMD cmdToSend = cmd;
	CX2->RawSend((char *)&cmdToSend, 1);
}

/* Send ID data check function command */
static void RBL_CmdUnlock(C_CX2 *CX2, const RBL_DATA *address, const RBL_DATA *ID)
{
	RBL_CMD cmd = RBL_CMD_ID_DATA_CHECK;
	RBL_DATA IDlen = RBL_ID_DATA_LEN;
	CX2->RawSend((char *)&cmd, 1);							/* ID_DATA_CHECK command [1 byte] */
	CX2->RawSend((char *)address, RBL_ID_DATA_ADR_LEN);	/* Address [3 bytes]*/
	CX2->RawSend((char *)&IDlen, 1);						/* ID_DATA len [1 byte] */
	CX2->RawSend((char *)ID, RBL_ID_DATA_LEN);				/* ID_DATA [7 bytes] */
}

/* Send Page Read command */
static void RBL_CmdPageRead(C_CX2 *CX2, RBL_ADDR addr)
{
	RBL_CMD cmd = RBL_CMD_PAGE_READ;
	RBL_DATA tmp;
	CX2->RawSend((char *)&cmd, 1);
	tmp = (addr & 0x0000FF00) >> 8;					/* Middle-order address */
	CX2->RawSend((char *)&tmp, 1);		
	tmp = (addr & 0x00FF0000) >> 16;				/* High-order address */
	CX2->RawSend((char *)&tmp, 1);		
}

/* Send Page Write command */
static void RBL_CmdPageWrite(C_CX2 *CX2, RBL_ADDR addr, RBL_DATA *data)
{
	RBL_CMD cmd = RBL_CMD_PAGE_PROGRAM;
	RBL_DATA tmp;
	CX2->RawSend((char *)&cmd, 1);
	tmp = (addr & 0x0000FF00) >> 8;					/* Middle-order address */
	CX2->RawSend((char *)&tmp, 1);
	tmp = (addr & 0x00FF0000) >> 16;				/* High-order address */
	CX2->RawSend((char *)&tmp, 1);
	CX2->RawSend((char *)data, RBL_PAGE_SIZE);
}

/* Send Erase whole flash command */
static void RBL_CmdEraseAll(C_CX2 *CX2)
{
	RBL_CMD cmd = RBL_CMD_ERASE_ALL_BLOCKS;
	RBL_DATA tmp = 0xD0;	/* Confirmation command */
	CX2->RawSend((char *)&cmd, 1);
	CX2->RawSend((char *)&tmp, 1);
}

/* Receive 1-byte */
static bool RBL_Rec1Byte(C_CX2 *CX2, RBL_DATA *data)
{
	unsigned long blDataLen = 1;
	bool retCode = false;

	if ((CX2->RawReceive((char *)data, blDataLen)) &&
		(blDataLen == 1))
	{
		retCode = true;
	}
	return retCode;
}

/* Receive 1-byte and check it against passed argument */
static bool RBL_RecCheckData(C_CX2 *CX2, RBL_DATA data)
{
	RBL_DATA tmp;
	return (RBL_Rec1Byte(CX2, &tmp) && (tmp == data));
}

/* Receive Status Register */
static bool RBL_RecStatusRegister(C_CX2 *CX2, RBL_SRD_t *SRD)
{
	return(RBL_Rec1Byte(CX2, &SRD->high) && RBL_Rec1Byte(CX2, &SRD->low));
}

/* Receive page read data */
static bool RBL_RecFlashPage(C_CX2 *CX2, RBL_DATA *data)
{
	unsigned long blDataLen = RBL_PAGE_SIZE;
	bool retCode = false;

	if ((CX2->RawReceive((char *)data, blDataLen)) &&
		(blDataLen == RBL_PAGE_SIZE))
	{
		retCode = true;
	}
	return retCode;
}

/* Flush */
static void RBL_Flush(C_CX2 *CX2)
{
	RBL_DATA dummy;
	while (RBL_Rec1Byte(CX2, &dummy) == true);
}

bool RBL_SendPage(C_CX2 *CX2, RBL_ADDR addr, RBL_DATA *data)
{
	bool sendOk;
	RBL_SRD_t statusReg;
	unsigned char waitCnt, retryCnt;
	RBL_DATA readbackData[RBL_PAGE_SIZE];

	retryCnt = 0;
	do
	{
		/* Send page to bootloader */
		RBL_CmdPageWrite(CX2, addr, data);

		/* Wait page program completion by checking the status register */
		sendOk = false;
		waitCnt = 0;
		do
		{
			RBL_CmdSend(CX2, RBL_CMD_READ_STATUS_REG);
			if (RBL_RecStatusRegister(CX2, &statusReg))
			{
				/* Check if program completed successfully */
				if (statusReg.SRD.program_status == 0)
				{
					sendOk = true;
				}
				break;
			}
		} while (++waitCnt < 5);

		if (sendOk == true)
		{
			/* Read back data */
			sendOk = false;
			RBL_CmdPageRead(CX2, addr);
			if (RBL_RecFlashPage(CX2, readbackData))
			{
				if (memcmp(data, readbackData, RBL_PAGE_SIZE) == 0)
				{
					sendOk = true;
				}
			}
		}

		if (sendOk == false)
		{
			/* Flush CX2 RX buffer */
			RBL_Flush(CX2);

			/* Clear status register */
			Sleep(RBL_INTRA_MSG_TIMEOUT_MSEC);
			RBL_CmdSend(CX2, RBL_CMD_CLEAR_STATUS_REG);
			Sleep(RBL_INTRA_MSG_TIMEOUT_MSEC);
		}
	} while ((++retryCnt < 5) && (sendOk == false));

	return sendOk;
}

void RBLHandlerThread(LPVOID pThisP)
{
	bool running = true;
	C_CX2 *CX2 = (C_CX2 *)pThisP;
	RBL_state_t RBL_state, RBL_nextState;
	CX2_srecord *fwData = NULL;

	/* Set external state: FW Upload in progress */
	CX2->SetFwUploadState(CXB_RC_UPLOAD_IN_PROGRESS);	// Already done in StartSession
	CX2->SetFwUploadPercentage(0);

	/* Machine state: INIT */
	RBL_state = RBL_STATE_INIT;

	while (running)
	{
		/* According to machine state */
		switch (RBL_state)
		{
			case RBL_STATE_INIT:
				/* Set GPIO for Renesas Boot Loader data transfer */
				if (GPIOHandler::IsOpen())
				{
					GPIOHandler::SaveConfig();				// Save current configuration
					if ((CX2->m_Device == CX2_DEV_HMI) || (CX2->m_Device == CX2_DEV_CCTALK))
					{
						GPIOHandler::EmpFlashSet(GPIO_OFF);
						GPIOHandler::GwFlashSet(GPIO_OFF);
					}
					else if (CX2->m_Device == CX2_DEV_INTERFACE)
					{
						GPIOHandler::EmpFlashSet(GPIO_ON);
						GPIOHandler::GwFlashSet(GPIO_OFF);
					}
					GPIOHandler::MasterReqSet(GPIO_OFF);	// Become master on CX2
					CX2->C_CX2Purge();						// Purge local communication channel
				}
				RBL_nextState = RBL_STATE_OPEN_FILE;
				break;

			case RBL_STATE_OPEN_FILE:
				/* Parse the input file */
				fwData = new CX2_srecord(CX2->GetFwUploadFilename(), RBL_PAGE_SIZE);
				if (fwData != NULL)
				{
					switch (fwData->GetParseResult())
					{
						case CX2_SREC_RC_OK:
							/* File is good, proceed starting the boootloader */
							RBL_nextState = RBL_STATE_START_BOOTLOADER;
							break;

						case CX2_SREC_RC_FILE_NOT_FOUND:
							CX2->SetFwUploadState(CXB_RC_FILE_NOT_FOUND);
							RBL_nextState = RBL_STATE_ERROR;
							break;

						case CX2_SREC_RC_FILE_IS_MALFORMED:
							CX2->SetFwUploadState(CXB_RC_FILE_MALFORMED);
							RBL_nextState = RBL_STATE_ERROR;
							break;

						case CX2_SREC_RC_FILE_NOT_SUPPORTED:
							CX2->SetFwUploadState(CXB_RC_FILE_NOT_SUPPORTED);
							RBL_nextState = RBL_STATE_ERROR;
							break;

						default:
							CX2->SetFwUploadState(CXB_RC_GENERIC_ERROR);
							RBL_nextState = RBL_STATE_ERROR;
							break;
					}
				}
				else
				{
					CX2->SetFwUploadState(CXB_RC_GENERIC_ERROR);
					RBL_nextState = RBL_STATE_ERROR;
				}
				break;

			case RBL_STATE_START_BOOTLOADER:
				/* Locally set baudrate to 9600 */
				if (CX2->m_RS232Handler->isSDL())
				{
					SDL_PTR->ChangeBaudRate(C_RS232Handler::Baud9600);
				}
				else
				{
					CX2->m_RS232Handler->ChangeBaudRate(C_RS232Handler::Baud9600);
				}

				/* Reset system */
				if (GPIOHandler::IsOpen())
				{
					GPIOHandler::HardReset();
					Sleep(RBL_BOOT_TIMEOUT_MSEC);
				}

				if (CX2->m_RS232Handler->isSDL())
				{
					CX2->RxDiscard(1);
				}

				/* Try to connect to Renesas bootloader:
				* 1. send 16 times the standard time data (0x00), waiting at
				*    least 20msec between data
				* 2. send bit rate 9600 command
				* 3. wait for bit rate 9600 command confirmation */
				for (unsigned int cnt = 0; cnt < 16; cnt++)
				{
					RBL_CmdSend(CX2, RBL_CMD_STANDARD_TIME_DATA);
					Sleep(RBL_INTRA_MSG_TIMEOUT_MSEC);
				}

				if (CX2->m_RS232Handler->isSDL())
				{
					CX2->RxDiscard(0);
				}

				RBL_CmdSend(CX2, RBL_CMD_BIT_RATE_9600);
				if (RBL_RecCheckData(CX2, RBL_CMD_BIT_RATE_9600))
				{
					/* Handshake with bootloader successfully completed */
					RBL_nextState = RBL_STATE_CONFIGURE_BOOTLOADER;
				}
				else
				{
					/* Set external state: BOOTLOADER_NOT_FOUND */
					CX2->SetFwUploadState(CXB_RC_BOOTLOADER_NOT_FOUND);
					RBL_nextState = RBL_STATE_ERROR;
				}
				break;

			case RBL_STATE_CONFIGURE_BOOTLOADER:
				{
					RBL_CMD baudrateCmd = RBL_CMD_BIT_RATE_9600;
					enum C_RS232Handler::T_BaudRate baudrateLocal = C_RS232Handler::Baud9600;
					const RBL_DATA *IdDataAddr, *IdData;
					RBL_SRD_t statusReg;

					/* Depending on device:
					 * - Set supported baudrate
					 * - Unlock flash content */
					if (CX2->m_Device == CX2_DEV_HMI)
					{
						baudrateCmd = RBL_CMD_BIT_RATE_19200;
						baudrateLocal = C_RS232Handler::Baud19200;
						IdDataAddr = IdDataAddress_HMI;
						IdData = IdData_HMI;
					}
					else if (CX2->m_Device == CX2_DEV_CCTALK)
					{
						baudrateCmd = RBL_CMD_BIT_RATE_38400;
						baudrateLocal = C_RS232Handler::Baud38400;
						IdDataAddr = IdDataAddress_ccTalk;
						IdData = IdData_ccTalk;
					}
					else if (CX2->m_Device == CX2_DEV_INTERFACE)
					{
						baudrateCmd = RBL_CMD_BIT_RATE_57600;
						baudrateLocal = C_RS232Handler::Baud57600;
						IdDataAddr = IdDataAddress_Interface;
						IdData = IdData_Interface;
					}

					/* Set baudrate */
					RBL_CmdSend(CX2, baudrateCmd);
					if (RBL_RecCheckData(CX2, baudrateCmd))
					{
						/* Set locally the same baudrate */
						if (CX2->m_RS232Handler->isSDL())
						{
							SDL_PTR->ChangeBaudRate(baudrateLocal);
						}
						else
						{
							CX2->m_RS232Handler->ChangeBaudRate(baudrateLocal);
						}

					}

					/* Clear status register */
					RBL_CmdSend(CX2, RBL_CMD_CLEAR_STATUS_REG);
					Sleep(RBL_INTRA_MSG_TIMEOUT_MSEC);

					/* Unlock flash content */
					RBL_CmdUnlock(CX2, IdDataAddr, IdData);
					Sleep(RBL_INTRA_MSG_TIMEOUT_MSEC);

					/* Check status register */
					RBL_CmdSend(CX2, RBL_CMD_READ_STATUS_REG);
					if ((RBL_RecStatusRegister(CX2, &statusReg)) &&
						(statusReg.SRD1.ID_check_bits == RBL_ID_DATA_FLASH_UNLOCKED))
					{
						/* Flash unlocked */
						RBL_nextState = RBL_STATE_TRANSFER_DATA;
						// For Debug: set here next state to RBL_STATE_DUMP_FLASH_CONTENT
					}
					else
					{
						/* Set external state: FLASH_UNLOCK_FAILURE */
						CX2->SetFwUploadState(CXB_RC_FLASH_UNLOCK_FAILURE);
						RBL_nextState = RBL_STATE_ERROR;
					}
				}
				break;

			/*********************************************************************************/
			case RBL_STATE_DUMP_FLASH_CONTENT:			// Only used for debug
				{
					/* HMI - ccTalk */
					//#define DUMP_START_ADDRESS	0xC000
					//#define DUMP_STOP_ADDRESS	0x10000
					/* Changer interface */
					#define DUMP_START_ADDRESS	0xE0000
					#define DUMP_STOP_ADDRESS	0x100000

					RBL_DATA blData[RBL_PAGE_SIZE];
					RBL_ADDR addr = DUMP_START_ADDRESS;
					ofstream myfile;

					myfile.open("dump_flash.bin", ios::out | ios::trunc | ios::binary);
					if (myfile.is_open())
					{
						while (addr < DUMP_STOP_ADDRESS)
						{
							RBL_CmdPageRead(CX2, addr);
							if (RBL_RecFlashPage(CX2, blData))
							{
								myfile.write((const char *)blData, RBL_PAGE_SIZE);
								addr += RBL_PAGE_SIZE;
							}
						}
						myfile.close();
					}
				}
				RBL_nextState = RBL_STATE_TRANSFER_DATA;
				break;
			/*********************************************************************************/

			case RBL_STATE_TRANSFER_DATA:
				{
					RBL_SRD_t statusReg;
					address_t address;
					unsigned char data[RBL_PAGE_SIZE];
					bool isEmpty;
					unsigned char retry;
					long originalSerialTimeout;

					/* Set the response timeout to 500 msec and use retry mechanism */
					originalSerialTimeout = CX2->m_RS232Handler->GetReadTimeoutConstant();
					CX2->m_RS232Handler->SetReadTimeoutConstant(500);

					/* Clear status register */
					RBL_CmdSend(CX2, RBL_CMD_CLEAR_STATUS_REG);
					Sleep(RBL_INTRA_MSG_TIMEOUT_MSEC);

					/* Erase all unlocked blocks */
					RBL_CmdEraseAll(CX2);
					Sleep(RBL_INTRA_MSG_TIMEOUT_MSEC);

					/* Wait erase completion by checking the status register */
					retry = 0;
					do
					{
						RBL_CmdSend(CX2, RBL_CMD_READ_STATUS_REG);
						if (RBL_RecStatusRegister(CX2, &statusReg))
						{
							/* Check if erase completed successfully */
							if (statusReg.SRD.erase_status)
							{
								/* Set external state: FLASH_ERROR */
								CX2->SetFwUploadState(CBB_RC_FLASH_ERROR);
								RBL_nextState = RBL_STATE_ERROR;
							}
							break;
						}
						else
						{
							if (retry < 20)	// Flash erase may be a very long operation
							{
								Sleep(RBL_BOOT_TIMEOUT_MSEC);
								retry++;
							}
							else
							{
								/* Set external state: FLASH_ERROR */
								CX2->SetFwUploadState(CBB_RC_FLASH_ERROR);
								RBL_nextState = RBL_STATE_ERROR;
								break;
							}
						}
					} while (1);

					if (RBL_nextState != RBL_STATE_ERROR)
					{
						/* Clear status register */
						RBL_CmdSend(CX2, RBL_CMD_CLEAR_STATUS_REG);
						Sleep(RBL_INTRA_MSG_TIMEOUT_MSEC);

						/* Now the flash is completely erased (0xff).
						 * Send the fw to the bootloader, page by page */
						bool isFirst = true;
						unsigned int pageNum, pageTotalNum;
						while (fwData->GetPageData(isFirst, &address, data, &isEmpty, &pageNum, &pageTotalNum) == false)
						{
							isFirst = false;
							
							/* Update percentage */
							unsigned char percentage = (unsigned char)(pageNum * 100 / pageTotalNum);
							CX2->SetFwUploadPercentage(percentage);

							/* Do not send empty pages */
							if (!isEmpty)
							{
								/* Send the page */
								if (!RBL_SendPage(CX2, address, data))
								{
									/* Set external state: FLASH_ERROR */
									CX2->SetFwUploadState(CBB_RC_FLASH_ERROR);
									RBL_nextState = RBL_STATE_ERROR;
									break;
								}
							}
						}
					}

					/* Restore the read timeout */
					CX2->m_RS232Handler->SetReadTimeoutConstant(originalSerialTimeout);

					if (RBL_nextState != RBL_STATE_ERROR)
					{
						/* All operation completed successfully */
						RBL_nextState = RBL_STATE_RESET_BOARD;
						CX2->SetFwUploadPercentage(100);
					}
				}
				break;

			case RBL_STATE_RESET_BOARD:
				/* If GPIO module available */
				if (GPIOHandler::IsOpen())
				{				
					/* Restore GPIO configuration */
					GPIOHandler::RestoreConfig();
					/* Reset whole Currenza */
					GPIOHandler::HardReset();
				}
				RBL_nextState = RBL_STATE_RESET_COMPLETED;
				break;

			case RBL_STATE_RESET_COMPLETED:
				Sleep(RBL_BOOT_TIMEOUT_MSEC);
				/* Exit with success */
				CX2->SetFwUploadState(CXB_RC_COMPLETED);
				running = false;
				break;

			case RBL_STATE_ERROR:
				/* If GPIO module available */
				if (GPIOHandler::IsOpen())
				{
					/* Restore GPIO configuration */
					GPIOHandler::RestoreConfig();
					/* Reset whole Currenza */
					GPIOHandler::HardReset();
				}
				/* Exit with error */
				running = false;
				break;
		}

		/* Change state if needed */
		if (RBL_nextState != RBL_state)
		{
			RBL_state = RBL_nextState;
		}
	}

	/* Free allocated resources */
	if (fwData != NULL)
	{
		delete fwData;
	}

	/* Make sure to restore the standard baud rate */
	if (CX2->m_RS232Handler->isSDL())
	{
		SDL_PTR->ChangeBaudRate(C_RS232Handler::Baud38400);
	}
	else
	{
		CX2->m_RS232Handler->ChangeBaudRate(C_RS232Handler::Baud38400);
	}

	/* Thread exit */
	AfxEndThread(0);
}