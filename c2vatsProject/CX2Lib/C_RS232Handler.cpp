#include "StdAfx.h"
#include "C_RS232Handler.h"

#include <ctime>

unsigned long m_BaudRates []= {CBR_9600, CBR_19200, CBR_38400, CBR_57600, CBR_115200};
wchar_t ComArray[15] = L"\\\\.\\COM";

C_RS232Handler::C_RS232Handler(void):
  m_ComPort (COM1), 
  m_BaudRate (Baud38400),
  m_Parity (NoParity),
  m_ReadIntervalTimeout   (50),     //intra packet timeout [msec]
  m_ReadTimeoutMultiplier  (0),
  m_ReadTimeoutConstant  (250)      //inter packet timeout [msec]
{
	m_HandlePort = INVALID_HANDLE_VALUE;
	m_HandleHIDin = INVALID_HANDLE_VALUE;
	m_OvrlpHIDin.hEvent = INVALID_HANDLE_VALUE;
	m_FlagFail=true;
}


C_RS232Handler::~C_RS232Handler(void)
{
	if (m_HandlePort!= INVALID_HANDLE_VALUE)
	{
		CloseHandle (m_HandlePort);
	}
	m_HandlePort = INVALID_HANDLE_VALUE;

	if (m_OvrlpHIDin.hEvent != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_OvrlpHIDin.hEvent);
	}
	m_OvrlpHIDin.hEvent = INVALID_HANDLE_VALUE;


	if (m_HandleHIDin != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_HandleHIDin);
	}
	m_HandleHIDin = INVALID_HANDLE_VALUE;
}

bool C_RS232Handler::Open (void)
{
	DCB dcb;
	
	//if the port is already opened (either in exlusive or multidrop mode, m_FlagFail=false) we cannot reopen it
	if (m_FlagFail==false && m_HandlePort!=INVALID_HANDLE_VALUE)
	{
		return false;
	}
	
	memset(ComArray, 0, 15);

	if (m_ComPort == SDL_COM_NUM)
	{
		BOOL fConnected = FALSE;

		m_HandleHIDin = CreateNamedPipe(
				L"\\\\.\\pipe\\toCX2",		// pipe name 
				PIPE_ACCESS_INBOUND |		// read access 
				FILE_FLAG_OVERLAPPED,		// overlapped
				PIPE_TYPE_BYTE,				// byte-read mode 
				1,							// max. instances  
				0,							// output buffer size 
				255,						// input buffer size 
				0,							// client time-out 
				NULL);

		DisconnectNamedPipe(m_HandleHIDin);

		m_HandlePort = CreateFile(
			L"\\\\.\\pipe\\fromCX2",   // pipe name 
			GENERIC_WRITE,	// write access
			0,              // no sharing 
			NULL,           // default security attributes
			CREATE_ALWAYS,  // opens existing pipe 
			FILE_FLAG_NO_BUFFERING,
			NULL);          // no template file 

		m_OvrlpHIDin.hEvent = CreateEvent(
			NULL,
			TRUE, //manual reset event 
			FALSE, //initial state is nonsignaled.
			NULL);

		m_OvrlpHIDin.Offset = 0;
		m_OvrlpHIDin.OffsetHigh = 0;

		if (m_HandlePort == INVALID_HANDLE_VALUE)
		{
			m_FlagFail = true;
			return false;
		}
		
		fConnected = ConnectNamedPipe(m_HandleHIDin, &m_OvrlpHIDin);

		if (fConnected == 0)
		{
			volatile DWORD LastError = ::GetLastError();

			if (LastError == ERROR_IO_PENDING)
			{
				DWORD Result = WaitForSingleObject(m_OvrlpHIDin.hEvent, 500);

				switch (Result)
				{
					case WAIT_OBJECT_0:
						if (HasOverlappedIoCompleted(&m_OvrlpHIDin))
						{
							m_FlagFail = false;
						}
						else
						{
							m_FlagFail = true;
						}
						break;

					default:
					case WAIT_TIMEOUT:
						CancelIo(m_HandleHIDin);
						break;
				}
			}
			else
			{
				m_FlagFail = true;
				return false;
			}
		}
		else
		{
			m_FlagFail = false;
		}		
	}
	else
	{
		wcscpy_s(ComArray, L"\\\\.\\COM\0\0\0\0\0\0\0");
		wchar_t portnum[5]=L"\0\0\0\0";
		_itow_s(m_ComPort + 1, portnum, 10);
		wcscat_s(ComArray, portnum);
		wcscat_s(ComArray,L"\0");

		m_HandlePort=CreateFile(ComArray,
			  				    GENERIC_READ | GENERIC_WRITE,
							    0,						// no share
							    NULL,					// no security attrs
								CREATE_ALWAYS,			// OPEN_EXISTING
							    FILE_FLAG_NO_BUFFERING,
							    NULL);					//hTemplate must be NULL for comm devices 
	
		if (m_HandlePort == INVALID_HANDLE_VALUE)
		{
			volatile DWORD LastError= ::GetLastError ();
			volatile char* lpMsgBuf;
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
						        NULL,
						        LastError,
						        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
						        (LPTSTR) &lpMsgBuf,
						        0,
						        NULL);
			m_FlagFail= true;
			return false;
		}

		COMMTIMEOUTS Timeouts;
		Timeouts.ReadIntervalTimeout=            m_ReadIntervalTimeout;
		Timeouts.ReadTotalTimeoutMultiplier=     m_ReadTimeoutMultiplier;
		Timeouts.ReadTotalTimeoutConstant=       m_ReadTimeoutConstant;
		Timeouts.WriteTotalTimeoutMultiplier=    10;
		Timeouts.WriteTotalTimeoutConstant=      1000;
		if (!SetCommTimeouts (m_HandlePort, &Timeouts))
		{
		  m_FlagFail= true;
			return false;
		}

		/* Get the current configuration.*/
		if (!GetCommState (m_HandlePort, &dcb))
		{	
			m_FlagFail= true;
			return false;
		}
		dcb.BaudRate=   m_BaudRates [m_BaudRate];
		dcb.ByteSize=   8;			//4 o 8
		dcb.StopBits=   ONESTOPBIT;
		dcb.fParity=    false;//30/4/2009 //true;         //Parit� abilitata
		dcb.Parity =    m_Parity;		// 0 no, 1 odd, 2 even, 3 mark, 4 space   
		dcb.fRtsControl= RTS_CONTROL_DISABLE;//30/4/2009 //RTS_CONTROL_TOGGLE;

		if (!SetCommState(m_HandlePort, &dcb))
		{
			m_FlagFail= true;
			return false;
		}
		m_FlagFail=false;

		m_FlagFail=(PurgeComm(m_HandlePort, PURGE_RXCLEAR|PURGE_TXCLEAR) == FALSE);

		if(m_FlagFail)
		{
			return false;
		}
	}

	return true;
}

bool C_RS232Handler::ChangeBaudRate(T_BaudRate BaudRate)
{
	DCB dcb;

	if (m_FlagFail == false)
	{
		m_FlagFail = true;
		
		/* Obtain COMM state */
		if (GetCommState(m_HandlePort, &dcb))
		{
			/* Set new baudrate */
			dcb.BaudRate = m_BaudRates[BaudRate];
			if (SetCommState(m_HandlePort, &dcb))
			{
				/* Purge buffers */
				m_BaudRate = BaudRate;
				m_FlagFail = (PurgeComm(m_HandlePort, PURGE_RXCLEAR | PURGE_TXCLEAR) == FALSE);
			}
		}
	}

	return !m_FlagFail;
}

void C_RS232Handler::SetReadTimeoutConstant(long ReadTimeoutConstant)
{
	COMMTIMEOUTS Timeouts;

	if (ReadTimeoutConstant != m_ReadTimeoutConstant)
	{
		if (m_FlagFail == false)
		{
			m_FlagFail = true;

			if (m_ComPort != SDL_COM_NUM)
			{
				/* Obtain current timeout configuration */
				if (GetCommTimeouts(m_HandlePort, &Timeouts))
				{
					/* Set new timeout */
					Timeouts.ReadTotalTimeoutConstant = ReadTimeoutConstant;
					if (SetCommTimeouts(m_HandlePort, &Timeouts))
					{
						m_ReadTimeoutConstant = ReadTimeoutConstant;
						m_FlagFail = false;
					}
				}
			}
			else
			{
				m_ReadTimeoutConstant = ReadTimeoutConstant;
				m_FlagFail = false;
			}
		}
	}
}

void C_RS232Handler::Close (void)
{
	if (m_HandlePort!= INVALID_HANDLE_VALUE)
	{
		CloseHandle (m_HandlePort);
	}
	m_HandlePort = INVALID_HANDLE_VALUE;

	if (m_OvrlpHIDin.hEvent != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_OvrlpHIDin.hEvent);
	}
	m_OvrlpHIDin.hEvent = INVALID_HANDLE_VALUE;

	if (m_HandleHIDin != INVALID_HANDLE_VALUE)
	{
		DisconnectNamedPipe(m_HandleHIDin);
		CloseHandle(m_HandleHIDin);
	}
	m_HandleHIDin = INVALID_HANDLE_VALUE;
}

void C_RS232Handler::Purge(void)
{
	if (m_ComPort != SDL_COM_NUM)
	{
		if (m_HandlePort != INVALID_HANDLE_VALUE)
		{
			PurgeComm(m_HandlePort, PURGE_RXCLEAR | PURGE_TXCLEAR);
		}
	}
}

bool C_RS232Handler::isSDL()
{
	return(m_ComPort == SDL_COM_NUM);
}

bool C_RS232Handler::Send (char* a_Buffer, unsigned long a_BufferSize)
{
	unsigned long DatiScrittiLetti= 0;

	if (!::WriteFile (m_HandlePort, a_Buffer, a_BufferSize, &DatiScrittiLetti, NULL))
	{
		volatile DWORD LastError= ::GetLastError ();
		volatile char* lpMsgBuf;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					        NULL,
					        LastError,
					        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
					        (LPTSTR) &lpMsgBuf,
					        0,
					        NULL);
		return false;
	}
 	//PurgeComm (m_HandlePort, PURGE_RXCLEAR| PURGE_TXCLEAR);
	if (DatiScrittiLetti!= a_BufferSize)
	{
		return false;
	}
	return true; // Tutto OK
}

bool C_RS232Handler::Receive (char* a_Buffer, unsigned long& a_BufferSize)
{
	unsigned long DatiScrittiLetti= 0;
	DWORD dwError = 0;
	
	if (m_ComPort == SDL_COM_NUM)
	{
		unsigned long partial = 0;
		unsigned long partial_to_read = a_BufferSize;
		unsigned int ms_timeout = m_ReadIntervalTimeout * m_ReadTimeoutMultiplier + m_ReadTimeoutConstant;

		unsigned int ms_elapsed = 0;

		using namespace std;
		clock_t begin = clock();

#if 1
		do
		{
			ResetEvent(m_OvrlpHIDin.hEvent);

			BOOL bResult = ReadFile(m_HandleHIDin, a_Buffer + DatiScrittiLetti, partial_to_read, &partial, &m_OvrlpHIDin);

			// Check for a problem or pending operation.
			if (!bResult)
			{
				dwError = GetLastError();

				switch (dwError)
				{
					case ERROR_IO_PENDING:
					{
						DWORD Result = WaitForSingleObject(m_OvrlpHIDin.hEvent, (ms_timeout - ms_elapsed));

						switch (Result)
						{
							case WAIT_OBJECT_0:
								GetOverlappedResult(m_HandleHIDin,
									&m_OvrlpHIDin,
									&partial,
									FALSE);
								break;

							default:
							case WAIT_TIMEOUT:
								CancelIo(m_HandleHIDin);
								break;
						}
						break;
					}
				}
			}
			
			DatiScrittiLetti += partial;
			partial_to_read -= partial;

			ms_elapsed = (int)((double)(clock() - begin) / (double)CLOCKS_PER_SEC * 1000.0);

		} while ( (DatiScrittiLetti != a_BufferSize) && (ms_elapsed < ms_timeout) );

#else
		do
		{
			if (PeekNamedPipe(m_HandleHIDin, NULL, NULL, NULL, &partial, NULL))
			{
				if (partial > 0)
				{
					if (!ReadFile(m_HandleHIDin, a_Buffer + DatiScrittiLetti, partial_to_read, &partial, NULL))
					{
						volatile DWORD LastError = ::GetLastError();
						volatile char* lpMsgBuf;
						FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
							NULL,
							LastError,
							MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
							(LPTSTR)&lpMsgBuf,
							0,
							NULL);
						return false;
					}

					DatiScrittiLetti += partial;
					partial_to_read -= partial;
				}
				else
				{
					Sleep(1);
				}
			}

			ms_elapsed = (int)((double)(clock() - begin) / (double)CLOCKS_PER_SEC * 1000.0);

		} while ( (DatiScrittiLetti != a_BufferSize) && (ms_elapsed < ms_timeout) );
#endif
	}
	else
	{
		COMMTIMEOUTS Timeouts;
		Timeouts.ReadIntervalTimeout=            m_ReadIntervalTimeout;
		Timeouts.ReadTotalTimeoutMultiplier=     m_ReadTimeoutMultiplier;
		Timeouts.ReadTotalTimeoutConstant=       m_ReadTimeoutConstant;
		Timeouts.WriteTotalTimeoutMultiplier=    0;
		Timeouts.WriteTotalTimeoutConstant=      0;
		if (!SetCommTimeouts (m_HandlePort, &Timeouts))
		{
			return false;
		} 

		if (!ReadFile (m_HandlePort, a_Buffer, a_BufferSize, &DatiScrittiLetti, NULL))
		{
			volatile DWORD LastError= ::GetLastError ();
			volatile char* lpMsgBuf;
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
						        NULL,
						        LastError,
						        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
						        (LPTSTR) &lpMsgBuf,
						        0,
						        NULL);
	  		return false;
		}
	}

	if (DatiScrittiLetti!= a_BufferSize)
	{
		return false;
	}
	return true;
}