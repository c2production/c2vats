// VersionInfo.h: interface for the CVersionInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VERSIONINFO_H__08E77140_9724_11D5_ABB6_0080ADB5BAAE__INCLUDED_)
#define AFX_VERSIONINFO_H__08E77140_9724_11D5_ABB6_0080ADB5BAAE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma comment(lib, "version")

class CVersionInfo
{
public:
   LPSTR GetProductName(void);
   LPSTR GetProductVersion(void);
   LPSTR GetCompanyName(void); 
   LPSTR GetCopyright(void);
   LPSTR GetTrademark(void);

   LPSTR GetFileVersion(void);
  
   CVersionInfo(void);        // for calling module version info
   CVersionInfo(LPTSTR);      // for external module version info
   virtual ~CVersionInfo(void);

private:
   BOOL m_bOk;
   BYTE* m_pVerData;
   LPSTR NoVerDataString();
   CString m_sSearch;
};

#endif // !defined(AFX_VERSIONINFO_H__08E77140_9724_11D5_ABB6_0080ADB5BAAE__INCLUDED_)
