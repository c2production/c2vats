//*****************************************************************************/
/* FILE: C_CX2.cpp                                                         */
/* AUTHOR: Giovanni Milanesi                                                  */
/* CREATION DATE: 17/07/08                                                    */
/*                                                                            */
/* DESCRIPTION:                                                               */
/*Cctalk Library                                                              */
/*                                                                            */ 
/******************************************************************************/

#include "StdAfx.h"
#include "C_CX2.h"
#include "C_RS232Handler.h"
#include "CX2Lib.h"

#define MAX_SEM_TIMEOUT              5000   //timeout for semaphore [msec]
#define FAST_COMMAND_TIMEOUT		   10

#define NO_BYTE					0
#define CX2_PC_MASTER_ADDR		1

#define CX2_ACK					0x00

//command codes
#define CX2_PING				1
#define CX2_IDENTIFICATION		2
#define CX2_RESET				3
#define CX2_POLL				4

//SDL internal commands
typedef enum _sdl_commands_t
{
	SDL_CX2_SEND_TO_C2 = 10,
	CX2_TO_SDL,
	SDL_CX2_SET_OUTPIN,
	SDL_CX2_RESET_OUTPIN,
	SDL_CX2_GET_OUTPIN,
	SDL_CX2_SET_BAUDRATE,
	SDL_CX2_RX_DISCARD,
	SDL_CX2_MIN_DELAY_SET,
	SDL_CX2_LAST
} sdl_commands_t;


#define CX2_GETVALUE			0xE0
#define CX2_SETVALUE			0xE1
#define CX2_GETSETACCESS		0xE2
#define CX2_EXECFUNCTION		0xEA
#define CX2_READTABLE			0xEB
#define CX2_WRITETABLE			0xEC
#define CX2_FWUPDATECOMMAND		0xEE

typedef enum _fw_upload_t
{
	FW_UPLOAD_CX2,
	FW_UPLOAD_RENESAS_BL,
	FW_UPLOAD_RENESAS_CCFS,
	FW_UPLOAD_UNKNOWN,
	FW_UPLOAD_NUM
} fw_upload_t;

/******************************************************************************/
/*Function:  C_CX2::C_CX2                                               */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/******************************************************************************/ 
C_CX2::C_CX2(void)
{
  m_RetryNumber = 1;
  m_fwUploadState = CXB_RC_NOT_INITIALIZED;
  m_fwUploadPercentage = 0;
}

/******************************************************************************/
/*Function:  C_CX2::~C_CX2()                                            */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/******************************************************************************/ 
C_CX2::~C_CX2()
{


}

/******************************************************************************/
/*Function:  C_CX2::C_CX2Init                                           */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/******************************************************************************/ 
bool C_CX2::C_CX2Init(C_RS232Handler* a_RS232Handler)
{
  m_RS232Handler = a_RS232Handler;

  m_Semaphore = NULL;

  if(m_RS232Handler->Open())
  {
    m_RS232Owner = true;
    m_Semaphore = CreateSemaphore(NULL, 1, 1, NULL);
	if (m_Semaphore != NULL)
	{
		return true;
	}
    else
      return false;
  }

  m_RS232Handler->Close();
  return false;
}

/******************************************************************************/
/*Function:  C_CX2::C_CX2Stop                                           */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/******************************************************************************/ 
void C_CX2::C_CX2Stop(void)
{
  Sleep(5);
  m_RS232Handler->Close();
  if(m_Semaphore != NULL)
	CloseHandle(m_Semaphore);
}

/******************************************************************************/
/*Function:  C_CX2::C_CX2Purge                                           */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/******************************************************************************/
void C_CX2::C_CX2Purge(void)
{
	m_RS232Handler->Purge();
}

/******************************************************************************/
/*Function:  C_CX2::GetSemaphore                                           */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/******************************************************************************/ 
bool C_CX2::GetSemaphore(void)
{
  DWORD dwwaiting;

  dwwaiting = WaitForSingleObject(m_Semaphore,MAX_SEM_TIMEOUT);
  if(WAIT_OBJECT_0 == dwwaiting)
    return true;
  else
    return false;
}

/******************************************************************************/
/*Function:  C_CX2::RelSemaphore                                           */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/******************************************************************************/ 
bool C_CX2::RelSemaphore(void)
{
 if(ReleaseSemaphore(m_Semaphore,1,NULL))
    return true;
  else
    return false;
}

void C_CX2::SDLPurgeRx()
{
	unsigned long rl = 1;
	while (m_RS232Handler->Receive(m_uchRXbuffer, rl));
}

/******************************************************************************/
/*Function:  C_CX2::uchCommand                                             */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*low level serial communication                                              */
/******************************************************************************/ 
bool C_CX2::uchCommand(unsigned char a_uchNumDataBytes, 
						  unsigned char a_uchCommand, 
						  char* pa_uchData)
{
  unsigned char i = 0;
  unsigned char uchCheckSum = 0;
  unsigned char uchTXSize = 0;

  if (m_RS232Handler->isSDL() )
  {
	  if ((a_uchCommand > SDL_CX2_SEND_TO_C2) && (a_uchCommand < SDL_CX2_LAST))
	  {
		  m_uchTXbuffer[i++] = CX2_TO_SDL;
	  }
	  else
	  {
		  m_uchTXbuffer[i++] = SDL_CX2_SEND_TO_C2;
	  }
  }
  
  m_uchTXbuffer[i] = m_DeviceAddress;
  uchCheckSum += m_uchTXbuffer[i++];
  m_uchTXbuffer[i] = a_uchNumDataBytes;
  uchCheckSum += m_uchTXbuffer[i++];
  m_uchTXbuffer[i] = m_SourceAddress;
  uchCheckSum += m_uchTXbuffer[i++];
  m_uchTXbuffer[i] = a_uchCommand;
  uchCheckSum += m_uchTXbuffer[i++];

  while(a_uchNumDataBytes && pa_uchData)                //esegue la somma e l'allocamento dei dati nel buffer
  {
    m_uchTXbuffer[i] = *pa_uchData++;
	uchCheckSum += m_uchTXbuffer[i++];
    a_uchNumDataBytes--;
  }

  m_uchTXbuffer[i++] = 256 - uchCheckSum;

  uchTXSize = i;   


    if(!m_RS232Handler->Send(m_uchTXbuffer, uchTXSize))
    {
		return false;
    }

	memset(m_uchRXbuffer, 0x0, CX2_BUFFER_DIMENSION);

	unsigned long rl = 2;
	if(!m_RS232Handler->Receive(m_uchRXbuffer, rl)) 
	{      
		return false;
	} 
	else 
	{	
		if (rl == 2) 
		{		
			//destination and length received.
			rl = 2+m_uchRXbuffer[1]+1; //remaining part: source byte + cmd byte + data + checksum byte 
			if (!m_RS232Handler->Receive(&(m_uchRXbuffer[2]), rl)) //receive remaining part
			{ 
				return false;
			} 
			else 
			{
				if (m_uchRXbuffer[0] != CX2_PC_MASTER_ADDR) 
				{
					return false;
				}
			}
		} 
		else 
		{
			return false;
		}
	}

	return bCheckAnswer((unsigned char *)m_uchRXbuffer, 4 + m_uchRXbuffer[1]);
}


/******************************************************************************/
/*Function:  C_CX2::bCheckAnswer                                           */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*Belonging to checksum type verify the message integrity                     */
/******************************************************************************/ 
bool C_CX2::bCheckAnswer(unsigned char *pa_uchBuff, unsigned char a_uchNumBytesRX)
{
	unsigned char uch_checksum = 0;

	while(a_uchNumBytesRX)
	{
		uch_checksum += *pa_uchBuff++;
		a_uchNumBytesRX--;
	}

	return (((256-uch_checksum) & 0xFF) == *pa_uchBuff);
}


/******************************************************************************/
/*Function:  C_CX2::PingDevice												  */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::PingDevice(char* pa_RetCode)  
{
  bool result;

  if(!GetSemaphore())
    return false;
  
  result = uchCommand(NO_BYTE, CX2_PING, NULL);
  if(result)
  {
	*pa_RetCode = (m_uchRXbuffer[3] == CX2_PING ? RC_ACK : m_uchRXbuffer[3]);
  }
  RelSemaphore();
  return result;  
}

/******************************************************************************/
/*Function:  C_CX2::GetIdInfo												  */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::GetIdInfo(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen)
{
  bool result;

  if(!GetSemaphore())
    return false;
  
  result = uchCommand(1, CX2_IDENTIFICATION, pa_Data);

  if(result)
  {
	*pa_RetCode = (m_uchRXbuffer[3] == CX2_IDENTIFICATION ? RC_ACK : m_uchRXbuffer[3]);

    if(pa_Data != NULL)
    {
		*pa_DataLen = m_uchRXbuffer[1];
		for(unsigned char LocCounter = 0; LocCounter < *pa_DataLen; LocCounter++)
		{
			pa_Data[LocCounter] = m_uchRXbuffer[4 + LocCounter];    
		}
    }
  }

  RelSemaphore();
  return result;  
}

/******************************************************************************/
/*Function:  C_CX2::ResetDevice                                               */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::ResetDevice(char* pa_RetCode)  
{
  bool result;

  if(!GetSemaphore())
    return false;
  
  result = uchCommand(NO_BYTE, CX2_RESET, NULL);
  if(result)
  {
	*pa_RetCode = (m_uchRXbuffer[3] == CX2_RESET ? RC_ACK : m_uchRXbuffer[3]);
  }
  RelSemaphore();
  return result;  
}

bool C_CX2::SetOutPin(unsigned char pin)
{
	bool result;

	if (!GetSemaphore())
		return false;

	result = uchCommand(1, SDL_CX2_SET_OUTPIN, (char *)&pin);
	if (result)
	{
		result = (m_uchRXbuffer[3] == CX2_ACK);
	}
	RelSemaphore();
	return result;
}

bool C_CX2::ResetOutPin(unsigned char pin)
{
	bool result;

	if (!GetSemaphore())
		return false;

	result = uchCommand(1, SDL_CX2_RESET_OUTPIN, (char *)&pin);
	if (result)
	{
		result = (m_uchRXbuffer[3] == CX2_ACK);
	}
	RelSemaphore();
	return result;
}

bool C_CX2::GetOutPin(unsigned char *pin)
{
	bool result;

	if (!GetSemaphore())
		return false;

	result = uchCommand(1, SDL_CX2_GET_OUTPIN, (char *)pin);
	if (result)
	{
		result = (m_uchRXbuffer[3] == CX2_ACK);
		*pin = (unsigned char)m_uchRXbuffer[4];
	}
	RelSemaphore();
	return result;
}

bool C_CX2::RxDiscard(char status)
{
	bool result;

	if (!GetSemaphore())
		return false;

	result = uchCommand(1, SDL_CX2_RX_DISCARD, &status);
	if (result)
	{
		result = (m_uchRXbuffer[3] == CX2_ACK);
	}
	RelSemaphore();
	return result;
}



bool C_CX2::ChangeIntraMsgDelay(char ms_delay)
{
	bool result;

	if (!GetSemaphore())
		return false;

	result = uchCommand(1, SDL_CX2_MIN_DELAY_SET, &ms_delay);
	if (result)
	{
		result = (m_uchRXbuffer[3] == CX2_ACK);
	}
	RelSemaphore();
	return result;
}

bool C_CX2::ChangeBaudRate(char br)
{
	bool result;

	if (!GetSemaphore())
		return false;

	result = uchCommand(1, SDL_CX2_SET_BAUDRATE, &br);
	if (result)
	{
		result = (m_uchRXbuffer[3] == CX2_ACK);
	}
	RelSemaphore();
	return result;
}

bool C_CX2::RawSend(char* a_Buffer, unsigned long a_BufferSize)
{
	bool ret;

	if (m_RS232Handler->isSDL())
	{
		char * buff = (char *)malloc(a_BufferSize + 1);
		buff[0] = SDL_CX2_SEND_TO_C2;
		memcpy(buff + 1, a_Buffer, a_BufferSize);

		ret = m_RS232Handler->Send(buff, a_BufferSize + 1);

		free(buff);
	}
	else
	{
		ret = m_RS232Handler->Send(a_Buffer, a_BufferSize);
	}
	
	return ret;
}

bool C_CX2::RawReceive(char* a_Buffer, unsigned long& a_BufferSize)
{
	bool ret = m_RS232Handler->Receive((char *)a_Buffer, a_BufferSize);

	return ret;
}

/******************************************************************************/
/*Function:  C_CX2::PollDevice                                                */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::PollDevice(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen)  
{
  bool result;

  if(!GetSemaphore())
    return false;
  
  result = uchCommand(NO_BYTE, CX2_POLL, NULL);

  if(result)
  {
	*pa_RetCode = (m_uchRXbuffer[3] == CX2_POLL ? RC_ACK : m_uchRXbuffer[3]);

    if(pa_Data != NULL)
    {
		*pa_DataLen = m_uchRXbuffer[1];
		for(unsigned char LocCounter = 0; LocCounter < *pa_DataLen; LocCounter++)
		{
			pa_Data[LocCounter] = m_uchRXbuffer[4 + LocCounter];    
		}
    }
  }

  RelSemaphore();
  return result;  
}

/******************************************************************************/
/*Function:  C_CX2::GetValue                                                */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::GetValue(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen)  
{
  bool result;

  if(!GetSemaphore())
    return false;
  
  result = uchCommand(1, CX2_GETVALUE, pa_Data);

  if(result)
  {
	*pa_RetCode = (m_uchRXbuffer[3] == CX2_GETVALUE ? RC_ACK : m_uchRXbuffer[3]);

    if(pa_Data != NULL)
    {
		*pa_DataLen = m_uchRXbuffer[1];
		for(unsigned char LocCounter = 0; LocCounter < *pa_DataLen; LocCounter++)
		{
			pa_Data[LocCounter] = m_uchRXbuffer[4 + LocCounter];    
		}
    }
  }

  RelSemaphore();
  return result;  
}

/******************************************************************************/
/*Function:  C_CX2::SetValue                                                  */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::SetValue(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen)  
{
  bool result;

  if(!GetSemaphore())
    return false;
  
  result = uchCommand(*pa_DataLen, CX2_SETVALUE, pa_Data);

  if(result)
  {
	*pa_RetCode = (m_uchRXbuffer[3] == CX2_SETVALUE ? RC_ACK : m_uchRXbuffer[3]);
  }

  RelSemaphore();
  return result;  
}

/******************************************************************************/
/*Function:  C_CX2::GetAccLevel                                               */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::GetAccLevel(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen)  
{
  bool result;

  if(!GetSemaphore())
    return false;
  
  result = uchCommand(NO_BYTE, CX2_GETSETACCESS, pa_Data);

  if(result)
  {
	*pa_RetCode = (m_uchRXbuffer[3] == CX2_GETSETACCESS ? RC_ACK : m_uchRXbuffer[3]);

    if(pa_Data != NULL)
    {
		*pa_DataLen = m_uchRXbuffer[1];
		for(unsigned char LocCounter = 0; LocCounter < *pa_DataLen; LocCounter++)
		{
			pa_Data[LocCounter] = m_uchRXbuffer[4 + LocCounter];    
		}
    }
  }

  RelSemaphore();
  return result;  
}

/******************************************************************************/
/*Function:  C_CX2::SetAccLevel                                                */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::SetAccLevel(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen)  
{
  bool result;

  if(!GetSemaphore())
    return false;
  
  result = uchCommand(*pa_DataLen, CX2_GETSETACCESS, pa_Data);

  if(result)
  {
	*pa_RetCode = (m_uchRXbuffer[3] == CX2_GETSETACCESS ? RC_ACK : m_uchRXbuffer[3]);

	if(pa_Data != NULL)
    {
		*pa_DataLen = m_uchRXbuffer[1];
		for(unsigned char LocCounter = 0; LocCounter < *pa_DataLen; LocCounter++)
		{
			pa_Data[LocCounter] = m_uchRXbuffer[4 + LocCounter];    
		}
    }
  }

  RelSemaphore();
  return result;  
}

/******************************************************************************/
/*Function:  C_CX2::ExecFunction                                              */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::ExecFunction(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen)  
{
  bool result;

  if(!GetSemaphore())
    return false;
  
  result = uchCommand(*pa_DataLen, CX2_EXECFUNCTION, pa_Data);

  if(result)
  {
	*pa_RetCode = (m_uchRXbuffer[3] == CX2_EXECFUNCTION ? RC_ACK : m_uchRXbuffer[3]);

	if(pa_Data != NULL)
    {
		*pa_DataLen = m_uchRXbuffer[1];
		for(unsigned char LocCounter = 0; LocCounter < *pa_DataLen; LocCounter++)
		{
			pa_Data[LocCounter] = m_uchRXbuffer[4 + LocCounter];    
		}
    }
  }

  RelSemaphore();
  return result;  
}

/******************************************************************************/
/*Function:  C_CX2::ReadTable                                                 */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::ReadTable(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen)  
{
  bool result;

  if(!GetSemaphore())
    return false;
  
  result = uchCommand(2, CX2_READTABLE, pa_Data);

  if(result)
  {
	*pa_RetCode = (m_uchRXbuffer[3] == CX2_READTABLE ? RC_ACK : m_uchRXbuffer[3]);

    if(pa_Data != NULL)
    {
		*pa_DataLen = m_uchRXbuffer[1];
		for(unsigned char LocCounter = 0; LocCounter < *pa_DataLen; LocCounter++)
		{
			pa_Data[LocCounter] = m_uchRXbuffer[4 + LocCounter];    
		}
    }
  }

  RelSemaphore();
  return result;  
}

/******************************************************************************/
/*Function:  C_CX2::WriteTable                                                */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::WriteTable(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen)  
{
  bool result;

  if(!GetSemaphore())
    return false;
  
  result = uchCommand(*pa_DataLen, CX2_WRITETABLE, pa_Data);

  if(result)
  {
	*pa_RetCode = (m_uchRXbuffer[3] == CX2_WRITETABLE ? RC_ACK : m_uchRXbuffer[3]);
  }

  RelSemaphore();
  return result;  
}

/******************************************************************************/
/*Function:  C_CX2::CX2_FwUpdateCommand                                       */
/*In:                                                                         */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:                                                                */
/*reset device                                                                */
/******************************************************************************/
bool C_CX2::FwUpdateCommand(char* pa_RetCode, char* pa_Data, unsigned char* pa_DataLen)
{
	bool result;

	if (!GetSemaphore())
		return false;

	result = uchCommand(*pa_DataLen, CX2_FWUPDATECOMMAND, pa_Data);

	if (result)
	{
		*pa_RetCode = (m_uchRXbuffer[3] == CX2_FWUPDATECOMMAND ? RC_ACK : m_uchRXbuffer[3]);

		if (pa_Data != NULL)
		{
			*pa_DataLen = m_uchRXbuffer[1];
			for (unsigned char LocCounter = 0; LocCounter < *pa_DataLen; LocCounter++)
			{
				pa_Data[LocCounter] = m_uchRXbuffer[4 + LocCounter];
			}
		}
	}

	RelSemaphore();
	return result;
}

bool C_CX2::FwUpload_StartSession(CString a_filename, unsigned char* pa_RetCode)
{
	fw_upload_t uploadType = FW_UPLOAD_UNKNOWN;
	void(*HandlerThread[FW_UPLOAD_NUM])(LPVOID) =
	{
		/* FW_UPLOAD_CX2		  */ CXBHandlerThread,
		/* FW_UPLOAD_RENESAS_BL   */ RBLHandlerThread,
		/* FW_UPLOAD_RENESAS_CCFS */ NULL,				// TODO: to be implemented
		/* FW_UPLOAD_UNKNOWN      */ NULL
	};

	/* Default output: GENERIC ERROR */
	*pa_RetCode = CXB_RC_GENERIC_ERROR;

	/* Is a FW upload in progress? */
	if (GetFwUploadState() == CXB_RC_NOT_INITIALIZED)
	{
		/* Select the fw upload type */
		switch (m_Device)
		{
			case CX2_DEV_VALIDATOR:
				/* File format: Ascii currenza CXB (.cxb) */
				if (a_filename.Find(L".cxb") > 0)
				{
					uploadType = FW_UPLOAD_CX2;
				}
				break;

			case CX2_DEV_AUDIT:
				/* Standard: File format: Ascii S-Record (.mot) or (.aud FF filled)
				 * Premium:  File format: Ascii currenza CXB (.cxa) */
				if (a_filename.Find(L".cxa") > 0)
				{
					uploadType = FW_UPLOAD_CX2;
				}
				else if ((a_filename.Find(L".mot") > 0) || (a_filename.Find(L".aud") > 0))
				{
					uploadType = FW_UPLOAD_RENESAS_CCFS;
				}
				break;

			case CX2_DEV_INTERFACE:
				/* Standard: File format: Ascii S-Record (.mot)
				 * Premium:  File format: Ascii currenza CXB (.cxp) */
				if (a_filename.Find(L".mot") > 0)
				{
					uploadType = FW_UPLOAD_RENESAS_BL;
				}
				else if (a_filename.Find(L".cxp") > 0)
				{
					uploadType = FW_UPLOAD_CX2;
				}
				break;

			case CX2_DEV_HMI:
			case CX2_DEV_CCTALK:
				/* File format : Ascii S-Record (.dsp) */
				if (a_filename.Find(L".dsp") > 0)
				{
					uploadType = FW_UPLOAD_RENESAS_BL;
				}
				break;
		}

		/* Create thread for the FW upload */
		if (HandlerThread[uploadType])
		{
			SetFwUploadState(CXB_RC_UPLOAD_IN_PROGRESS);
			SetFwUploadFilename(a_filename);
			if (AfxBeginThread((AFX_THREADPROC)HandlerThread[uploadType], this, 0, 0, 0, 0) != NULL)
			{
				*pa_RetCode = CXB_RC_SUCCESS;
			}
			else
			{
				SetFwUploadState(CXB_RC_NOT_INITIALIZED);
			}
		}
		else
		{
			*pa_RetCode = CXB_RC_FILE_NOT_SUPPORTED;
		}
	}
	else
	{
		*pa_RetCode = CXB_RC_UPLOAD_IN_PROGRESS;
	}

	/* Return success only if CXB transfer is going to start */
	return (*pa_RetCode == CXB_RC_SUCCESS);
}

bool C_CX2::FwUpload_GetStatus(unsigned char *pa_Percentage, unsigned char* pa_RetCode)
{
	*pa_Percentage = m_fwUploadPercentage;
	*pa_RetCode = m_fwUploadState;

	return true;
}

bool C_CX2::FwUpload_EndSession(void)
{
	bool retCode = false;

	/* Is a CXB upload in progress? */
	if (GetFwUploadState() != CXB_RC_UPLOAD_IN_PROGRESS)
	{
		SetFwUploadState(CXB_RC_NOT_INITIALIZED);
		SetFwUploadFilename(NULL);
		SetFwUploadPercentage(0);
		retCode = true;
	}

	return retCode;
}

unsigned char C_CX2::GetFwUploadState(void)
{
	return(m_fwUploadState);
}

void C_CX2::SetFwUploadState(unsigned char state)
{
	m_fwUploadState = state;
}

CString C_CX2::GetFwUploadFilename(void)
{
	return(m_fwUploadFilename);
}

void C_CX2::SetFwUploadFilename(CString filename)
{
	m_fwUploadFilename = filename;
}

unsigned char C_CX2::GetFwUploadPercentage(void)
{
	return(m_fwUploadPercentage);
}

void C_CX2::SetFwUploadPercentage(unsigned char percentage)
{
	m_fwUploadPercentage = percentage;
}

