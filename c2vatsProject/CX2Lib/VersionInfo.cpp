// VersionInfo.cpp: implementation of the CVersionInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <winver.h>

#include "VersionInfo.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


// ---------------------------------------------------------------------
// use this constructer to get the version info for the calling program
CVersionInfo::CVersionInfo(void)
{   
   m_bOk = FALSE;
   m_pVerData = NULL;
   CString LocalStr;

   wchar_t fname[_MAX_PATH + 1];
   if (!GetModuleFileName(AfxGetInstanceHandle(), fname, _MAX_PATH))
      return;
   LocalStr = fname; //actual path of calling EXE
   LocalStr.ReverseFind(_T(',')); 
   LocalStr.Delete(LocalStr.ReverseFind(_T('\\')),(LocalStr.GetLength()-LocalStr.ReverseFind(_T('\\')))); //delete the exe reference and then append the dll reference
   LocalStr.Append(L"\\CX2lib.dll");
   memcpy(fname,LocalStr.GetBuffer(LocalStr.GetLength()),LocalStr.GetLength());

   DWORD handle;
   DWORD size = GetFileVersionInfoSize(fname, &handle);
   if (size == 0) {
      //DWORD err = GetLastError();
      return;
   }

   m_pVerData = new BYTE[size+1];
   
   m_bOk = GetFileVersionInfo(fname, handle, size, m_pVerData);
   AfxCheckMemory();

	// VerQueryValue returns pairs of Language/characterset information
	// (need to use debug to get the exact format)
	UINT cbTrans = 0;
	DWORD *lpdwTrans = NULL;
	int rc = VerQueryValue(m_pVerData, _T("\\VarFileInfo\\Translation"), (VOID**)&lpdwTrans, &cbTrans);
	if (rc) {
      //  WinNT = 040904B0, Win95 = 040904E4, etc
      m_sSearch.Format(L"\\StringFileInfo\\%04x%04x\\", LOWORD(lpdwTrans[(cbTrans/sizeof(DWORD))-1]), HIWORD(lpdwTrans[(cbTrans/sizeof(DWORD))-1]));
	}
}

// ---------------------------------------------------------------------
// use this constructor to get the version info for a dll or external program
CVersionInfo::CVersionInfo(LPTSTR pFname)
{   
   m_bOk = FALSE;
   
   DWORD handle;
   DWORD size = GetFileVersionInfoSize(pFname, &handle);
   if (size == 0) {
      //DWORD err = GetLastError();
      m_pVerData = NULL;
      return;
   }

   m_pVerData = new BYTE[size+1];
   
   m_bOk = GetFileVersionInfo(pFname, handle, size, m_pVerData);
   AfxCheckMemory();

   // VerQueryValue returns pairs of Language/characterset information
	// (need to use debug to get the exact format)
	UINT cbTrans = 0;
	DWORD *lpdwTrans = NULL;
	int rc = VerQueryValue(m_pVerData, _T("\\VarFileInfo\\Translation"), (VOID**)&lpdwTrans, &cbTrans);
	if (rc) {
      //  WinNT = 040904B0, Win95 = 040904E4, etc
      m_sSearch.Format(L"\\StringFileInfo\\%04x%04x\\", LOWORD(lpdwTrans[(cbTrans/sizeof(DWORD))-1]), HIWORD(lpdwTrans[(cbTrans/sizeof(DWORD))-1]));
	}
}
// ---------------------------------------------------------------------
CVersionInfo::~CVersionInfo(void)
{
   delete [] m_pVerData;
}

// -----------------------------------------------------------------------
LPSTR CVersionInfo::NoVerDataString(void)
{
   return "<no version info found>";   
}

// -----------------------------------------------------------------------
LPSTR CVersionInfo::GetProductName(void)
{   
   if (!m_bOk)
      return NoVerDataString();
   
   LPBYTE data = NULL;
   unsigned len = 0;
   m_sSearch.AppendFormat(L"ProductName");
   VerQueryValue((LPVOID)m_pVerData, m_sSearch, (VOID FAR* FAR*)&data, (UINT FAR*)&len);
    
   return (LPSTR)data;
}

// ------------------------------------------------------------------------
LPSTR CVersionInfo::GetProductVersion(void)
{   
   if (!m_bOk)
      return NoVerDataString();
   
   LPBYTE data = NULL;
   unsigned len = 0;
   m_sSearch.AppendFormat(L"ProductVersion");
   VerQueryValue((LPVOID)m_pVerData, m_sSearch, (VOID FAR* FAR*)&data, (UINT FAR*)&len);
    
   return (LPSTR)data;
}

// ------------------------------------------------------------------------
LPSTR CVersionInfo::GetCompanyName(void)
{   
   if (!m_bOk)
      return NoVerDataString();
   
   LPBYTE data = NULL;
   unsigned len = 0;
   m_sSearch.AppendFormat(L"CompanyName");
   VerQueryValue((LPVOID)m_pVerData, m_sSearch, (VOID FAR* FAR*)&data, (UINT FAR*)&len);
    
   return (LPSTR)data;
}

// ------------------------------------------------------------------------
LPSTR CVersionInfo::GetCopyright(void)
{   
   if (!m_bOk)
      return NoVerDataString();
   
   LPBYTE data = NULL;
   unsigned len = 0;
   m_sSearch.AppendFormat(L"LegalCopyright");
   VerQueryValue((LPVOID)m_pVerData, m_sSearch, (VOID FAR* FAR*)&data, (UINT FAR*)&len);
    
   return (LPSTR)data;
}

// ------------------------------------------------------------------------
LPSTR CVersionInfo::GetTrademark(void)
{   
   if (!m_bOk)
      return NoVerDataString();
   
   LPBYTE data = NULL;
   unsigned len = 0;
   m_sSearch.AppendFormat(L"LegalTrademarks");
   VerQueryValue((LPVOID)m_pVerData, m_sSearch, (VOID FAR* FAR*)&data, (UINT FAR*)&len);
    
   return (LPSTR)data;
}

// ------------------------------------------------------------------------

LPSTR CVersionInfo::GetFileVersion(void)
{   
   if (!m_bOk)
      return NoVerDataString();
   
   LPBYTE data = NULL;
   unsigned len = 0;
   m_sSearch.AppendFormat(L"FileVersion");
   VerQueryValue((LPVOID)m_pVerData, m_sSearch, (VOID FAR* FAR*)&data, (UINT FAR*)&len);
    
   return (LPSTR)data;
}