// CX2Lib.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "CX2Lib.h"
#include "C_RS232Handler.h"
#include "GPIOHandler.h"
#include "C_CX2.h"
#include "VersionInfo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/* CX2 device addresses */
typedef unsigned char CX2_ADDRESS;
#define CX2_ADDR_PC			((CX2_ADDRESS)0x01)
#define CX2_ADDR_VALIDATOR	((CX2_ADDRESS)0x08)
#define CX2_ADDR_AUDIT		((CX2_ADDRESS)0x0A)
#define CX2_ADDR_INTERFACE	((CX2_ADDRESS)0x0C)
#define CX2_ADDR_HMI		((CX2_ADDRESS)0x0D)
#define CX2_ADDR_CCTALK		((CX2_ADDRESS)0xCC)
#define CX2_ADDR_SDL		((CX2_ADDRESS)0xFF)

/* Device to address table */
static const CX2_ADDRESS CX2_addresses[CX2_DEV_NUM] =
{
	/* CX2_DEV_PC			*/ CX2_ADDR_PC,
	/* CX2_DEV_VALIDATOR	*/ CX2_ADDR_VALIDATOR,
	/* CX2_DEV_AUDIT		*/ CX2_ADDR_AUDIT,
	/* CX2_DEV_INTERFACE	*/ CX2_ADDR_INTERFACE,
	/* CX2_DEV_HMI			*/ CX2_ADDR_HMI,
	/* CX2_DEV_CCTALK		*/ CX2_ADDR_CCTALK,
	/* CX2_DEV_SDL			*/ CX2_ADDR_SDL
};

static C_CX2 *paCX2Obj[CX2_DEV_NUM] = { NULL };
static C_RS232Handler *paRS232Obj[CX2_DEV_NUM] = { NULL };
static unsigned char ComDevId = 0xFF;

C_CX2 ** pSDL = &paCX2Obj[CX2_DEV_SDL];

/******************************************************************************/
/*Function:  CX2_GetCurrentDllInfo                                            */
/*In:                                                                         */
/*Out:    a_Major:  major release                                             */
/*        a_Minor:  minor release                                             */
/*        a_BugFix:  bug fix release                                          */
/*        a_BuildCode:  build code release                                    */
/*        a_Day:  deployment day                                              */
/*        a_Month:  deployment month                                          */
/*        a_Year:  deployment year                                            */
/*                                                                            */
/*In/out:                                                                     */
/*Description:                                                                */
/*Call this function to retrieve some dll infos                               */
/******************************************************************************/ 
void  CX2LIB_API CX2_GetCurrentDllInfo(unsigned char &a_Major, unsigned char &a_Minor, unsigned char &a_BugFix, unsigned char &a_BuildCode, unsigned char &a_Day, unsigned char &a_Month, unsigned int &a_Year)
{

  CVersionInfo CVers;
  CString LocalStr;
   
  LocalStr = CVers.GetProductVersion();
  a_Major = (unsigned char)_wtoi(LocalStr.Left(LocalStr.FindOneOf(L",")));
  LocalStr.Delete(0,(LocalStr.FindOneOf(L",")+1));
  a_Minor = (unsigned char)_wtoi(LocalStr.Left(LocalStr.FindOneOf(L",")));
  LocalStr.Delete(0, (LocalStr.FindOneOf(L",") + 1));
  a_BugFix = (unsigned char)_wtoi(LocalStr.Left(LocalStr.FindOneOf(L",")));
  LocalStr.Delete(0, (LocalStr.FindOneOf(L",") + 1));
  a_BuildCode = (unsigned char)_wtoi(LocalStr.Left(LocalStr.FindOneOf(L",")));
  LocalStr.Delete(0, (LocalStr.FindOneOf(L",") + 1));

  LocalStr = CVers.GetFileVersion();
  a_Day = (unsigned char)_wtoi(LocalStr.Left(LocalStr.FindOneOf(L",")));
  LocalStr.Delete(0, (LocalStr.FindOneOf(L",") + 1));
  a_Month = (unsigned char)_wtoi(LocalStr.Left(LocalStr.FindOneOf(L",")));
  LocalStr.Delete(0, (LocalStr.FindOneOf(L",") + 1));
  a_Year = (unsigned int)_wtoi(LocalStr.Left(LocalStr.FindOneOf(L",")));
}

/******************************************************************************/
/*Function:  CX2_StartSession                                                 */
/*In:   CX2_COMPORT a_ComPort:  serial communication port number              */
/*      a_DeviceAddress: slave cct address									  */
/*Out:   true if is all OK                                                    */
/*In/out: *pa_DeviceID: pointer to device ID                                  */
/*Description:                                                                */
/*call this function to start a session                                       */
/******************************************************************************/ 
BOOL  CX2LIB_API CX2_StartSession(unsigned char a_ComPort, unsigned char* pa_DeviceID, unsigned char a_Device)
{
  if (a_Device >= CX2_DEV_NUM)
  {
	// Device is out of limit
    *pa_DeviceID = 0xFF;
    return false;
  }

  // Assign device identifier for output parameter
  *pa_DeviceID = a_Device;

  // Is the session already started for specified device?
  if ((paCX2Obj[a_Device] != NULL) && (paRS232Obj[a_Device] != NULL))
  {
	// Simply return success
	return true;
  }

  paCX2Obj[a_Device] = new (C_CX2);
  if(ComDevId == 0xFF)
  {
	paRS232Obj[a_Device] = new (C_RS232Handler);

	paRS232Obj[a_Device]->SetComPort((C_RS232Handler::T_ComPort) a_ComPort);

	if (a_ComPort != SDL_COM_NUM)
	{
		paRS232Obj[a_Device]->SetBaudRate(C_RS232Handler::Baud38400);
	}

	if (paCX2Obj[a_Device]->C_CX2Init(paRS232Obj[a_Device]))
	{
		paCX2Obj[a_Device]->m_Device = a_Device;
		paCX2Obj[a_Device]->m_DeviceAddress = CX2_addresses[a_Device];
		paCX2Obj[a_Device]->m_SourceAddress = CX2_addresses[CX2_DEV_PC];
		ComDevId = a_Device;
		
		if (a_ComPort == SDL_COM_NUM)
		{
			paCX2Obj[a_Device]->ChangeBaudRate(C_RS232Handler::Baud38400);
			paCX2Obj[a_Device]->ChangeIntraMsgDelay(0);
		}
		return true;
	}
	else
	{
		paCX2Obj[a_Device]->C_CX2Stop();

		delete (paRS232Obj[a_Device]);
		paRS232Obj[a_Device] = NULL;
		delete (paCX2Obj[a_Device]);
		paCX2Obj[a_Device] = NULL;
		return false;
	}
  }
  else
  {
	paRS232Obj[a_Device] = paRS232Obj[ComDevId];
	
	paCX2Obj[a_Device]->m_RS232Owner = false;
	paCX2Obj[a_Device]->m_RS232Handler = paRS232Obj[a_Device];
	paCX2Obj[a_Device]->m_Device = a_Device;
	paCX2Obj[a_Device]->m_DeviceAddress = CX2_addresses[a_Device];
	paCX2Obj[a_Device]->m_SourceAddress = CX2_addresses[CX2_DEV_PC];
	paCX2Obj[a_Device]->m_Semaphore = paCX2Obj[ComDevId]->m_Semaphore;
	return true;
  }
}

/******************************************************************************/
/*Function:  CX2_EndSession                                                   */
/*In:                                                                         */
/*Out:   true if is all OK                                                    */
/*In/out: *pa_DeviceID: pointer to device ID                                  */
/*Description:                                                                */
/*call this function to end a session                                         */
/******************************************************************************/ 
BOOL  CX2LIB_API CX2_EndSession(unsigned char a_DeviceID)
{
  if((paCX2Obj[a_DeviceID] != NULL) && (paRS232Obj[a_DeviceID] != NULL))
  {
    if(paCX2Obj[a_DeviceID]->m_RS232Owner)
	{
		paCX2Obj[a_DeviceID]->C_CX2Stop(); 
		delete (paRS232Obj[a_DeviceID]);

		ComDevId = 0xFF;
	}
    paRS232Obj[a_DeviceID] = NULL;
    delete (paCX2Obj[a_DeviceID]);
    paCX2Obj[a_DeviceID] = NULL;
    return true;
  }
  else
    return false;
}

BOOL  CX2LIB_API CX2_Purge(void)
{
	BOOL retcode = FALSE;
	if ( (ComDevId != 0xFF) && (paRS232Obj[ComDevId] != NULL) && (paCX2Obj[ComDevId] != NULL) )
	{
		if (paCX2Obj[ComDevId]->m_RS232Owner)
		{
			paCX2Obj[ComDevId]->C_CX2Purge();
			retcode = TRUE;
		}
	}
	return retcode;
}

/******************************************************************************/
/*Function:  PingDevice                                                       */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL CX2LIB_API CX2_PingDevice(unsigned char a_DeviceID, CX2_RC * pa_RetCode)
{
  if(paCX2Obj[a_DeviceID] != NULL)  
    return (paCX2Obj[a_DeviceID]->PingDevice((char *)pa_RetCode));
  else
    return false;
}

/******************************************************************************/
/*Function:  CX2_GetIdInfo                                                    */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL  CX2LIB_API CX2_GetIdInfo(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode)
{
  if(paCX2Obj[a_DeviceID] != NULL)  
    return (paCX2Obj[a_DeviceID]->GetIdInfo((char *)pa_RetCode, pa_Data, pa_DataLen));
  else
    return false;
}

/******************************************************************************/
/*Function:  ResetDevice                                                      */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL CX2LIB_API CX2_ResetDevice(unsigned char a_DeviceID, CX2_RC * pa_RetCode)
{
  if(paCX2Obj[a_DeviceID] != NULL)  
    return (paCX2Obj[a_DeviceID]->ResetDevice((char *)pa_RetCode));
  else
    return false;
}

/******************************************************************************/
/*Function:  PollDevice                                                       */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL CX2LIB_API CX2_PollDevice(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode)
{
  if(paCX2Obj[a_DeviceID] != NULL)  
    return (paCX2Obj[a_DeviceID]->PollDevice((char *)pa_RetCode, pa_Data, pa_DataLen));
  else
    return false;
}

/******************************************************************************/
/*Function:  CX2_GetValue                                                     */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL  CX2LIB_API CX2_GetValue(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode)
{
  if(paCX2Obj[a_DeviceID] != NULL)  
    return (paCX2Obj[a_DeviceID]->GetValue((char *)pa_RetCode, pa_Data, pa_DataLen));
  else
    return false;
}

/******************************************************************************/
/*Function:  CX2_SetValue                                                    */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL  CX2LIB_API CX2_SetValue(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode)
{
  if(paCX2Obj[a_DeviceID] != NULL)  
    return (paCX2Obj[a_DeviceID]->SetValue((char *)pa_RetCode, pa_Data, pa_DataLen));
  else
    return false;
}

/******************************************************************************/
/*Function:  CX2_GetAccLevel                                                  */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL  CX2LIB_API CX2_GetAccLevel(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode)
{
  if(paCX2Obj[a_DeviceID] != NULL)  
    return (paCX2Obj[a_DeviceID]->GetAccLevel((char *)pa_RetCode, pa_Data, pa_DataLen));
  else
    return false;
}

/******************************************************************************/
/*Function:  CX2_SetAccLevel                                                  */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL  CX2LIB_API CX2_SetAccLevel(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode)
{
  if(paCX2Obj[a_DeviceID] != NULL)  
    return (paCX2Obj[a_DeviceID]->SetAccLevel((char *)pa_RetCode, pa_Data, pa_DataLen));
  else
    return false;
}

/******************************************************************************/
/*Function:  CX2_ExecFunction                                                  */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL  CX2LIB_API CX2_ExecFunction(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode)
{
  if(paCX2Obj[a_DeviceID] != NULL)  
    return (paCX2Obj[a_DeviceID]->ExecFunction((char *)pa_RetCode, pa_Data, pa_DataLen));
  else
    return false;
}

/******************************************************************************/
/*Function:  CX2_ReadTable                                                    */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL  CX2LIB_API CX2_ReadTable(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode)
{
  if(paCX2Obj[a_DeviceID] != NULL)  
    return (paCX2Obj[a_DeviceID]->ReadTable((char *)pa_RetCode, pa_Data, pa_DataLen));
  else
    return false;
}

/******************************************************************************/
/*Function:  CX2_WriteTable                                                   */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL  CX2LIB_API CX2_WriteTable(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode)
{
  if(paCX2Obj[a_DeviceID] != NULL)  
    return (paCX2Obj[a_DeviceID]->WriteTable((char *)pa_RetCode, pa_Data, pa_DataLen));
  else
    return false;
}

/******************************************************************************/
/*Function:  CX2_FwUpdateCommand                                              */
/*In:        a_DeviceID: device ID                                            */
/*Out:                                                                        */
/*In/out:                                                                     */
/*Description:  (01)                                                          */
/*reset device                                                                */
/******************************************************************************/
BOOL  CX2LIB_API CX2_FwUpdateCommand(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode)
{
	if (paCX2Obj[a_DeviceID] != NULL)
		return (paCX2Obj[a_DeviceID]->FwUpdateCommand((char *)pa_RetCode, pa_Data, pa_DataLen));
	else
		return false;
}

BOOL  CX2LIB_API CXB_FwUpload_StartSession(unsigned char a_DeviceID, char *a_filename, CXB_RC * pa_RetCode)
{
	BOOL retCode = false;
	CString filename = CString(a_filename);

	if (paCX2Obj[a_DeviceID] != NULL)
	{
		retCode = paCX2Obj[a_DeviceID]->FwUpload_StartSession(filename, (unsigned char *)pa_RetCode);
	}

	return retCode;
}

BOOL  CX2LIB_API CXB_FwUpload_GetStatus(unsigned char a_DeviceID, unsigned char *pa_Percentage, CXB_RC * pa_RetCode)
{
	BOOL retCode = false;

	if (paCX2Obj[a_DeviceID] != NULL)
	{
		retCode = paCX2Obj[a_DeviceID]->FwUpload_GetStatus(pa_Percentage, (unsigned char *)pa_RetCode);
	}

	return retCode;
}

BOOL  CX2LIB_API CXB_FwUpload_EndSession(unsigned char a_DeviceID)
{
	BOOL retCode = false;

	if (paCX2Obj[a_DeviceID] != NULL)
	{
		retCode = paCX2Obj[a_DeviceID]->FwUpload_EndSession();
	}

	return retCode;
}

BOOL  CX2LIB_API CXB_RC_str(CXB_RC a_code, const char **pa_strCode)
{
	const char *CXB_RC_str[] =
	{
		/* CXB_RC_NOT_INITIALIZED		*/ "Not initialized",
		/* CXB_RC_IDLE					*/ "Idle",
		/* CXB_RC_SUCCESS				*/ "Success",
		/* CXB_RC_UPLOAD_IN_PROGRESS	*/ "Upload in progress",
		/* CXB_RC_COMM_FAILURE			*/ "Communication failure",
		/* CXB_RC_BOARD_UNREACHABLE		*/ "Board unreachable",
		/* CXB_RC_FILE_NOT_FOUND		*/ "File not found",
		/* CXB_RC_FILE_MALFORMED		*/ "File is malformed",
		/* CXB_RC_FILE_NOT_SUPPORTED	*/ "File not supported",
		/* CXB_RC_BOOTLOADER_NOT_FOUND	*/ "Bootloader not found",
		/* CXB_RC_FLASH_UNLOCK_FAILURE  */ "Flash unlock failure",
		/* CXB_RC_FILE_TRANSFER_ERROR	*/ "File transfer error",
		/* CBB_RC_FLASH_ERROR           */ "Flash error",
		/* CXB_RC_COMPLETED				*/ "Completed",
		/* CXB_RC_GENERIC_ERROR			*/ "Generic error"
	};

	*pa_strCode = CXB_RC_str[a_code];
	return true;
}

BOOL CX2_GPIO_StartSession(void)
{
	bool ret = GPIOHandler::Open();

	if (!ret)
	{
		unsigned char dummy_id = 0;
		//try to connect to SDL
		ret = (CX2_StartSession(SDL_COM_NUM, &dummy_id, CX2_DEV_SDL) == TRUE);
	}

	if (ret)
	{
		/* Set default output values */
		CX2_GPIO_MasterReqClear();
		CX2_GPIO_GwFlashClear();
		CX2_GPIO_EmpFlashClear();
		CX2_GPIO_HardResetClear();
		CX2_GPIO_PrinterClear();
	}

	return ret;
}

BOOL CX2_GPIO_EndSession(void)
{
	bool ret = GPIOHandler::Close();

	if (!ret)
	{
		unsigned char dummy_id = 0;
		//try to connect to SDL
		ret = (CX2_EndSession(CX2_DEV_SDL) == TRUE);
	}

	if (ret)
	{
		/* Set default output values */
		CX2_GPIO_MasterReqClear();
		CX2_GPIO_GwFlashClear();
		CX2_GPIO_EmpFlashClear();
		CX2_GPIO_HardResetClear();
		CX2_GPIO_PrinterClear();
	}

	return ret;
}
BOOL CX2_GPIO_HardResetClear(void)
{
	bool ret = GPIOHandler::HardResetSet(GPIO_ON);

	return ret;
}

BOOL CX2_GPIO_PrinterClear(void)
{
	bool ret = GPIOHandler::StartPrintSet(GPIO_ON);

	return ret;
}

BOOL CX2_GPIO_HardReset(void)
{
	bool ret = GPIOHandler::HardReset();

	return ret;
}

BOOL CX2_GPIO_MasterReqSet(void)
{
	bool ret = GPIOHandler::MasterReqSet(GPIO_OFF);

	return ret;
}

BOOL CX2_GPIO_MasterReqClear(void)
{
	bool ret = GPIOHandler::MasterReqSet(GPIO_ON);

	return ret;
}

BOOL CX2_GPIO_GwFlashSet(void)
{
	bool ret = GPIOHandler::GwFlashSet(GPIO_OFF);

	return ret;
}

BOOL CX2_GPIO_GwFlashClear(void)
{
	bool ret = GPIOHandler::GwFlashSet(GPIO_ON);

	return ret;
}

BOOL CX2_GPIO_EmpFlashSet(void)
{
	bool ret = GPIOHandler::EmpFlashSet(GPIO_OFF);

	return ret;
}

BOOL CX2_GPIO_EmpFlashClear(void)
{
	bool ret = GPIOHandler::EmpFlashSet(GPIO_ON);

	return ret;
}

BOOL CX2_GPIO_StartPrint(void)
{
	bool ret = GPIOHandler::StartPrint();

	return ret;
}

BOOL CX2_GPIO_PowerOn(void)
{
	bool ret = GPIOHandler::PowerOn();

	return ret;
}

BOOL CX2_GPIO_PowerOff(void)
{
	bool ret = GPIOHandler::PowerOff();

	return ret;
}

BOOL CX2_GPIO_MDBPowerOn(void)
{
	bool ret = GPIOHandler::MDBPowerOn();

	return ret;
}

BOOL CX2_GPIO_MDBPowerOff(void)
{
	bool ret = GPIOHandler::MDBPowerOff();

	return ret;
}