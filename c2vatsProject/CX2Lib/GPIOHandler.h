
#pragma once

/* Digital IO states */
#define GPIO_OFF			((unsigned char)0x00)
#define GPIO_ON				((unsigned char)0x01)
#define GPIO_UNKNOWN		((unsigned char)0x02)

/* Pinout */
#define GPIO_POWER			((unsigned char)0x08)

#define GPIO_MDB_POWER		((unsigned char)0x05)
#define GPIO_START_PRINT	((unsigned char)0x04)
#define GPIO_GWFLASH		((unsigned char)0x03)
#define GPIO_MASTERREQ		((unsigned char)0x02)
#define GPIO_EMPFLASH		((unsigned char)0x01)
#define GPIO_RESET			((unsigned char)0x00)

/* Maximum number of GPIO */
#define GPIO_MAX_NUM		8

class GPIOHandler
{
public:

	static bool Open(void);
	static bool Close(void);
	static bool IsOpen(void);

	static bool HardReset(void);
	static bool HardResetSet(unsigned char state);
	static bool MasterReqSet(unsigned char state);
	static bool GwFlashSet(unsigned char state);
	static bool EmpFlashSet(unsigned char state);
	static bool StartPrint(void);
	static bool StartPrintSet(unsigned char state);
	static bool PowerOn(void);
	static bool PowerOff(void);
	static bool MDBPowerOn(void);
	static bool MDBPowerOff(void);

	static bool SaveConfig(void);
	static bool RestoreConfig(void);

private:

	static bool SetCmd(unsigned char id);
	static bool ClearCmd(unsigned char id);
	static bool ReadCmd(unsigned char id, unsigned char *state);
	static char *ExtractResponse(char *request, char *response);
	static bool Set(unsigned char id, unsigned char state);

	static HANDLE m_hComPort;
	static char m_cmdBuffer[32];
	static char m_responseBuffer[32];
	static char m_response[32];
	static unsigned char m_DIOState[GPIO_MAX_NUM];
	static unsigned char m_DIOStateSaved[GPIO_MAX_NUM];
};
