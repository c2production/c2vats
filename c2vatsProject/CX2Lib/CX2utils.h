#pragma once

#include <list>

typedef unsigned int address_t;
typedef enum _CX2_SREC_RC
{
	CX2_SREC_RC_OK,
	CX2_SREC_RC_GENERIC_ERROR,
	CX2_SREC_RC_FILE_NOT_FOUND,
	CX2_SREC_RC_FILE_IS_MALFORMED,
	CX2_SREC_RC_FILE_NOT_SUPPORTED
} CX2_SREC_RC;

class CX2_utils
{
public:
	CX2_utils(void);
	~CX2_utils(void);
	int char2int(char input);
	unsigned char hex2bin(char *src, char *target);
};

class CX2_flashpage
{
public:
	CX2_flashpage(unsigned int pageSize, unsigned char filler, address_t startAddress);
	~CX2_flashpage(void);

	bool SetData(address_t address, unsigned char *data);
	bool GetPageData(unsigned char *data);
	address_t GetStartAddress(void);

private:
	address_t m_startAddress;
	unsigned char m_filler;
	unsigned int m_pageSize;
	unsigned char *m_data;
	bool m_empty;
};

class CX2_srecord
{
public:
	CX2_srecord(CString filename, unsigned int pageSize, unsigned char filler = 0xFF);
	~CX2_srecord(void);

	CX2_SREC_RC GetParseResult(void);
	bool GetPageData(bool isFirst, address_t *address, unsigned char *data, bool *empty, unsigned int *pageNum, unsigned int *totalPageNum);

private:
	unsigned int m_pageSize;
	unsigned char m_filler;
	address_t m_baseAddress;
	CX2_SREC_RC m_parseResult;
	std::list<CX2_flashpage> m_pages;
	std::list<CX2_flashpage>::iterator m_pagesIt;
	unsigned int m_pagesCnt;

	void ParseFile(CString filename);
};
