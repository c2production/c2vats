#pragma once
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the CX2LIB_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// CX2LIB_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef CX2LIB_EXPORTS
#define CX2LIB_API __declspec(dllexport)
#else
#define CX2LIB_API __declspec(dllimport)
#endif
	
typedef enum CX2_COMPORT_Y : unsigned char
{
  COM1,
  COM2,
  COM3,
  COM4,
  COM5,
  COM6,
  COM7,
  COM8
} CX2_COMPORT; 

typedef enum CX2_DEVICE_Y : unsigned char
{
  CX2_DEV_PC = 0,			/* PC */
  CX2_DEV_VALIDATOR,		/* Currenza Validator module */
  CX2_DEV_AUDIT,			/* Currenza audit module */
  CX2_DEV_INTERFACE,		/* Currenza Changer Interface module */
  CX2_DEV_HMI,				/* Currenza Human Machine Interface (display) module */
  CX2_DEV_CCTALK,			/* Currenza Hopper ccTalk module */
  CX2_DEV_SDL,				/* SDL interface */

  CX2_DEV_NUM				/* Number of currenza devices*/
} CX2_DEVICE;

typedef enum CX2_RC_Y : unsigned char
{
  RC_ACK = 0,
  RC_ERR = 0xFA,
  RC_UNK = 0xFB,
  RC_NAL = 0xFC,
  RC_BAD = 0xFD,
  RC_NAK = 0xFF
} CX2_RC;

typedef enum CXB_FW_UPDATE_STATE_Y : unsigned char
{
  CXB_RC_NOT_INITIALIZED,
  CXB_RC_IDLE,
  CXB_RC_SUCCESS,
  CXB_RC_UPLOAD_IN_PROGRESS,
  CXB_RC_COMM_FAILURE,
  CXB_RC_BOARD_UNREACHABLE,
  CXB_RC_FILE_NOT_FOUND,
  CXB_RC_FILE_MALFORMED,
  CXB_RC_FILE_NOT_SUPPORTED,
  CXB_RC_BOOTLOADER_NOT_FOUND,
  CXB_RC_FLASH_UNLOCK_FAILURE,
  CXB_RC_FILE_TRANSFER_ERROR,
  CBB_RC_FLASH_ERROR,
  CXB_RC_COMPLETED,
  CXB_RC_GENERIC_ERROR
} CXB_RC;

extern "C"
{
  void  CX2LIB_API CX2_GetCurrentDllInfo(unsigned char &a_Major, unsigned char &a_Minor, unsigned char &a_BugFix, unsigned char &a_BuildCode, unsigned char &a_Day, unsigned char &a_Month, unsigned int &a_Year);

  /* CX2 communication start/close/purge */
  BOOL  CX2LIB_API CX2_StartSession(unsigned char a_ComPort, unsigned char* pa_DeviceID, unsigned char a_Device);
  BOOL  CX2LIB_API CX2_EndSession(unsigned char a_DeviceID);
  BOOL  CX2LIB_API CX2_Purge(void);

  /* CX2 raw commands */
  BOOL  CX2LIB_API CX2_PingDevice(unsigned char a_DeviceID, CX2_RC * pa_RetCode);
  BOOL  CX2LIB_API CX2_GetIdInfo(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode);
  BOOL  CX2LIB_API CX2_ResetDevice(unsigned char a_DeviceID, CX2_RC * pa_RetCode);
  BOOL  CX2LIB_API CX2_PollDevice(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode);
  BOOL  CX2LIB_API CX2_GetValue(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode);
  BOOL  CX2LIB_API CX2_SetValue(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode);
  BOOL  CX2LIB_API CX2_GetAccLevel(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode);
  BOOL  CX2LIB_API CX2_SetAccLevel(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode);
  BOOL  CX2LIB_API CX2_ExecFunction(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode); 
  BOOL  CX2LIB_API CX2_ReadTable(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode);
  BOOL  CX2LIB_API CX2_WriteTable(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode);
  BOOL  CX2LIB_API CX2_FwUpdateCommand(unsigned char a_DeviceID, char* pa_Data, unsigned char* pa_DataLen, CX2_RC * pa_RetCode);

  /* CXB FW_update commands */
  BOOL  CX2LIB_API CXB_FwUpload_StartSession(unsigned char a_DeviceID, char *a_filename, CXB_RC * pa_RetCode);
  BOOL  CX2LIB_API CXB_FwUpload_GetStatus(unsigned char a_DeviceID, unsigned char *pa_Percentage, CXB_RC * pa_RetCode);
  BOOL  CX2LIB_API CXB_FwUpload_EndSession(unsigned char a_DeviceID);
  BOOL  CX2LIB_API CXB_RC_str(CXB_RC a_code, const char **pa_strCode);

  /* CX2 GPIO commands */
  BOOL  CX2LIB_API CX2_GPIO_StartSession(void);
  BOOL  CX2LIB_API CX2_GPIO_EndSession(void);
  BOOL  CX2LIB_API CX2_GPIO_HardResetClear(void);
  BOOL  CX2LIB_API CX2_GPIO_PrinterClear(void);
  BOOL  CX2LIB_API CX2_GPIO_HardReset(void);
  BOOL  CX2LIB_API CX2_GPIO_StartPrint(void);
  BOOL  CX2LIB_API CX2_GPIO_MasterReqSet(void);
  BOOL  CX2LIB_API CX2_GPIO_MasterReqClear(void);
  BOOL  CX2LIB_API CX2_GPIO_GwFlashSet(void);
  BOOL  CX2LIB_API CX2_GPIO_GwFlashClear(void);
  BOOL  CX2LIB_API CX2_GPIO_EmpFlashSet(void);
  BOOL  CX2LIB_API CX2_GPIO_EmpFlashClear(void);
  BOOL  CX2LIB_API CX2_GPIO_PowerOn(void);
  BOOL  CX2LIB_API CX2_GPIO_PowerOff(void);
  BOOL  CX2LIB_API CX2_GPIO_MDBPowerOn(void);
  BOOL  CX2LIB_API CX2_GPIO_MDBPowerOff(void);
}