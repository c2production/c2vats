
#pragma once

#include <vector>
#include <string>

class CEnumerateSerial
{
public:
	static BOOL UsingSetupAPI2(std::vector<UINT>& ports, std::vector<std::wstring>& friendlyNames);

protected:
	static BOOL RegQueryValueString(HKEY kKey, LPCTSTR lpValueName, LPTSTR& pszValue);
	static BOOL QueryRegistryPortName(HKEY hDeviceKey, int& nPort);
	static HMODULE LoadLibraryFromSystem32(LPCTSTR lpFileName);
	static BOOL IsNumeric(LPCSTR pszString, BOOL bIgnoreColon);
	static BOOL IsNumeric(LPCWSTR pszString, BOOL bIgnoreColon);
};

class CAutoHeapAlloc
{
public:
	CAutoHeapAlloc(HANDLE hHeap = GetProcessHeap(), DWORD dwHeapFreeFlags = 0) : m_pData(NULL),
		m_hHeap(hHeap),
		m_dwHeapFreeFlags(dwHeapFreeFlags)
	{
	}

	BOOL Allocate(SIZE_T dwBytes, DWORD dwFlags = 0)
	{
		m_pData = HeapAlloc(m_hHeap, dwFlags, dwBytes);
		return (m_pData != NULL);
	}

	~CAutoHeapAlloc()
	{
		if (m_pData != NULL)
		{
			HeapFree(m_hHeap, m_dwHeapFreeFlags, m_pData);
			m_pData = NULL;
		}
	}

	LPVOID m_pData;
	HANDLE m_hHeap;
	DWORD  m_dwHeapFreeFlags;
};