﻿/**
 * Class: VutDoor
 * This is the representation of the upper door of the navegator. It cointains information
 * of the status (open/close), of the door.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Utils;

namespace C2ValidatorATS.Vut
{
    
    class VutDoor
    {
        private byte VALIDATOR_TYPE_ID = 1;
        public bool IsUpperDoorOpen { get; set; }

        String s = Msgs.Error.NO_ABLE_TO_GET_IN_CONTACT_WITH_VALIDATOR; //Testing
        public VutDoor()
        {
            
        }
        //If the "ResultStatusMsg" = Ok and retult is True the door is Open or bad mounted(ambiental light is comming in)
        //If the "ResultStatusMsg" = Ok and retult is False the door is Closed
        public bool CheckDoorStatus(ref string ResulStatustMsg)
        {
            byte[] requestCmdId = new byte[1];
            byte[] responseData = null;

            // Ping the validator just to check that there is contact
            if (!CX2ProtAdaptLayer.PingDevice(VALIDATOR_TYPE_ID))
            {
                ResulStatustMsg = Msgs.Error.NO_ABLE_TO_PING_THE_VALIDATOR;
                return false;
            }

            // prepare to send the getValue() command:CX2 Spec: 0x13H "Diagnostic I"
            requestCmdId[0] = 0x13;
            //Send and check if the command was properly sent
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, requestCmdId, out responseData))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return false;
            }

            // Diganostic I brings 12 byte information data. Byte nr5, index 4 contain ls status "blocked" = 1, "ok" = 0
            if (responseData.Length != 12)
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_RECEIVE_CORRECT_RESPONSE_AFTER_SENDING_GET_VALUE;
                return false;
            }
            // MSB 0111 = LS3, LS2 and LS1 get ambiental light which means that door is open
            if (responseData[4] == 0x70)
            {
                ResulStatustMsg = Msgs.Info.RESULT_OK;
                return true;
            }
            //Door is closed. The ResultMsg indicate that the check of the LS has been correctly done. The return true or false 
            //is to tell the status, "open" or "close" respectively.
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return false;
        }
    }
}
