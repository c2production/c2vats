﻿/*Class ValidatorUnderTest
 * This class is the representation of the Validator under test, VUT. 
 * Naming convention used here is:
 * var VarName for Public properties
 * var varName for private varibles
 
 * Author: Leonardo Garcia, PayComplete
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
//using C2Validator.Utils;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Vut.VutDBlocksInfo;


namespace C2ValidatorATS.Vut
{
    public sealed class ValidatorUnderTest
    {

        #region private members
        //Part of the Validator
        private VutSensors CVutSensors;
        private VutHmi CVutHmi;
        private VutDoor CVutDoor;
        private VutSorterGates CVutSorterGates;
        private VutHmiDisplay CVutHmiDisplay;
        private VutIdentificationData CVutIdentificationData;
        private TblProductId CTTblProductId;
        private TblFunctionSetup CTblFunctionSetup;
        private TblChannelSetupBlkA CTblChannelSetupBlkA;
        private TblChannelSetupBlkB CTblChannelSetupBlkB;
        private TblChannelIds CTblChannelIds;
        private TblCoinLimits CTblCoinLimits;
        //End parts
        //ID used for device identification when accesing Cx2 libraries
        private byte VALIDATOR_TYPE_ID = 1;
        //private byte HMI_TYPE_ID = 4;
        private Leogger Log;

        #endregion

        #region Public members

        public byte ValidatorId { get; set; }
        public bool IsValidatorDoorOpen { get; set; }
        public bool IsDisplayDoorOpen { get; set; }
        public bool IsHMIDisplayPresent { get; set; }
        public bool IsHMIKeysOnlyPresent { get; set; }
        public bool IsSorterDoorPresent { get; set; }
        public bool IsVutConnected { get; set; }
        public bool IsRIMPresent { get; set; }
        public int SorterSelectedByUser { get; set; }
        //Vut data manipulation por eso publics
        public VutTableOps CVutTableOps;

        #endregion

        public ValidatorUnderTest()
        {
            //Empty validator
            ValidatorId = VALIDATOR_TYPE_ID;
            IsVutConnected = false;
            IsHMIDisplayPresent = false;
            IsHMIKeysOnlyPresent = false;
            Log = Logger.instance;
            //CTblFunctionSetup = new TblFunctionSetup(CVutTableOps);//###### Solo para test, borrar luego ########
        }
 
        /// <summary>
        /// Validator detected is connected. Init the Values
        /// </summary>
        public void InitializeValues()
        {
            string result = Msgs.Info.RESULT_OK; // used during test status check
            CVutHmi = new VutHmi();
            CVutSensors = new VutSensors();
            CVutDoor = new VutDoor();
            CVutSorterGates = new VutSorterGates();
            CVutHmiDisplay = new VutHmiDisplay();
            CVutTableOps = new VutTableOps(this);
            CVutIdentificationData = new VutIdentificationData();
            CTTblProductId = new TblProductId(CVutTableOps);
            CTblFunctionSetup = new TblFunctionSetup(CVutTableOps);
            CTblChannelSetupBlkA = new TblChannelSetupBlkA(CVutTableOps);
            CTblChannelSetupBlkB = new TblChannelSetupBlkB(CVutTableOps);
            CTblChannelIds = new TblChannelIds(CVutTableOps);
            CTblCoinLimits = new TblCoinLimits(CVutTableOps);

            IsHMIDisplayPresent = CVutHmi.IsHmiDisplayPresent;
            IsHMIKeysOnlyPresent = CVutHmi.IsHmiKeysOnlyPresent;
            //CVutHmi.CheckDoorStatus(ref result);
            //If the status is true means the ambiental light is blocking the CPs which means that the door is not mounted or open
            //We asume at this stage the open is not mounted
            if (CVutHmi.CheckDoorStatus(ref result) && (result == Msgs.Info.RESULT_OK))
                IsSorterDoorPresent = false;
            else IsSorterDoorPresent = true;
            IsRIMPresent = false; //CVutSensors.RIMSensorPresent; I will skipp this info. The rim sensor presence is pointed out by the user
            IsVutConnected = true;
            SorterSelectedByUser = -1;
            
        }

        /// <summary>
        /// Set all values Null. The Device is not connected
        /// </summary>
        public void NullifyValues()
        {
            CVutSensors = null;
            CVutHmi = null;
            CVutDoor = null;
            CVutSorterGates = null;
            CVutHmiDisplay = null;
            CVutTableOps = null;
            CVutIdentificationData = null;
            IsHMIDisplayPresent = false;
            IsHMIKeysOnlyPresent = false;
            IsSorterDoorPresent = false;
            IsVutConnected = false;
        }

        ///// <summary>
        ///// Move the Gates(Gate0(Acceptance Gate), GateSC, GateLR, GateSM) according to the gate Id
        ///// </summary>
        ///// <param name="sorterGateId"> 0 -> all, 1 -> GateSC, 2 -> GateRL, 3-> Gate SM, 7 -> Acceptance Gate</param>
        ///// <param name="ResulStatustMsg">Execution Result</param>
        ///// <returns>True is all were correct. False = something failed</returns>      
        public bool MoveSorterGates(byte sorterGateId, ref string ResulStatustMsg)
        {
            return CVutSorterGates != null ? CVutSorterGates.MoveSorterGate(sorterGateId, ref ResulStatustMsg) : false;
        }

        /// <summary>
        /// Checks if the Door is open or not.
        /// </summary>
        /// <param name="ResulStatustMsg">Execution Result</param>
        /// <returns>True if the door is open, False = close</returns>
        public bool GetValidatorDoorOpenCloseStatus(ref string ResulStatustMsg)
        {
            return CVutDoor != null ? CVutDoor.CheckDoorStatus(ref ResulStatustMsg) : false;
        }

        /// <summary>
        /// Checks if the HMI Door is open or not. Use the protocol adaptation layer to send GetValue() to Vut
        /// </summary>
        /// <param name="ResulStatustMsg">Execution Result</param>
        /// <returns>True if the door is open, False = close</returns>
        public bool GetSorterDoorOpenCloseStatus(ref string ResulStatustMsg)
        {
            return CVutHmi != null ? CVutHmi.CheckDoorStatus(ref ResulStatustMsg) : false;         
        }

        /// <summary>
        /// Checks if the HMI Door is open or not. Use the protocol adaptation layer to send GetValue() to Vut
        /// </summary>
        /// <param name="ResulStatustMsg">Execution Result</param>
        /// <returns>Return the string represented by the pressed key</returns>
        public string GetPressedKey(ref string ResulStatustMsg)
        {
            return CVutHmi.GetPressedKey(ref ResulStatustMsg); 
        }
        /// <summary>
        /// Retreive the measured value of the Light Sensors
        /// </summary>
        /// <param name="ResulStatustMsg"> String with the Execution Result</param>
        /// <returns>True if there were no problems reading the values</returns>
        public byte[] GetLightSensorsStatus(ref string ResulStatustMsg)
        {
            return CVutSensors.GetAllLightSensorsStatus(ref ResulStatustMsg);
        }

        /// <summary>
        /// The Methods below retreive the measured value of the Light Sensors LS1  Ls2 and Ls3 separately
        /// </summary>
        /// <param name="ResulStatustMsg"> String with the Execution Result</param>
        /// <returns>The measured value of LS1s</returns>
        public byte GetLightSensorLs1Status(ref string ResulStatustMsg)
        {
            return CVutSensors.GetSensorLS1Status(ref ResulStatustMsg);
        }

        public byte GetLightSensorLs2Status(ref string ResulStatustMsg)
        {
            return CVutSensors.GetSensorLS2Status(ref ResulStatustMsg);
        }

        public byte GetLightSensorLs3Status(ref string ResulStatustMsg)
        {
            return CVutSensors.GetSensorLS3Status(ref ResulStatustMsg);
        }

        /// <summary>
        /// Retreive the measured value of the Coin Position Sensors
        /// </summary>
        /// <param name="ResulStatustMsg"> String with the Execution Result</param>
        /// <returns>True if there were no problems reading the values</returns>
        public byte[] GetCoinPositionSensorsStatus(ref string ResulStatustMsg)
        {
            return CVutSensors.GetAllCoinPositionSensorsStatus(ref ResulStatustMsg);
        }

        public byte GetCoinPositionSensorCP3sStatus(ref string ResulStatustMsg)
        {
            return CVutSensors.GetSensorCP3SStatus(ref ResulStatustMsg);
        }

        public byte GetCoinPositionSensorCP4rStatus(ref string ResulStatustMsg)
        {
            return CVutSensors.GetSensorCP4RStatus(ref ResulStatustMsg);
        }

        public byte GetCoinPositionSensorCP4lStatus(ref string ResulStatustMsg)
        {
            return CVutSensors.GetSensorCP4LStatus(ref ResulStatustMsg);
        }
        /// <summary>
        /// Retreive the measured value of the Coin Position Sensors
        /// </summary>
        /// <param name="ResulStatustMsg"> String with the Execution Result</param>
        /// <returns>True if there were no problems reading the values</returns>
        public byte[] GetMaterialSensorStatus(bool withPulse, ref string ResulStatustMsg)
        {
            return CVutSensors.GeAllMaterialSensorStatus(withPulse, ref ResulStatustMsg);
        }

        public bool HmiDisplayActionExecutor(int action, int bitMap, string stringToWrite, ref string ResulStatustMsg)
        {
            switch (action)
            {
                case 0: return CVutHmiDisplay.ClearScreen(ref ResulStatustMsg);
                case 1: return CVutHmiDisplay.WriteString(stringToWrite, ref ResulStatustMsg);
                case 2: return CVutHmiDisplay.WriteBitMap(bitMap, ref ResulStatustMsg);
                case 3: return CVutHmiDisplay.ShowMessage(stringToWrite, ref ResulStatustMsg);
                case 4: return CVutHmiDisplay.ShowAsciiCharacters(ref ResulStatustMsg);
                case 5: return CVutHmiDisplay.BackGrdLightOff(ref ResulStatustMsg);
                case 6: return CVutHmiDisplay.ShowAnimation(ref ResulStatustMsg);
            }
            return true;
        }
        //###############

        public byte[] PollVutAndReadInfo(ref string resulStatustMsg)
        {
            byte[] responseData = null;

            if (CX2ProtAdaptLayer.PollDevice(VALIDATOR_TYPE_ID, out responseData))
            {
                resulStatustMsg = Msgs.Info.RESULT_OK;
                return responseData;
            }
            return null;
        }
        public bool ResetVut(ref string resulStatustMsg)
        {
            if (CX2ProtAdaptLayer.ResetDevice(VALIDATOR_TYPE_ID))
            {
                resulStatustMsg = Msgs.Info.RESULT_OK;
                return true;
            }
            return false;
        }
        public byte[] PollHMIAndReadInfo(ref string resulStatustMsg)
        {
            return CVutHmi.PollHMI(ref resulStatustMsg);
        }

        public bool ResetHMI(ref string resulStatustMsg)
        {
            return CVutHmi.ResetHMI(ref resulStatustMsg); 
        }

        public byte[] GetMeasuredValues(ref string resulStatustMsg)
        {
            byte[] responseData = null;
            byte [] requestData = new byte[] {(byte)0x12};
            if (CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, requestData,  out responseData))
            {
                Log.Debug(this.GetType().Name, "Get value command successfuly executed"); 
                resulStatustMsg = Msgs.Info.RESULT_OK;
                return responseData;
            }
            resulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
            Log.Debug(this.GetType().Name, "Fail to execute GetValue Command. Response = " + resulStatustMsg); 
            return null;
        }

        //Read values from ProductID table 0xF0
        public byte[] GetProductionIdData()
        {
            return CVutTableOps.GetDataFromProductionIDTbl();
        }
        //WriteValuesTo ProductId table
        public bool WriteDataToProdIdTable(int fromPosition, byte[] dataToWrite)
        {
            return CVutTableOps.WriteDataToProductIdTbl(fromPosition, dataToWrite);
        }
        //Read values from table 82
        public byte[] GetFunctionSetupData()
        {
            return CVutTableOps.GetDataFromFunctionSetupTbl();
        }
        //WriteValuesTo ProductId table
        public bool WriteToFunctionSetupTable(int fromPosition, byte[] dataToWrite)
        {
            return CVutTableOps.WriteDataToFunctionSetupTbl(fromPosition, dataToWrite);
        }
        //Get identification data using identification command and fillup in id representation
        public VutIdentificationData  GetVutIdentificationData()
        {
            return CVutIdentificationData;
        }

        public TblProductId GetTblProductId()
        {
            return CTTblProductId;
        }
        public TblFunctionSetup GetTblFunctionSetup()
        {
            return CTblFunctionSetup;
        }
        public TblChannelSetupBlkA GetTblChannelSetupBlkA()
        {
            return CTblChannelSetupBlkA;
        }
        public TblChannelSetupBlkB GetTblChannelSetupBlkB()
        {
            return CTblChannelSetupBlkB;
        }
        public TblChannelIds GetTblChannelIds()
        {
            return CTblChannelIds;
        }
        public TblCoinLimits GetTblCoinLimits()
        {
            return CTblCoinLimits;
        }
        
    }
  
}
