﻿/**
 * Class: VutSorterGates
 * This is the representation of the Sorter Gates of the validator. This class control/move the 
 * Sorter Gates one by one.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Utils;

namespace C2ValidatorATS.Vut
{
    class VutSorterGates
    {
        private byte VALIDATOR_TYPE_ID = 1;
        private const byte TEST_GATES = 0x01;

        /// <summary>
        /// Move the Gates(Gate0, GateSC, GateLR, GateSM) according to the gate Id
        /// </summary>
        /// <param name="sorterGateId"> 0 -> all, 1 -> GateSC, 2 -> GateRL, 3-> Gate SM, 5 -> Gate HR 6 -> GateEC, 7 -> Acceptance Gate</param>
        /// <param name="ResulStatustMsg">Execution Result</param>
        /// <returns>True is all were correct. False = something failed</returns>
        public bool MoveSorterGate(byte sorterGateId, ref string ResulStatustMsg)
        {
            //See details of Commands in C2_Validator_PI.pdf
            byte[] requestCmdId = new byte[2];
            byte[] responseData = null;

            // Ping the validator just to check that there is contact
            if (!CX2ProtAdaptLayer.PingDevice(VALIDATOR_TYPE_ID))
            {
                ResulStatustMsg = Msgs.Error.NO_ABLE_TO_PING_THE_VALIDATOR;
                return false;
            }
            //prepare to send the execution command. 
            //CX2 spec: 0x01 = "Test Gates". Second byte = sorterID param
            requestCmdId[0] = TEST_GATES;
            requestCmdId[1] = sorterGateId;
            if (!CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, requestCmdId, out responseData))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_MOVE_SORTER_GATE + "Execution Response: " + responseData;
                return false;
            }
            return true;
        } 
    }
}
