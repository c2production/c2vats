﻿/**
 * Class: VutHmi
 * This is the representation of the door that in some case bare the HMI interface, display, keys, 
 * of the navegator. If the HMI is not present it cointains only information of the status,
 * open or close, of the door.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Interfaces;

namespace C2ValidatorATS.Vut
{
    class VutHmi
    {
        #region Private members
        private byte HMI_TYPE_ID = 4;
        private byte VALIDATOR_TYPE_ID = 1;
        private Leogger Log;
        const byte KEY_PRESSED = 0x30;   //xx11 xxxx 
        const byte DISPLAY_PRESENT = 0x80;
        #endregion

        #region Public Members
        public bool IsHmiDoorOpen { get; set; }
        public bool IsHmiDisplayPresent { get; set; }
        public bool IsHmiKeysOnlyPresent { get; set; }
       
        #endregion

        public VutHmi()
        {
            Log = Logger.instance;
            if (CommAssistant.Instance.ComPortIndex != 0xFF)
            {
                IsHmiDisplayPresent = IsHMIBoardPresent() && IsDisplayPresent(); //if trueDisplay and Keys

                IsHmiKeysOnlyPresent = IsHMIBoardPresent() && !IsDisplayPresent(); //if true, Only keys 
                if (IsHmiKeysOnlyPresent)
                    Log.Debug(this.GetType().Name, "The Validator has Only Keys and no Display");
            }
            else Log.Debug(this.GetType().Name, Msgs.Error.FAIL_TO_CREATE_OBJECT_REPRESENTATION_FOR_THIS_VUT + " ComportIndex = FF");
        }
      
        //Poll the hmi board. If there is response the board  exist and the hmi is blure type
        private bool IsHMIBoardPresent()
        {
            for (int i = 0; i < 3; i++)
            {
                if (CX2ProtAdaptLayer.PingDevice(HMI_TYPE_ID))
                {
                    Log.Debug(this.GetType().Name, "The Validator has HMI board");
                    return true;
                }

            }

            Log.Info(this.GetType().Name, "NO HMI board present");
            return false;
        }
        private Boolean IsDisplayPresent()
        {
            byte[] responseData = null;
            if (CX2ProtAdaptLayer.PollDevice(HMI_TYPE_ID, out responseData) && (responseData[0] & 0x80) == DISPLAY_PRESENT)
            {
                Log.Debug(this.GetType().Name, "Device Polled and Display and Keys present are present"); 
                return true;
            }
            return false;
        }
        //If the "ResultStatusMsg" = Ok and retult is True the door is Open or bad mounted(ambiental light is comming in)
        //If the "ResultStatusMsg" = Ok and retult is False the door is Closed
        public bool CheckDoorStatus(ref string ResulStatustMsg)
        {
            byte[] requestCmdId = new byte[1];
            byte[] responseData = null;

            // Ping the validator just to check that there is contact
            if (!CX2ProtAdaptLayer.PingDevice(VALIDATOR_TYPE_ID))
            {
                ResulStatustMsg = Msgs.Error.NO_ABLE_TO_PING_THE_VALIDATOR;
                return false;
            }

            // prepare to send the getValue() command:CX2 Spec: 0x13H "Diagnostic I"
            requestCmdId[0] = 0x13;
            //Send and check if the command was properly sent
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, requestCmdId, out responseData))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return false;
            }

            // Diganostic I brings 12 byte information data. Byte nr4, index 4 contain cp status "blocked" = 1, "ok" = 0
            if (responseData.Length != 12)
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_RECEIVE_CORRECT_RESPONSE_AFTER_SENDING_GET_VALUE;
                return false;
            }
            // LSB 0111 = CP3s CP4l CP4r get ambiental light which means that door is open or bad mounted
            if ((responseData[3] & 0x07) == 0x07)
            {
                ResulStatustMsg = Msgs.Info.RESULT_OK;
                return true;
            }
            //Door is closed. The ResultMsg indicate that the check of the CPs has been correctly done. The return true or false 
            //is to tell the status, "open" or "close" respectively.
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return false;
        }

        private string PresedKey(byte keyByte)
        {
            switch (keyByte)
            {
                case 0x01: return "A";
                case 0x10: return "B";
                case 0x02: return "C" ;
                case 0x04: return "D";
                case 0x20: return "E";
                case 0x08: return "F";
                case 0x40: return "MENU";
                default: return "";
            }
        }
        /// <summary>
        /// Analise if a key was pressed and return the pressed key
        /// </summary>
        /// <returns>Pressed key. Enum av type Keys</returns>
        public string GetPressedKey(ref string resulStatustMsg)
        {
            byte[] responseData = null;

            if (CX2ProtAdaptLayer.PollDevice(HMI_TYPE_ID, out responseData) && ((responseData[0] & 0x30) == KEY_PRESSED))
            {
                resulStatustMsg = Msgs.Info.RESULT_OK;
                return PresedKey(responseData[1]);
            }
            else if (responseData == null)
            {
                resulStatustMsg = Msgs.Error.FAIL_TO_POLL_DEVICE;
                return "";
            }

            resulStatustMsg = Msgs.Info.NO_KEY_PRESSED;
            return "";
        }

        public byte[] PollHMI(ref string resulStatustMsg)
        {
            byte[] responseData = null;
            if (CX2ProtAdaptLayer.PollDevice(HMI_TYPE_ID, out responseData))
            {
                resulStatustMsg = Msgs.Info.RESULT_OK;
                return responseData;
            }
            return new byte[] { };
        }

        public bool ResetHMI(ref string resulStatustMsg)
        {
           // byte[] responseData = null;
            if (CX2ProtAdaptLayer.ResetDevice(HMI_TYPE_ID))
            {
                resulStatustMsg = Msgs.Info.RESULT_OK;
                return true;
            }
            return false;
        }

    }
}
