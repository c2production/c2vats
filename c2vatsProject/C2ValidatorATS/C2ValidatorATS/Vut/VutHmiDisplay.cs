﻿/**
 * Class: VutHmiDisplay
 * This is the representation of the display in HMI interface, of the navegator. 
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Utils;

namespace C2ValidatorATS.Vut
{
    //See details of Commands in C2_Validator_PI.pdf
    class VutHmiDisplay
    {
        #region Private members
        private byte HMI_TYPE_ID = 4;
        private const byte WRITE_STRING = 0xD2;
        private const byte CLEAN_SCREEN = 0xD0;
        private const byte WRITE_BIT_MAP = 0xD3;
        private const byte SHOW_MSG = 0xD4;
        private const byte VIEW_CHAR_SET = 0xD8;
        private const byte BACKGRD_LIGHT_OFF = 0xD9;
        private const byte SHOW_ANIMATION = 0xDA;
        private const byte DISPLAY_LINE_LENGHT = 16;
        #endregion

    
        public bool WriteString( string stringToWrite, ref string ResulStatustMsg)
        {
            byte[] requestCmdParam = new byte[DISPLAY_LINE_LENGHT]; 
            byte[] responseData = null;
        
            for (int i = 2; i < DISPLAY_LINE_LENGHT; i++)
            {
                requestCmdParam[i] = (byte)45;  // - char
            }
            requestCmdParam[0] = WRITE_STRING;
            requestCmdParam[1] = 0x04;  //Row place. Y position
            for (int i = 0; i < stringToWrite.Count(); i++)
            {  //5 shift is for X position 
                requestCmdParam[i + 5] = (byte)stringToWrite.ElementAt(i);
            }
            //requestCmdParam[2 + stringToWrite.Count()] = (byte)32;
            
            if (!CX2ProtAdaptLayer.ExecFunction(HMI_TYPE_ID, requestCmdParam, out responseData))
            {
                ResulStatustMsg = this.GetType().Name + ": " + Msgs.Error.FAIL_TO_WRITE_STRING + "Execution Response data: " + responseData;
                return false;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return true;
        }

        public bool ClearScreen(ref string ResulStatustMsg)
        {
            byte[] requestCmdParam = new byte[1];
            requestCmdParam[0] = CLEAN_SCREEN;
            byte[] responseData = null;

            if (!CX2ProtAdaptLayer.ExecFunction(HMI_TYPE_ID, requestCmdParam, out responseData))
            {
                ResulStatustMsg = this.GetType().Name + ": " + Msgs.Error.FAIL_TO_CLEAN_THE_DISPLAY + "Execution Response data: " + responseData;
                return false;
            }           
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return true;
        }

        public bool WriteBitMap(int bitMap, ref string ResulStatustMsg)
        {
            byte[] requestCmdParam = new byte[4];
            requestCmdParam[0] = WRITE_BIT_MAP;
            requestCmdParam[1] = 7; //position X
            requestCmdParam[2] = 3; //position Y
            requestCmdParam[3] = (byte)bitMap;
            byte[] responseData = null;

            if (!CX2ProtAdaptLayer.ExecFunction(HMI_TYPE_ID, requestCmdParam, out responseData))
            {
                ResulStatustMsg = this.GetType().Name + ": " + Msgs.Error.FAIL_TO_SHOW_BIT_MAP + "Execution Response data: " + responseData;
                return false;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return true;
        }

        public bool ShowMessage(string stringMsg, ref string ResulStatustMsg)
        {
            byte[] requestCmdParam = new byte[50];
            requestCmdParam[0] = SHOW_MSG;
            requestCmdParam[1] = 0x02;
            byte[] responseData = null;
            for (int i = 0; i < stringMsg.Count(); i++)
            {
                requestCmdParam[i + 18] = (byte)stringMsg.ElementAt(i);
            }
            if (!CX2ProtAdaptLayer.ExecFunction(HMI_TYPE_ID, requestCmdParam, out responseData))
            {
                ResulStatustMsg = this.GetType().Name + ": " + Msgs.Error.FAIL_TO_SHOW_MSG + "Execution Response data: " + responseData;
                return false;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return true;
        }
        
        /// <summary>
        /// Displays ASCII characters for about 5 seconds--> Cmd 0xD8, ASCII set  från 0..127 -->Cmd 0x00
        /// </summary>
        /// <param name="ResulStatustMsg"></param>
        /// <returns>Result after command execution</returns>
        public bool ShowAsciiCharacters(ref string ResulStatustMsg)
        {
            byte[] requestCmdParam = new byte[2];
            requestCmdParam[0] = VIEW_CHAR_SET;     
            requestCmdParam[1] = 0x00;    
            byte[] responseData = null;

            if (!CX2ProtAdaptLayer.ExecFunction(HMI_TYPE_ID, requestCmdParam, out responseData))
            {
                ResulStatustMsg = this.GetType().Name + ": " + Msgs.Error.FAIL_TO_SHOW_ASCII_CHARS + "Execution Response data: " + responseData;
                return false;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return true;
        }

        public bool ShowAnimation(ref string ResulStatustMsg)
        {
            byte[] requestCmdParam = new byte[2];
            requestCmdParam[0] = SHOW_ANIMATION;     
            requestCmdParam[1] = 0x01;    
            byte[] responseData = null;
 
            if (!CX2ProtAdaptLayer.ExecFunction(HMI_TYPE_ID, requestCmdParam, out responseData))
            {
                ResulStatustMsg = this.GetType().Name + ": " + Msgs.Error.FAIL_TO_SHOW_ANIMATION + "Execution Response data: " + responseData;
                return false;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return true;
        }

        public bool BackGrdLightOff(ref string ResulStatustMsg)
        {
            byte[] requestCmdParam = new byte[1];
            requestCmdParam[0] = BACKGRD_LIGHT_OFF;
            byte[] responseData = null;
            if (!CX2ProtAdaptLayer.ExecFunction(HMI_TYPE_ID, requestCmdParam, out responseData))
            {
                ResulStatustMsg = this.GetType().Name + ": " + Msgs.Error.FAIL_TURN_OFF_BKGR_LIGHT + "Execution Response data: " + responseData;
                return false;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return true;
        }
    }
}
