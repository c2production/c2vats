﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2ValidatorATS.Vut.VutDBlocksInfo
{
    public class TblCoinLimits
    {
        private int COIN_LIMITS_TABLE_MAX_NRS = 24;
        private VutTableOps CVutTableOps;
        private byte[] coinLimitsTblContent;
        private byte[][] allCoinLimits = new byte[24][];
        public bool isTblEmpty { get; set; }
        public int nrOfTableFetched{get; set; }

        //public List<CoinIdChannels> CCoinIdChannels;
       

        public TblCoinLimits(VutTableOps tableOps)
        {
            CVutTableOps = tableOps;
           // CCoinIdChannels = new List<CoinIdChannels> { };
            isTblEmpty = true;
            
        }

        public bool RetreiveAllData()
        {
            nrOfTableFetched = 0;
            for(int i = 0; i < COIN_LIMITS_TABLE_MAX_NRS; i++)
            {

                coinLimitsTblContent = CVutTableOps.GetDataFromCoinLimits(i + 1);
                if (coinLimitsTblContent != null && (coinLimitsTblContent.Count() == 48))
                {
                   allCoinLimits[i] = coinLimitsTblContent;
                   nrOfTableFetched++;
                }
            }
            if (allCoinLimits == null)
                return false;
            return true;
        }

        public byte[] GetTable(int tblNr)   //Real Table Nr 1, 2....
        {
            return allCoinLimits[tblNr - 1];  //index to data
        }
    }
}
