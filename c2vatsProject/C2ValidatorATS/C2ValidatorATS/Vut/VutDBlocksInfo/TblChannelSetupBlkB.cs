﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2ValidatorATS.Vut.VutDBlocksInfo
{
    public class TblChannelSetupBlkB
    {
         private VutTableOps CVutTableOps;
        private byte[] ChannelSetupTableContent;
        public bool isTblEmpty { get; set; }
        public List<byte> channels;


        public TblChannelSetupBlkB(VutTableOps tableOps)
        {
            CVutTableOps = tableOps;
            ChannelSetupTableContent = null;
            isTblEmpty = true;
            channels = new List<byte>();
        }

        public bool RetreiveAllData()
        {
            ChannelSetupTableContent = CVutTableOps.GetDataFromChannelSetupTbl(0x8A);

            if (ChannelSetupTableContent != null && (ChannelSetupTableContent.Count() == 28))
            {
                for (int i = 0; i < 24; i++)
                {
                    channels.Add( ChannelSetupTableContent[i]);
                }
                isTblEmpty = false;
                return true; 
            }
            isTblEmpty = true;
            return false;
        }

    }
}
