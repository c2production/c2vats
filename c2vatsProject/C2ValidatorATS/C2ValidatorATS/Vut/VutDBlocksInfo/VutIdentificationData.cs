﻿/**
 * Class: VutIdentificationData
 * This class retreive and save all data related to vut identification
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Utils;

namespace C2ValidatorATS.Vut.VutDBlocksInfo
{
    public class VutIdentificationData
    {
        public bool IsIdDataEmpty { get; set; }
        public string Id { get; set; }
        public string Manufacturer { get; set; }
        public string ManufacturerCont { get; set; }
        public string SerialNumber { get; set; }
        public string ProductionDate { get; set; }
        public string FirmwareVersion { get; set; }
        public string DataBlock { get; set; }
   
        const int MAX_DATA = 6;
        const byte VUT_ID = 1;

        public VutIdentificationData()
        {
            IsIdDataEmpty = true;
        
        }
        
        public bool RetreiveAllData()
        {
            byte[] rspData = null;
            for (byte infoType = 0; infoType <= MAX_DATA; infoType++)
            {
                CX2ProtAdaptLayer.GetVutIdentificationInfo(VUT_ID, infoType, out rspData );
                if (rspData == null) return false;
                switch(infoType)
                {
                    case 0:
                        {
                            Id = BitConverter.ToString(rspData).Replace("-", "");
                            break;
                        }
                    case 1:
                        {
                            Manufacturer = System.Text.Encoding.ASCII.GetString(rspData);
                            break;
                        }
                    case 2:
                        {
                            ManufacturerCont = System.Text.Encoding.ASCII.GetString(rspData);
                            break;
                        }
                    case 3:
                        {
                            SerialNumber = BitConverter.ToString(rspData).Replace("-", "").Substring(0, 10);
                            SerialNumber += "-" + BitConverter.ToString(rspData).Replace("-", "").Substring(10, 4);
                            SerialNumber += "-" + BitConverter.ToString(rspData).Replace("-", "").Substring(14, 4);
                            break;
                        }
                    case 4:
                        {
                            ProductionDate = BitConverter.ToString(rspData).Replace("-", "");
                            ProductionDate = ProductionDate.Substring(0, 2) + "-" + ProductionDate.Substring(2, 2) + "-20" + ProductionDate.Substring(4, 2);
                            break;
                        }
                    case 5:
                        {
                            FirmwareVersion = BitConverter.ToString(rspData).Replace("-", "");
                            FirmwareVersion = FirmwareVersion.Substring(1,3) + "." + FirmwareVersion.Substring(4,2) + "." + FirmwareVersion.Substring(6,2);
                            break;
                        }
                    case 6:
                        {
                            DataBlock = System.Text.Encoding.ASCII.GetString(rspData);
                            break;
                        }
                }
            }
            IsIdDataEmpty = false;
            return true;
        }

    }
}
