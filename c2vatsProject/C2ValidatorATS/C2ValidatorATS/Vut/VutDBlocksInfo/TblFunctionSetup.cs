﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Utils;

namespace C2ValidatorATS.Vut.VutDBlocksInfo
{
    public class TblFunctionSetup
    {
        private VutTableOps CVutTableOps;
        private byte[] FunctionSetupTableContent;
        private byte[] MMVdata;
        public bool isTblEmpty { get; set; }
        public string Formula { get; set; }
        public string CurrencyCode { get; set; }
        public string DecimalPlaces { get; set; }
        public string ResulStatustMsg;
        //Extra info
        public string MMV { get; set; }
        public string MMVmin { get; set; }
        public string MMVmax { get; set; }

        public TblFunctionSetup(VutTableOps tableOps)
        {
            CVutTableOps = tableOps;
            FunctionSetupTableContent = null;
            MMVdata = null;
            isTblEmpty = true;
        }

        public bool RetreiveAllData()
        {
            FunctionSetupTableContent = CVutTableOps.GetDataFromFunctionSetupTbl();
            MMVdata = CVutTableOps.GetMMVRelatedInfo(ref ResulStatustMsg);
            //Prepare data if needed
            if (FunctionSetupTableContent != null && (FunctionSetupTableContent.Count() == 32) && ResulStatustMsg == Msgs.Info.RESULT_OK)
            {
                DecimalPlaces = FunctionSetupTableContent[26].ToString();
                CurrencyCode = FunctionSetupTableContent[27].ToString() + FunctionSetupTableContent[28].ToString();
                Formula = FunctionSetupTableContent[29].ToString();
                MMV = MMVdata[0].ToString();
                MMVmin = MMVdata[1].ToString();
                MMVmax = MMVdata[2].ToString();
                isTblEmpty = false;
                return true;  
            }
            else
            {
                isTblEmpty = true;
                return false;
            }
        }
    }
}
