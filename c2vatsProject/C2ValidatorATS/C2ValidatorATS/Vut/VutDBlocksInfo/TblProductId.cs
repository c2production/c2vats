﻿/**
 * Class: TblProductId
 * This is the representation of the table 80.
 * This file will be only used by RD operations executed in Garage Mode 
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2ValidatorATS.Vut.VutDBlocksInfo
{
    public class TblProductId
    {
        private VutTableOps CVutTableOps;
        private byte[] prodIdTableContent;
        private const byte RIM_SENSOR_MASK = 0x01;
        public bool isTblEmpty { get; set; }
        public string RIMSensor { get; set; }
        public string PCBId { get; set; }

        public TblProductId(VutTableOps tableOps)
        {
            CVutTableOps = tableOps;
            prodIdTableContent = null;
            isTblEmpty = true;
        }

        public bool RetreiveAllData()
        {
            prodIdTableContent = CVutTableOps.GetDataFromProductionIDTbl();
            if (prodIdTableContent != null && (prodIdTableContent.Count() == 27))
            {
                if (0x01 == (prodIdTableContent[22] & RIM_SENSOR_MASK))
                {
                    RIMSensor = "Yes";
                }
                else RIMSensor = "No";

                PCBId = prodIdTableContent[19].ToString();
              
            }
            else
            {
                isTblEmpty = false;
                return false;
            }

            return true;
        }
    }
}
