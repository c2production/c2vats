﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2ValidatorATS.Vut.VutDBlocksInfo
{
    public class TblChannelIds
    {
        private VutTableOps CVutTableOps;
        private byte[] channelsIdTblContent;
        public bool isTblEmpty { get; set; }
        public List<CoinIdChannels> CCoinIdChannels;
        private byte PRECISION_MASK = 0x07;
        private byte REVISION_MASK = 0xF8;
        
        public TblChannelIds(VutTableOps tableOps)
        {
            CVutTableOps = tableOps;
            CCoinIdChannels = new List<CoinIdChannels> { };
            isTblEmpty = true;
            
        }

        public bool RetreiveAllData()
        {
            channelsIdTblContent = CVutTableOps.GetDataFromChannelIdsTbl();

            if (channelsIdTblContent != null && (channelsIdTblContent.Count() == 196))
            {
                for (int i = 0; i < 24; i++)
                {
                    CoinIdChannels channelIds = new CoinIdChannels();
                    channelIds.channelId = (byte)(i + 1);
                    channelIds.iSo = GetIsoToString(channelsIdTblContent[i * 8 ], channelsIdTblContent[i * 8 + 1]);
                    channelIds.mantise = channelsIdTblContent[i*8 + 2];
                    channelIds.exponent = GetExponent(channelsIdTblContent[i*8 + 3]);  //0 a 9 = 0-9; A a Z= -6 -1      
                    channelIds.coinID = GetCoinId(channelsIdTblContent[i*8 + 4], channelsIdTblContent[i*8 + 5]);
                    channelIds.precision = (byte)(channelsIdTblContent[i * 8 + 6] & PRECISION_MASK);
                    channelIds.revision = GetRevisionString((byte)((channelsIdTblContent[i * 8 + 6] & REVISION_MASK) >> 3));
                    channelIds.thikness = 0; //channelsIdTblContent[i * 8 + 7];  //Need to be correctly converted ### TODO ###
                    CCoinIdChannels.Add(channelIds);
                }
                isTblEmpty = false;
                return true; 
            }
            isTblEmpty = true;
            return false;
        }

        private string GetIsoToString(byte byteOne, byte byteTwo)
        {
    
            byte[] iSoBytes = new byte[] { (byte)(((byteOne & 0x7C) >> 2) + 0x40), (byte)((((byteOne & 0x03) << 3) | ((byteTwo & 0xE0) >> 5)) + 0x40), (byte)((byteTwo & 0x1F) + 0x40) };
            return Encoding.ASCII.GetString(iSoBytes);
           
        }
        private string GetExponent(byte exponent)
        {
            switch(exponent)
            {
                case 0x0A: return "-6";
                case 0x0B: return "-5";
                case 0x0C: return "-4";
                case 0x0D: return "-3";
                case 0x0E: return "-2";
                case 0x0F: return "-1";
                default: return exponent.ToString();

            }
        }
        private string GetRevisionString(byte rev)
        {
            switch (rev)
            {
                case 1: return "A";
                case 2: return "B";
                case 3: return "C";
                case 4: return "D";
                case 5: return "E";
                case 6: return "F";
                case 7: return "G";
            }
            return "Z"; //????
        }
        private ushort GetCoinId(byte msb, byte lsb)
        {
            //            *cid = ((unsigned char)data.at(8 * ch + 4)) * 256 ;
            //*cid += (unsigned char)data.at(8 * ch + 5);
            return (ushort)(msb * 256 + lsb);
        }
    }

    public class CoinIdChannels
    {
       public byte channelId { get; set; }
       public string iSo { get; set; }
       public byte mantise { get; set; }
       public string exponent { get; set; }
       public int coinID { get; set; }
       public byte precision { get; set; }
       public string revision { get; set; }
       public byte thikness { get; set; }

    }
}
