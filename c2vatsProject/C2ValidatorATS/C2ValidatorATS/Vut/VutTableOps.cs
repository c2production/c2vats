﻿/**
 * Class: VutTables
 * This is the representation of the tables inside the validator,
 * This file ather classes with operations to specific tables, ProductionIDTable for example 
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Interfaces;


namespace C2ValidatorATS.Vut
{
    public class VutTableOps

    {
        public const byte DEVICE_ID = 1;
        private bool operationResultOK;
        //public Leogger Log = Logger.instance;
        private ProductionIDTableOps CProductionIDTableOps;
        private FunctionSetupTableOps CFunctionSetupTableOps;
        private ChannelSetupTableOps CChannelSetupTableOps;
        private ChannelIdsTableOps CChannelIdsTableOps;
        private CoinLimitsTableOps CCoinLimitsTableOps;

        public VutTableOps(ValidatorUnderTest vut)
        {

            CProductionIDTableOps = new ProductionIDTableOps();
            CFunctionSetupTableOps = new FunctionSetupTableOps();
            CChannelSetupTableOps = new ChannelSetupTableOps();
            CChannelIdsTableOps = new ChannelIdsTableOps();
            CCoinLimitsTableOps = new CoinLimitsTableOps(vut);
        }

        public byte[] GetDataFromProductionIDTbl()
        {
            byte[] tableDataBuffer = new byte[128];

            tableDataBuffer = CProductionIDTableOps.GetData(ref operationResultOK);
            if (operationResultOK)
            {
                Logger.instance.Info(this.GetType().Name, " Getting Data From ProductId table - F0 successfully done");
                return (byte[])tableDataBuffer.Skip(2).ToArray(); //Send data without commands
            }
            Logger.instance.Error(this.GetType().Name, " Getting Data From ProductId table - F0 FAIL");
            return null;
        }

        public bool WriteDataToProductIdTbl(int fromPosition, byte[] dataToTable)
        {
            if (CProductionIDTableOps.WriteData(fromPosition, dataToTable))
            {
                Logger.instance.Info(this.GetType().Name, " Data successfuly Written to Production ID table");
                return true;
            }
            Logger.instance.Error(this.GetType().Name, " Writing Data to ProductId table - F0 FAIL");
            return false;
        }

        public byte[] GetDataFromFunctionSetupTbl() //##########
        {
            byte[] tableDataBuffer = new byte[128];

            tableDataBuffer = CFunctionSetupTableOps.GetData(ref operationResultOK);
            if (operationResultOK)
            {
                Logger.instance.Info(this.GetType().Name, " Getting Data From Function Setup table - 82 successfully done");
                return tableDataBuffer;
            }
            Logger.instance.Error(this.GetType().Name, " Getting Data From Function Setup table - 82 FAIL");
            return null;
        }

        public bool WriteDataToFunctionSetupTbl(int fromBytePosition, byte[] dataToTable)
        {
            if (CFunctionSetupTableOps.WriteData(fromBytePosition, dataToTable))
            {
                Logger.instance.Info(this.GetType().Name, " Data successfuly Written to Function Setup table");
                return true;
            }
            Logger.instance.Error(this.GetType().Name, " Writing Data to Function Setup table - F0 FAIL");
            return false;
        }

        public byte[] GetMMVRelatedInfo(ref string ResulStatustMsg)
        {
            byte[] mmvValueRsp;
            byte[] requestData = new byte[] { 0x1A };
            if (!CX2ProtAdaptLayer.GetValue(DEVICE_ID, requestData, out mmvValueRsp))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return new byte[] { 0 }; //Return emty byte
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return mmvValueRsp;
        }
        //Table 81(blockA) and table 8A(block B)
        public byte[] GetDataFromChannelSetupTbl(byte tableNr) 
        {
            byte[] tableDataBuffer = new byte[128];

            tableDataBuffer = CChannelSetupTableOps.GetData(ref operationResultOK, tableNr);
            if (operationResultOK)
            {
                Logger.instance.Info(this.GetType().Name, " Getting Data From Channel Setup table - 81 successfully done");
                return tableDataBuffer;
            }
            Logger.instance.Error(this.GetType().Name, " Getting Data From Channel Setup table - 81 FAIL");
            return null;
        }

        public bool WriteDataToChannelSetupTbl(int fromBytePosition, byte[] dataToTable)
        {
            if (CFunctionSetupTableOps.WriteData(fromBytePosition, dataToTable))
            {
                Logger.instance.Info(this.GetType().Name, " Data successfuly Written to Channel Setup table");
                return true;
            }
            Logger.instance.Error(this.GetType().Name, " Writing Data to Channel Setup table - F0 FAIL");
            return false;
        }

        //Table 80
        public byte[] GetDataFromChannelIdsTbl()
        {
            byte[] tableDataBuffer = new byte[128];

            tableDataBuffer = CChannelIdsTableOps.GetData(ref operationResultOK);
            if (operationResultOK)
            {
                Logger.instance.Info(this.GetType().Name, " Getting Data From Channel Ids table - 80 successfully done");
                return tableDataBuffer;
            }
            Logger.instance.Error(this.GetType().Name, " Getting Data From Channel Ids table - 80 FAIL");
            return null;
        }
        //Table 01...18 CoinDataTable
        public byte[] GetDataFromCoinLimits(int tblNr)
        {
            byte[] tableDataBuffer = new byte[48];  //24 upper and 24 lower

            tableDataBuffer = CCoinLimitsTableOps.GetData(ref operationResultOK, tblNr);
            if (operationResultOK)
            {
                Logger.instance.Info(this.GetType().Name, " Getting Data From CoinLimits table " + tblNr + " successfully done");
                return tableDataBuffer;
            }
            Logger.instance.Error(this.GetType().Name, " Getting Data From CoinLimits table " + tblNr + " FAIL");
            return null;
        }
    }


    //####### ALL classes  ######3
    //Table F0  #########################
    class ProductionIDTableOps : TableOperations
    {
        public byte[] GetData(ref bool operationResult)
        {
            byte[] tableDataBuffer = new byte[128];
            byte[] requestData = new byte[2] { 0xF0, 0x00 };
            if (!CX2ProtAdaptLayer.ReadTable(VALIDATOR_TYPE_ID, requestData, out tableDataBuffer))
            {
                operationResult = false;
                return tableDataBuffer;
            }
            if (tableDataBuffer.Count() == 2 + PRODUCTION_ID_TBL_SIZE)
                operationResult = true;
            else operationResult = false;
            return tableDataBuffer;
        }

        public bool WriteData(int fromBytePosition, byte[] dataToTable)
        {
            byte[] tableData;
            byte[] responseData;
           // bool operationResult;

            if (!ChangeTableAccessLevelToAll())
            {
                Log.Error(this.GetType().Name, " FAIL to Change access Level");
                return false;
            }

            if (dataToTable.Count() > PRODUCTION_ID_TBL_SIZE)
                return false;
            tableData = GetData(ref operationResultOK);
            if (!operationResultOK)
            {
                Log.Error(this.GetType().Name, " FAIL to retreive Data from table Production ID");
                return false;
            }
            //Set the new data into table. " first bytes are the command send, i.e. tableNr and 00
            for (int i = 0; i < dataToTable.Count(); i++)
            {
                tableData[i + 2 + fromBytePosition] = dataToTable[i];
            }

            //Delete existing table....0x02 Executecomand for prodid table
            if (!DeleteTable(0x02))
            {
                Log.Error(this.GetType().Name, " FAIL to delete Production ID table");
                return false;
            }
            System.Threading.Thread.Sleep(500);

            if (CX2ProtAdaptLayer.WriteTable(VALIDATOR_TYPE_ID, tableData, out responseData))
            {
                Log.Info(this.GetType().Name, " New data successfuly Written to Production ID table");
                return true;
            }
            Log.Error(this.GetType().Name, " FAIL to write new datat to Production ID table. Response " );//+ responseData[0]);
            return false;
        }
    }
           
    //Table 82   ###############################
    class FunctionSetupTableOps : TableOperations
    {
        public byte[] GetData(ref bool operationResult)
        {
            byte[] tableDataBuffer = new byte[128];
            List<byte> returnDataBuffer = new List<byte>{};
            byte[] requestData = new byte[2];
 
            for (int i = 0; i < 2; i++) //FUNCTION_SETUP_TBL_SIZE / DATA_BLOCK_SIZE; i++)
            {
                requestData[0] = 0x82;
                requestData[1] = (byte)i;
                CX2ProtAdaptLayer.ReadTable(VALIDATOR_TYPE_ID, requestData, out tableDataBuffer);
                if (tableDataBuffer != null && (tableDataBuffer.Count() > 2))
                {
                    for (int j = 2; j < tableDataBuffer.Count(); j++)   //[i * DATA_BLOCK_SIZE + j]
                    {

                        returnDataBuffer.Add(tableDataBuffer[j]);
                        operationResult = true;
                    }
                }
                else operationResult = false;
            }
            return returnDataBuffer.ToArray();
        }

        public bool WriteData(int fromPosition, byte[] dataToTable)
        {
            List<byte> tableDataReq = new List<byte> { };
            byte[] tableData = new byte[]{};
            byte[] responseData;
            byte blockIndex = 0;
            if (!ChangeTableAccessLevelToAll())
            {
                Log.Error(this.GetType().Name, " FAIL to Change access Level");
                return false;
            }
            tableData = GetData(ref operationResultOK);
           
            if (!operationResultOK)
            {
                Log.Error(this.GetType().Name, " FAIL to retreive Data from table Function Setup, 82");
                return false;
            }
            //Set the new data we want to copy into tabledata
            for (int i = 0; i < dataToTable.Count(); i++)
            {
                tableData[i + fromPosition] = dataToTable[i];
            }
            AddCheckSumToData(tableData.Count(), ref tableData);
            System.Threading.Thread.Sleep(100);
            PrepareForUpdate();

            tableDataReq.Add(0x82);
            tableDataReq.Add(blockIndex);   //El otro elemnt of the write command 
            //Aui copio de 28 en 28 bytes o menos si el lenght es menos
            for (int j = 0; j < tableData.Count(); j++)
            {                
                tableDataReq.Add(tableData[j]);
                if ( j > 0 && ((j % (DATA_BLOCK_SIZE - 1) == 0) || (j == tableData.Count() - 1)))
                {
                    System.Threading.Thread.Sleep(200);
                    if (!CX2ProtAdaptLayer.WriteTable(VALIDATOR_TYPE_ID, tableDataReq.ToArray(), out responseData))
                    {
                        Log.Error(this.GetType().Name, " FAIL to write new datat to Function Setup-82- table. Response " + ((responseData != null) ? responseData[0].ToString(): "null"));
                        return false;
                    }
                    Log.Info(this.GetType().Name, " New data successfuly Written to Function Setup-82- table. Response = " + responseData);
                    tableDataReq.Clear();

                    tableDataReq.Add(0x82);
                    tableDataReq.Add(blockIndex +=1);   //El otro elemnt of the write command 

                }
            }
            
            if (IsDeviceRedyToBeFlashed())
            {
                if (FlasWrittenTable())
                {
                    Log.Info(this.GetType().Name, " Written table Successfully Flashed");
                    return true;
                }
                else
                {
                    Log.Error(this.GetType().Name, " Written Function Setup-82- table Fail to be Flashed");
                    return false;
                }
            } 
            Log.Error(this.GetType().Name, " FAIL to write new datat to Function Setup-82- table. Response ");
            return false;
        }

    }

    //Table 81   ###############################
    class ChannelSetupTableOps : TableOperations
    {
        public byte[] GetData(ref bool operationResult, byte tableNr)
        {
            byte[] tableDataBuffer = new byte[128];
            List<byte> returnDataBuffer = new List<byte> { };
            byte[] requestData = new byte[2];


            requestData[0] = tableNr; //0x81;
            requestData[1] = (byte)0;
            CX2ProtAdaptLayer.ReadTable(VALIDATOR_TYPE_ID, requestData, out tableDataBuffer);
            if (tableDataBuffer != null && (tableDataBuffer.Count() > 2))
            {
                for (int j = 2; j < tableDataBuffer.Count(); j++)   //[i * DATA_BLOCK_SIZE + j]
                {

                    returnDataBuffer.Add(tableDataBuffer[j]);
                    operationResult = true;
                }
            }
            else operationResult = false;
            
            return returnDataBuffer.ToArray();
        }

        public bool WriteData(int fromPosition, byte[] dataToTable, byte tableNr)
        {
            List<byte> tableDataReq = new List<byte> { };
            byte[] tableData = new byte[] { };
            byte[] responseData;
            byte blockIndex = 0;
            if (!ChangeTableAccessLevelToAll())
            {
                Log.Error(this.GetType().Name, " FAIL to Change access Level");
                return false;
            }
            tableData = GetData(ref operationResultOK, tableNr);

            if (!operationResultOK)
            {
                Log.Error(this.GetType().Name, " FAIL to retreive Data from table Function Setup, 82");
                return false;
            }
            //Set the new data we want to copy into tabledata
            for (int i = 0; i < dataToTable.Count(); i++)
            {
                tableData[i + fromPosition] = dataToTable[i];
            }
            AddCheckSumToData(tableData.Count(), ref tableData);
            System.Threading.Thread.Sleep(100);
            PrepareForUpdate();

            tableDataReq.Add(0x82);
            tableDataReq.Add(blockIndex);   //El otro elemnt of the write command 
            //Aui copio de 28 en 28 bytes o menos si el lenght es menos
            for (int j = 0; j < tableData.Count(); j++)
            {
                tableDataReq.Add(tableData[j]);
                if (j > 0 && ((j % (DATA_BLOCK_SIZE - 1) == 0) || (j == tableData.Count() - 1)))
                {
                    System.Threading.Thread.Sleep(200);
                    if (!CX2ProtAdaptLayer.WriteTable(VALIDATOR_TYPE_ID, tableDataReq.ToArray(), out responseData))
                    {
                        Log.Error(this.GetType().Name, " FAIL to write new datat to Function Setup-82- table. Response " + ((responseData != null) ? responseData[0].ToString() : "null"));
                        return false;
                    }
                    Log.Info(this.GetType().Name, " New data successfuly Written to Function Setup-82- table. Response = " + responseData);
                    tableDataReq.Clear();

                    tableDataReq.Add(0x82);
                    tableDataReq.Add(blockIndex += 1);   //El otro elemnt of the write command 

                }
            }

            if (IsDeviceRedyToBeFlashed())
            {
                if (FlasWrittenTable())
                {
                    Log.Info(this.GetType().Name, " Written table Successfully Flashed");
                    return true;
                }
                else
                {
                    Log.Error(this.GetType().Name, " Written Function Setup-82- table Fail to be Flashed");
                    return false;
                }
            }
            Log.Error(this.GetType().Name, " FAIL to write new datat to Function Setup-82- table. Response ");
            return false;
        }

    }

       //Table 01 to 18 Coin Datachannel tbl Limits  ###############################
    //class CoinLimitsTableOps : TableOperations
    //{    
    //    ValidatorUnderTest CVut;
    //    public CoinLimitsTableOps(ValidatorUnderTest vut)
    //    {
    //        CVut = vut;
    //    }
    //    public byte[] GetData(ref bool operationResult, byte tableNr)
    //    {
    //        byte[] tableDataBuffer = new byte[128];
    //        List<byte> returnDataBuffer = new List<byte> { };
    //        byte[] requestData = new byte[2];

    //        requestData[0] = tableNr; 
    //        requestData[1] = (byte)0;
    //        CX2ProtAdaptLayer.ReadTable(VALIDATOR_TYPE_ID, requestData, out tableDataBuffer);
    //        if (tableDataBuffer != null && (tableDataBuffer.Count() > 2))
    //        {
    //            for (int j = 2; j < tableDataBuffer.Count(); j++)   //[i * DATA_BLOCK_SIZE + j]
    //            {

    //                returnDataBuffer.Add(tableDataBuffer[j]);
    //                operationResult = true;
    //            }
    //        }
    //        else operationResult = false;

    //        if (operationResult)
    //        {

    //        }
    //        return returnDataBuffer.ToArray();
    //    }

    //}

   
    //Table 80  Channel Ids  ##############
    class ChannelIdsTableOps: TableOperations
    {
        public byte[] GetData(ref bool operationResult)
        {
            byte[] tableDataBuffer = new byte[CHANNEL_IDS_TBL_SIZE];
            List<byte> returnDataBuffer = new List<byte> { };
            byte[] requestData = new byte[2];
           
           
            for (int i = 0; i < CHANNEL_IDS_TBL_SIZE / DATA_BLOCK_SIZE; i++)//Si esta vacia checkear como saberlo.
            {
                requestData[0] = CHANNEL_IDS_TBL_ID; //0x80;
                requestData[1] = (byte)i;
                CX2ProtAdaptLayer.ReadTable(VALIDATOR_TYPE_ID, requestData, out tableDataBuffer);

                if (tableDataBuffer != null && (tableDataBuffer.Count() > 2))
                {
                    for (int j = 2; j < tableDataBuffer.Count(); j++) //Emppiezo en 2 para evitar el data de comando
                    {

                        returnDataBuffer.Add(tableDataBuffer[j]);
                        operationResult = true;
                    }
                }
                else operationResult = false;
            }
            return returnDataBuffer.ToArray();

        }
    }
   
 //Table 01....18 Coin Limits Coin Data Tblae  ##############
    class CoinLimitsTableOps: TableOperations
    {
        ValidatorUnderTest CVut;
        SecurityManager CSecMngr = null;
        int UPPER_LOWER_LIMITS_SIZE = 48;
        public CoinLimitsTableOps(ValidatorUnderTest vut)
        {
            CVut = vut;
           
        }
        public byte[] GetData(ref bool operationResult, int tblNr)
        {
            byte[] tableDataBuffer = new byte[COIN_LIMITS_TBL_SIZE];
            byte[] requestData = new byte[2];
            byte[] fetchedLimits = new byte[UPPER_LOWER_LIMITS_SIZE];
            int limitNrsIdx = 0;

            if (CSecMngr == null && CVut.GetProductionIdData() != null)
                CSecMngr = new SecurityManager((byte[])CVut.GetProductionIdData()); //Prepare the key for decoding

            for (int i = 0; i < 2; i++) //Serian dos blockes.
            {
                requestData[0] = (byte)tblNr;
                requestData[1] = (byte)i;

                CX2ProtAdaptLayer.ReadTable(VALIDATOR_TYPE_ID, requestData, out tableDataBuffer);

                 if (tableDataBuffer != null && (tableDataBuffer.Count() > 2))
                {
                    for (int j = 2; j < tableDataBuffer.Count(); j++) //Emppiezo en 2 para evitar el data de comando
                    {
                        if (limitNrsIdx >= UPPER_LOWER_LIMITS_SIZE)
                            break;
                        fetchedLimits[limitNrsIdx++] = tableDataBuffer[j];    
                    }
                    operationResult = true;
                }
                else operationResult = false;
            }
            if (operationResult)
            {
                CSecMngr.Decrypt(ref fetchedLimits, 0); //the second param is the offset i.e. from wherea i want to decrypt/encrypt
            }
            return fetchedLimits;
        }
    }
    
    //    ProductionIdTableData = GetProductionIdData();
                    //Log.Info(this.GetType().Name, "Product Table AFTER: " + ProductionIdTableData[2] + " " + ProductionIdTableData[3] + " " + ProductionIdTableData[4] + " " + ProductionIdTableData[5] + " " + ProductionIdTableData[6] + " " + ProductionIdTableData[7] + " " + ProductionIdTableData[8]
                    // + " " + ProductionIdTableData[9] + " " + ProductionIdTableData[10] + " " + ProductionIdTableData[11] + " " + ProductionIdTableData[12] + " " + ProductionIdTableData[13]
                    //  + " " + ProductionIdTableData[14] + " " + ProductionIdTableData[15] + " " + ProductionIdTableData[16] + " " + ProductionIdTableData[17] + " " + ProductionIdTableData[18]
                    //   + " " + ProductionIdTableData[19] + " " + ProductionIdTableData[20] + " " + ProductionIdTableData[21] + " " + ProductionIdTableData[22] + " " + ProductionIdTableData[23]
                    //    + " " + ProductionIdTableData[24] + " " + ProductionIdTableData[25] + " " + ProductionIdTableData[26] + " " + ProductionIdTableData[27]);
}


