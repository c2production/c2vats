﻿/**
 * Class: VutSensors
 * This is the representation of the LS1, LS2, LS3, CP and Material sensors of the Validator.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Utils;

namespace C2ValidatorATS.Vut
{
    class VutSensors
    {
        private byte VALIDATOR_TYPE_ID = 1;
        byte[] lightSensorStatustReq;
        byte[] lightSensorStatustRes;
        byte[] coinPositionSensorStatusReq;
        byte[] coinPositionSensorStatusRes;
        byte[] materialSensorStatusReq;
        byte[] materialSensorStatusRes;
        public bool RIMSensorPresent { get; set; }

        public enum LSSensorType
        {
            LS1 = 0,
            LS2,
            LS3
        }
        public enum CPSensorType
        {
            CP3S,
            CP4L,
            CP4R
        }
        public enum MaterialSensorPinNr
        {
            TC1 = 0,
            TC3,
            TC9,
            TA1,
            TA3,
            TA9,
            RA1,
            RA3,
            RA9,
            RB1,
            RB3,
            RB9
        }

        public VutSensors()
        {
            //init the commands according to the specification
            lightSensorStatustReq = new byte[1] { 0x0A };
            coinPositionSensorStatusReq = new byte[1] { 0x0B };
            materialSensorStatusReq = new byte[1] { 0x10 };

            lightSensorStatustRes = null;
            coinPositionSensorStatusRes = null;
            materialSensorStatusRes = null;
            RIMSensorPresent = IsRIMSensorPresent();//Check if the sensor is there

        }
        //At the moment I will avoid using this method as it could raise confution as "empty" 
        //validator kan have bit actives at this position 
        private bool IsRIMSensorPresent()
        {
            byte[] tableDataBuffer = new byte[128];
            // 0xF0 Production Id Table
            byte[] requestData = new byte[2] { 0xF0, 0x00 };
            CX2ProtAdaptLayer.ReadTable(VALIDATOR_TYPE_ID, requestData, out tableDataBuffer);
            if (tableDataBuffer == null) return false;
            return true ? (tableDataBuffer[22] & 0x01) == 1 : false;
        }

        public byte GetSensorLS1Status(ref string ResulStatustMsg)
        {
            if (!CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, lightSensorStatustReq, out lightSensorStatustRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return lightSensorStatustRes[0];
        }

        public byte GetSensorLS2Status(ref string ResulStatustMsg)
        {
            if (!CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, lightSensorStatustReq, out lightSensorStatustRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return lightSensorStatustRes[1];
        }

        public byte GetSensorLS3Status(ref string ResulStatustMsg)
        {
            if (!CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, lightSensorStatustReq, out lightSensorStatustRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return lightSensorStatustRes[2];
        }

        public byte[] GetAllLightSensorsStatus(ref string ResulStatustMsg)
        {
            if (!CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, lightSensorStatustReq, out lightSensorStatustRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                return lightSensorStatustRes = null;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return lightSensorStatustRes;
        }

        // Coin position Sensors 4 bytes byte 1(CP3S), 2(CP4L) and 3(CP4R) used!

        public byte GetSensorCP3SStatus(ref string ResulStatustMsg)
        {
            if (!CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, coinPositionSensorStatusReq, out coinPositionSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return coinPositionSensorStatusRes[1];
        }

        public byte GetSensorCP4LStatus(ref string ResulStatustMsg)
        {
            if (!CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, coinPositionSensorStatusReq, out coinPositionSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return coinPositionSensorStatusRes[2];
        }

        public byte GetSensorCP4RStatus(ref string ResulStatustMsg)
        {
            if (!CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, coinPositionSensorStatusReq, out coinPositionSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return coinPositionSensorStatusRes[3];
        }
        public byte GetAllCoinPositionSensorStatus(CPSensorType CPsensor, ref string ResulStatustMsg)
        {
            switch (CPsensor)
            {
                case CPSensorType.CP3S: return GetSensorCP3SStatus(ref ResulStatustMsg);
                case CPSensorType.CP4L: return GetSensorCP4LStatus(ref ResulStatustMsg);
                case CPSensorType.CP4R: return GetSensorCP4RStatus(ref ResulStatustMsg);
                default: return 0xff;
            }
        }

        public byte[] GetAllCoinPositionSensorsStatus(ref string ResulStatustMsg)
        {
            if (!CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, coinPositionSensorStatusReq, out coinPositionSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                return coinPositionSensorStatusRes = null;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return coinPositionSensorStatusRes;
        }

        //Coil Sensors. They concist of 2 coils with 6 pin each

        public byte GetMaterialSensorRestingValuePinTC1(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[1];
        }

        public byte GetMaterialSensorRestingValuePinTC3(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[3];
        }

        public byte GetMaterialSensorRestingValuePinTC9(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[5];
        }

        public byte GetMaterialSensorRestingValuePinTA1(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[7];
        }

        public byte GetMaterialSensorRestingValuePinTA3(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[9];
        }

        public byte GetMaterialSensorRestingValuePinTA9(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[11];
        }

        public byte GetMaterialSensorRestingValuePinRA1(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[13];
        }

        public byte GetMaterialSensorRestingValuePinRA3(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[15];
        }

        public byte GetMaterialSensorRestingValuePinRA9(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[17];
        }

        public byte GetMaterialSensorRestingValuePinRB1(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[19];
        }

        public byte GetMaterialSensorRestingValuePinRB3(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[21];
        }

        public byte GetMaterialSensorRestingValuePinRB9(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return 0xff;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes[23];
        }

        public byte GetMaterialSensorRestingValue( bool withPulse, MaterialSensorPinNr pinNr, ref string ResulStatustMsg)
        {
            switch (pinNr)
            {
                case MaterialSensorPinNr.RA1: return GetMaterialSensorRestingValuePinRA1(withPulse, ref ResulStatustMsg);
                case MaterialSensorPinNr.RA3: return GetMaterialSensorRestingValuePinRA3(withPulse, ref ResulStatustMsg);
                case MaterialSensorPinNr.RA9: return GetMaterialSensorRestingValuePinRA9(withPulse, ref ResulStatustMsg);
                case MaterialSensorPinNr.RB1: return GetMaterialSensorRestingValuePinRB1(withPulse, ref ResulStatustMsg);
                case MaterialSensorPinNr.RB3: return GetMaterialSensorRestingValuePinRB3(withPulse, ref ResulStatustMsg);
                case MaterialSensorPinNr.RB9: return GetMaterialSensorRestingValuePinRB9(withPulse, ref ResulStatustMsg);
                case MaterialSensorPinNr.TA1: return GetMaterialSensorRestingValuePinTA1(withPulse, ref ResulStatustMsg);
                case MaterialSensorPinNr.TA3: return GetMaterialSensorRestingValuePinTA3(withPulse, ref ResulStatustMsg);
                case MaterialSensorPinNr.TA9: return GetMaterialSensorRestingValuePinTA9(withPulse, ref ResulStatustMsg);
                case MaterialSensorPinNr.TC1: return GetMaterialSensorRestingValuePinTC1(withPulse, ref ResulStatustMsg);
                case MaterialSensorPinNr.TC3: return GetMaterialSensorRestingValuePinTC3(withPulse, ref ResulStatustMsg);
                case MaterialSensorPinNr.TC9: return GetMaterialSensorRestingValuePinTC9(withPulse, ref ResulStatustMsg);
                default: return 0xff;
            }
        }

        public byte[] GeAllMaterialSensorStatus(bool withPulse, ref string ResulStatustMsg)
        {
            materialSensorStatusReq[0] = GetProperCommand(withPulse);
            if (!CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, materialSensorStatusReq, out materialSensorStatusRes))
            {
                ResulStatustMsg = Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR;
                return null;
            }
            ResulStatustMsg = Msgs.Info.RESULT_OK;
            return materialSensorStatusRes;
        }

        private byte GetProperCommand(bool withPulse)
        {
            //return (byte)(withPulse ? 0x11 : 0x10);
            return (byte)(withPulse ? 0x0F : 0x10);
        }
        /// <summary>
        /// Function used in general and in case GetValue for single lghit sensors doesnt work
        /// </summary>
        //public bool IsLightSensorDefective()
        //{
        //    return true;
        //    //CX2ProtAdaptLayer.GetValue()
        //}
    }
}
