﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;
using C2ValidatorATS.Vut.VutDBlocksInfo;

namespace C2ValidatorATS.RDProcedures
{
    class RDShowChannelIDs
    {
        ValidatorUnderTest CVut;

        public RDShowChannelIDs(ValidatorUnderTest vut, List<Control> lViewChannels)
        {
            TblChannelIds CTblChannelIds;
           
            CVut = vut;
            CTblChannelIds = CVut.GetTblChannelIds();


            ListView lViewChannelIds = lViewChannels[0] as ListView;
            

            //ListViewItem lVwCHItems = new ListViewItem();
            if (lViewChannelIds == null || CTblChannelIds == null || !CTblChannelIds.RetreiveAllData()) 
                return;


            lViewChannelIds.Columns.Add("Ch Nr");
            lViewChannelIds.Columns.Add("ISO");
            lViewChannelIds.Columns.Add("Mantisse");
            lViewChannelIds.Columns.Add("Exponent");
            lViewChannelIds.Columns.Add("Coin ID");
            lViewChannelIds.Columns.Add("Precision");
            lViewChannelIds.Columns.Add("Revision");
            lViewChannelIds.Columns.Add("Thikness");

            for (int i = 0; i < 24; i++)
            {
                ListViewItem lVwCHItems = new ListViewItem(CTblChannelIds.CCoinIdChannels[i].channelId.ToString()); //(i + 1).ToString()); //lVwCHItems.SubItems.Add((i + 1).ToString());  //Channnel Nr

                if (CTblChannelIds.CCoinIdChannels[i].coinID != 0 || CTblChannelIds.CCoinIdChannels[i].iSo.ToString().Contains("TOK"))
                {
                    lVwCHItems.SubItems.Add(CTblChannelIds.CCoinIdChannels[i].iSo.ToString());
                    lVwCHItems.SubItems.Add(CTblChannelIds.CCoinIdChannels[i].mantise.ToString());
                    lVwCHItems.SubItems.Add(CTblChannelIds.CCoinIdChannels[i].exponent.ToString());
                    lVwCHItems.SubItems.Add(CTblChannelIds.CCoinIdChannels[i].coinID.ToString());
                    lVwCHItems.SubItems.Add(CTblChannelIds.CCoinIdChannels[i].precision.ToString());
                    lVwCHItems.SubItems.Add(CTblChannelIds.CCoinIdChannels[i].revision.ToString());
                    lVwCHItems.SubItems.Add(CTblChannelIds.CCoinIdChannels[i].thikness.ToString());
                }
                else
                {
                    for (int j = 0; j < 7; j++)
                    {
                        lVwCHItems.SubItems.Add("--");
                    }
                }
                lViewChannelIds.Items.AddRange(new ListViewItem[] { lVwCHItems });

                //item.UseItemStyleForSubItems = false              
            }
        }
    }
}
