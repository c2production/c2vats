﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;
using C2ValidatorATS.Vut.VutDBlocksInfo;


namespace C2ValidatorATS.RDProcedures
{
    class RDShowChannelSetup
    {
        ValidatorUnderTest CVut;

        public RDShowChannelSetup(ValidatorUnderTest vut, List<Control> lViewChannelSetup)
        {
            TblChannelSetupBlkA CTblFunctionSetupBlkA;
            TblChannelSetupBlkB CTblFunctionSetupBlkB;
            CVut = vut;
            CTblFunctionSetupBlkA = CVut.GetTblChannelSetupBlkA();
            CTblFunctionSetupBlkB = CVut.GetTblChannelSetupBlkB();

            ListView lViewChannelSetupBlkA = lViewChannelSetup[0] as ListView;
            ListView lViewChannelSetupBlkB = lViewChannelSetup[1] as ListView; 

            //ListViewItem lVwCHItems = new ListViewItem();
            if (CTblFunctionSetupBlkA == null || CTblFunctionSetupBlkB == null || !CTblFunctionSetupBlkA.RetreiveAllData() || !CTblFunctionSetupBlkB.RetreiveAllData()) return;
            

            lViewChannelSetupBlkA.Columns.Add("Ch Nr");
            lViewChannelSetupBlkA.Columns.Add("Coin Type");
            lViewChannelSetupBlkA.Columns.Add("Ch Active");
            lViewChannelSetupBlkA.Columns.Add("Reject Funct");
            lViewChannelSetupBlkA.Columns.Add("Ignore Altern Values");
            
            
            for (int i = 0; i < 24; i++)
            {
                ListViewItem lVwCHItems = new ListViewItem((i + 1).ToString()); //lVwCHItems.SubItems.Add((i + 1).ToString());  //Channnel Nr
                lVwCHItems.SubItems.Add((CTblFunctionSetupBlkA.channels[i] & 0x0F).ToString()); //cointype
                lVwCHItems.SubItems.Add(((CTblFunctionSetupBlkA.channels[i] & (byte)0x80) == 0) ? "No": "Yes");  //activated
                lVwCHItems.SubItems.Add(((CTblFunctionSetupBlkA.channels[i] & 0x40) == 0) ? "No": "Yes");  // RejectFunction
                lVwCHItems.SubItems.Add(((CTblFunctionSetupBlkA.channels[i] & 0x20) == 0) ? "No": "Yes");  //Ignore
                lViewChannelSetupBlkA.Items.AddRange(new ListViewItem[] { lVwCHItems });
               
                               //item.UseItemStyleForSubItems = false              
            }

            lViewChannelSetupBlkB.Columns.Add("Ch Nr");
            lViewChannelSetupBlkB.Columns.Add("Coin Type");
            lViewChannelSetupBlkB.Columns.Add("Ch Ative");
            lViewChannelSetupBlkB.Columns.Add("Reject Funct");
            lViewChannelSetupBlkB.Columns.Add("Ignore Altern Values");


            for (int i = 0; i < 24; i++)
            {
                //lVwItemUpperLimits.SubItems.Add("XXX");
                ListViewItem lVwCHItems = new ListViewItem((i + 1).ToString()); //lVwCHItems.SubItems.Add((i + 1).ToString());  //Channnel Nr
                lVwCHItems.SubItems.Add((CTblFunctionSetupBlkB.channels[i] & 0x0F).ToString()); //cointype
                lVwCHItems.SubItems.Add(((CTblFunctionSetupBlkB.channels[i] & (byte)0x80) == 0) ? "No" : "Yes");  //activated
                lVwCHItems.SubItems.Add(((CTblFunctionSetupBlkB.channels[i] & 0x40) == 0) ? "No" : "Yes");  // RejectFunction
                lVwCHItems.SubItems.Add(((CTblFunctionSetupBlkB.channels[i] & 0x20) == 0) ? "No" : "Yes");  //Ignore
                lViewChannelSetupBlkB.Items.AddRange(new ListViewItem[] { lVwCHItems });
                //item.UseItemStyleForSubItems = false              
            }
            
            
         }
    }
}
