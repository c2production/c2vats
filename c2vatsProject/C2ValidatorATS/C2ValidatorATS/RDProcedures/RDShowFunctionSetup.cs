﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;
using C2ValidatorATS.Vut.VutDBlocksInfo;

namespace C2ValidatorATS.RDProcedures
{
    class RDShowFunctionSetup
    {
         ValidatorUnderTest CVut;

         public RDShowFunctionSetup(ValidatorUnderTest vut, List<Control> lblToShowInfo)
        {
            TblFunctionSetup CTblFunctionSetup;
            CVut = vut;
            CTblFunctionSetup = CVut.GetTblFunctionSetup();

            
            if (CTblFunctionSetup == null || !CTblFunctionSetup.RetreiveAllData()) return;
            

            lblToShowInfo[0].Text = CTblFunctionSetup.CurrencyCode;
            lblToShowInfo[1].Text = CTblFunctionSetup.Formula;
            lblToShowInfo[2].Text = CTblFunctionSetup.MMV;
            lblToShowInfo[3].Text = CTblFunctionSetup.MMVmin;
            lblToShowInfo[4].Text = CTblFunctionSetup.MMVmax;
            lblToShowInfo[5].Text = CTblFunctionSetup.DecimalPlaces;
           // string srNum = CVut.g.SerialNumber;
            
        }
    }
}
