﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;
using C2ValidatorATS.Vut.VutDBlocksInfo;
using System.Drawing;

namespace C2ValidatorATS.RDProcedures
{
    class RDShowCoinLimits
    {
        ValidatorUnderTest CVut;
        //ListView lviewMeasures;

        public RDShowCoinLimits(ValidatorUnderTest vut, List<Control> lViewCoinLimits)
        {
            TblCoinLimits CTblCoinLimits;
            TblChannelIds CTblChannelIds;
            byte[] tableXContent = null;
            CVut = vut;
            CTblCoinLimits = CVut.GetTblCoinLimits();
            CTblChannelIds = CVut.GetTblChannelIds();
            ListView lviewLimits = lViewCoinLimits[0] as ListView;

            //ListViewItem lVwCHItems = new ListViewItem();
            if (lviewLimits == null || CTblCoinLimits == null || !CTblCoinLimits.RetreiveAllData() || !CTblChannelIds.RetreiveAllData())
                return;
         
            //lviewMeasures = new ListView();

            //lviewMeasures.View = View.Details;
            //lviewMeasures.BackColor = Color.LightBlue;
            //lviewMeasures.Location = new Point(0, 185);
            //lviewMeasures.Size = new Size(500, 150);
            lviewLimits.Columns.Add("    "); //First one empty
            
            for (int i = 1; i < 25; i++)
            {
                lviewLimits.Columns.Add("   M" + i);//, -1, HorizontalAlignment.Left);
            }
            //lviewLimits.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);

            ListViewItem lVwItemInformation;
            ListViewItem lVwItemUpperLimits; // = new ListViewItem("UpL", 0);
            ListViewItem lVwItemLowerLimits; // = new ListViewItem("LoL", 1);
 

            for (int tblIndx = 1; tblIndx <= CTblCoinLimits.nrOfTableFetched; tblIndx++ )
            {
                if (CTblChannelIds.CCoinIdChannels[tblIndx - 1].coinID == 0 && !CTblChannelIds.CCoinIdChannels[tblIndx - 1].iSo.ToString().Contains("TOK"))
                    continue;
                tableXContent = CTblCoinLimits.GetTable(tblIndx);

                lVwItemInformation = new ListViewItem(" ", tblIndx);
                SetLimitsInformation(lVwItemInformation, tblIndx, CTblChannelIds);
                
                lVwItemUpperLimits = new ListViewItem("UpL", tblIndx + 1); //first param is the row, tblIndx indicate the row nr
                lVwItemLowerLimits = new ListViewItem("LoL", tblIndx + 2);
               
                for (int i = 0; i < 24; i++)
                {                
                    lVwItemUpperLimits.SubItems.Add(tableXContent[i].ToString());
                }
                for (int i = 24; i < 48; i++)
                {
         
                    lVwItemLowerLimits.SubItems.Add(tableXContent[i].ToString());
                }
                lviewLimits.Items.AddRange(new ListViewItem[] { lVwItemInformation });
                lviewLimits.Items.AddRange(new ListViewItem[] { lVwItemUpperLimits });
                lviewLimits.Items.AddRange(new ListViewItem[] { lVwItemLowerLimits });
               
               
              }
            lviewLimits.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
         
        }

        private void SetLimitsInformation(ListViewItem lVwItemInformation, int tblIndx, TblChannelIds CTblChannelIds)
        {
            lVwItemInformation.SubItems[0].ForeColor = Color.Red;
            lVwItemInformation.BackColor = Color.LightSkyBlue;
            lVwItemInformation.SubItems.Add("CH Nr"); 
            lVwItemInformation.SubItems.Add((tblIndx).ToString());  //Add Chnr
            lVwItemInformation.SubItems.Add(" ");                        //Space
            lVwItemInformation.SubItems.Add("ISO");
            lVwItemInformation.SubItems.Add(CTblChannelIds.CCoinIdChannels[tblIndx - 1].iSo.ToString());
            lVwItemInformation.SubItems.Add(" ");                        //Space
            if (CTblChannelIds.CCoinIdChannels[tblIndx - 1].iSo.ToString().Contains("TOK")) return;
            lVwItemInformation.SubItems.Add("Coin ID");
            lVwItemInformation.SubItems.Add(CTblChannelIds.CCoinIdChannels[tblIndx - 1].coinID.ToString());
            lVwItemInformation.SubItems.Add(" ");                        //Space
            lVwItemInformation.SubItems.Add("Rev.");
            lVwItemInformation.SubItems.Add(CTblChannelIds.CCoinIdChannels[tblIndx - 1].revision.ToString());
            lVwItemInformation.SubItems.Add(" ");                        //Space
            lVwItemInformation.SubItems.Add("Prec.");
            lVwItemInformation.SubItems.Add(CTblChannelIds.CCoinIdChannels[tblIndx - 1].precision.ToString());
        }
    
        
    }
}
