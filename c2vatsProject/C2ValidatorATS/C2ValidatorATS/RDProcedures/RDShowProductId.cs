﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;
using C2ValidatorATS.Vut.VutDBlocksInfo;

namespace C2ValidatorATS.RDProcedures
{
    class RDShowProductId
    {
        ValidatorUnderTest CVut;

        public RDShowProductId(ValidatorUnderTest vut, List<Control> lblToShowInfo)
        {
            VutIdentificationData CVutIdentificationData;
            TblProductId CTblProductId;
            CVut = vut;
            CVutIdentificationData = CVut.GetVutIdentificationData();
            CTblProductId = CVut.GetTblProductId();
            if (CVutIdentificationData == null || CTblProductId == null) return;
            if (CVutIdentificationData.IsIdDataEmpty || CTblProductId.isTblEmpty)
            { // I bring data ffrom 2 places. Using identification cmmand and extracting data from Prod Id table
                CVutIdentificationData.RetreiveAllData();
                CTblProductId.RetreiveAllData();
            }

            lblToShowInfo[0].Text = CVutIdentificationData.SerialNumber;
            lblToShowInfo[1].Text = CVutIdentificationData.DataBlock;
            lblToShowInfo[2].Text = CVutIdentificationData.ProductionDate;
            lblToShowInfo[3].Text = CVutIdentificationData.FirmwareVersion;
            lblToShowInfo[4].Text = CTblProductId.PCBId;
            lblToShowInfo[5].Text = CTblProductId.RIMSensor;
           // string srNum = CVut.g.SerialNumber;
            
        }
        
    }
}
