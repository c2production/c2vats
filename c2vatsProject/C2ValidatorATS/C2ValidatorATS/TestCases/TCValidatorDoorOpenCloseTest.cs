﻿/**
 * Class: TCValidatorDoorOpenCloseTest
 * Test Case Class. It checks the functionality "open" and "close" of the upper door of the validator
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Properties;
using C2ValidatorATS.Utils;
using System.ComponentModel;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.TestCases
{
    //"Sorte Door open/close test"
    class TCValidatorDoorOpenCloseTest : TCBaseAbstract
    {
        //const int MAX_TC_COMPLETION_TIME = 20000; // 20 seconds
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        List<Image> animationInfo;

        public TCValidatorDoorOpenCloseTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            TEST_CASE_NAME = TestProvider.VALIDATOR_DOOR_TEST;
            animationInfo = new List<Image>();
            CTestManager.TestExecSuspended = false;
        }


        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }
 
        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            byte openCloseResult = 0x00;
            int sleepTime = 1000;
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME;
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);

            //Initial Door state shall be closed. While the door is open wait and indicate the user to close it
            while (CVut.GetValidatorDoorOpenCloseStatus(ref executionResultMsg) && userInteractionTimeOut > 0)
            {
                CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.PLEASE_CLOSE_VALIDATOR_DOOR, Color.DarkBlue);
                System.Threading.Thread.Sleep(sleepTime);
                userInteractionTimeOut = userInteractionTimeOut - sleepTime;
            }
            if (userInteractionTimeOut <= 0)
            {
                //Close remains open or it is defective!
                executionResultMsg = Msgs.Error.DOOR_INITIAL_STATE_CLOSED_FAIL;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg);
                return false;
            }
            animationInfo.Clear();
            animationInfo.Add(Resources.vutDoorClosed);
            animationInfo.Add(Resources.vutDoorOpen);
            CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.OPEN_AND_CLOSE_VALIDATOR_DOOR, Color.DarkBlue);
            CTestManager.ShowUserAnimationInfo(animationInfo);
            CTestManager.ShowFailPnlVutDoesntRespond(new Point(500, 60));
            userInteractionTimeOut = MAX_TC_COMPLETION_TIME;

            while ((userInteractionTimeOut > 0) && !CTestManager.TestExecSuspended) //&& CTestManager.ActiveThread)
            {   //The door has to bee opened first and then closed
                System.Threading.Thread.Sleep(sleepTime);

                if (CVut.GetValidatorDoorOpenCloseStatus(ref executionResultMsg))
                {
                    openCloseResult = (byte)(openCloseResult | (byte)0x01);
                }
                if (!CVut.GetValidatorDoorOpenCloseStatus(ref executionResultMsg) && openCloseResult == 0x01)
                {
                    openCloseResult = (byte)(openCloseResult | (byte)0x10);
                }
                if (openCloseResult == 0x11)
                {
                    CTestManager.DestroyFailPnlVutDoesntRespond();
                    CTestManager.StopUserAnimationInfo();
                    CTestManager.StopShowingInfoInUserIndicationLabel();
                    executionResultMsg = Msgs.Info.RESULT_OK;
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return true;
                }
                userInteractionTimeOut = userInteractionTimeOut - sleepTime;
            }

            if (userInteractionTimeOut <= 0) 
            {
                executionResultMsg = Msgs.Error.TIME_OUT_OCURRED;
                Log.Error(this.GetType().Name, executionResultMsg);
            }
            else if (CTestManager.TestExecSuspended)
            {
                executionResultMsg = Msgs.Error.VUT_FAIL_TO_REACT_ON_USER_INTERACTION;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
            }
            CTestManager.DestroyFailPnlVutDoesntRespond();
            CTestManager.StopUserAnimationInfo();
            CTestManager.StopShowingInfoInUserIndicationLabel();
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return false;
        } 
    }
}


