﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Properties;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.TestCases
{
    class TCRimSensorTest : TCBaseAbstract
    {
        const int NUMBER_OF_MEASURES = 24;
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        SecurityManager CSecMngr;
        byte[] upperLimits;
        byte[] lowerLimits;
        byte[] originalProdIdTableContent;
        byte[] originalFunctionSetupContent;
        List<Image> animationInfo;

        public TCRimSensorTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            TEST_CASE_NAME = TestProvider.RIM_SENSOR_TEST;
            PrepareKeyForDecryption();
            upperLimits = CoinProvider.SwissFrancCoins.FiveCHFUpperLimitsRIM;
            lowerLimits = CoinProvider.SwissFrancCoins.FiveCHFLowerLimitsRIM;
            CTestManager.TestExecSuspended = false;
            animationInfo = new List<Image>();
            animationInfo = new List<Image>();
            animationInfo.Add(Resources.coinDrop5chf);
            originalProdIdTableContent = null;
            originalFunctionSetupContent = null;
            CTestManager.TestExecSuspended = false;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        private void PrepareKeyForDecryption()
        {
            Log.Debug(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Preparing Key for Decryption ");
            if (CVut.GetProductionIdData() != null)
                CSecMngr = new SecurityManager((byte[])CVut.GetProductionIdData());
        }

        private bool ForceValidatorToUseRIM(ref string executionResultMsg)
        {
            byte[] rimActivation = new byte[]{0x01};  //First bit active 
            byte[] formulaActivation = new byte[] { 0x08 }; //Byte 30 = 8 . CHF formula

            if (CVut.IsRIMPresent && IsChFFormulaActivatedInTable()) return true;

            if ((originalProdIdTableContent = CVut.GetProductionIdData()) == null)
            {
                executionResultMsg = Msgs.Error.FAIL_TO_READ_DATA_FROM_TABLE;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg + "  Production Id Table");
                return false;
            }
            rimActivation[0] = (byte)(rimActivation[0] | originalProdIdTableContent[20]);
            CVut.WriteDataToProdIdTable(20, rimActivation );

            //Function setup changes
            if ((originalFunctionSetupContent = CVut.GetFunctionSetupData()) == null)
            {
                executionResultMsg = Msgs.Error.FAIL_TO_READ_DATA_FROM_TABLE;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg + "  Function Setup Table");
                return false;
            }
            CVut.WriteToFunctionSetupTable(29, formulaActivation);
            return true;
        }

        private bool IsChFFormulaActivatedInTable()  //Formula indication in table 82
        {
            byte[] functionSetupData = CVut.GetFunctionSetupData();
            if (functionSetupData[29] == 0x08)  //byte 30 lsb bit 1 = 1--> chf formula 8
            {
                return true;
            }
            return false;
        }
        private bool RestoreValidatorOriginalData()
        {
            if ((originalProdIdTableContent != null) && !CVut.WriteDataToProdIdTable(0, originalProdIdTableContent))
            {
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " No able to restore date in Validator. Original data corrupted");
                return false;
            }
            
            if ((originalFunctionSetupContent != null) && !CVut.WriteToFunctionSetupTable(0, originalFunctionSetupContent))
            {
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " No able to restore date in Validator. Original data corrupted");
                return false;
            }
            
            return true;
        }
        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME * 10; //30*100 5 min
            int sleepTime = 1000;
            byte[] measureRsp = null;
            byte[] measures = new byte[NUMBER_OF_MEASURES];
            bool coinMeasureFail = false;
            int dropAttempts = 0;

            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg) || CSecMngr == null)
            {
                if (CSecMngr == null)
                    Log.Error(this.GetType().Name, "Error communicating with Validator");
                else
                    Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            if (!ForceValidatorToUseRIM(ref executionResultMsg))
            {
                return false;
            }
           
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);
            Log.Debug(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Empty buffer before getting Measurement ");
            CVut.GetMeasuredValues(ref executionResultMsg); //Empty the buffer before tc start
            CTestManager.ShowUserAnimationInfo(animationInfo);
            CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.INSERT_FIVE_CHF, Color.DarkBlue);
            CTestManager.ShowFailPnlVutDoesntRespond(new Point(20, 20));
            while ((userInteractionTimeOut > 0) && !CTestManager.TestExecSuspended)
            {
                coinMeasureFail = false;
                if ((measureRsp = CVut.GetMeasuredValues(ref executionResultMsg)) != null && measureRsp.Count() == 28)
                {
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Coin droped and measured");
                    CSecMngr.Decrypt(ref measureRsp, 2); //the second param is the offset i.e. from where i want to decrypt/encrypt
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Coin measured values decrypted");	
                    dropAttempts += 1;
					for (int i = 0; i < NUMBER_OF_MEASURES; i++)
					{
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Measure M" + (i + 1) + " ------");
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Upper Limit = " + upperLimits[i]);
                        measures[i] = measureRsp[i + 4];
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Measured Value= " + measures[i]);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Lower Limit = " + lowerLimits[i]);
                        if (!(measures[i] <= upperLimits[i] && measures[i] >= lowerLimits[i]))
                        {
                            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " M" + (i + 1) +" Value " + measures[i] + " is OUT OF LIMITS");
                            coinMeasureFail = true;
                            CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.DROP_FAIL_INSERT_AGAIN, Color.Red);
                        }
                        
                    }
                    if (coinMeasureFail)
                    {
                                               executionResultMsg = Msgs.Error.COIN_MEAUSRED_OUT_OF_LIMITS;
                        if (dropAttempts == 2)  //Max 2 attempts
                        {
                            CTestManager.DestroyFailPnlVutDoesntRespond();
                            CTestManager.StopUserAnimationInfo();
                            CTestManager.StopShowingInfoInUserIndicationLabel();
                            Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                            return false;
                        }
                        Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg + ", try again!");
                        
                    }     
                    else
                    {
                        CTestManager.DestroyFailPnlVutDoesntRespond();
                        CTestManager.StopUserAnimationInfo();
                        CTestManager.StopShowingInfoInUserIndicationLabel();
                        executionResultMsg = Msgs.Info.RESULT_OK;
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                        if (!RestoreValidatorOriginalData())
                        {
                            Log.Warning(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + "There were some errors restoring Validator data");
                        }
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                        return true;
                    }    
                    
                }
                System.Threading.Thread.Sleep(sleepTime);
                userInteractionTimeOut = userInteractionTimeOut - sleepTime;
            }
            if (userInteractionTimeOut <= 0)
            {
                executionResultMsg = Msgs.Error.TIME_OUT_OCURRED;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
            }
            else if (CTestManager.TestExecSuspended)
            {
                executionResultMsg = Msgs.Error.VUT_FAIL_TO_REACT_ON_USER_INTERACTION;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
            }
            CTestManager.DestroyFailPnlVutDoesntRespond();
            CTestManager.StopShowingInfoInUserIndicationLabel();
            CTestManager.StopUserAnimationInfo();
            if (!RestoreValidatorOriginalData())
            {
                Log.Warning(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + "There were some errors restoring Validator data");
            }
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return false;
        }
    }
}
