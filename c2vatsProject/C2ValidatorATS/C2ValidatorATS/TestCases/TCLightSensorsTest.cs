﻿/**
 * Class: TCLightSensorsTest
 * Test Case Class. It checks the status of the light sensors LS1, Ls2 and Ls3 located behind the Validtor's door
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.TestCases
{
    class TCLightSensorsTest : TCBaseAbstract
    {   
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        byte[] lightSensorsStatustRes;

        public TCLightSensorsTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            lightSensorsStatustRes = null;
            TEST_CASE_NAME = TestProvider.LIGHT_SENSOR_TEST;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);

            lightSensorsStatustRes = CVut.GetLightSensorsStatus(ref executionResultMsg);
            if (lightSensorsStatustRes == null)
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                return false;
            }
            //If no DBLOCK modify byte 2 bit 4 in Function Setup 
            if ((lightSensorsStatustRes[0] & lightSensorsStatustRes[1] & lightSensorsStatustRes[2]) == 0xFE) //No datblock in it
            {
                byte[] dataTableFuncSetup = CVut.CVutTableOps.GetDataFromFunctionSetupTbl();
                if (!((dataTableFuncSetup.Count() != 0) && CVut.CVutTableOps.WriteDataToFunctionSetupTbl(1, new byte[] { (byte)(dataTableFuncSetup[1] & 0xDF) })))
                {
                    Log.Error(this.GetType().Name, " NO able To Measure the Light Sensors. Modification of datablock to meka meassurement available Fail!");
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return false;
                }
            }

           lightSensorsStatustRes = CVut.GetLightSensorsStatus(ref executionResultMsg);
           if (lightSensorsStatustRes != null)
           {
               executionResultMsg = Msgs.Info.RESULT_OK;
            
                //Valid value 1 to 3
                if (((lightSensorsStatustRes[0] | lightSensorsStatustRes[1] | lightSensorsStatustRes[2]) <= 3)
                    && ((lightSensorsStatustRes[0] & lightSensorsStatustRes[1] & lightSensorsStatustRes[2]) != 0))
                {
                    Log.Info(this.GetType().Name, executionResultMsg);
                    Log.Info(this.GetType().Name, "Sensor values: " + lightSensorsStatustRes[0] + " " + lightSensorsStatustRes[1] + " " + lightSensorsStatustRes[2]);
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return true;
                }//According to the spec max value is 8 
                else if ((lightSensorsStatustRes[0] <= 8 && lightSensorsStatustRes[1] <= 8 && lightSensorsStatustRes[2] <= 8))//According to the spec
                {
                    executionResultMsg = Msgs.Warning.LS_SENSORS_VALUE_IN_TOLERANCE;
                    Log.Warning(this.GetType().Name, executionResultMsg);
                    Log.Info(this.GetType().Name, "Sensor values: " + lightSensorsStatustRes[0] + " " + lightSensorsStatustRes[1] + " " + lightSensorsStatustRes[2]);
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return true;
                }
                else
                {
                    executionResultMsg = Msgs.Error.LS_SENSORS_VALUE_OUT_OF_TOLERANCE;
                    Log.Info(this.GetType().Name, "Sensor values: " + lightSensorsStatustRes[0] + " " + lightSensorsStatustRes[1] + " " + lightSensorsStatustRes[2]);
                    Log.Error(this.GetType().Name, executionResultMsg);
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return false;
                }
               
           }
           executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
           Log.Error(this.GetType().Name, executionResultMsg);
            return false;
        }
    }
}
