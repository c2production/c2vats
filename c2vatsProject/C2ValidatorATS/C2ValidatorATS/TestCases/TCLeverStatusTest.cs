﻿/**
 * Class: TCLeverStatusTest
 * Test Case Class. It checks the status of the Lever by pushing the lever down
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Properties;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.TestCases
{
    class TCLeverStatusTest: TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        List<Image> animationInfo;
        byte[] currentLightSensorsStatustRes;
        byte[] InitialLightSensorsStatustRes;

        public TCLeverStatusTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            TEST_CASE_NAME = TestProvider.LEVER_STATUS_TEST;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            currentLightSensorsStatustRes = null;
            InitialLightSensorsStatustRes = null;
            animationInfo = new List<Image>();
            CTestManager.TestExecSuspended = false;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            int sleepTime = 500;
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME;
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");
            bool IsSensorStable = true;
            animationInfo.Add(Resources.leverDown);
            animationInfo.Add(Resources.leverUpp);

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);
            if ((InitialLightSensorsStatustRes = CVut.GetLightSensorsStatus(ref executionResultMsg)) == null)
           {
               Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Fail to retreive data " + executionResultMsg);
               Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
               return false;
           }
            IsSensorStable = IsSensorsMeasureStable(ref executionResultMsg);
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Initial values: " + InitialLightSensorsStatustRes[0] + "," + InitialLightSensorsStatustRes[1] + "," + InitialLightSensorsStatustRes[2]); 
            CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.PUSH_AND_RELEASE_LEVER, Color.DarkBlue);
            CTestManager.ShowUserAnimationInfo(animationInfo);
            CTestManager.ShowFailPnlVutDoesntRespond(new Point(10, 30));
           userInteractionTimeOut = MAX_TC_COMPLETION_TIME;
           while (userInteractionTimeOut > 0 && !CTestManager.TestExecSuspended)
           {
               System.Threading.Thread.Sleep(sleepTime);
               if ((currentLightSensorsStatustRes = CVut.GetLightSensorsStatus(ref executionResultMsg)) == null)
               {
                   userInteractionTimeOut = userInteractionTimeOut - sleepTime;
                   Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg); 
                   continue;
                   //return false;
               }
               Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Current values: " + currentLightSensorsStatustRes[0] + "," + currentLightSensorsStatustRes[1] + "," + currentLightSensorsStatustRes[2]);
               if (IsSensorStable)
               {
                   for (int i = 0; i < 3; i++)
                   {
                       if (InitialLightSensorsStatustRes[i] < currentLightSensorsStatustRes[i])
                       {
                           executionResultMsg = Msgs.Info.RESULT_OK;
                           Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                           Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                           CTestManager.DestroyFailPnlVutDoesntRespond();
                           return true;
                       }
                   }
               }
               else
               {              
                    if ((InitialLightSensorsStatustRes[0] < currentLightSensorsStatustRes[0]) && (InitialLightSensorsStatustRes[1] < currentLightSensorsStatustRes[1]))
                    {
                        executionResultMsg = Msgs.Info.RESULT_OK;
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                        CTestManager.DestroyFailPnlVutDoesntRespond();
                        return true;
                    }
               }
                           
               userInteractionTimeOut = userInteractionTimeOut - sleepTime;
           }
            if (userInteractionTimeOut <= 0)
            {
                executionResultMsg = Msgs.Error.TIME_OUT_OCURRED;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
            }else if(CTestManager.TestExecSuspended)
            {
                executionResultMsg = Msgs.Error.VUT_FAIL_TO_REACT_ON_USER_INTERACTION;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
            }
            CTestManager.DestroyFailPnlVutDoesntRespond();
            CTestManager.StopUserAnimationInfo();
            CTestManager.StopShowingInfoInUserIndicationLabel();
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return false;
        }
        //This is necessary because Ive been observing than there are some units where sensors varied without any change, moving lever or door at all.
        //In those cases the test pass without any user interaction because a variation is detected. To prevent this a short test is executed before and the lever check
        //is adapted to the result
        private bool IsSensorsMeasureStable(ref string executionResultMsg)
        {
            int check = 5;
            byte[] initialSensorValues;
            initialSensorValues = CVut.GetLightSensorsStatus(ref executionResultMsg);
            while (check > 0)
            {
                currentLightSensorsStatustRes = CVut.GetLightSensorsStatus(ref executionResultMsg);
                if ((currentLightSensorsStatustRes != null) && (InitialLightSensorsStatustRes[0] != currentLightSensorsStatustRes[0]) || (InitialLightSensorsStatustRes[1] != currentLightSensorsStatustRes[1]) || (InitialLightSensorsStatustRes[2] != currentLightSensorsStatustRes[2]))
                {
                    return false;
                }
                check--;
            }
            if (currentLightSensorsStatustRes == null) return false;
            return true;
        }
    }
}
