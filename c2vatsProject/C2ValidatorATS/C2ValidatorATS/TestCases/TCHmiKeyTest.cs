﻿/**
 * Class: TCHmiKeyTest
 * Test Case Class. It checks the status of the keys.
 * For info about the comands and response of the keyboard actions see doc CX2_W99_Keyboard_Display_V2_1.pdf
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Properties;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.TestCases
{  
    class TCHmiKeyTest : TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        List<Image> animationInfo;

        public TCHmiKeyTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            TEST_CASE_NAME = TestProvider.HMI_KEY_TEST;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            animationInfo = new List<Image>();
            animationInfo.Add(Resources.keyboard);
            CTestManager.TestExecSuspended = false;
        }

        private string GetRandomString()
        {
            Random rndNr = new Random();

            switch (rndNr.Next(1, 4)) //3 generados
            {
                case 1: return "A F C D E B   \'MENU\'";
                case 2: return "D E F A B C   \'MENU\'";
                case 3: return "B F E C D A   \'MENU\'";
            }
            return "A B C D E F";
        }

        private int[] getRectPosition(string expectedPressedKey)
        {
            int[] coordinates;
            switch (expectedPressedKey)
            {
                case "A": return coordinates = new int[]{153, 137, 37, 37};
                case "B": return coordinates = new int[] { 153, 186, 37, 37 };
                case "C": return coordinates = new int[] { 199, 186, 37, 37 };
                case "D": return coordinates = new int[] { 250, 186, 37, 37 };
                case "E": return coordinates = new int[] { 297, 186, 37, 37 };
                case "F": return coordinates = new int[] { 297, 137, 37, 37 };
                default: return coordinates = new int[] { 344, 188, 37, 37 };
            }
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            int sleepTime = 100;
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME * 10;
            int[] stringRepresentation = new int[6];
            int[] RectPosition = new int[4];
            string stringToType = "";
            string expectedPressedKey; 

            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");
            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);
            CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.TYPE_KEYS_IN_THIS_SEQUENCE, Color.DarkBlue);
            CTestManager.ShowUserAnimationInfo(animationInfo);           
            CTestManager.ShowInfoInAuxUserIndicationLabel(stringToType = GetRandomString());
            CTestManager.ShowFailPnlVutDoesntRespond(new Point(20, 140));
            while ((userInteractionTimeOut > 0) && !CTestManager.TestExecSuspended)
            {
                if (stringToType.Count() >= 10)
                    expectedPressedKey = stringToType.ElementAt(0).ToString();
                else expectedPressedKey = "MENU";
                RectPosition = getRectPosition(expectedPressedKey);
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + "Key to be Pressed: " + expectedPressedKey);
                while (!CTestManager.TestExecSuspended && (CVut.GetPressedKey(ref executionResultMsg) != expectedPressedKey) && (userInteractionTimeOut > 0))
                {
                    CTestManager.paintFigure(Color.Red, "Rectangle", RectPosition[0], RectPosition[1], RectPosition[2], RectPosition[3]);
                    System.Threading.Thread.Sleep(sleepTime);
                    userInteractionTimeOut = userInteractionTimeOut - sleepTime;
                }
                if (userInteractionTimeOut > 0 && !CTestManager.TestExecSuspended)
                {
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + "Result: " + Msgs.Info.RESULT_OK);
                    CTestManager.ShowInfoInAuxUserIndicationLabel(stringToType = stringToType.Remove(0, 2));
                    if ((stringToType.Count() <= 10) && (expectedPressedKey == "MENU"))
                    {
                        CTestManager.CleanInfoInAuxUserIndicationLbl();
                        CTestManager.DestroyFailPnlVutDoesntRespond();
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                        return true;
                    }
                }
            }
            
            if (userInteractionTimeOut <= 0)
            {
                executionResultMsg = Msgs.Error.TIME_OUT_OCURRED;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);               
            }else if (CTestManager.TestExecSuspended)
            {
                executionResultMsg = Msgs.Error.VUT_FAIL_TO_REACT_ON_USER_INTERACTION;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
            }
            CTestManager.CleanInfoInAuxUserIndicationLbl();
            CTestManager.DestroyFailPnlVutDoesntRespond();
            CTestManager.StopUserAnimationInfo();
            CTestManager.StopShowingInfoInUserIndicationLabel();
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");    
            return false;
        }

    }
}
