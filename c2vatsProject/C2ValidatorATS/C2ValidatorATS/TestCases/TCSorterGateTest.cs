﻿/**
 * Class: TCSorterGateTest
 * Test Case Class. It checks the functionality of the sorters. Needs user interaction
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Properties;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.TestCases
{
    //Test Case: Sorter Gate Funcionality Test
    class TCSorterGateTest: TCBaseAbstract
    {
        const int MAX_TC_COMPLETION_TIME_1 = 60000; // 60 seconds
        const int NUMBER_OF_SORTER_GATES = 4;
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        Random rndSorterGate;
        int[] sorterGatesStatus;
        List<Image> animationInfo;
       
        public TCSorterGateTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            TEST_CASE_NAME = TestProvider.SORTER_GATE_TEST;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            rndSorterGate = new Random();
            sorterGatesStatus = new int[] {0,0,0,0};
            animationInfo = new List<Image>();
            CTestManager.TestExecSuspended = false;
        }

         private int GetNewSorterGate()
        {   //"sorterGateId" =  0 -> all, 1 -> GateSC, 2 -> GateRL, 3-> Gate SM, 7 -> Acceptance Gate</param>            
             bool newSorterDoorToTestFound = false;
             while (!newSorterDoorToTestFound)
             {
                 int sd = rndSorterGate.Next(0, NUMBER_OF_SORTER_GATES);

                 for (int i = 0; i < NUMBER_OF_SORTER_GATES; i++)
                 {
                     if (sorterGatesStatus[i] == 0)
                         break;
                     if (i == NUMBER_OF_SORTER_GATES - 1)
                         return -1;//no gate founds
                 }
                 if (sorterGatesStatus[sd] == 0)
                 {
                     sorterGatesStatus[sd] = 1; //done
                     //newSorterDoorToTestFound = true;
                     return sd;
                 }
                
             }
             return -1;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        public int GetUserSelectedMovingSorterGate()
        {
            return TranslateCoordinatesToSorterGate(CTestManager.GetUserImageClickCoordinates());
        }
 
        public int TranslateCoordinatesToSorterGate(int[,] coordinates)
        {
            if ((coordinates[0, 0] > 340) && (coordinates[0, 0] < 475) && (coordinates[0, 1] > 104) && (coordinates[0, 1] < 150))
            {
                return 0; // SorterGate 0 is the number 7 which is the Acceptance gate
            }
            else if ((coordinates[0, 0] > 345) && (coordinates[0, 0] < 475) && (coordinates[0, 1] > 165) && (coordinates[0, 1] < 200))
            {
                return 1; 
            }
            else if ((coordinates[0, 0] > 220) && (coordinates[0, 0] < 360) && (coordinates[0, 1] > 205) && (coordinates[0, 1] < 265))
            {
                return 2;
            }
            else if ((coordinates[0, 0] > 140) && (coordinates[0, 0] < 350) && (coordinates[0, 1] > 270) && (coordinates[0, 1] < 330))
            {
                return 3;
            }
            return -1;
        }
        /// <summary>
        /// Check the Door is open
        /// returns when it is open(true) or  time runned out(false)
        /// </summary>
        /// <param name="executionResultMsg"></param>
        /// <returns>get the result of the execution</returns>
        public bool IsDoorOpen(ref string executionResultMsg)
        {
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME;
            int sleepTime = 1000;
            //check that the door is open
            animationInfo.Clear();
            animationInfo.Add(Resources.hmiDoorOpen);
            CTestManager.ShowUserAnimationInfo(animationInfo);
            while (!CVut.GetSorterDoorOpenCloseStatus(ref executionResultMsg) && (userInteractionTimeOut > 0) && !CTestManager.TestExecSuspended)
            {
                CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.PLEASE_OPEN_SOTER_DOOR, Color.DarkBlue);
                System.Threading.Thread.Sleep(sleepTime);
                userInteractionTimeOut = userInteractionTimeOut - sleepTime;
            }
            CTestManager.StopShowingInfoInUserIndicationLabel();
            //CTestManager.StopUserAnimationInfo();
            if (userInteractionTimeOut <= 0)
            {
                executionResultMsg = Msgs.Error.TIME_OUT_OCURRED;
                return false;
            }
            else if (CTestManager.TestExecSuspended)
            {
                executionResultMsg = Msgs.Error.VUT_FAIL_TO_REACT_ON_USER_INTERACTION;
                return false;
            }
            

            CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.CLICK_ON_THE_MOVING_SORTER_AREA, Color.DarkBlue);
            //CTestManager.ActivateUserImageMouseClickEvenHandler(true);
            animationInfo.Clear();
            animationInfo.Add(Resources.sorterGates);
            CTestManager.ShowUserAnimationInfo(animationInfo);
            return true;
        }

        public override bool RunTestCase(ref string executionResultMsg)
        {
            int sleepTime = 1000;
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME_1;
            int repeatSorterGate = -1;
            int sorterGate = -1;
            int sorterGateClickedByUser = -1; //none sortergate
            //bool allSorterGatesTested = false;

            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");
            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);
            //"sorterGateId" =  0 -> all, 1 -> GateSC, 2 -> GateRL, 3-> Gate SM, 7 -> Acceptance Gate</param>
            CTestManager.ShowFailPnlVutDoesntRespond(new Point(20, 20));
            while ((userInteractionTimeOut > 0) && !CTestManager.TestExecSuspended)
            {
                //This is just in case we need to repeat the check of a gate because the test was interrupted caused by door closed
                if (repeatSorterGate == -1)
                    sorterGate = GetNewSorterGate();
                else
                {
                    sorterGate = repeatSorterGate;
                    repeatSorterGate = -1;
                }
               
                //allSorterGatesTested = true ? (sorterGate == -1) : false;

                if (sorterGate == -1)  //SorteGate = -1 = all sortergates tested
                {
                    executionResultMsg = Msgs.Info.RESULT_OK;
                    CTestManager.DestroyFailPnlVutDoesntRespond();
                    CTestManager.StopShowingInfoInUserIndicationLabel();
                    CTestManager.StopUserAnimationInfo();
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg);
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return true;
                }
                //Check the Door is open
                //returns when it is open(true) or  time runned out(false)
                if (!IsDoorOpen(ref executionResultMsg)) //If the door check return with false means a time out occurred
                {
                    CTestManager.StopShowingInfoInUserIndicationLabel();
                    CTestManager.StopUserAnimationInfo();
                    CTestManager.DestroyFailPnlVutDoesntRespond();
                    Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg);
                    Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " The sorrter Door Is Closed or Defective! ");
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return false;
                }
                    
               
                //Convenction 0 = 7 which is the acceptance gate
                if (sorterGate == 0) sorterGate = 7;
                while ((sorterGateClickedByUser != sorterGate) && CVut.GetSorterDoorOpenCloseStatus(ref executionResultMsg) 
                    && (userInteractionTimeOut > 0) && !CTestManager.TestExecSuspended) 
                {
                    int movingAttempts = 0;
                    while (!CVut.MoveSorterGates((byte)sorterGate, ref executionResultMsg) & movingAttempts < 3)
                    {
                        movingAttempts++;
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Fail to move the gate. Attempt " + movingAttempts);
                        System.Threading.Thread.Sleep(200);
                    }
                    if (movingAttempts == 3)
                    {
                        CTestManager.DestroyFailPnlVutDoesntRespond();
                        CTestManager.StopShowingInfoInUserIndicationLabel();
                        CTestManager.StopUserAnimationInfo();
                        executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                        Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                        return false;
                    }
                    System.Threading.Thread.Sleep(sleepTime);
                    userInteractionTimeOut = userInteractionTimeOut - sleepTime;
                    if((sorterGateClickedByUser = GetUserSelectedMovingSorterGate()) == 0)
                        sorterGateClickedByUser = 7;
                    
                } //while ((sorterGateClickedByUser != sorterGate) && CVut.GetSorterDoorOpenCloseStatus(ref executionResultMsg) && userInteractionTimeOut > 0);
                if (!CVut.GetSorterDoorOpenCloseStatus(ref executionResultMsg))
                {
                    repeatSorterGate = sorterGate;
                }
            }
            if (userInteractionTimeOut <= 0)
            {
                executionResultMsg = Msgs.Error.TIME_OUT_OCURRED;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
            }
            else if (CTestManager.TestExecSuspended)
            {
                executionResultMsg = Msgs.Error.VUT_FAIL_TO_REACT_ON_USER_INTERACTION;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
            }
            CTestManager.DestroyFailPnlVutDoesntRespond();
            CTestManager.StopShowingInfoInUserIndicationLabel();
            CTestManager.StopUserAnimationInfo();
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return false;
        }
    }
}
