﻿/**
 * Class: TCCoinPositionSensorTest
 * Test Case Class. It checks the status of the light sensors CP3c, CP3s and CP4l located behind the Sorter's door
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.TestCases
{
    class TCCoinPositionSensorTest : TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;

        byte[] cPSensorsStatustRes;

        public TCCoinPositionSensorTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            TEST_CASE_NAME = TestProvider.COIN_POSITION_SENSOR_TEST;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            cPSensorsStatustRes = null;
        }

                /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");
            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);
            cPSensorsStatustRes = CVut.GetCoinPositionSensorsStatus(ref executionResultMsg);

            if (cPSensorsStatustRes != null)
           {
               executionResultMsg = Msgs.Info.RESULT_OK;
               if ((cPSensorsStatustRes[1] & cPSensorsStatustRes[2] & cPSensorsStatustRes[3]) == 20)
               {
                   Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + cPSensorsStatustRes[1] + " " + cPSensorsStatustRes[2] + " " + cPSensorsStatustRes[3]);
                   Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                   Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                   return true;
               }
               else if ((cPSensorsStatustRes[1] >= 0x15) && (cPSensorsStatustRes[1] >= 0x15) && (cPSensorsStatustRes[1] >= 0x15))
               {
                   executionResultMsg = Msgs.Warning.CP_SENSORS_VALUE_IN_TOLERANCE;
                   Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + " Sensor Values: " + cPSensorsStatustRes[1] + " " + cPSensorsStatustRes[2] + " " + cPSensorsStatustRes[3]);
                   Log.Warning(this.GetType().Name, "\"" + GetTestCaseName() + executionResultMsg );
                   Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                   return true;
               }
               else
               {
                   executionResultMsg = Msgs.Error.CP_SENSORS_VALUE_OUT_OF_TOLERANCE;
                   Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                   Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + " Sensor Values: " + cPSensorsStatustRes[1] + " " + cPSensorsStatustRes[2] + " " + cPSensorsStatustRes[3]);
                   Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                   return false;
               }
           }
            executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
            Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return false;
        }
   
    }
}
