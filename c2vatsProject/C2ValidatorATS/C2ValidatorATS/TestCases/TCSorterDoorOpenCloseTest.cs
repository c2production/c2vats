﻿/**
 * Class: TCSorterDoorOpenCloseTest
 * Test Case Class. It checks the functionality "open" and "close" the door that cover the sorter gates
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Properties;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.TestCases
{
     //"Sorte Door open/close test"
    class TCSorterDoorOpenCloseTest: TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        List<Image> animationInfo;

        public TCSorterDoorOpenCloseTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            TEST_CASE_NAME = TestProvider.SORTER_DOOR_TEST; 
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            animationInfo = new List<Image>();
            CTestManager.TestExecSuspended = false;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        private bool DoorInitialConditionFulfilled(ref string executionResultMsg)
        {
            int sleepTime = 1000;
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME;
            animationInfo.Clear();
            animationInfo.Add(Resources.hdmiDoorClosed);

            if (CVut.GetSorterDoorOpenCloseStatus(ref executionResultMsg))
            {
                CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.PLEASE_CLOSE_SOTER_DOOR, Color.DarkBlue);
                CTestManager.ShowUserAnimationInfo(animationInfo);
            }
            //Initial Door state shall be closed. While the door is open wait and indicate the user to close it
            while (CVut.GetSorterDoorOpenCloseStatus(ref executionResultMsg) && userInteractionTimeOut > 0)
            {             
                System.Threading.Thread.Sleep(sleepTime);
                userInteractionTimeOut = userInteractionTimeOut - sleepTime;
            }
            CTestManager.StopUserAnimationInfo();
            if (userInteractionTimeOut <= 0 || executionResultMsg != Msgs.Info.RESULT_OK)
            {
                //Door remains open or it is defective!
                return false;
            }
            return true;
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            byte openCloseResult = 0x00;
            int sleepTime = 1000;
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME;
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " execution Starts ");
            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);
            
            if (!DoorInitialConditionFulfilled(ref executionResultMsg))
            {
                //Log door opened or defective
                executionResultMsg = Msgs.Error.DOOR_INITIAL_STATE_CLOSED_FAIL;
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg);
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                return false;
            }
            animationInfo.Clear();
            animationInfo.Add(Resources.hmiDoorOpen);
            animationInfo.Add(Resources.hmiDoorClose);

            CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.OPEN_AND_CLOSE_SORTER_DOOR, Color.DarkBlue);
            CTestManager.ShowUserAnimationInfo(animationInfo);
            CTestManager.ShowFailPnlVutDoesntRespond(new Point(10, 120));

            while ((userInteractionTimeOut > 0) && !CTestManager.TestExecSuspended)
            {   //The door has to bee opened first and then closed
                System.Threading.Thread.Sleep(sleepTime);

                if (CVut.GetSorterDoorOpenCloseStatus(ref executionResultMsg))
                {
                    openCloseResult = (byte)(openCloseResult | (byte)0x01);
                }
                if (!CVut.GetSorterDoorOpenCloseStatus(ref executionResultMsg) && openCloseResult == 0x01)
                {
                    openCloseResult = (byte)(openCloseResult | (byte)0x10);
                }
                if (openCloseResult == 0x11)
                {
                    CTestManager.DestroyFailPnlVutDoesntRespond();
                    CTestManager.StopUserAnimationInfo();
                    CTestManager.StopShowingInfoInUserIndicationLabel();
                    executionResultMsg = Msgs.Info.RESULT_OK;
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return true;
                }
                userInteractionTimeOut = userInteractionTimeOut - sleepTime;
            }

            if (userInteractionTimeOut <= 0)  //CTestManager.ActiveThread &&
            {
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + Msgs.Error.TIME_OUT_OCURRED);
                executionResultMsg = Msgs.Error.TIME_OUT_OCURRED;
            }
            else if (CTestManager.TestExecSuspended)
            {
                executionResultMsg = Msgs.Error.VUT_FAIL_TO_REACT_ON_USER_INTERACTION;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
            }
            CTestManager.DestroyFailPnlVutDoesntRespond();
            CTestManager.StopUserAnimationInfo();
            CTestManager.StopShowingInfoInUserIndicationLabel();
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return false;
        }
    }
}
