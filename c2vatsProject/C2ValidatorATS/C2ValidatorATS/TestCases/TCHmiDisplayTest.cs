﻿/**
 * Class: TCHmiDisplayTest
 * Test Case Class. It checks some display features, animation, bitmap, string, ascii charaters and so on.
 * See doc CX2_W99_Keyboard_Display_V2_1.pdf
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Properties;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.TestCases
{
    class TCHmiDisplayTest: TCBaseAbstract
    {
        const int NUMBER_OF_DISPLAY_ACTIONS = 7;
        string testCaseStatus;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        ValidatorUnderTest CVut;
        List<Image> animationInfo;
        bool[] displayActionConsumed;
        Random rndDisplayAction;

        private enum DisplayAction
        {
            CLEAR_SCREEN = 0,
            WRITE_STRING,
            WRITE_BITMAP,
            SHOW_MSG,
            SHOW_ASCII_CHARS,
            BGD_LIGHT_OFF,
            SHOW_ANIMATION,
            ALL_TESTED = -1  //No action found, All tested
        }
        public TCHmiDisplayTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            TEST_CASE_NAME = TestProvider.HMI_DISPLAY_TEST;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            animationInfo = new List<Image>();
            displayActionConsumed = new bool[] { false, false, false, false, false, false, false };
            rndDisplayAction = new Random();
            CTestManager.TestExecSuspended = false;
        }

        private DisplayAction GetNewDisplayAction()
        {
            
            bool newDisplayActionFound = false;
             while (!newDisplayActionFound)
             {
                Log.Debug(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Looking for a new display feature to test "); 
                int rDa = rndDisplayAction.Next(0, NUMBER_OF_DISPLAY_ACTIONS);
                for (int i = 0; i < NUMBER_OF_DISPLAY_ACTIONS; i ++ )
                {   //If There are still actions to test
                    if (!displayActionConsumed[i]) break;
                    //If all actions has been consumed
                    if (i == NUMBER_OF_DISPLAY_ACTIONS - 1) return DisplayAction.ALL_TESTED;                  
                }
                if (!displayActionConsumed[rDa])
                {
                    displayActionConsumed[rDa] = true; //Consumed
                    return (DisplayAction)rDa;
                }
            }
            return DisplayAction.CLEAR_SCREEN;
        }

        private DisplayAction GetUserSelectedScreenRepresentation()
        {
            Log.Debug(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Translating iser mouse click coordinates into a screen representation"); 
            return (DisplayAction)TranslateCoordinatesToScreenRepresentation(CTestManager.GetUserImageClickCoordinates());
        }
        //TRanslate the coordinates to a nr representing a Screen
        private int TranslateCoordinatesToScreenRepresentation(int[,] coordinates)
        {
            if ((coordinates[0, 0] > 0) && (coordinates[0, 0] < 178) && (coordinates[0, 1] > 0) && (coordinates[0, 1] < 112))
            {
                return 0; // Clear Screen
            }
            else if ((coordinates[0, 0] > 178) && (coordinates[0, 0] < 356) && (coordinates[0, 1] > 0) && (coordinates[0, 1] < 112))
            {
                return 1; // Write String
            }
            else if ((coordinates[0, 0] > 356) && (coordinates[0, 0] < 534) && (coordinates[0, 1] > 0) && (coordinates[0, 1] < 112))
            {
                return 2; // Write Bit map
            }//-----------Second Row--------
            else if ((coordinates[0, 0] > 0) && (coordinates[0, 0] < 178) && (coordinates[0, 1] > 112) && (coordinates[0, 1] < 224))
            {
                return 3;  // Show Message
            }
            else if ((coordinates[0, 0] > 178) && (coordinates[0, 0] < 356) && (coordinates[0, 1] > 112) && (coordinates[0, 1] < 224))
            {
                return 4;  // Show ASCII
            }
            else if ((coordinates[0, 0] > 356) && (coordinates[0, 0] < 534) && (coordinates[0, 1] > 112) && (coordinates[0, 1] < 224))
            {
                return 5; //Background light off
            }//-----------Thrith Row--------
            else if ((coordinates[0, 0] > 0) && (coordinates[0, 0] < 178) && (coordinates[0, 1] > 224) && (coordinates[0, 1] < 336))
            {
                return 6;  //Show Animation
            }
            //else if ((coordinates[0, 0] > 178) && (coordinates[0, 0] < 356) && (coordinates[0, 1] > 270) && (coordinates[0, 1] < 330))
            //{
            //    return 3;
            //}
            //else if ((coordinates[0, 0] > 356) && (coordinates[0, 0] < 534) && (coordinates[0, 1] > 270) && (coordinates[0, 1] < 330))
            //{
            //    return 3;
            //}
            return -1;
        }
        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            int sleepTime = 1000;
            int testTimeOut = MAX_TC_COMPLETION_TIME * 10 ;
            int bitMap = 11;
            string strToShowInDisplay = "It Works!";
            DisplayAction displayAction = DisplayAction.CLEAR_SCREEN;
            animationInfo.Clear();
            animationInfo.Add(Resources.screens1);
            animationInfo.Add(Resources.screens2);
            animationInfo.Add(Resources.screens3);
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);
            CTestManager.ShowUserAnimationInfo(animationInfo);
            CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.CLICK_ON_THE_PICTURE_REPRESENTING_VUT_DISPLAY, Color.DarkBlue);
            CTestManager.ShowFailPnlVutDoesntRespond(new Point(260, 230));
            while ((testTimeOut > 0) && !CTestManager.TestExecSuspended)
            {
                while (!CVut.HmiDisplayActionExecutor((int)DisplayAction.CLEAR_SCREEN, bitMap, strToShowInDisplay, ref executionResultMsg) && !CTestManager.TestExecSuspended)
                {
                    Log.Debug(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Screen could not be cleared " + displayAction + " " + executionResultMsg);
                    System.Threading.Thread.Sleep(300);
                }
                if ((displayAction = GetNewDisplayAction()) == DisplayAction.ALL_TESTED)
                {
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " All display actions has been tested " + executionResultMsg);
                    CTestManager.StopUserAnimationInfo();
                    CTestManager.StopShowingInfoInUserIndicationLabel();
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    CTestManager.DestroyFailPnlVutDoesntRespond();
                    return true;
                }              
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Screen cleared " + executionResultMsg);
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Next Action: " + displayAction.ToString());
                do
                {
                    System.Threading.Thread.Sleep(sleepTime);
                    CVut.HmiDisplayActionExecutor((int)displayAction, bitMap, strToShowInDisplay, ref executionResultMsg);
                    Log.Debug(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Display action executed " + displayAction.ToString() + " " + executionResultMsg);
                    testTimeOut = testTimeOut - sleepTime;

                } while ((testTimeOut > 0) && !CTestManager.TestExecSuspended && (GetUserSelectedScreenRepresentation() != displayAction));
                if (GetUserSelectedScreenRepresentation() == displayAction)
                 Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Display Action: " + displayAction.ToString() + " " + executionResultMsg);
            }
            CTestManager.StopUserAnimationInfo();
            if (testTimeOut <= 0)
            {
                executionResultMsg = Msgs.Error.TIME_OUT_OCURRED;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg);
            }
            else if (CTestManager.TestExecSuspended)
            {
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + Msgs.Error.VUT_FAIL_TO_REACT_ON_USER_INTERACTION);
                executionResultMsg = Msgs.Error.VUT_FAIL_TO_REACT_ON_USER_INTERACTION;
            }
            CTestManager.DestroyFailPnlVutDoesntRespond();
            CTestManager.StopUserAnimationInfo();
            CTestManager.StopShowingInfoInUserIndicationLabel();
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return false;
        }
    }
}
