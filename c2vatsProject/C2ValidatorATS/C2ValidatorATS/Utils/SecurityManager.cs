﻿/**
 * Class: SecurityManager
 * This class take care of all actions regarding encryption/decryption and other security related functions
 * Author: Leonardo Garcia, PayComplete
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2ValidatorATS.Utils
{
    class SecurityManager
    {
        
        public SecurityManager(byte[] srcToGenerateKey)
        {
            GenerateKey(ref srcToGenerateKey); 
        }


        static ushort[] CipherKey = new ushort[4];
        static ushort[] AuxKey = new ushort[4];

        //Hash the Source Data used to generate the Cipher key.
        //Hasing and Cipher algorith got to be the same as in the Validator FW
        static ushort KeyHasher(ushort[] data)
        {
            ushort A = data[3];
            ushort B = data[2];
            ushort C = data[1];
            ushort D = data[0];

            A = (ushort)(B + RotateLeft((ushort)(((B & C) | (~B & D)) + A), 3));
            D = (ushort)(A + RotateLeft((ushort)(((A & B) | (~A & C)) + D), 5));
            C = (ushort)(D + RotateLeft((ushort)(((D & A) | (~D & B)) + C), 7));
            B = (ushort)(C + RotateLeft((ushort)(((C & D) | (~C & A)) + B), 9));

            A = (ushort)(B + ((B & D) | (C & ~D)) + A);
            D = (ushort)(A + ((A & C) | (B & ~C)) + D);
            C = (ushort)(D + ((D & B) | (A & ~B)) + C);
            B = (ushort)(C + ((C & A) | (D & ~A)) + B);

            A = (ushort)(B + (B + C + D) + A);
            D = (ushort)(A + (A + B + C) + D);
            C = (ushort)(D + (D + A + B) + C);
            B = (ushort)(C + (C + D + A) + B);

            A = (ushort)(B + (C + (B | ~D)) + A);
            D = (ushort)(A + (B + (A | ~C)) + D);
            C = (ushort)(D + (A + (D | ~B)) + C);
            B = (ushort)(C + (D + (C | ~A)) + B);

            return B;
        }

        static ushort RotateLeft(ushort data, int steps)
        {
           return (ushort) ((data << steps) | (data >> (16 - steps)));
        }
        
        //Generate the Key base on source data.
        void GenerateKey(ref byte[] sourceData)
        {
            ushort[] srcDataRearangeInTwoBytes = new ushort[7];

            for (int i = 0; i < 7; i++)
            {
                srcDataRearangeInTwoBytes[i] = (ushort)(sourceData[i * 2] | (sourceData[i * 2 + 1] << 8));
            }

            for (int i = 0; i < 4; i++)
            {
                ushort[] aux = new ushort[] { srcDataRearangeInTwoBytes[i], srcDataRearangeInTwoBytes[i + 1], srcDataRearangeInTwoBytes[i + 2], srcDataRearangeInTwoBytes[i + 3]};
                CipherKey[i] = KeyHasher(aux);
               
            }
        }

        public void Decrypt(ref byte[] data, int arrayOffset)
        {
            CipherProces(ref data, arrayOffset, true); //True = Decrypt
        }

        public void Encrypt(ref byte[] data, int arrayOffset)
        {
            CipherProces(ref data, arrayOffset, false); //False = Encrypt
        }

        private void CipherProces(ref byte[] srcData, int arrayOffset, bool decrypt) //True = Decrypt
        {
		    ushort srcDataInTwoBytes; //srcData (8 bit array) is copied into srcDataInTwoBytes(16 bit array)
            ushort xoredResult;  //The result of the Decription or Encryption

		    for (int i = 0; i<4; i++)
            {
                AuxKey[i] = CipherKey[i];
            }

            for (int i = arrayOffset; i < srcData.Count(); i += 2)
		    {  
			    srcDataInTwoBytes = (ushort)((srcData[i] << 8) | srcData[i + 1]);
			    xoredResult = (ushort)(srcDataInTwoBytes ^ KeyHasher(AuxKey));
                AuxKey[0] ^= AuxKey[3]; 
                for (int j = 3; j > 0; j--)
                {
                    AuxKey[j] = AuxKey[j - 1];
                }
                AuxKey[0] = decrypt ? srcDataInTwoBytes : xoredResult;
                srcData[i] = (byte)(xoredResult >> 8);
                srcData[i + 1] = (byte)xoredResult;
		    }
        }
    }
}
