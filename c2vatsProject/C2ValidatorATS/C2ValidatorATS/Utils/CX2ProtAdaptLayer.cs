﻿/**
 * Class: CX2ProtAdaptLayer
 * Singletone class that serves as protocol adaptation and make it possible the use of CX2 library implemented in C++
 * This is a simplified version(with some changes) to the CX2Wrappers implemented in flute, cute projects
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace C2ValidatorATS.Utils
{
    static class CX2ProtAdaptLayer
    {
        public const int RESPONSE_ACK = 0;
        public enum CXB_RC : byte
        {
            CXB_RC_NOT_INITIALIZED,
            CXB_RC_IDLE,
            CXB_RC_SUCCESS,
            CXB_RC_UPLOAD_IN_PROGRESS,
            CXB_RC_COMM_FAILURE,
            CXB_RC_BOARD_UNREACHABLE,
            CXB_RC_FILE_NOT_FOUND,
            CXB_RC_FILE_MALFORMED,
            CXB_RC_FILE_NOT_SUPPORTED,
            CXB_RC_BOOTLOADER_NOT_FOUND,
            CXB_RC_FLASH_UNLOCK_FAILURE,
            CXB_RC_FILE_TRANSFER_ERROR,
            CBB_RC_FLASH_ERROR,
            CXB_RC_COMPLETED,
            CXB_RC_GENERIC_ERROR
        }
        public static bool StartSession(byte comPortIndex, byte devTypeId) 
        {
            byte responseDeviceId = 0;

            if (CX2_StartSession(comPortIndex, ref responseDeviceId, devTypeId))
                {
                    Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Info.DEVICE_OBJECT_REPRESENTATION_CREATED);
                    return true;
                }
            Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Error.FAIL_TO_CREATE_OBJECT_REPRESENTATION_FOR_THIS_VUT);
            return false;
        }
       

        public static bool EndSession(byte deviceId)
        {
            if (deviceId != (byte)0xFF)
            {
                if (CX2_EndSession(deviceId))
                {
                    Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Info.DEVICE_OBJECT_DEALLOCATION_DONE);
                    return true;
                }
                Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Error.FAIL_TO_DEALLOCATE_DEVICE_OBJECT);
            }

            return false;
        }

        public static bool PingDevice(byte deviceId)
        {
            bool responseValue = false;
            byte returnedCode = 1; //returnedCode = 0 if the device was successfully executed
            //Sometimes the pinging does not work even if the device is connected
            int pingAttempt = 0; 
            try
            {
                //Clean the buffer
                if (CX2_Purge())
                {
                    // call to Cx2Lib
                    while (((responseValue = CX2_PingDevice(deviceId, ref returnedCode)) == false) && (pingAttempt < 3))
                    {
                        Logger.instance.Debug("CX2ProtAdaptLayer-->", "Pinging device. Attempt " + pingAttempt);
                        pingAttempt++;
                    }

                    if ((returnedCode == RESPONSE_ACK) && pingAttempt < 3 )
                    {
                        Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Info.PINGING_RESULT_OK);
                        return true;
                    }
                    else if (pingAttempt == 3)
                    {
                        Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Error.NO_ABLE_TO_PING_THE_VALIDATOR + " Pinging Dev " + deviceId);
                        return false;
                    }
                }
                else Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Error.CLEANING_BUFFER_FAIL);
            }
            catch (Exception e)
            {
                Logger.instance.Debug("CX2ProtAdaptLayer-->", "Exception Thrown while attempting to ping.");
                Logger.instance.Debug("CX2ProtAdaptLayer-->", "Stack Trace = " + e.StackTrace);
                Logger.instance.Debug("CX2ProtAdaptLayer-->", "Exception Message = " + e.Message);
                Debug.WriteLine(e.StackTrace.ToString());
                Debug.WriteLine(e.Message.ToString());
            }
            return false;
        }

        public static bool ResetDevice(byte deviceId)
        {

            byte returnedCode = 0;

            if ((CX2_ResetDevice(deviceId, ref returnedCode)) && (returnedCode == RESPONSE_ACK))
            {
                Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Info.DEVICE_RESET_OK + " DeviceId = " + deviceId);
                return true;
            }
            Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Error.FAIL_TO_RESET_DEVICE + " DeviceId = " + deviceId);
            return false;

        }

        public static bool PollDevice(byte deviceId, out byte[] responseData)
        {
            bool responseValue = false;
            byte retunedCode = 0;

            byte[] cx2_payload_buffer = new byte[256];
            byte length = 0;
            responseData = null;

            unsafe
            {
                fixed (byte* ptrPayload = cx2_payload_buffer)
                {
                    responseValue = CX2_PollDevice(deviceId, ptrPayload, ref length, ref retunedCode);
                }
            }

            if ((responseValue) && (retunedCode == RESPONSE_ACK))
            {
                responseData = new byte[length];
                Array.Copy(cx2_payload_buffer, responseData, length);
                Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Info.DEVICE_POLL_RESPONSE_OK);
                return true;
            }
            Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Error.FAIL_TO_POLL_DEVICE + "Poll response data = " + responseData);
            return false;

        }

        public static bool GetValue(byte deviceId, byte[] getValueId, out byte[] responseData)
        {
            byte retunedCode = 0;
            responseData = null;
            byte[] requestCmdPayload = new byte[256];
            byte length = (byte)getValueId.Length;

            Array.Copy(getValueId, requestCmdPayload, length);

            unsafe
            {
                fixed (byte* requestCmd = requestCmdPayload)
                {
                    if (CX2_GetValue(deviceId, requestCmd, ref length, ref retunedCode) && (retunedCode == RESPONSE_ACK))
                    {
                        responseData = new byte[length];
                        Array.Copy(requestCmdPayload, responseData, length);
                        Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Info.RESPONSE_DATA_CORRECTLY_RECEIVED_AFTER_GET_VALUE_CMD);
                        return true;
                    }
                }
            }

            Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Error.FAIL_TO_RECEIVE_CORRECT_RESPONSE_AFTER_SENDING_GET_VALUE + "Response = " + responseData);
            return false;
        }

        public static bool SetValue(byte deviceId, byte[] setValueId)
        {
            byte retunedCode = 0;

            byte[] requestCmdPayload = new byte[256];
            byte length = (byte)setValueId.Length;

            Array.Copy(setValueId, requestCmdPayload, length);

            unsafe
            {
                fixed (byte* requestCmd = requestCmdPayload)
                {
                    if ((CX2_SetValue(deviceId, requestCmd, ref length, ref retunedCode)) && (retunedCode == RESPONSE_ACK))
                    {
                        Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Info.SET_VALUE_CMD_CORRECTLY_EXECUTED);
                        return true;
                    }
                }
            }
            Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Error.FAIL_TO_SEND_SETVALUE_CMD + "Response is : " + retunedCode);
            return false;
        }

        public static bool ExecFunction(byte deviceId, byte[] execFunctionId, out byte[] responseData)
        {
            byte retunedCode = 0;
            responseData = null;

            byte[] requestCmdPayload = new byte[256];
            Array.Copy(execFunctionId, requestCmdPayload, execFunctionId.Length);
            byte length = (byte)execFunctionId.Length;

            unsafe
            {
                fixed (byte* requestCmd = requestCmdPayload)
                {
                    if ((CX2_ExecFunction(deviceId, requestCmd, ref length, ref retunedCode)) && (retunedCode == RESPONSE_ACK))
                    {
                        Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Info.EXECUTE_CMD_CORRCTLY_EXECUTED);
                        responseData = new byte[length];
                        Array.Copy(requestCmdPayload, responseData, length);
                        Logger.instance.Debug("CX2ProtAdaptLayer-->", "Response = " + responseData);
                        return true;
                    }
                }
            }

            Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR + " returnde Code = " + retunedCode);
            return false;
        }

        public static bool ReadTable(byte deviceId, byte[] tableId, out byte[] responseData)
        {
  
            byte retunedCode = 0;
            responseData = null;

            byte[] requestCmdPayload = new byte[256];
            Array.Copy(tableId, requestCmdPayload, tableId.Length);
            byte length = (byte)tableId.Length;

            unsafe
            {
                fixed (byte* requestCmd = requestCmdPayload)
                {
                   
                    if ((CX2_ReadTable(deviceId, requestCmd, ref length, ref retunedCode)) && (retunedCode == RESPONSE_ACK))
                    {
                        Logger.instance.Info("CX2ProtAdaptLayer-->", Msgs.Info.READTABLE_CMD_CORRECTLY_EXECUTED);
                        responseData = new byte[length];
                        Array.Copy(requestCmdPayload, responseData, length);
                        return true;
                    }
                }
            }
            Logger.instance.Debug("CX2ProtAdaptLayer-->", Msgs.Error.FAIL_TO_SEND_CMD_READTABLE_TO_VALIDATOR + " returnde Code = " + retunedCode);
            return false;
        }

        public static bool WriteTable(byte deviceId, byte[] requestData, out byte[] responseData)
        {
            byte returnedCode = 0;
            responseData = null;

            byte[] requestCmdPayload = new byte[256];
            Array.Copy(requestData, requestCmdPayload, requestData.Length);
            byte length = (byte)requestData.Length;

            unsafe
            {
                fixed (byte* requestCmd = requestCmdPayload)
                {
                    if ((CX2_WriteTable(deviceId, requestCmd, ref length, ref returnedCode)) && (returnedCode == RESPONSE_ACK))
                    {
                        Logger.instance.Info("CX2ProtAdaptLayer-->", "Data written to table " + requestCmdPayload[0].ToString() + " " + Msgs.Info.WRITETABLE_CMD_CORRECTLY_EXECUTED);
                        responseData = new byte[length];
                        Array.Copy(requestCmdPayload, responseData, length);
                        return true;
                    }
                }
            }

            Logger.instance.Info("CX2ProtAdapterLayer-->", Msgs.Error.FAIL_TO_SEND_CMD_WRITETABLE_TO_VALIDATOR + " returned Code = " + returnedCode);
            return false;
        }

        public static bool SetAccessLevel(byte deviceId, byte[] requestData, out byte[] responseData)
        {
            byte returnedCode = 0;
            responseData = null;

            byte[] requestCmdPayload = new byte[256];
            Array.Copy(requestData, requestCmdPayload, requestData.Length);
            byte length = (byte)requestData.Length;

            unsafe
            {
                fixed (byte* requestCmd = requestCmdPayload)
                {
                    if((CX2_SetAccLevel(deviceId, requestCmd, ref length, ref returnedCode)) && (returnedCode == RESPONSE_ACK))
                    {
                        Logger.instance.Info("CX2ProtAdaptLayer-->", "Changing Table Access, " + " " + Msgs.Info.SET_ACCESS_LEVEL_CMD_CORRECTLY_EXECUTED);
                        responseData = new byte[length];
                        Array.Copy(requestCmdPayload, responseData, length);
                        return true;
                    }
                }
            }
            Logger.instance.Info("CX2ProtAdapterLayer-->", Msgs.Error.FAIL_TO_SEND_CMD_SET_ACCESS_LEVEL_TO_VALIDATOR + " returned Code = " + returnedCode);
            return false;
        }

        public static bool GetAccessLevel(byte deviceId, out byte[] responseData)
        {
            byte returnedCode = 0;
            responseData = null;

            byte[] requestCmdPayload = new byte[256];
            byte length = 0;

            unsafe
            {
                fixed (byte* payload_ptr = requestCmdPayload)
                {
                    if (CX2_GetAccLevel(deviceId, payload_ptr, ref length, ref returnedCode) && (returnedCode == RESPONSE_ACK))
                    {
                        Logger.instance.Info("CX2ProtAdaptLayer--> ",  Msgs.Info.GET_ACCESS_LEVEL_CMD_CORRECTLY_EXECUTED);
                        responseData = new byte[length];
                        Array.Copy(requestCmdPayload, responseData, length);
                        return true;
                    }
                }
            }
            Logger.instance.Info("CX2ProtAdapterLayer-->", Msgs.Error.FAIL_TO_SEND_CMD_GET_ACCESS_LEVEL_TO_VALIDATOR + " returned Code = " + returnedCode);
            return false;

        }

        public static bool GetVutIdentificationInfo(byte deviceId, byte requestedInfo, out byte[] responseData)
        {
            byte returnedCode = 0;
            responseData = null;
            byte[] requestCmdPayload = new byte[256];
            requestCmdPayload[0] = requestedInfo;
            byte length = 0;

            unsafe
            {
                fixed (byte* payload_ptr = requestCmdPayload)
                {
                    if (CX2_GetIdInfo(deviceId, payload_ptr, ref length, ref returnedCode) && (returnedCode == RESPONSE_ACK))
                    {
                        Logger.instance.Info("CX2ProtAdaptLayer--> ", Msgs.Info.GET_VUT_IDENTIFICATION_INFO_CMD_CORRECTLY_EXECUTED);
                        responseData = new byte[length];
                        Array.Copy(requestCmdPayload, responseData, length);
                        return true;
                    }
                }
            }
            Logger.instance.Info("CX2ProtAdapterLayer-->", Msgs.Error.FAIL_TO_SEND_CMD_GET_VUT_IDENTIFICATION_INFO_TO_VALIDATOR + " returned Code = " + returnedCode);
            return false;
        }

        public static bool FWFlash(byte deviceId, string fwFileName)
        {
            byte returnedCode = 0;


            if (CXB_FwUpload_StartSession(deviceId, fwFileName, ref returnedCode) != 0)// && (returnedCode == (byte)CXB_RC.CXB_RC_SUCCESS))
            {
                return true;
            }
            else
            {
                Logger.instance.Info("CX2ProtAdapterLayer-->", "Failed to uppload the FW");
                return false;
            }
        }

        public static bool GetFWFlashingProcessStatus(byte deviceId, ref byte elapsedPercentage)
        {
            bool inProgress = false;
            byte returnedCode = 0;


            CXB_FwUpload_GetStatus(deviceId, ref elapsedPercentage, ref returnedCode);
            if (returnedCode == (byte)CXB_RC.CXB_RC_UPLOAD_IN_PROGRESS)
            {
                inProgress = true;
            }
            else if (returnedCode == (byte)CXB_RC.CXB_RC_COMPLETED)
            {
                inProgress = false;
                
            }
            else
            {

                Logger.instance.Info("CX2ProtAdapterLayer-->", "Failed to uppload the FW");
            }

            return inProgress;
        }

        public static bool EndFWFlash(byte deviceId)
        {
            if (CXB_FwUpload_EndSession(deviceId) != 0)
            {
                return true;
            }
            return false;
        }

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern bool CX2_StartSession(byte a_ComPort, ref byte pa_DeviceID, byte a_Device);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern bool CX2_EndSession(byte pa_DeviceID);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern bool CX2_PingDevice(byte a_DeviceID, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern bool CX2_Purge();

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private unsafe static extern bool CX2_ResetDevice(byte a_DeviceID, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private unsafe static extern bool CX2_PollDevice(byte a_DeviceID, byte* pa_Data, ref byte pa_DataLen, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private unsafe static extern bool CX2_GetValue(byte a_DeviceID, byte* pa_Data, ref byte pa_DataLen, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private unsafe static extern bool CX2_SetValue(byte a_DeviceID, byte* pa_Data, ref byte pa_DataLen, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private unsafe static extern bool CX2_ExecFunction(byte a_DeviceID, byte* pa_Data, ref byte pa_DataLen, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private unsafe static extern bool CX2_ReadTable(byte a_DeviceID, byte* pa_Data, ref byte pa_DataLen, ref byte pa_RetCode);
        //Write data table como este ya lo cambieare por mi propio solucion
        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private unsafe static extern bool CX2_WriteTable(byte a_DeviceID, byte* pa_Data, ref byte pa_DataLen, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private unsafe static extern bool CX2_GetAccLevel(byte a_DeviceID, byte* pa_Data, ref byte pa_DataLen, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private unsafe static extern bool CX2_SetAccLevel(byte a_DeviceID, byte* pa_Data, ref byte pa_DataLen, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private unsafe static extern bool CX2_GetIdInfo(byte a_DeviceID, byte* pa_Data, ref byte pa_DataLen, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CXB_FwUpload_StartSession(byte a_DeviceID, string a_filename, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CXB_FwUpload_GetStatus(byte a_DeviceID, ref byte pa_Percentage, ref byte pa_RetCode);

        [DllImport("CX2Lib.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CXB_FwUpload_EndSession(byte a_DeviceID);
    }
}
