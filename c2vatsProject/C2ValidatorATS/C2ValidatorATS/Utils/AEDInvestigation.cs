﻿/**
 * 
 * This class was exclusively created to used during the AED fake coins investigation
 * Now it keeps same name but it is used for all statistics investigation
 * Author: Leonardo
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using C2ValidatorATS.Vut.VutDBlocksInfo;

namespace C2ValidatorATS.Utils
{
    class AEDInvestigation
    {
        static readonly AEDInvestigation _instance = new AEDInvestigation();
        public static AEDInvestigation Instance
        {
            get
            {
                return _instance;
            }
        }
        //public List<byte> mesuredData;
        private List<List<byte>> dataCollector = null;
        private List<List<int>> failedMeasPosition = null; 
        private byte[] upperLimits = new byte[24];
        private byte[] lowerLimits = new byte[24]; 
        private bool fakeCoin = false;
        private bool newCoin = false;

        public string coinType {get; set;}
        public string fakeCoinNr {get; set;}
        public string valNr {get; set;}
        public bool statisticsActivated { get; set; }
        public bool fakeCoinStatistics { get; set; }

        string DefaultLogPath;
        ResultData resultData;
        List<ResultData> result4Analisys = new List<ResultData>();
        List<string> result2File = new List<string>();
        List<string> usedCoins = new List<string>();
        private double fails = 0;

        public struct ResultData
        {
           public string coinType;
            public int measNr;
            public int mediam;
            public int avg;
            public int max;
            public int min;

            public ResultData(string ct, int mNr, int md, int av, int mx, int mn)
            {
                coinType = ct;
                measNr = mNr;
                mediam = md;
                avg = av;
                max = mx;
                min = mn;
            }
        }

        public AEDInvestigation()
        {
            //DefaultLogPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Coin Statistics\\";
            DefaultLogPath = "c:\\PayComplete\\C2VATS\\Coin Statistics\\";
            Directory.CreateDirectory(DefaultLogPath);
            statisticsActivated = false;
            fakeCoinStatistics = false;
        }

        public  string[] GetFakeCoinNr()
        {
            return new string[] { "Fake Coin 1", "Fake Coin 2", "Fake Coin 3", "Fake Coin 4", "Fake Coin 5", "Fake Coin 6", "Fake Coin 7", "Fake Coin 8", "Fake Coin 9", "Fake Coin 10" };
        }

        public  string[] GetCoinNr()
        {
            return new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "Mix" };
        }
        public  string[] GetValNr()
        {
            return new string[] { "Val 1", "Val 2", "Val 3", "Val 4", "Val 5", "Val 6", "Val 7", "Val 8", "Val 9", "Val 10", "Val 11", "Val 12" };
        }

        public void nullifyValues()  //llamado desde el panel cusndo se clicka stop
        {
            dataCollector = null;
            dataCollector = new List<List<byte>>();
            failedMeasPosition = new List<List<int>>();
            fails = 0;
        }

        public void storeLimits(byte[] lowerLimits, byte[] upperLimits)
        {
            this.lowerLimits = lowerLimits;
            this.upperLimits = upperLimits;
        }
        public void StoreMeasuredData(List<byte> measuredData, bool measureFail, List<int> failedMeasurmentPos) //Llamado desde el Testcase
        {
            List<int> failedColumns = new List<int> { };
            dataCollector.Add(measuredData);
            if (measureFail)
            { 
                fails++;
                for (int i = 0; i < failedMeasurmentPos.Count(); i+=2)
                {
                    if (failedMeasurmentPos[i + 1] == 1)
                        failedColumns.Add(failedMeasurmentPos[i]);                  
                }
                failedMeasPosition.Add(failedColumns);
            }
            else failedMeasPosition.Add(new List<int> {});

            //if(coinType == "AED 1 AED Rev B" || coinType == "OMR50 NM" )//Check 
            //{
            //    if (measuredData[22] > 189 || measuredData[22] < 116 || measuredData[23] > 250 || measuredData[23] < 220)
            //    {
            //        fails++;
            //    }
            // }

            //if(coinType == "AED REVB" || coinType ==  "PKR 5" || coinType == "OMR50 NM" )//Check 
            //{
            //    if (measuredData[22] > 195 || measuredData[22] < 125)//
            //    {
            //        fails++;
            //    }
            //}
            //if (coinType == "AED REVC" || coinType == "OMR50 M" || coinType == "PHP 1")//Check 
            //{
            //    if (measuredData[16] > 155 || measuredData[16] < 80)
            //    {
            //        fails++;
            //    }
            //}
        }
        private bool IsMeasurementFail(int row, int measNr)
        {
               for(int i = 0; i < failedMeasPosition[row].Count; i++)
               {
                   if(failedMeasPosition[row][i] == measNr)
                       return true;
               }
               return false;
        }
        public void Save2File()
        {
            List<string> dataToFile = new List<string>();
            string infoString = null;
            double acceptancePercentage = 0;
            string titelString = null;
            string insertionNrThatFail = "";
            VutIdentificationData CVutIdentificationData = new VutIdentificationData();
            CVutIdentificationData.RetreiveAllData();
            acceptancePercentage = ((dataCollector.Count - fails) * 100) / dataCollector.Count;
            acceptancePercentage = Math.Round(((acceptancePercentage)));

            if (fakeCoinStatistics) titelString = "FAKE Coin " + fakeCoinNr + " VS " + coinType + " ";
            else titelString = coinType;
            titelString += "   Nr Insertions = " + dataCollector.Count + ", Validator = " + valNr + ",  FW ver " + CVutIdentificationData.FirmwareVersion;
            dataToFile.Add(titelString);
            //Rows that fails
            for (int i = 0; i < failedMeasPosition.Count(); i++) 
            {
                if (failedMeasPosition[i].Count() != 0)
                    insertionNrThatFail += (i + 1) + ", " ; 
            }
                
            dataToFile.Add(" ");  //space
            dataToFile.Add(" COIN ACCEPTANCE = " + acceptancePercentage + " %" + "       " + fails + " Fails out of " + dataCollector.Count + " Insertions"
                + (insertionNrThatFail != "" ? "    Failing Insertions : " + insertionNrThatFail : ""));
            dataToFile.Add(" ");  //space

            for (int i = 0; i < dataCollector.Count; i++)  //en cada grupo de medicion
            {
                for (int k = 0; k < dataCollector[i].Count; k++)//dentro de cada medicion en cada medida
                {
                    infoString += (k + 1) + ":" + (IsMeasurementFail(i, k) ? "**" : " ") + dataCollector[i][k].ToString() + ",  ";
                    //dataToFile.Add(k + ": " + dataCollector[i][k].ToString());
                }
                dataToFile.Add((i + 1) +"- "  + infoString);
                infoString = "";
            }
            dataToFile.Add(" ");  //space

            for (int i = 0; i < 24; i++)
                dataToFile.Add(PrepareResultData(i)); //Final information

            dataToFile.Add(" ");  //space
            dataToFile.Add(" ");  //space

            dataToFile.Add(" Measurement Limits:");
            
            string upLimitsInfo = "";
            string loLimitsInfo = "";
            for(int i = 0; i < 24; i ++)
            {
                upLimitsInfo += (i + 1) + ": " + upperLimits[i].ToString() + ",  ";
                loLimitsInfo += (i + 1) + ": " + lowerLimits[i].ToString() + ",  ";
            }
            dataToFile.Add(upLimitsInfo);
            dataToFile.Add(loLimitsInfo);
            //Save to file

            File.WriteAllLines(DefaultLogPath + GetLogFileName(), dataToFile);
            
            nullifyValues();
        }
        //No used for the moment
        //public void SaveResultToFile() //llamado desde el panel
        //{
        //    List<string> dataToFile = new List<string>();
        //    string infoString = null;
        //    string titelString = null;
        //    bool coinAlradyInList = false;
        //    for(int i = 0; i < usedCoins.Count(); i++)
        //    {
        //        if (usedCoins[i] == coinType) //if already exist then exit the loop
        //        {
        //            coinAlradyInList = true;
        //            break;
        //        }
        //    }
        //    if (!coinAlradyInList)
        //    {
        //        usedCoins.Add(coinType);
        //    }

        //    if (!coinType.Contains("AED")) titelString = "FAKE Coin " + coinType;
        //    else titelString = "AED Coin ";
        //    titelString += "  Coin Number = " + fakeCoinNr + "  " + " Nr Measurments = " + dataCollector.Count + " Validator Nr = " + valNr;
        //    dataToFile.Add(titelString);
        //    dataToFile.Add(" ");  //space

        //    for(int i = 0; i < dataCollector.Count; i++)  //en cada grupo de medicion
        //    {
        //        for (int k = 0; k < dataCollector[i].Count; k++)//dentro de cada medicion en cada medida
        //        {
        //            infoString += (k + 1) + ": " + dataCollector[i][k].ToString() + ",  ";
        //            //dataToFile.Add(k + ": " + dataCollector[i][k].ToString());
        //        }
        //        dataToFile.Add(infoString);
        //        infoString = "";
        //    }
        //    dataToFile.Add(" ");  //space
            
        //    for (int i = 0; i < 24; i++ )
        //        dataToFile.Add(PrepareResultData(i)); //Final information
        //    //Save to file
        //    File.WriteAllLines(DefaultLogPath + GetLogFileName(), dataToFile);
        //}

        //public void saveTotalResult2File()
        //{
        //    result2File.Add(" "); //Space
        //    Analyze();
        //    File.WriteAllLines(DefaultLogPath + GetTotalResultFileName(), result2File);
        //    result2File = null;
        //    result2File = new List<string>();
        //}

        private void Analyze()
        {
            List<string> avgMediamResults = new List<string>();
            int medianAvg = 0, counter = 0;
            List<int> allMedianAvg = new List<int>();

            for (int i = 0; i < usedCoins.Count(); i++) //Por coins
            {
                result2File.Add(usedCoins[i]);
                for (int meas = 0; meas < 24; meas++)  //Analyze travez decimal las 24 mediciones
                {
                    for (int k = 0 + meas; k < result4Analisys.Count(); k += 24) //a travez de todas la mediciones de 24 2n 24
                    {
                        if (result4Analisys[k].coinType == usedCoins[i])
                        {
                            medianAvg += result4Analisys[k].mediam;
                            counter++;
                        }
                    }
                    allMedianAvg.Add(medianAvg / counter);
                    result2File.Add("M" + (meas + 1) + "  " + usedCoins[i] + "  Medeian Average = " + (medianAvg / counter).ToString());
                    counter = 0;
                    medianAvg = 0;
                }                  
                //avgMediamResults.Add(usedCoins[i] + "  Medeian Average = " +(medianAvg / counter).ToString())              
            }
            calculateGain(allMedianAvg);   
        }

        private void calculateGain(List<int> medians)
        {
            List<string> medianGain = new List<string>();
            result2File.Add("");
            result2File.Add("Medians Gains");
            for (int i = 1; i < usedCoins.Count(); i++)
            {
                result2File.Add("");
                result2File.Add("Gain Analisys AED -- " + usedCoins[i]);
                for (int meas = 0; meas < 24; meas++)
                {
                    //medianGain.Add("M1 = " +  (medians[meas] - medians[meas + 24 * i]).ToString());
                    result2File.Add("M" + (meas + 1) + " = " + (medians[meas] - medians[meas + 24 * i]).ToString());
                }
            }                 
        }

        private string GetLogFileName()
        {
                return ((fakeCoinStatistics) ? "FAKE " + fakeCoinNr + " VS " : "") + coinType + "-Acceptance Statistics-" + valNr + " --"+ DateTime.Now.ToString("yy-MM-dd_HH-mm-ss") + ".txt";       
        }
        private string GetTotalResultFileName()
        {
            return DateTime.Now.ToString("yy-MM-dd_HH-mm-ss") + "_Validator " + valNr + ".txt"; 
        }
        private string PrepareResultData(int measNr)
        {
            string resultInfo = null;
            resultData = new ResultData(coinType, measNr + 1, GetMedian(measNr), GetAverage(measNr), GetMaxValue(measNr), GetMinValue(measNr));
            result4Analisys.Add(resultData);
            result2File.Add("  M" + resultData.measNr + " Median = " + resultData.mediam + "    Avg = " + resultData.avg + "    Max = " + resultData.max
                + "    Min= " + resultData.min);
            return resultInfo = "  M" + resultData.measNr + " Median = " + resultData.mediam + "    Avg = " + resultData.avg + "    Max = " + resultData.max
                + "    Min= " + resultData.min ;
            
        }

        private byte GetMedian(int measNr)
        {
                if ((dataCollector.Count % 2) == 0)
                    return (byte)((dataCollector[(dataCollector.Count / 2) ][measNr]+ dataCollector[(dataCollector.Count / 2 - 1)][measNr]) / 2);
                else return (byte)(dataCollector[dataCollector.Count / 2][measNr]);
        }

        private byte GetAverage(int measNr)
        {
            int summa = 0;
            int among = 0;
            for (int i = 0; i < dataCollector.Count; i++)
            {
                summa += dataCollector[i][measNr];
                among++;
            }
            return (byte)(summa/among);
        }

        private byte GetMinValue(int measNr)
        {  byte minValue = dataCollector[0][measNr];

            for (int i = 1; i < dataCollector.Count; i++)
            {
                if(dataCollector[i][measNr] < minValue)
                    minValue = dataCollector[i][measNr];
                
            }
            return minValue;
        }
        private byte GetMaxValue(int measNr)
        {
            byte maxValue = dataCollector[0][measNr];
            for(int i = 1; i < dataCollector.Count; i++)
            {
                if(dataCollector[i][measNr] > maxValue)
                    maxValue = dataCollector[i][measNr];
            }
            return maxValue;
        }

        public void SetFakeCoin(bool fc)
        {
            fakeCoin = fc; 
        }
    }
}
