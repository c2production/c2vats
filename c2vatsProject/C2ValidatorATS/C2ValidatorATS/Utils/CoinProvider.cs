﻿/**
 * Class: CommAssistant
 * Static Class. Contains class definition for different coins. The coin classses have the limits of every coin as defined int the database.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2ValidatorATS.Utils
{
    static class CoinProvider
    {
        public static string[] GetCoinsPerIso(string iso)
        {
            switch (iso)
            {
                case "EUR": return EuroCoins.GetAllEuroCoins();
                default: return new string[]{};
            }
        }

        public static byte[] GetCoinUpperLimits(string coin, bool RIMpresent)
        {
            if (coin.Contains("Eur"))
            {
                return RIMpresent ? EuroCoins.GetUpperLimitsRIM(coin): EuroCoins.GetUpperLimits(coin) ;
            }
            else if (coin.Contains("CHF"))
            {
                return RIMpresent ? SwissFrancCoins.GetUpperLimitsRIM(coin) :SwissFrancCoins.GetUpperLimits(coin);
            }
            else if (coin.Contains("AED"))
            {
                return EmiratesCoins.GetUpperLimits(coin);
            }
            else if (coin.Contains("Grosz" ) || coin.Contains("Zloty"))
            {
                return PolishCoins.GetUpperLimits(coin);
            }
            return new byte[] { };
        }

        public static byte[] GetCoinLowerLimits(string coin, bool RIMpresent)
        {
            if (coin.Contains("Eur"))
            {
                return RIMpresent ? EuroCoins.GetLowerLimitsRIM(coin) : EuroCoins.GetLowerLimits(coin);
            }
            else if (coin.Contains("CHF"))
            {
                return RIMpresent ? SwissFrancCoins.GetLowerLimitsRIM(coin) : SwissFrancCoins.GetLowerLimits(coin);
            }
            else if (coin.Contains("AED"))
            {
                return EmiratesCoins.GetLowerLimits(coin);
            }
            else if (coin.Contains("Grosz") || coin.Contains("Zloty"))
            {
                return PolishCoins.GetLowerLimits(coin);
            }
            return new byte[] { };
        }
        /// <summary>
        /// Define ISO 
        /// </summary>
        public class Iso
        {
            public static string EUR = "EUR";
            public static string CHF = "CHF";
            public static string PLN = "PLN";
            public static string AED = "AED";
            public static string[] GetIsos()
            {
                return new string[] { EUR, CHF, AED, PLN };
            }
        }
        /// <summary>
        /// Describe upper and Lower limits for 5 eur, 1 eur and 10 cent eur
        /// </summary>
        public class EuroCoins
        {
            public const string ONE_EURO = "1 Eur";
            public const string TWO_EURO = "2 Eur";
            public const string TEN_CENT_EURO = "10 cent Eur";
            public const string TWENTY_CENT_EURO = "20 cent Eur";
            public const string FIFTY_CENT_EURO = "50 cent Eur";
            public const string ONE_CENT_EURO = "1 cent Eur";
            public const string TWO_CENT_EURO = "2 cent Eur";
            public const string FIVE_CENT_EURO = "5 cent Eur";
            // formula 0
            public static byte[] OneCentEuroUpperLimits = { 43, 126, 107, 138, 179, 160, 187, 255, 255, 102, 104, 107, 105, 255, 255, 234, 255, 255, 255, 255, 255, 255, 255, 255 };//Cdataset 336
            public static byte[] TwoCentEuroUpperLimits = { 83, 46, 61, 93, 101, 108, 147, 255, 255, 117, 121, 107, 109, 255, 255, 235, 255, 255, 255, 255, 255, 255, 255, 255 };//Cdataset 337
            public static byte[] FiveCentEuroUpperLimits = { 113, 26, 39, 67, 58, 77, 103, 255, 255, 116, 123, 112, 103, 255, 219, 239, 255, 255, 255, 255, 255, 255, 255, 255 };//338
            public static byte[] TenCentEuroUpperLimits = { 95, 26, 70, 197, 55, 86, 207, 68, 64, 254, 255, 9, 99, 255, 255, 133, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] TwentyCentsEuroUpperLimits = { 124, 16, 59, 181, 31, 65, 184, 67, 63, 240, 255, 6, 99, 255, 255, 123, 255, 255, 255, 255, 255, 255, 255, 255 };//Cdataset 317
            public static byte[] FiftyCentsEuroUpperLimits = { 142, 10, 45, 161, 24, 49, 160, 67, 62, 230, 250, 6, 98, 237, 255, 117, 255, 255, 255, 255, 255, 255, 255, 255 };//Cdataset 333
            public static byte[] OneEuroUpperLimits = { 133, 18, 71, 181, 30, 73, 189, 130, 107, 124, 151, 54, 255, 255, 255, 177, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] TwoEuroUpperLimits = { 155, 15, 38, 158, 30, 58, 177, 94, 84, 242, 242, 55, 143, 255, 255, 178, 255, 255, 255, 255, 255, 255, 255, 255 };

            public static byte[] OneCentEuroLowerLimits = { 28, 45, 52, 93, 126, 114, 138, 156, 118, 15, 22, 78, 3, 213, 232, 153, 0, 0, 0, 0, 0, 0, 0, 0 };//Cdataset 336
            public static byte[] TwoCentEuroLowerLimits = { 71, 10, 14, 56, 47, 56, 94, 149, 109, 11, 18, 78, 0, 210, 216, 153, 0, 0, 0, 0, 0, 0, 0, 0 };//Cdataset 337
            public static byte[] FiveCentEuroLowerLimits = { 101, 1, 2, 33, 22, 28, 70, 155, 110, 13, 19, 79, 0, 186, 162, 154, 0, 0, 0, 0, 0, 0, 0, 0 };// 338
            public static byte[] TenCentEuroLowerLimits = { 82, 4, 39, 154, 33, 59, 158, 25, 19, 159, 173, 0, 57, 215, 235, 74, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] TwentyCentsEuroLowerLimits = {109, 0, 27, 136, 12, 36, 133, 24, 18, 154, 161, 0, 57, 215, 232, 72, 0, 0, 0, 0, 0, 0, 0, 0 }; //Cdataset 317
            public static byte[] FiftyCentsEuroLowerLimits = { 130, 0, 14, 117, 5, 22, 111, 23, 18, 145, 149, 0, 55, 151, 221, 65, 0, 0, 0, 0, 0, 0, 0, 0 };//Cdataset 333
            public static byte[] OneEuroLowerLimits = { 121, 0, 25, 123, 10, 39, 134, 72, 56, 65, 75, 35, 146, 193, 223, 109, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] TwoEuroLowerLimits = { 143, 0, 0, 51, 7, 19, 88, 40, 32, 136, 145, 17, 74, 97, 203, 94, 0, 0, 0, 0, 0, 0, 0, 0 };

            // Upper Limits. formula 8. Based on: OneEuro- Cdataset 334. TwoEuro- Cdataset 320. TenCentEuro- Cdataset 331, 20eur cdataset 332, 50cent Cdataset 318 
            public static byte[] OneCentEuroUpperLimitsRIM = {41, 125,109, 139, 181, 164, 190, 255, 248, 95, 255, 105, 100, 255, 255, 255, 255, 255, 255, 155, 255, 255, 255, 255}; //Cdataset 346
            public static byte[] TwoCentEuroUpperLimitsRIM = { 81, 47, 62, 94, 102, 112, 151, 255, 251, 104, 255, 106, 114, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };//322
            public static byte[] FiveCentEuroUpperLimitsRIM = { 112, 27, 41, 68, 57, 79, 111, 255, 255, 121, 255, 116, 124, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 }; //347
            public static byte[] TenCentEuroUpperLimitsRIM = { 95, 25, 70, 197, 55, 88, 208, 57, 53, 226, 255, 9, 90, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] TwentyCentsEuroUpperLimitsRIM = { 123, 14, 59, 181, 31, 65, 183, 56, 52, 221, 255, 6, 89, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] FiftyCentsEuroUpperLimitsRIM = { 141, 10,	46, 163, 22, 48, 162, 56, 52, 211, 255, 6, 87, 216, 255, 255, 255, 192, 255, 255, 255, 255, 255, 255 };
            public static byte[] OneEuroUpperLimitsRIM = { 132, 16, 73, 180, 30, 74, 191, 119, 95, 114, 255, 54, 255, 254, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] TwoEuroUpperLimitsRIM = { 155, 14, 38, 155, 29, 59, 177, 82, 74, 223, 255, 54, 131, 247, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255};

            //Lower Limits. formula 8. Based on: OneEuro- cdataset 334. TwoEuro- cdataset 320 . TenCentEuro- cdataset 331,  20eur cdataset 332 , 50cent Cdataset 318
            public static byte[] OneCentEuroLowerLimitsRIM = { 29, 48, 54, 95, 128, 115, 141, 171, 130, 24, 0, 78, 9, 244, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };// Cdataset 346
            public static byte[] TwoCentEuroLowerLimitsRIM = { 71, 10, 15, 58, 46, 58, 94, 159, 117, 18, 0, 78, 5, 241, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };//322
            public static byte[] FiveCentEuroLowerLimitsRIM = {103, 0, 1, 31, 20, 24, 67, 146, 109, 12, 0, 78, 0, 211, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };//347
            public static byte[] TenCentEuroLowerLimitsRIM = { 83, 2, 39, 155, 34, 61, 159, 34, 29, 180, 0, 0, 67, 245, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] TwentyCentsEuroLowerLimitsRIM = { 110, 0, 27, 136, 14, 38, 134, 33, 27, 165, 0, 0, 66, 245, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] FiftyCentsEuroLowerLimitsRIM = { 131, 0, 15, 116, 6, 23, 112, 32, 27, 154, 0, 0, 64, 164, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] OneEuroLowerLimitsRIM = {122, 0, 29, 126, 11, 41, 137, 84, 65, 75, 0, 34, 172, 227, 0, 0, 0,	0, 0, 0, 0, 0, 0, 0 };
            public static byte[] TwoEuroLowerLimitsRIM = { 144, 0, 0, 56, 9, 21, 93, 48, 41, 147, 0, 17, 81, 183, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        


            public static string[] GetAllEuroCoins()
            {
                return new string[] { ONE_EURO, TWO_EURO, TEN_CENT_EURO, TWENTY_CENT_EURO, FIFTY_CENT_EURO, ONE_CENT_EURO, TWO_CENT_EURO, FIVE_CENT_EURO }; 
            }

            public static byte[] GetUpperLimits(string euroCoin)
            {
                switch (euroCoin)
                {
                    case ONE_CENT_EURO: return OneCentEuroUpperLimits;
                    case TWO_CENT_EURO: return TwoCentEuroUpperLimits;
                    case FIVE_CENT_EURO: return FiveCentEuroUpperLimits;
                    case TEN_CENT_EURO: return TenCentEuroUpperLimits;
                    case TWENTY_CENT_EURO: return TwentyCentsEuroUpperLimits;
                    case FIFTY_CENT_EURO: return FiftyCentsEuroUpperLimits;
                    case ONE_EURO: return OneEuroUpperLimits;
                    case TWO_EURO: return TwoEuroUpperLimits;
                    default: return new byte[] { };
                }
            }
            public static byte[] GetLowerLimits(string euroCoin)
            {
                switch (euroCoin)
                {
                    case ONE_CENT_EURO: return OneCentEuroLowerLimits;
                    case TWO_CENT_EURO: return TwoCentEuroLowerLimits;
                    case FIVE_CENT_EURO: return FiveCentEuroLowerLimits;
                    case TEN_CENT_EURO: return TenCentEuroLowerLimits;
                    case TWENTY_CENT_EURO: return TwentyCentsEuroLowerLimits;
                    case FIFTY_CENT_EURO: return FiftyCentsEuroLowerLimits;
                    case ONE_EURO: return OneEuroLowerLimits;
                    case TWO_EURO: return TwoEuroLowerLimits;
                    default: return new byte[] { };
                }
            }

            public static byte[] GetUpperLimitsRIM(string euroCoin)
            {
                switch (euroCoin)
                {
                    case ONE_CENT_EURO: return OneCentEuroUpperLimitsRIM;
                    case TWO_CENT_EURO: return TwoCentEuroUpperLimitsRIM;
                    case FIVE_CENT_EURO: return FiveCentEuroUpperLimitsRIM;
                    case TEN_CENT_EURO: return TenCentEuroUpperLimitsRIM;
                    case TWENTY_CENT_EURO: return TwentyCentsEuroUpperLimitsRIM;
                    case FIFTY_CENT_EURO: return FiftyCentsEuroUpperLimitsRIM;
                    case ONE_EURO: return OneEuroUpperLimitsRIM;
                    case TWO_EURO: return TwoEuroUpperLimitsRIM;
                    default: return new byte[] { };
                }
            }
            public static byte[] GetLowerLimitsRIM(string euroCoin)
            {
                switch (euroCoin)
                {
                    case ONE_CENT_EURO: return OneCentEuroLowerLimitsRIM;
                    case TWO_CENT_EURO: return TwoCentEuroLowerLimitsRIM;
                    case FIVE_CENT_EURO: return FiveCentEuroLowerLimitsRIM;
                    case TEN_CENT_EURO: return TenCentEuroLowerLimitsRIM;
                    case TWENTY_CENT_EURO: return TwentyCentsEuroLowerLimitsRIM;
                    case FIFTY_CENT_EURO: return FiftyCentsEuroLowerLimitsRIM;
                    case ONE_EURO: return OneEuroLowerLimitsRIM;
                    case TWO_EURO: return TwoEuroLowerLimitsRIM;
                    default: return new byte[] { };
                }
            }
        }
        // ######
        public class SwissFrancCoins
        {
            public const string FIVE_CENT_CHF = "5 cent CHF";
            public const string TENT_CENT_CHF = "10 cent CHF";
            public const string TWENTY_CENT_CHF = "20 cent CHF";
            public const string FIFTY_CENT_CHF = "50 cent CHF";
            public const string ONE_CHF = "1 CHF";
            public const string TWO_CHF = "2 CHF";  
            public const string FIVE_CHF = "5 CHF"; 

            //No RIM
            //UpperLimits Base on: 5 cent--> datset 1179, 10 cent->dataset 1180, 20 cent->dataset 1181, 50 cent->dataset 1182, 1 chf--> dataset 1183, 2 chf--> dataset 1184,5 chf--> dataset 1185
            public static byte[] FiveCentCHFUpperLimits = { 57, 131, 168, 255, 163, 186, 255, 97, 92, 157, 182, 33, 156, 255, 255, 157, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] TenCentCHFUpperLimits = { 87, 148, 211, 255, 143, 216, 255, 125, 120, 72, 92, 36, 255, 255, 255, 158, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] TwentyCentCHFUpperLimits = { 109, 125, 194, 255, 102, 197, 255, 118, 106, 89, 123, 34, 226, 255, 255, 158, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] FiftyCentCHFUpperLimits = { 74, 199, 233, 255, 180, 238, 255, 140, 135, 52, 68, 35, 255, 255, 255, 158, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] OneCHFUpperLimits = { 131, 130, 193, 255, 95, 191, 255, 120, 106, 84, 123, 34, 235, 255, 255, 158, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] TwoCHFUpperLimits = { 167, 68, 157, 255, 42, 148, 250, 101, 91, 123, 165, 32, 169, 182, 255,156, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] FiveCHFUpperLimits = { 199, 49, 145, 252, 31, 133, 242, 97, 88, 137, 169, 30, 158, 117, 255, 154, 255, 255, 255, 255, 255, 255, 255, 255 };

            //LowerLimits Base on:  5 cent--> datset 1179, 10 cent->dataset 1180, 20 cent->dataset 1181, 50 cent->dataset 1182, 1 chf--> dataset 1183, 5 chf--> dataset 1185
            public static byte[] FiveCentCHFLowerLimits = { 45, 65, 128, 226, 117, 155, 235, 51, 46, 85, 96, 19, 89, 220, 235, 95, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] TenCentCHFLowerLimits = { 75, 109, 183, 237, 102, 191, 245, 78, 70, 26, 37, 22, 183, 220, 235, 98, 0, 0, 0, 0, 0, 0, 0, 0 }; //226 dsetId
            public static byte[] TwentyCentCHFLowerLimits = { 98, 92, 163, 235, 63, 168, 243, 71, 62, 41, 60, 21, 145, 220, 235, 97, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] FiftyCentCHFLowerLimits = { 58, 141, 205, 238, 142, 212, 245, 82, 75, 8, 15, 22, 220, 220, 235, 98, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] OneCHFLowerLimits = { 120, 95, 166, 235, 57, 167, 242, 73, 61, 37, 61, 21, 154, 219, 235, 98, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] TwoCHFLowerLimits = { 156, 30, 129, 223, 22, 121, 226, 59, 47, 69, 91, 18, 101, 113, 232, 94, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] FiveCHFLowerLimits = { 184, 15, 115, 216, 13, 105, 218, 55, 45, 74, 95, 15, 90, 66, 220, 91, 0, 0, 0, 0, 0, 0, 0, 0 };


            //RIM Sensor Formula 8 coinboxes
            //UpperLimits Base on:  5 cent--> datset 225, 10 cent->dataset 226, 20 cent->dataset 227, 50 cent->dataset 228, 1 chf--> dataset 229, 2 chf--> dataset 230, 5 chf--> dataset 
            public static byte[] FiveCentCHFUpperLimitsRIM = { 57, 137, 173, 255, 166, 191, 255, 95, 89, 157, 255, 33, 159, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] TenCentCHFUpperLimitsRIM = { 88, 155, 216, 255, 147, 219,	255, 121, 117, 71, 255, 35, 255, 255, 91, 143, 255, 255, 255, 255, 255, 175, 255, 255};
            public static byte[] TwentyCentCHFUpperLimitsRIM = { 110, 125, 195, 255, 103, 196, 255, 115, 103, 88, 255, 34, 217, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] FiftyCentCHFUpperLimitsRIM = { 73, 187, 233, 255, 178, 237, 255, 136, 132, 49, 255, 38, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] OneCHFUpperLimitsRIM = { 131, 134, 197, 255, 95, 192, 255, 116, 104, 81, 173, 34, 224, 255, 149, 255, 255, 255, 255, 152, 255, 255, 255, 255 };
            public static byte[] TwoCHFUpperLimitsRIM = { 168, 72, 160, 255, 42, 151, 251, 98, 89, 124, 255, 32, 169, 168, 255, 140, 255, 255, 255, 255, 255, 255, 255, 74};
            public static byte[] FiveCHFUpperLimitsRIM = { 202, 52, 147, 252, 31, 133, 243, 93, 85, 137, 255, 30, 156, 113, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255 };

            public static byte[] FiveCentCHFLowerLimitsRIM = { 45, 69, 125, 225, 117, 155, 235, 54, 48, 79, 0, 17, 91, 234, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] TenCentCHFLowerLimitsRIM = { 76, 109,	181, 237, 100, 189, 244, 81, 73, 28, 0, 22, 190, 235, 61, 113, 0, 0, 0, 222, 0, 105, 0, 0 }; //226 dsetId
            public static byte[] TwentyCentCHFLowerLimitsRIM = {101, 89, 165, 235, 62, 171, 242, 74, 64, 41, 0, 20, 140, 235, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] FiftyCentCHFLowerLimitsRIM = { 60, 144, 205, 238, 145, 213, 245, 86, 77, 10, 0, 22, 230, 234, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] OneCHFLowerLimitsRIM = { 122, 101, 168, 234, 57, 167, 241, 77, 65, 38, 106, 20, 162, 228, 109, 0, 0, 0, 0, 111, 0, 0, 0, 0 };
            public static byte[] TwoCHFLowerLimitsRIM = { 157, 29, 127, 222, 23, 120, 227, 61, 51, 69, 0, 18, 100, 126, 0, 109, 0, 0, 121, 0, 0, 0, 0, 0 };
            public static byte[] FiveCHFLowerLimitsRIM = { 185,13, 114, 215, 13, 105, 217, 58, 49, 74, 0, 14, 92, 68, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0 };

            public static string[] GetAllCHFCoins(bool rimPresent)
            {
                if (rimPresent)
                    return new string[] { FIVE_CENT_CHF, TENT_CENT_CHF, TWENTY_CENT_CHF, FIFTY_CENT_CHF, ONE_CHF, TWO_CHF, FIVE_CHF };
                else return new string[] { FIVE_CENT_CHF, TENT_CENT_CHF, TWENTY_CENT_CHF, FIFTY_CENT_CHF, ONE_CHF, TWO_CHF};
            }

            public static byte[] GetUpperLimits(string CHFCoin)
            {
                switch (CHFCoin)
                {
                    case FIVE_CENT_CHF: return FiveCentCHFUpperLimits;
                    case TENT_CENT_CHF: return TenCentCHFUpperLimits;
                    case TWENTY_CENT_CHF: return TwentyCentCHFUpperLimits;
                    case FIFTY_CENT_CHF: return FiftyCentCHFUpperLimits;
                    case ONE_CHF: return OneCHFUpperLimits;
                    case TWO_CHF: return TwoCHFUpperLimits;
                    case FIVE_CHF: return FiveCHFUpperLimits;
                    default: return new byte[] { };
                }
            }
            public static byte[] GetLowerLimits(string CHFCoin)
            {
                switch (CHFCoin)
                {
                    case FIVE_CENT_CHF: return FiveCentCHFLowerLimits;
                    case TENT_CENT_CHF: return TenCentCHFLowerLimits;
                    case TWENTY_CENT_CHF: return TwentyCentCHFLowerLimits;
                    case FIFTY_CENT_CHF: return FiftyCentCHFLowerLimits;
                    case ONE_CHF: return OneCHFLowerLimits;
                    case TWO_CHF: return TwoCHFLowerLimits;
                    case FIVE_CHF: return FiveCHFLowerLimits;
                    default: return new byte[] { };
                }
            }

            public static byte[] GetUpperLimitsRIM(string CHFCoin)
            {
                switch (CHFCoin)
                {
                    case FIVE_CENT_CHF: return FiveCentCHFUpperLimitsRIM;
                    case TENT_CENT_CHF: return TenCentCHFUpperLimitsRIM;
                    case TWENTY_CENT_CHF: return TwentyCentCHFUpperLimitsRIM;
                    case FIFTY_CENT_CHF: return FiftyCentCHFUpperLimitsRIM;
                    case ONE_CHF: return OneCHFUpperLimitsRIM;
                    case TWO_CHF: return TwoCHFUpperLimitsRIM;
                    case FIVE_CHF: return FiveCHFUpperLimitsRIM;
                    default: return new byte[] { };
                }
            }
            public static byte[] GetLowerLimitsRIM(string euroCoin)
            {
                switch (euroCoin)
                {
                    case FIVE_CENT_CHF: return FiveCentCHFLowerLimitsRIM;
                    case TENT_CENT_CHF: return TenCentCHFLowerLimitsRIM;
                    case TWENTY_CENT_CHF: return TwentyCentCHFLowerLimitsRIM;
                    case FIFTY_CENT_CHF: return FiftyCentCHFLowerLimitsRIM;
                    case ONE_CHF: return OneCHFLowerLimitsRIM;
                    case TWO_CHF: return TwoCHFLowerLimitsRIM;
                    case FIVE_CHF: return FiveCHFLowerLimitsRIM;
                    default: return new byte[] { };
                }
            }
        }
        //#######
        public class EmiratesCoins
        {
            public const string ONE_DIRHAM_REVC = "1 AED Rev C";
            public const string ONE_DIRHAM_REVB = "1 AED Rev B";
            public const string HALF_DIRHAM_REVC = "0.5 AED Rev C";
            public const string HALF_DIRHAM_REVB = "0.5 AED Rev B";


            ////UpperLimits Base on: 1 Dirham->dataset 356  rev B No magnetico
            public static byte[] OneDirhamRevBUpperLimits = { 138, 82, 165, 255, 54, 161, 255, 95, 86, 113, 157, 34, 171, 255, 255, 148, 255, 162, 49, 243, 171, 117, 197, 255 };

            //LowerLimits Base on: 1 Dirham->dataset 356  
            public static byte[] OneDirhamRevBLowerLimits = { 128, 37, 133, 227, 33, 129, 232, 69, 59, 72, 92, 19, 114, 228, 245, 105, 0, 90, 25, 197, 92, 72, 124, 0 }; 

            //UppLimits 1 Driham --dataset 358 rev C Magnetico                                                                     //155
            public static byte[] OneDirhamRevCUpperLimits = { 139, 18, 23, 46, 31, 44, 76, 255, 255, 87, 96, 110, 66, 216, 179, 218, 181, 149, 67, 59, 193, 158, 255, 255 };
            //LowerLimits 1 Dirham -358 Rev C . Magnetic                                                                       //80
            public static byte[] OneDirhamRevCLowerLimits = { 129, 0, 0, 19, 12, 14, 48, 244, 154, 34, 45, 84, 9, 178, 122, 169, 88, 39, 42, 22, 137, 100, 0, 0 };
           
            //For test
            //REV C Limits
            //public static byte[] OneDirhamUpperLimits = { 139, 18, 23, 46, 31, 44, 76, 255, 255, 87, 96, 110, 66, 216, 179, 218, 255, 149, 67, 59, 193, 158, 255, 255 };

            //////LowerLimits Base on: 1 Dirham->dataset 356  
            //public static byte[] OneDirhamLowerLimits = { 129, 0, 0, 19, 12, 14, 48, 244, 154, 34, 45, 84, 9, 178, 122, 169, 0, 39, 42, 22, 137, 100, 0, 0 };

            //0.5 AED RevC coin Id 2369   dataaset id 360  formula 13
            public static byte[] HalfDirhamRevCUpperLimits = { 110, 31, 42, 70, 55, 75, 115, 255, 255, 67, 77, 125, 43, 254, 221, 226, 255, 130, 95, 225, 171, 207, 255, 255 };
            ////LowerLimits 0.5 AED RevC coin Id 2369   dataaset id 360  formula 13
            public static byte[] HalfDirhamRevCLowerLimits = { 96, 9, 13, 42, 32, 44, 82, 245, 175, 24, 31, 91, 0, 215, 156, 181, 0, 30, 69, 177, 102, 151, 0, 127 };

            //0.5 AED RevB coin Id 69   dataaset id 354  formula 13
            public static byte[] HalfDirhamRevBUpperLimits = { 112, 108, 182, 255, 91, 184, 255, 100, 92, 92, 136, 35, 191, 255, 255, 149, 255, 171, 82, 160, 209, 100, 255, 113 };
            ////LowerLimits 0.5 AED RevB coin Id 69   dataaset id 354  formula 13
            public static byte[] HalfDirhamRevBLowerLimits = { 97, 70, 152, 234, 53, 155, 239, 76, 65, 60, 78, 21, 133, 245, 245, 107, 0, 94, 56, 97, 142, 62, 0, 0 };


            public static string[] GetAllAEDCoins()
            {
                return new string[] { ONE_DIRHAM_REVC, ONE_DIRHAM_REVB, HALF_DIRHAM_REVC, HALF_DIRHAM_REVB };
            }
            public static byte[] GetUpperLimits(string emirateCoin)
            {
                switch (emirateCoin)
                {
                    case ONE_DIRHAM_REVB: return OneDirhamRevBUpperLimits;
                    case ONE_DIRHAM_REVC: return OneDirhamRevCUpperLimits;
                    case HALF_DIRHAM_REVB: return HalfDirhamRevBUpperLimits;
                    case HALF_DIRHAM_REVC: return HalfDirhamRevCUpperLimits;
                    
                    default: return new byte[] { };
                }
            }
            public static byte[] GetLowerLimits(string emirateCoin)
            {
                switch (emirateCoin)
                {
                    case ONE_DIRHAM_REVB: return OneDirhamRevBLowerLimits;
                    case ONE_DIRHAM_REVC: return OneDirhamRevCLowerLimits;
                    case HALF_DIRHAM_REVB: return HalfDirhamRevBLowerLimits;
                    case HALF_DIRHAM_REVC: return HalfDirhamRevCLowerLimits;
                   
                    default: return new byte[] { };
                }
            } 
      
            //Rev C Magnetic
            //public static byte[] GetUpperLimitsRevCMag(string emirateCoin)
            //{
            //    switch (emirateCoin)
            //    {
            //        //case ONE_DIRHAM: return OneDirhamUpperLimitsRevCMagnetic;

            //        default: return new byte[] { };
            //    }
            //}
            //public static byte[] GetLowerLimitsRevCMag(string emirateCoin)
            //{
            //    switch (emirateCoin)
            //    {
            //        //case ONE_DIRHAM: return OneDirhamLowerLimitsRevCMagnetic;

            //        default: return new byte[] { };
            //    }
            //}    
        }

        public class PolishCoins
        {
            public const string ONE_GROSZ = "1 Grosz";   //0.01  RevA
            public const string FIVE_GROSZY = "5 Groszy";  //0.05 RevA
            public const string TWENTEE_GROSZY = "20 Groszy"; //0.2 Rev A
            public const string ONE_ZLOTY = "1 Zloty";  //1 RevA
            public const string FIVE_ZLOTY = "5 Zloty"; //5 Rev A

            //No RIM
            //UpperLimits Fromula 4, Base on: 1 Grosz->dataset 449 . 5 Groszy--> dataset 451. 20 Groszy--> dataset 453, 1 Zloty-->dataset 455, 5 Zloty--> datset 457
            public static byte[] OneGroszUpperLimits = { 24, 249, 197, 255, 205, 200, 255, 96, 92, 165, 189, 32, 155, 255, 255, 156, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] FiveGroszyUpperLimits = { 90, 52, 107, 228, 81, 122, 230, 75, 68, 248, 255, 24, 107, 255, 255, 146, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] TwenteeGroszyUpperLimits = { 77, 135, 202, 255, 145, 210, 255,	123, 116, 81, 101, 34, 255, 255, 255, 158, 255, 255, 255, 255, 255, 255, 255, 255 };
            public static byte[] OneZlotyUpperLimits = {129, 121, 184, 255, 83, 180, 255, 116, 101, 99, 146, 34, 211, 255, 255, 158, 255, 255, 159, 158, 255, 255, 255, 255 };
            public static byte[] FiveZlotyUpperLimits = { 141, 29, 81, 216, 36, 97, 217, 73, 69, 234, 255, 19, 107, 255, 255, 142, 255, 255, 255, 255, 255, 255, 255, 255 };

            public static byte[] OneGroszLowerLimits = { 3, 96, 126, 221, 162, 165, 232, 48, 42, 82, 87, 16, 82, 213, 235, 92, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] FiveGroszyLowerLimits = { 76, 15, 72, 187, 46, 91, 199, 30, 23, 157, 178, 4, 62, 214, 235, 86, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] TwenteeGroszyLowerLimits = { 64, 96, 172, 237, 101, 183, 243, 74, 66, 31, 45, 22, 158, 213, 235, 98, 0, 0, 0, 0, 0, 0, 0, 0 };
            public static byte[] OneZlotyLowerLimits = { 117, 77, 154, 232, 47, 151, 238, 67, 56, 46, 69, 21, 126, 214, 235, 97, 0, 0, 90, 88, 0, 0, 0, 0 };
            public static byte[] FiveZlotyLowerLimits = { 127, 0, 45, 167, 15, 63, 177, 29, 24, 147, 161, 0, 61, 203, 235, 80, 0, 0, 0, 0, 0, 0, 0, 0 };

            public static string[] GetAllPLNCoins()
            {
                return new string[] { ONE_GROSZ, FIVE_GROSZY, TWENTEE_GROSZY, ONE_ZLOTY, FIVE_ZLOTY };
            }

            public static byte[] GetUpperLimits(string CHFCoin)
            {
                switch (CHFCoin)
                {
                    case ONE_GROSZ: return OneGroszUpperLimits;
                    case FIVE_GROSZY: return FiveGroszyUpperLimits;
                    case TWENTEE_GROSZY: return TwenteeGroszyUpperLimits;
                    case ONE_ZLOTY: return OneZlotyUpperLimits;
                    case FIVE_ZLOTY: return FiveZlotyUpperLimits;
                    default: return new byte[] { };
                }
            }
            public static byte[] GetLowerLimits(string CHFCoin)
            {
                switch (CHFCoin)
                {
                    case ONE_GROSZ: return OneGroszLowerLimits;
                    case FIVE_GROSZY: return FiveGroszyLowerLimits;
                    case TWENTEE_GROSZY: return TwenteeGroszyLowerLimits;
                    case ONE_ZLOTY: return OneZlotyLowerLimits;
                    case FIVE_ZLOTY: return FiveZlotyLowerLimits;
                    default: return new byte[] { };
                }
            }
        }
    }
}
