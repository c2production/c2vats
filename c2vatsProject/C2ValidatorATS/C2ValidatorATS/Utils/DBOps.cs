﻿/**
 * Class: DB
 * To makes operation to the DB. Using Mysqlconnector
 * Author: Leonardo Garcia, PayComplete
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace C2ValidatorATS.Utils
{
    class DBOps
    {
        private string server;
        private string uid;
        private string password;
        private string dataBaseName;
        private string port;
        private MySqlConnection conn;

   
        public DBOps(string dbName)
        {
            dataBaseName = dbName;
            server = "10.0.1.71";
            uid = "c2prod";
            password = "currenza2prod";
            port = "3306"; 
        }

      

        private bool Open()
        {
            
            string connectionTo;
            connectionTo = "SERVER = 10.0.1.71; DATABASE=prod_coin; UID= c2prod; PASSWORD= currenza2prod;";
            conn = new MySqlConnection(connectionTo);
            try
            {
                conn.Open();
                return true;
            }
            catch (MySqlException ex)
            { 
                            
                switch (ex.Number)
                {
                    case 0: //0: Cannot connect to server.
                        MessageBox.Show("Cannot connect to server");
                        break;

                    case 1045: //1045: Invalid user name and/or password.  
                        MessageBox.Show("Invalid username or password, please try again");
                        break;
                    default: MessageBox.Show(ex.Message);
                        break;
                }
                return false;
            }
        }

        private bool Close()
        {
            try
            {
                conn.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public List<List<string>> Select(string query)
        {
            List<List<string>> dataByColumns = new List<List<string>>(); //[ColumnNames][AllColumnData]
            
            if (this.Open() == true)
            {
                //Command
                MySqlCommand cmd = new MySqlCommand(query, conn);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();
                int columnNumbers = dataReader.FieldCount;
                //Read the data and store them in the list
                for (int i = 0; i < columnNumbers; i++)
                {
                    dataByColumns.Add(new List<string>());
                    dataByColumns[i].Add(dataReader.GetName(i).ToString());//Storing clumn Names
                }
                
                
                while (dataReader.Read())
                { 
                    for (int i = 0; i < columnNumbers; i++)
                    {//Storing data for row rowIdx in column i(every column)  
                        dataByColumns[i].Add(dataReader.GetValue(i).ToString());
                    }
                    
                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.Close();

                return dataByColumns;
            }
            else
            {
                return dataByColumns;
            }
        }


    }
}
