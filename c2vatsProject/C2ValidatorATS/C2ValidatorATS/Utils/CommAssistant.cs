﻿/**
 * Class: CommAssistant
 * Singletone class which is able to stablish communication and provide communication status about the device under test
 * Author: Leonardo Garcia, PayComplete
 * 
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace C2ValidatorATS.Utils
{    
    class CommAssistant 
    {
        private string[] comPorts;
        private byte currentComPortIdx = 0xFF;
        private byte VALIDATOR_TYPE_ID = 1;
        private byte HMI_TYPE_ID = 4;
        public byte ComPortIndex { get; set; }
        public bool isComObjectCreated { get; set; }
        Logger Log;

       #region Singletone implementation
        //thread-safe Singletone class implementation
        static readonly CommAssistant instance = new CommAssistant();
        public static CommAssistant Instance
        {
            get
            {
                return instance;
            }
        }
        CommAssistant()
        {
            Log = Logger.instance;
        }
        #endregion

        private byte GetComPortIndex(string comportName)
        {

            switch (comportName)
            {
                case "COM1": return 0;
                case "COM2": return 1;
                case "COM3": return 2;
                case "COM4": return 3;
                case "COM5": return 4;
                case "COM6": return 5;
                case "COM7": return 6;
                case "COM8": return 7;
                case "COM9": return 8;
                case "COM10": return 9;
                case "COM11": return 10;
                default: return 0xFF;
            }
        }

        //Check if there is any serial connected. If connected does it means that VUT is connected
        private bool SearchAndRetreiveComPort()
        {
            Log.Debug(this.GetType().Name,  Msgs.Info.SERCHING_COM_PORT);
            comPorts = SerialPort.GetPortNames();
            if (comPorts.Length == 0)
            {
                Log.Error(this.GetType().Name, Msgs.Error.NO_COMPORT_USB_TO_SERIAL_FOUND);
                ComPortIndex = 0xFF;  //NA
                return false;
            }
            //ComPortIndex = GetComPortIndex(comPorts[0]);
            Log.Debug(this.GetType().Name, Msgs.Info.COM_PORT_FOUND + "Comport Name " + comPorts[0] +" Comport Index = " + ComPortIndex);
            return true;
        }

        public bool CreateCommunicationObjectRepresentation()
        {
            if (SearchAndRetreiveComPort())
            {
                for (int i = 0; i < comPorts.Count(); i++)
                {
                    ComPortIndex = GetComPortIndex(comPorts[i]);
                    //currentComPortIdx = ComPortIndex;
                    if (!CX2ProtAdaptLayer.StartSession(ComPortIndex, VALIDATOR_TYPE_ID))
                    {
                        Log.Debug(this.GetType().Name, Msgs.Error.FAIL_TO_CREATE_OBJECT_REPRESENTATION_TO_COMMUNICATE_TO_VUT);
                        //return false;
                        continue;
                    }
                    else if (!CX2ProtAdaptLayer.StartSession(ComPortIndex, HMI_TYPE_ID))//Create the object for HMI which is the device nr 4
                    {
                        Log.Debug(this.GetType().Name, Msgs.Error.FAIL_TO_CREATE_OBJECT_REPRESENTATION_TO_COMMUNICATE_TO_HMI);
                        //return false;
                        continue;
                    }
                    isComObjectCreated = true;
                    if (!IsComPortValid() && DestroyCommunicationObjectRepresentation())
                    {
                        continue;
                    }

                    Log.Debug(this.GetType().Name, " Communication object created " + Msgs.Info.RESULT_OK);
                    isComObjectCreated = true;
                    return true;
                }
            }
            return false;
        }

        private bool IsComPortValid()
        {
            return CX2ProtAdaptLayer.PingDevice(VALIDATOR_TYPE_ID);
        }

        public bool DestroyCommunicationObjectRepresentation()
        {
            if (isComObjectCreated)
            {
                if (!CX2ProtAdaptLayer.EndSession(VALIDATOR_TYPE_ID))
                {
                    Log.Debug(this.GetType().Name, Msgs.Error.FAIL_TO_DESTROY_OBJECT_REPRESENTATION_TO_COMMUNICATE_TO_VUT);
                    return false;
                }
                else if (!CX2ProtAdaptLayer.EndSession(HMI_TYPE_ID))//Create the object for HMI which is the device nr 4
                {
                    Log.Debug(this.GetType().Name,  Msgs.Error.FAIL_TO_DESTROY_OBJECT_REPRESENTATION_TO_COMMUNICATE_TO_HMI);
                    return false;
                }
                Log.Debug(this.GetType().Name, " Communication object destroyed " + Msgs.Info.RESULT_OK);
                isComObjectCreated = false;
                return true;
            }
            Log.Debug(this.GetType().Name, " There is not communication object to destroy");
            return true;
        }


        //Return false if usb serial cable is not connected Or preparing the Validator communication
        //Object fails Or is not able to receive response from a ping to the validator board.
        public bool IsValidatorConnected(byte device_id, ref string resultMsg)
        {       
            if (!CX2ProtAdaptLayer.PingDevice(device_id))
            {
                //CX2ProtAdaptLayer.EndSession(device_id);
                resultMsg = Msgs.Error.NO_ABLE_TO_PING_THE_VALIDATOR;
                Log.Debug(this.GetType().Name, resultMsg + "Pinging Dev " + device_id);
                Log.Debug(this.GetType().Name, Msgs.Info.NO_DEVICE_CONNECTED);
                return false;
            }
            else
            {
                resultMsg = Msgs.Info.RESULT_OK;
                return true;
            }
        }
       
    }
}
