﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace C2ValidatorATS.Utils
{
    class FirmWareFlash
    {
        public bool FWFlashingInProgress;
        private string fwFullPathAndName;
        public FirmWareFlash(string fwFullPathAndName)
        {
            this.fwFullPathAndName = fwFullPathAndName;
            FWFlashingInProgress = false;
        }
        //private string GetFWPath()
        //{
        //    string fwPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ValFW\\c2mm_391804.cxb";
            
        //    return fwPath;
        //}
        public bool FlashFirmWare()
        {
            if (CX2ProtAdaptLayer.FWFlash(1, fwFullPathAndName))
            {
                FWFlashingInProgress = true;
                return true;
            }
            return false;
        }

        public bool CheckFirmwareProgress(byte deviceId, ref byte progressInPercentage)
        {
            return CX2ProtAdaptLayer.GetFWFlashingProcessStatus(deviceId, ref progressInPercentage);
        }

        public bool EndFlashingProcess(byte deviceId)
        {
            return CX2ProtAdaptLayer.EndFWFlash(deviceId);
        }

    }
}
