﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2ValidatorATS.Utils
{
    class DBHandler
    {
        string prodCoinDB;
        DBOps CDb;

        public DBHandler()
        {
            prodCoinDB = "prod_coin";
        }

        public List<List<string>> GetISOs()
        {
            CDb = new DBOps(prodCoinDB);
            return CDb.Select("SELECT distinct iso FROM prod_coin.AllCoins" );
        }

        public List<List<string>> GetDataFromISO(string iso, List<string> data)
        {
            string data2Columns = "";
            for(int i = 0; i < data.Count(); i++ )
            {
                data2Columns += data[i];
                if ((i + 1) != data.Count()) data2Columns += ",";
            }
            return CDb.Select("SELECT " + data2Columns + " FROM prod_coin.AllCoins WHERE iso = " + iso);
        }
    }
}
