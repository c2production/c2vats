﻿/**
 * Class: TestCaseProvider
 * Static class that define all testcases applicable to the device under test
 * Author: Leonardo Garcia, PayComplete
 * 
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2ValidatorATS.Utils
{
    public static class TestProvider
    {
        //Test Cases
        public const string NO_APPLICABLE = "0"; //In case the test is not applicable for the Validator configaration
        public const string LIGHT_SENSOR_TEST = "Light Sensor Test";
        public const string COIN_POSITION_SENSOR_TEST = "Coin Position Sensor Test";
        public const string VALIDATOR_DOOR_TEST = "Validator Door Open/Close Test";
        public const string SORTER_GATE_TEST = "Sorter Gate Funtionality Test";
        public const string MATERIAL_SENSOR_NO_PULSE_TEST = "Material Sensor, No Pulse Test";
        public const string MATERIAL_SENSOR_WITH_PULSE_TEST = "Material Sensor, With Pulse Test";
        public const string SORTER_DOOR_TEST = "Sorter Door Open/Close Test";
        public const string HMI_KEY_TEST = "HMI Key Status Test";
        public const string HMI_DISPLAY_TEST = "HMI Display Test";
        public const string RIM_SENSOR_TEST = "RIM Sensor Test";
        public const string LEVER_STATUS_TEST = "Lever Status Test";
        public const string COIN_INSERT_ONE_EUR_TEST = "Coin Insertion. 1 Eur Coin Test";

        //Debug Cases
        public const string PING_DEVICE_TEST = "Ping Device Test";
        public const string POLL_DEVICE_TEST = "Poll Device Test";
        public const string COIN_DROP_EVALUATION_TEST = "Coin Drop Evaluation Test";
        public const string VALIDATOR_MEAS_PERFORMANCE_ANALYSIS_TEST = "Validator Meausrement Performance Test";
        public const string LIGHT_SENSOR_LS1_TEST = "Light Sensor LS1 Test";
        public const string LIGHT_SENSOR_LS2_TEST = "Light Sensor LS2 Test";
        public const string LIGHT_SENSOR_LS3_TEST = "Light Sensor LS3 Test";
        public const string LS1_STABILITY_TEST = "LS1 Stability Test";
        public const string LS2_STABILITY_TEST = "LS2 Stability Test";
        public const string LS3_STABILITY_TEST = "LS3 Stability Test";
        public const string COIN_POSITION_SENSOR_CP3S_TEST = "Coin Position Sensor CP3S Test";
        public const string COIN_POSITION_SENSOR_CP4L_TEST = "Coin Position Sensor CP4L Test";
        public const string COIN_POSITION_SENSOR_CP4R_TEST = "Coin Position Sensor CP4R Test";
        public const string MATERIAL_SENSOR_ANALYSIS_TEST = "Material Sensor Analysis Test";
        public const string MATERIAL_SENSOR_TRANSMISSIVE_MEAS_NO_PULSE = "Mat. Sensor Trans. Meas. No Pulse Test";
        public const string MATERIAL_SENSOR_REFLECTIVE_MEAS_NO_PULSE = "Mat. Sensor Refl. Meas. No Pulse Test";
        public const string MATERIAL_SENSOR_TRANSMISSIVE_MEAS_WITH_PULSE = "Mat. Sensor Trans. Meas. W/ Pulse Test";
        public const string MATERIAL_SENSOR_REFLECTIVE_MEAS_WITH_PULSE = "Mat. Sensor Refl. Meas. W/ Pulse Test";
        public const string COIN_INSERT_TWO_EUR_TEST = "Coin insertion. 2 Eur Coin Test";
        public const string COIN_INSERT_TEN_CENT_EUR_TEST = "Coin Insertion. 10 cent Eur Coin Test";
       // public const string COIN_INSERT_ONE_EUR_TEST = "Coin Insertion. 1 Eur Coin Test"; Declare whithin the Test Cases
        public const string TABLE_TEST = "Table Test";
        public const string POLL_HMI_TEST = "Poll HMI board Test";
        public const string SOLENOID_TEST = "Quick Solenoid Test";
        public const string SORTER_GATE_CHECK_TEST = "Sorter Gates Check. Single SG Test";


        public static List<string> getAllTCs()
        {
            List<string> allTCs = new List<string>();
            allTCs.Add(LIGHT_SENSOR_TEST);
            allTCs.Add(COIN_POSITION_SENSOR_TEST);
            allTCs.Add(MATERIAL_SENSOR_NO_PULSE_TEST);
            allTCs.Add(MATERIAL_SENSOR_WITH_PULSE_TEST);
            allTCs.Add(LEVER_STATUS_TEST);
            allTCs.Add(SORTER_DOOR_TEST);
            allTCs.Add(VALIDATOR_DOOR_TEST);
            allTCs.Add(SORTER_GATE_TEST);
            allTCs.Add(HMI_KEY_TEST);
            allTCs.Add(HMI_DISPLAY_TEST);
            allTCs.Add(COIN_INSERT_ONE_EUR_TEST);
            allTCs.Add(RIM_SENSOR_TEST);
            return allTCs;
        }

        public static List<string> getHMIKeyOnlyTCs()
        {
            List<string> allTCs = new List<string>();
            allTCs.Add(LIGHT_SENSOR_TEST);
            allTCs.Add(COIN_POSITION_SENSOR_TEST);
            allTCs.Add(MATERIAL_SENSOR_NO_PULSE_TEST);
            allTCs.Add(MATERIAL_SENSOR_WITH_PULSE_TEST);
            allTCs.Add(LEVER_STATUS_TEST);
            allTCs.Add(SORTER_DOOR_TEST);
            allTCs.Add(VALIDATOR_DOOR_TEST);
            allTCs.Add(SORTER_GATE_TEST);
            allTCs.Add(HMI_KEY_TEST);
            allTCs.Add(COIN_INSERT_ONE_EUR_TEST);
            allTCs.Add(RIM_SENSOR_TEST);
            return allTCs;
        }

        public static List<string> getNoHmiTCs()
        {
            List<string> allTCs = new List<string>();
            allTCs.Add(LIGHT_SENSOR_TEST);
            allTCs.Add(COIN_POSITION_SENSOR_TEST);
            allTCs.Add(MATERIAL_SENSOR_NO_PULSE_TEST);
            allTCs.Add(MATERIAL_SENSOR_WITH_PULSE_TEST);
            allTCs.Add(LEVER_STATUS_TEST);
            allTCs.Add(SORTER_DOOR_TEST);
            allTCs.Add(VALIDATOR_DOOR_TEST);
            allTCs.Add(SORTER_GATE_TEST);
            allTCs.Add(COIN_INSERT_ONE_EUR_TEST);
            allTCs.Add(RIM_SENSOR_TEST);
            return allTCs;
        }

        public static List<string> getSorterDoorNoMountedTCs()
        {
            List<string> allTCs = new List<string>();
            allTCs.Add(LIGHT_SENSOR_TEST);
            allTCs.Add(MATERIAL_SENSOR_NO_PULSE_TEST);
            allTCs.Add(MATERIAL_SENSOR_WITH_PULSE_TEST);
            allTCs.Add(LEVER_STATUS_TEST);
            allTCs.Add(VALIDATOR_DOOR_TEST);
            allTCs.Add(SORTER_GATE_TEST);
            allTCs.Add(COIN_INSERT_ONE_EUR_TEST);
            allTCs.Add(RIM_SENSOR_TEST);
            return allTCs;
        }

        public static List<string> GetDoorOnlyTest()
        {
            List<string> allTCs = new List<string>();
            allTCs.Add(COIN_POSITION_SENSOR_TEST);
            allTCs.Add(HMI_KEY_TEST);
            allTCs.Add(HMI_DISPLAY_TEST);
            return allTCs;
        }

        /// <summary>
        /// All debug cases, Important to keep the order as the tests will shows up in the user interface
        /// </summary>
        /// <returns></returns>
        public static  List<string> getAllDebuggCases()
        {
            List<string> allDCs = new List<string>();
            allDCs.Add(SOLENOID_TEST);
            allDCs.Add(SORTER_GATE_CHECK_TEST);
            allDCs.Add(PING_DEVICE_TEST);
            allDCs.Add(LIGHT_SENSOR_LS1_TEST);
            allDCs.Add(LIGHT_SENSOR_LS2_TEST);
            allDCs.Add(LIGHT_SENSOR_LS3_TEST);
            allDCs.Add(LS1_STABILITY_TEST);
            allDCs.Add(LS2_STABILITY_TEST);
            allDCs.Add(LS3_STABILITY_TEST);
            allDCs.Add(COIN_POSITION_SENSOR_CP3S_TEST);
            allDCs.Add(COIN_POSITION_SENSOR_CP4L_TEST);
            allDCs.Add(COIN_POSITION_SENSOR_CP4R_TEST);
            allDCs.Add(MATERIAL_SENSOR_ANALYSIS_TEST);
            allDCs.Add(MATERIAL_SENSOR_TRANSMISSIVE_MEAS_NO_PULSE);
            allDCs.Add(MATERIAL_SENSOR_REFLECTIVE_MEAS_NO_PULSE);
            allDCs.Add(MATERIAL_SENSOR_TRANSMISSIVE_MEAS_WITH_PULSE);
            allDCs.Add(MATERIAL_SENSOR_REFLECTIVE_MEAS_WITH_PULSE);
            allDCs.Add(COIN_DROP_EVALUATION_TEST);
           // allDCs.Add(VALIDATOR_MEAS_PERFORMANCE_ANALYSIS_TEST);
            allDCs.Add(COIN_INSERT_TEN_CENT_EUR_TEST);
            allDCs.Add(COIN_INSERT_ONE_EUR_TEST);
            allDCs.Add(COIN_INSERT_TWO_EUR_TEST);
            allDCs.Add(POLL_DEVICE_TEST);
            //allDCs.Add(TABLE_TEST);
            //allDCs.Add(POLL_HMI_TEST);
            return allDCs; 
        }

 
    }
}
