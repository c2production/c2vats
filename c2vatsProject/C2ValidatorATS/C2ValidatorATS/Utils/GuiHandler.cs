﻿/**
 * Class: GuiHandler
 * This class is used by the TestManager to interact and modify TestGui class which is the Graphical User Interface
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using C2ValidatorATS.Properties;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.UIs;

namespace C2ValidatorATS.Utils
{
    public class GuiHandler
    {
        private float WARNING_TEST_SIZE = 20F;
        private Color WARNING_RED_COLOR = Color.Red;
        private Color USER_INFO_BLUE_COLOR = Color.DarkBlue;
        private Color PASS_GREEN_COLOR = Color.Green;
        private UiCtrlBuilderCoinDrop CUiCtrlBuilderCoinDrop;
        private UiCtrlBuilderVutMeasPerformance CUiCtrlBuilderCoinDropStatistAndComp;
        private UiCtrlBuilderClickOnFail CUiCtrlBuilderClickOnFail;
        private UiCtrlBuilderGarage CUiCtrlBuilderGarage;
        private UiCtrlBuilderFlashInProgress CUiCtrlBuilderFlashInProgress;
        private UiCtrlBuilderDecodeData CUiCtrlBuilderDecodeData;
        private UICtrlBuilderProdInfo CUICtrlBuilderProdInfo;
        private UiCtrlBuilderMaterialSensorAnalysis CUiMaterialSensorAnalysis;

        #region Singletone implementation

        static readonly GuiHandler _instance = new GuiHandler();
        public static GuiHandler Instance
        {
            get
            {
                return _instance;
            }
        }

        GuiHandler()
        {
            CUiCtrlBuilderCoinDrop = new UiCtrlBuilderCoinDrop();
            CUiCtrlBuilderCoinDropStatistAndComp = new UiCtrlBuilderVutMeasPerformance();
            CUiCtrlBuilderClickOnFail = new UiCtrlBuilderClickOnFail();
            CUiCtrlBuilderGarage = new UiCtrlBuilderGarage();
            CUiCtrlBuilderFlashInProgress = new UiCtrlBuilderFlashInProgress();
            CUiCtrlBuilderDecodeData = new UiCtrlBuilderDecodeData();
            CUICtrlBuilderProdInfo = new UICtrlBuilderProdInfo();

        }
        #endregion

        public void ResetTCVerdict(C2ATS CTestGUI)
        {
            foreach (ListViewItem item in CTestGUI.lView_TC_List.Items)
            {
                item.SubItems[2].Text = "";
                item.SubItems[2].BackColor = Color.White;

            }
        }

        public void ChangeInfoInLblsInfo(C2ATS CTestGUI, string text, int lblNr, Color color)
        {
            switch (lblNr)
            {
                case 1: CTestGUI.lbl_info1.Text = text;
                    CTestGUI.lbl_info1.ForeColor = color;
                    break;
                case 2: CTestGUI.lbl_info2.Text = text;
                    CTestGUI.lbl_info2.ForeColor = color;
                    break;
                case 3: CTestGUI.lbl_info3.Text = text;
                    CTestGUI.lbl_info3.ForeColor = color;
                    break;
            }

        }

        /// <summary>
        /// Fill up labels in Test Session Area
        /// </summary>
        /// <param name="CTestGUI"></param>
        /// <param name="lblInfo"></param>
        public void ShowMsgInLabelsInTestSessionInfoArea(C2ATS CTestGUI, List<string> lblInfo)
        {
            CTestGUI.setLablesInTestSessionInfoAreaToDefaultValues();
            //Info of the lables are
            //1-Running Mode 2-Unit Does not or has HMI, 3-
            CTestGUI.lbl_info1.Text = lblInfo[0];
            CTestGUI.lbl_info2.Text = lblInfo[1];
            //CTestGUI.lbl_info3.Text = lblInfo[2];
        }

        public void ShowMsgInUserIndicatorLabel(C2ATS CTestGUI, string info, Color color)
        {
            if (CTestGUI.lbl_user_indication.InvokeRequired)
            {
                CTestGUI.lbl_user_indication.Invoke((MethodInvoker)delegate
                {
                    if (info.Count() > 30)
                    {
                        CTestGUI.lbl_user_indication.Location = new Point(395, 189);
                    }
                    else
                    {
                        CTestGUI.lbl_user_indication.Location = new Point(490, 195);
                    }

                    CTestGUI.lbl_user_indication.Font = new System.Drawing.Font("Microsoft Sans Serif", WARNING_TEST_SIZE);
                    if (color == null)
                        CTestGUI.lbl_user_indication.ForeColor = USER_INFO_BLUE_COLOR;
                    else CTestGUI.lbl_user_indication.ForeColor = color;
                    if (CTestGUI.lbl_user_indication.Text == info)
                    {
                        CTestGUI.lbl_user_indication.Text = "";
                    }
                    else
                    {
                        CTestGUI.lbl_user_indication.Text = info;
                    }
                });
            }
            else
            {
                if (info.Count() > 30)
                {
                    CTestGUI.lbl_user_indication.Location = new Point(395, 189);
                }
                else
                {
                    CTestGUI.lbl_user_indication.Location = new Point(490, 195);
                }

                CTestGUI.lbl_user_indication.Font = new System.Drawing.Font("Microsoft Sans Serif", WARNING_TEST_SIZE);
                if (color == null)
                    CTestGUI.lbl_user_indication.ForeColor = USER_INFO_BLUE_COLOR;
                else CTestGUI.lbl_user_indication.ForeColor = color;
                if (CTestGUI.lbl_user_indication.Text == info)
                {
                    CTestGUI.lbl_user_indication.Text = "";
                }
                else
                {
                    CTestGUI.lbl_user_indication.Text = info;
                }
            }
        }

        public void CleanMsgInUserIndicatorLabel(C2ATS CTestGUI)
        {
            if (CTestGUI.lbl_user_indication.InvokeRequired)
            {
                CTestGUI.lbl_user_indication.Invoke((MethodInvoker)delegate
                {
                    CTestGUI.lbl_user_indication.Text = "";
                });
            }
            else CTestGUI.lbl_user_indication.Text = "";
        }


        public void ShowTestCaseList(C2ATS CTestGUI, List<string> tc_List, List<bool> testCaseSelectionStatus)
        {
            CTestGUI.lView_TC_List.Items.Clear();
            for (int i = 0; i < tc_List.Count; i++)
            {
                ListViewItem item = new ListViewItem("", 0);
                item.Checked = testCaseSelectionStatus[i];
                item.SubItems.Add(tc_List[i]);
                item.SubItems.Add("");
                CTestGUI.lView_TC_List.Items.AddRange(new ListViewItem[] { item });
            }
        }

        public bool SetTestCaseVerdict(C2ATS CTestGUI, string verdict, int tcIndex, List<string> tcList)
        {
            int tcNameColumnIndex = 1;
            int statusColumnIndex = 2;

            CTestGUI.lView_TC_List.Invoke((MethodInvoker)delegate
            {
                foreach (ListViewItem item in CTestGUI.lView_TC_List.Items)
                {
                    if (item.SubItems[tcNameColumnIndex].Text == tcList[tcIndex])
                    {
                        item.SubItems[statusColumnIndex].Text = verdict;
                        if (verdict == Msgs.Verdict.TC_PASS)
                        {
                            item.SubItems[statusColumnIndex].BackColor = Color.LightGreen;
                        }
                        else if (verdict == Msgs.Verdict.TC_FAIL || verdict == Msgs.Verdict.TC_EXECUTION_STOPPED)
                        {
                            item.SubItems[statusColumnIndex].BackColor = Color.LightSalmon;
                        }
                        else if (verdict == Msgs.Verdict.TC_INCONCLUSIVE)
                        {
                            item.SubItems[statusColumnIndex].BackColor = Color.Yellow;
                        }
                        else item.SubItems[statusColumnIndex].BackColor = Color.LightYellow;
                        item.UseItemStyleForSubItems = false;
                        break;
                    }
                }
                ;
            });


            return true;
        }
        public void ResetSessionVerdictLabel(C2ATS CTestGUI)
        {
            //CTestGUI.lbl_Session_Verdict.Text = "";
        }

        public void RunningBarMove(C2ATS CTestGUI)
        {
            if (CTestGUI.pbx_running.InvokeRequired)
            {
                CTestGUI.pbx_running.Invoke((MethodInvoker)delegate
                {
                    if (CTestGUI.pbx_running.Image == global::C2ValidatorATS.Properties.Resources.running1)
                    {
                        CTestGUI.pbx_running.Image = global::C2ValidatorATS.Properties.Resources.running2;
                    }
                    else if (CTestGUI.pbx_running.Image == global::C2ValidatorATS.Properties.Resources.running2)
                    {
                        CTestGUI.pbx_running.Image = global::C2ValidatorATS.Properties.Resources.running3;
                    }
                    else if (CTestGUI.pbx_running.Image == global::C2ValidatorATS.Properties.Resources.running3)
                    {
                        CTestGUI.pbx_running.Image = global::C2ValidatorATS.Properties.Resources.running4;
                    }
                    else if (CTestGUI.pbx_running.Image == global::C2ValidatorATS.Properties.Resources.running4)
                    {
                        CTestGUI.pbx_running.Image = global::C2ValidatorATS.Properties.Resources.running5;
                    }
                    else if (CTestGUI.pbx_running.Image == global::C2ValidatorATS.Properties.Resources.running5)
                    {
                        CTestGUI.pbx_running.Image = global::C2ValidatorATS.Properties.Resources.running6;

                    }
                    else
                    {
                        CTestGUI.pbx_running.Image = global::C2ValidatorATS.Properties.Resources.running1;
                    }
                    ;
                });
            }
            else
            {
                if (CTestGUI.pbx_running.Tag.ToString() == "0")
                {
                    CTestGUI.pbx_running.Image = C2ValidatorATS.Properties.Resources.running1;
                    CTestGUI.pbx_running.Tag = "1";
                }
                else if (CTestGUI.pbx_running.Tag.ToString() == "1")
                {
                    CTestGUI.pbx_running.Image = C2ValidatorATS.Properties.Resources.running2;
                    CTestGUI.pbx_running.Tag = "2";
                }
                else if (CTestGUI.pbx_running.Tag.ToString() == "2")
                {
                    CTestGUI.pbx_running.Image = C2ValidatorATS.Properties.Resources.running3;
                    CTestGUI.pbx_running.Tag = "3";
                }
                else if (CTestGUI.pbx_running.Tag.ToString() == "3")
                {
                    CTestGUI.pbx_running.Image = C2ValidatorATS.Properties.Resources.running4;
                    CTestGUI.pbx_running.Tag = "4";
                }
                else if (CTestGUI.pbx_running.Tag.ToString() == "4")
                {
                    CTestGUI.pbx_running.Image = C2ValidatorATS.Properties.Resources.running5;
                    CTestGUI.pbx_running.Tag = "5";

                }
                else if (CTestGUI.pbx_running.Tag.ToString() == "5")
                {
                    CTestGUI.pbx_running.Image = C2ValidatorATS.Properties.Resources.running6;
                    CTestGUI.pbx_running.Tag = "0";
                }

                //thePicture.Image = Properties.Resources.hamster;
                CTestGUI.pbx_running.Refresh();
                CTestGUI.pbx_running.Visible = true;
            }
        }

        public void RunningBarStop(C2ATS CTestGUI)
        {
            CTestGUI.pbx_running.Image = null;
        }

        public void ShowPictureInImageInfoArea(C2ATS CTestGUI, Image infoPicture)
        {
            CTestGUI.pbx_user_img.Image = infoPicture;
        }

        public void CleanPictureInImageInfoArea(C2ATS CTestGUI)
        {
            CTestGUI.pbx_user_img.Image = null;
        }

        public void ShowAnimationInUserImageInfoArea(C2ATS CTestGUI, List<Image> imagesToShow)
        {
            Image nextImage = null;

            for (int i = 0; i < imagesToShow.Count(); i++)
            {
                if (CTestGUI.pbx_user_img.Image == imagesToShow[i])
                {   //If it is the last one or the only one to show
                    if (i == imagesToShow.Count() - 1)
                    {
                        nextImage = imagesToShow[0];
                    }
                    else
                    {
                        nextImage = imagesToShow[i + 1];
                    }
                    break;
                }
            }
            //If image not fond, show the first one
            if (nextImage == null) nextImage = imagesToShow[0];
            CTestGUI.pbx_user_img.Image = nextImage;
        }

        public void ShowSessionVerdictStatus(C2ATS CTestGUI, string status)
        {
            Color textColor = Color.Black;
            if ((status == "FAIL") || (status == "INCONCLUSIVE"))
            {
                textColor = WARNING_RED_COLOR;
            }
            else if(status == "PASS")
            {
                textColor = PASS_GREEN_COLOR;
            }

            if (CTestGUI.lbl_Session_Verdict.InvokeRequired)
            {
                CTestGUI.lbl_Session_Verdict.Invoke((MethodInvoker)delegate
                {
                    if (status.Count() < 6)
                        CTestGUI.lbl_Session_Verdict.Location = new Point(70, 38);
                    else CTestGUI.lbl_Session_Verdict.Location = new Point(20, 38);
                    CTestGUI.lbl_Session_Verdict.ForeColor = textColor;
                    CTestGUI.lbl_Session_Verdict.Text = status;
                });
            }
            else
            {
                if (status.Count() < 6)
                    CTestGUI.lbl_Session_Verdict.Location = new Point(70, 38);
                else CTestGUI.lbl_Session_Verdict.Location = new Point(20, 38);
                CTestGUI.lbl_Session_Verdict.ForeColor = textColor;
                CTestGUI.lbl_Session_Verdict.Text = status;
            }
        }

        public void CleanInfoInSessionVerdictSatatus(C2ATS CTestGUI)
        {
            CTestGUI.lbl_Session_Verdict.Text = "";
        }

        public void CleanAnimationInUserImageInfoArea(C2ATS CTestGUI)
        {
            CTestGUI.pbx_user_img.Image = null;
        }

        public void ShowDebugInformation(C2ATS CTestGUI, List<string> debugTarget)
        {
            if (debugTarget.Contains("ERROR"))
            {

            }
            CTestGUI.lbx_debug_info.Items.AddRange(debugTarget.ToArray());
            CTestGUI.lbx_debug_info.SelectedIndex = CTestGUI.lbx_debug_info.Items.Count - 1;
        }

        public void ShowInfoInAuxUserIndicationLbl(C2ATS CTestGUI, string auxInfo)
        {
            if (CTestGUI.lbl_aux_user_info.InvokeRequired)
            {
                CTestGUI.lbl_aux_user_info.Invoke((MethodInvoker)delegate
                {
                    CTestGUI.lbl_aux_user_info.Visible = true;
                    CTestGUI.lbl_aux_user_info.BringToFront();
                    CTestGUI.lbl_aux_user_info.Text = auxInfo;
                });
            }
            else
            {
                CTestGUI.lbl_aux_user_info.Visible = true;
                CTestGUI.lbl_aux_user_info.Text = auxInfo;
            }
        }

        public void CleanInfoInAuxUserIndicationLbl(C2ATS CTestGUI)
        {
            if (CTestGUI.lbl_aux_user_info.InvokeRequired)
            {
                CTestGUI.lbl_aux_user_info.Invoke((MethodInvoker)delegate
                {
                    CTestGUI.lbl_aux_user_info.Visible = false;
                    CTestGUI.lbl_aux_user_info.Text = "";
                });
            }
            else
            {
                CTestGUI.lbl_aux_user_info.Visible = false;
                CTestGUI.lbl_aux_user_info.Text = "";
            }
        }
        public void EnableDisableRunBtn(C2ATS CTestGUI, bool enable)
        {
            if (CTestGUI.btn_run.InvokeRequired)
            {
                CTestGUI.btn_run.Invoke((MethodInvoker)delegate
                {
                    CTestGUI.btn_run.Enabled = enable;
                });
            }
            else
            {
                CTestGUI.btn_run.Enabled = enable;
            }

        }

        public void EnableDisableStopBtn(C2ATS CTestGUI, bool enable)
        {
            if (CTestGUI.btn_stop.InvokeRequired)
            {
                CTestGUI.btn_stop.Invoke((MethodInvoker)delegate
                {
                    CTestGUI.btn_stop.Enabled = enable;
                });
            }
            else
            {
                CTestGUI.btn_stop.Enabled = enable;
            }
        }
        public void EnableDisableAtsBtn(C2ATS CTestGUI, bool enable)
        {
            CTestGUI.btn_ats.Enabled = enable;
            CTestGUI.runAcceptanceTestToolStripMenuItem.Checked = !enable;
            CTestGUI.runDebugToolStripMenuItem.Checked = enable;
        }
        public void EnableDisableDbgBtn(C2ATS CTestGUI, bool enable)
        {
            CTestGUI.btn_debug.Enabled = enable;
            CTestGUI.runAcceptanceTestToolStripMenuItem.Checked = enable;
            CTestGUI.runDebugToolStripMenuItem.Checked = !enable;
        }
        public void EnableDisableSaveLogBtn(C2ATS CTestGUI, bool enable)
        {
            CTestGUI.btn_save_log.Enabled = enable;
        }

        public void EnableDisableMenu(C2ATS CTestGUI, bool enable)
        {
            CTestGUI.MainMenuStrip.Enabled = enable;
        }
        public void ShowSelectAll(C2ATS CTestGUI, bool show)
        {
            CTestGUI.cbx_select_all.Checked = false;
            CTestGUI.cbx_select_all.Visible = show;
        }
        public void ShowRIMSensorPresent(C2ATS CTestGUI, bool show, bool checkedStatus)
        {
            CTestGUI.cbx_rim_present.Checked = checkedStatus;
            CTestGUI.cbx_rim_present.Visible = show;
        }
        public void PaintFigure(C2ATS CTestGUI, Color color, string shape, int x, int y, int width, int height )
        {
            CTestGUI.paintFigure(color, shape, x, y, width, height);
        }
        public void ChangePictureBtnSaveLog(C2ATS CTestGUI, bool saveLog)
        {
            if (saveLog)
            {
                CTestGUI.btn_save_log.Image = Resources.btnStopSave;
                CTestGUI.saveLogFileToolStripMenuItem.Checked = true;
            }
            else
            {
                CTestGUI.btn_save_log.Image = Resources.btnSave;
                CTestGUI.saveLogFileToolStripMenuItem.Checked = false;
            }
        }

        /// <summary>
        /// Related to Coin Drop Evaluation Test
        /// </summary>
        /// <param name="CTestGUI"></param>

        public void ShowCoinMeasuresTable(C2ATS CTestGUI)
        {
            CTestGUI.Invoke((MethodInvoker)delegate
            {
                CUiCtrlBuilderCoinDrop.ShowCoinMeasuresTable(CTestGUI);
            });
            
        }
        public void ShowMeasuredDataInTable(C2ATS CTestGUI, byte[] measures, List<int> meaureRemarks)
        {
            CTestGUI.Invoke((MethodInvoker)delegate
            {
                CUiCtrlBuilderCoinDrop.UpdateRowWithNewMeasuredData(measures, meaureRemarks);
            });
        }
        public void UpdateUpperLowerLimitsInTable(String CurrentTestName, C2ATS CTestGUI, byte[] upperLimits, byte[] lowerLimits)
        {
            CTestGUI.Invoke((MethodInvoker)delegate
            {
                if (CurrentTestName == TestProvider.COIN_DROP_EVALUATION_TEST)
                    CUiCtrlBuilderCoinDrop.UpdateUpperLowerLimits(upperLimits, lowerLimits);
                else CUiCtrlBuilderCoinDropStatistAndComp.UpdateUpperLowerLimits(upperLimits, lowerLimits);
            });
        }
        public void DestroyMeasuresTable(C2ATS CTestGUI)
        {
            CTestGUI.Invoke((MethodInvoker)delegate
            {
                CUiCtrlBuilderCoinDrop.DestroyMeasuresPnl();
            });

        }
        //End Related to Coin Drop Evaluation Test


        /// <summary>
        /// Related to Coin Statistic and comparison Test
        /// </summary>
        /// <param name="CTestGUI"></param>
        public void ShowCoinStatisticsAndComparisonUI(C2ATS CTestGUI)
        {
             CTestGUI.Invoke((MethodInvoker)delegate
            {
                CUiCtrlBuilderCoinDropStatistAndComp.ShowCoinDropStatisticsUi(CTestGUI);
            });
        }

        public void ShowNewMeasuredValuesToPlotGraphic(C2ATS CTestGUI, byte[] measures)
        {
             CTestGUI.Invoke((MethodInvoker)delegate
            {
                CUiCtrlBuilderCoinDropStatistAndComp.NewMeasuredValue(measures);
            });
        }
        public void ShowFailPnlIfNoResponseFromVut(C2ATS CTestGUI, Point location)
        {
            CTestGUI.Invoke((MethodInvoker)delegate
            {
                CUiCtrlBuilderClickOnFail.ShowClickOnFailPanel( CTestGUI, location);
            });
        }
        public void DestroyFailPnlIfNoResponseFromVut(C2ATS CTestGUI)
        {
            CTestGUI.Invoke((MethodInvoker)delegate
            {
                CUiCtrlBuilderClickOnFail.DestroyPnlpnlClickOnFail();
            });
        }
        public void ShowGarageUI(C2ATS CtestGUI)
        {
            CUiCtrlBuilderGarage.ShowGaragePanel( CtestGUI);
        }

        public void ShowFWFlashProgressUI(C2ATS CtestGUI)
        {
            CUiCtrlBuilderFlashInProgress.ShowFlashInProgressPanel(CtestGUI);
        }

        public void ProgressBarChange(byte progressInPercentage)
        {
            CUiCtrlBuilderFlashInProgress.FlashProgressChanged(progressInPercentage);
        }
        public void CloseFWFlashingProgressBar()
        {
            CUiCtrlBuilderFlashInProgress.ExitFlashProgressPnl();
        }
        public void DecodeDataClicked(C2ATS CtestGUI)
        {
            CUiCtrlBuilderDecodeData.ShowFlashInProgressPanel(CtestGUI);
        }
        //End Related Statistic and Comparison


        /// <summary>
        /// Releated to info panel in Debug Mode
        /// </summary>
        /// <param name="CTestGUI"></param>

        public void ShowProductInfoPnl(C2ATS CTestGUI)
        {
            CUICtrlBuilderProdInfo.ShowProdInfoPanel(CTestGUI);
        }

        public void DestroyPanelProductInfo()
        {
            CUICtrlBuilderProdInfo.DestroyPnlProductInfo();
        }
        // END Releated to info panel in Debug Mode

        /// <summary>
        /// Releated to info Material Sensor analysis
        /// </summary>
        /// <param name="CTestGUI"></param>
        public void ShowMaterialSensorPanel(C2ATS CTestGUI)
        {
            CTestGUI.Invoke((MethodInvoker)delegate
            {
                CUiMaterialSensorAnalysis = new UiCtrlBuilderMaterialSensorAnalysis();
                CUiMaterialSensorAnalysis.ShowMaterialSensorTable(CTestGUI);
            });
        }
        public void ShowMaterialResultInTbl(C2ATS CTestGUI, int[] resultsAndRemarks)
        {
            CTestGUI.Invoke((MethodInvoker)delegate
            {
                CUiMaterialSensorAnalysis.ShowMaterialSensorResults(resultsAndRemarks);
            });
        }
        public void StopOnFailTriggered(C2ATS CTestGUI)
        {
            CTestGUI.Invoke((MethodInvoker)delegate
            {
                CUiMaterialSensorAnalysis.StopOnFailTriggered();
            });
        }
        public void DestroyMaterialSensorAnalysis(C2ATS CTestGUI)
        {
            CTestGUI.Invoke((MethodInvoker)delegate
            {
                if (CUiMaterialSensorAnalysis != null) CUiMaterialSensorAnalysis.DestroyThisPnl();
            });
        }
        //END  Releated to info Material Sensor analysis
    }
}
