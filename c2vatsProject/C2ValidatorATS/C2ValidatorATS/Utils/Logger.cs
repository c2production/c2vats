﻿/**
 * Class: Logger
 * This abstract class implement Leogger defined abstract methods
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;

namespace C2ValidatorATS.Utils
{
    class Logger: Leogger
    {
        #region singletone implementation
        //thread-safe singletone class implementation
        static readonly Logger Instance = new Logger();
        public static Logger instance
        {
            get
            {
                return Instance;
            }
        }
        
        public Logger()
        {   //Set it always as default
            SetLogLevel(LLevel.DEBUG); 
        }
        #endregion

        //private bool debug = true ? (LogLevel == LLevel.DEBUG || LogLevel == LLevel.ALL) : false;
        //private bool info = true ? (LogLevel == LLevel.INFO || LogLevel == LLevel.DEBUG || LogLevel == LLevel.ALL) : false;
        //private bool warning = true ? (LogLevel == LLevel.WARNING || LogLevel == LLevel.INFO || LogLevel == LLevel.DEBUG || LogLevel == LLevel.ALL) : false;
        public override void Debug(String className, string logMsg)
        {
            if (debug)
            {
                //DisplayText.GetTextToDisplay().Add(DateTime.Now.ToString("HH:mm:ss:ms") + " From-->" + className + ":   " + logMsg);
                DisplayText.SetTextToDisplay(DateTime.Now.ToString("HH:mm:ss:ms") + " From-->" + className + ":   " + logMsg);
            }
        }
        public override void Debug(String className, string logMsg, ref List<string> target)
        {

            //target.Add(DateTime.Now.ToString("HH:mm:ss:ms ") + " From--> " + className + ":   " + logMsg);
        }

        public override void Debug(String className, string logMsg, ref List<string> target, bool saveToFile)
        {

        }

        public override void Info(String className, string logMsg)
        {
            if (info)
            {
               // DisplayText.GetTextToDisplay().Add(DateTime.Now.ToString("HH:mm:ss:ms") + " From--> " + className + "   " + logMsg);
                DisplayText.SetTextToDisplay(DateTime.Now.ToString("HH:mm:ss:ms") + " From-->" + className + ":   " + logMsg);
            }           
        }

        public override void Info(String className, string logMsg, ref List<string> target)
        {
           // target.Add(DateTime.Now.ToString("HH:mm:ss:ms") + " From: " + className + "   " + logMsg);
        }

        public override void Info(string classname, string logMsg, ref List<string> target, bool savetofile)
        {

        }

        public override void Warning(String className, string logMsg)
        {
            if (warning)
            {
                //DisplayText.GetTextToDisplay().Add(DateTime.Now.ToString("HH:mm:ss:ms") + " From--> " + className + "   " + logMsg);
                DisplayText.SetTextToDisplay(DateTime.Now.ToString("HH:mm:ss:ms") + " From-->" + className + ":   " + logMsg);
            }  
        }

        public override void Warning(String className, string logMsg, ref List<string> target)
        {
           
        }

        public override void Warning(String className, string logMsg, ref List<string> target, bool saveToFile)
        {

        }

        public override void Error(String className, string logMsg)
        {
            if (error)
            {
                //DisplayText.GetTextToDisplay().Add(DateTime.Now.ToString("HH:mm:ss:ms") + "ERROR From--> " + className + "   " + logMsg);
                DisplayText.SetTextToDisplay(DateTime.Now.ToString("HH:mm:ss:ms") + " From-->" + className + ":   " + logMsg);
            }
        }

        public override void Error(String className, string logMsg, ref List<string> target)
        {
            //target.Add(DateTime.Now.ToString("HH:mm:ss tt ") + "  ERROR" + " From-->: " + className + "   " + logMsg);
        }

        public override void Error(String className, string logMsg, ref List<string> target, bool saveToFile)
        {

        }
    }
}
