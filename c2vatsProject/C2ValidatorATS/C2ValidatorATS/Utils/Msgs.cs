﻿/**
 * Class: Msgs
 * Static class that contains all strings used in this project
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2ValidatorATS.Utils
{

    public static class Msgs
    {
        //Name and Version. Here should we stepup the version of the app
        public static class AppNameAndVersion
        {
            public static string APP_C2VATS_NAME = "C2 Validator Acceptance Test System. Ver";
            public static string APP_C2VATS_VERSION = " 5.0";
        }

        //Test Case Verdicts
        public static class Verdict
        {
            public static string TC_PASS         = "PASS";
            public static string TC_FAIL         = "FAIL";
            public static string TC_INCONCLUSIVE = "INCONCLUSIVE";
            public static string TC_NONE         = "NONE";
            public static string TC_RUNNING      = "Running...";
            public static string TC_EXECUTION_STOPPED = "STOPPED";
        }

        //Error Messages
        public class Error
        {
            public static string NO_ABLE_TO_GET_IN_CONTACT_WITH_VALIDATOR = "No able to get in contact with the Validator under test. Please check that its properly connected";
            public static string NO_ABLE_TO_PING_THE_VALIDATOR = "No able to ping the validator under test, please check that the validator is ON and properly connected to the test system";
            public static string FAIL_TO_MOVE_SORTER_GATE = "Fail to move sorter gate: The command was sent but no response received";
            public static string FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR = "Fail to sent command Get_Value() to validator. Recieved False!";
            public static string FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR = "Fail to sent command Execute() to validator. Recieved False!";
            public static string FAIL_TO_RECEIVE_CORRECT_RESPONSE_AFTER_SENDING_GET_VALUE = "Wrong received response after sending Get_Value(). Response should be 12 bytes long";
            public static string NO_COMPORT_USB_TO_SERIAL_FOUND = "No -usb to serial- comport found connected to this PC. Check connections";
            public static string FAIL_TO_CREATE_OBJECT_REPRESENTATION_FOR_THIS_VUT = "Fail to create the object representatino for this Validator. Please try again";//Delete
            public static string FAIL_TO_CREATE_OBJECT_REPRESENTATION_TO_COMMUNICATE_TO_VUT = "Fail to create the object representatino to communicate to a VUT";
            public static string FAIL_TO_CREATE_OBJECT_REPRESENTATION_TO_COMMUNICATE_TO_HMI = "Fail to create the object representatino to communicate to HMI";
            public static string FAIL_TO_DESTROY_OBJECT_REPRESENTATION_TO_COMMUNICATE_TO_VUT = "Fail to destroy the object representatino to communicate to a VUT";
            public static string FAIL_TO_DESTROY_OBJECT_REPRESENTATION_TO_COMMUNICATE_TO_HMI = "Fail to destroy the object representatino to communicate to HMI";
            public static string TIME_OUT_OCURRED = "Time Out ocurred";
            public static string FAIL_WHILE_RUNNING_THE_TC = "The Door was closed or open when the timeout occurred";
            public static string DOOR_INITIAL_STATE_CLOSED_FAIL = "Door initial state -CLOSED- fail. The door is open or defective";
            public static string LS_SENSORS_VALUE_OUT_OF_TOLERANCE = "Light Sensor's values out of tolerance";
            public static string CP_SENSORS_VALUE_OUT_OF_TOLERANCE = "Coin Position Sensor's values out of tolerance";
            public static string MATERIAL_SENSORS_VALUE_OUT_OF_TOLERANCE = "Material Sensor's values out of tolerance";
            public static string CLEANING_BUFFER_FAIL = "Cleaning buffer fail. Command 'Purge()'";
            public static string FAIL_TO_DEALLOCATE_DEVICE_OBJECT = "Fail to deallocate device object representation";
            public static string FAIL_TO_RESET_DEVICE = "Fail to reset device";
            public static string FAIL_TO_POLL_DEVICE = "Fail to poll the device";
            public static string FAIL_TO_SEND_SETVALUE_CMD = "Fail to send SetValue(). Wrong response received";
            public static string FAIL_TO_SEND_CMD_READTABLE_TO_VALIDATOR = "Fail to send ReadTable(). Wrong response received";
            public static string FAIL_TO_SEND_CMD_WRITETABLE_TO_VALIDATOR = "Fail to send WriteTable(). Wrong response received";
            public static string FAIL_TO_SEND_CMD_SET_ACCESS_LEVEL_TO_VALIDATOR = "Fail to send SetAccessLevel(). Wrong Response received";
            public static string FAIL_TO_SEND_CMD_GET_ACCESS_LEVEL_TO_VALIDATOR = "Fail to send GetAccessLevel(). Wrong Response received";
            public static string FAIL_TO_SEND_CMD_GET_VUT_IDENTIFICATION_INFO_TO_VALIDATOR = "Fail to send GetVutIdentificationInfo(). Wrong Response received";
            public static string FAIL_TO_SHOW_ASCII_CHARS = "Fail to Show ASCII characters. Execution cmd return false";
            public static string FAIL_TO_SHOW_BIT_MAP = "Fail to show bitmap";
            public static string FAIL_TO_CLEAN_THE_DISPLAY = "Fail to clean the display";
            public static string FAIL_TO_WRITE_STRING = "Fail to write a string";
            public static string FAIL_TURN_OFF_BKGR_LIGHT = "Fail to turn the background light off";
            public static string FAIL_TO_SHOW_ANIMATION = "Fail to show the animation";
            public static string FAIL_TO_SHOW_MSG = "Fail to show the message";
            public static string FAIL_DURING_TEST_CASE_INITIALIZATION = "TestCase fail during initialization";
            public static string FAIL_TO_READ_DATA_FROM_TABLE = "No able to read data from Table";
            public static string VUT_FAIL_TO_REACT_ON_USER_INTERACTION = "VUT does not react correctly to user interaction. Test Fail!";
            public static string COIN_MEAUSRED_OUT_OF_LIMITS = "Coin measured is out of limits";
        }

        //Warning Messages
        public class Warning
        {
           public static string DONT_REMOVE_CABLE = "Do NOT Remove the serial cable conneting the VUT";
           public static string LS_SENSORS_VALUE_IN_TOLERANCE = "Light Sensors values still in tolerance but should be checked";
           public static string CP_SENSORS_VALUE_IN_TOLERANCE = "Coin Position Sensor's values still in tolerance but should be checked";  

        }

        //General Messages
        public static class Info
        {
           public static string RESULT_OK = "OK";
           public static string TC_INITIAL_CONDITIONS_OK = "Test Case initialization done!";
           public static string SERCHING_COM_PORT = "Serching Comport connected";
           public static string COM_PORT_FOUND = "Com port found";
           public static string DEVICE_UNDER_TEST_CONNECTED = "Device under test connected!";
           public static string NO_DEVICE_CONNECTED = "No device connected";
           public static string REDY_TO_START_TEST_SESSION = "System ready to start the test session";
           public static string TEST_SESSION_STARTED = "---- TEST SESSION STARTED ----";
           public static string REDY_TO_STABLISHE_COMMUNICATION_WITH_DEVICE = "Ready to stablishe communication with device";
           public static string PINGING_RESULT_OK = "Pinging the device was Ok";
           public static string DEVICE_OBJECT_DEALLOCATION_DONE = "Device under test object representation Deallocation done";
           public static string DEVICE_OBJECT_REPRESENTATION_CREATED = "Device under test object representation created";
           public static string DEVICE_RESET_OK = "Device under test successfully reseted";
           public static string DEVICE_POLL_RESPONSE_OK = "Device under test poll response ok";
           public static string RESPONSE_DATA_CORRECTLY_RECEIVED_AFTER_GET_VALUE_CMD = "Response Value correctly received after GetValue()";
           public static string SET_VALUE_CMD_CORRECTLY_EXECUTED = "SetValue() cx2 command correctly executed";
           public static string EXECUTE_CMD_CORRCTLY_EXECUTED = "Execute() cx2 command correctly executed";
           public static string READTABLE_CMD_CORRECTLY_EXECUTED = "ReadTable() cx2 command correctly executed";
           public static string WRITETABLE_CMD_CORRECTLY_EXECUTED = "WriteTable() cx2 command correctly executed";
           public static string SET_ACCESS_LEVEL_CMD_CORRECTLY_EXECUTED = "Set Access Level command correctly executed";
           public static string GET_ACCESS_LEVEL_CMD_CORRECTLY_EXECUTED = "Get Access Level command correctly executed";
           public static string GET_VUT_IDENTIFICATION_INFO_CMD_CORRECTLY_EXECUTED = "Get Vut Identification command correctly executed";
           public static string NO_KEY_PRESSED = "None Key has been pressed";
           public static string EXECUTION_STOPPED_BY_USER = "Test Case execution stopped by user";
        }

        //User Indication Information
        public static class Uii
        {
            //Info used in the User Information label
            public static string CONNECT_A_NEW_VALIDATOR = "Connect a Validator for testing";
            public static string OPEN_AND_CLOSE_VALIDATOR_DOOR = "Open and close Validator door!";
            public static string PLEASE_CLOSE_VALIDATOR_DOOR = "Please, close Valildator door!";
            public static string PLEASE_CLOSE_SOTER_DOOR = "Please, close Sorter door!";
            public static string PLEASE_OPEN_SOTER_DOOR = "Please, open Sorter door";
            public static string OPEN_AND_CLOSE_SORTER_DOOR = "Open and close Sorter door!";
            public static string PUSH_AND_RELEASE_LEVER = "Push and release the Lever!";
            public static string CLICK_ON_THE_MOVING_SORTER_AREA = "Click coloured area where Sorter Gate moves";
            public static string ACTIVATE_SG_CLICKING_IN_COLORED_AREA = "Click coloured area to activate Sorter Gate";
            public static string TYPE_KEYS_IN_THIS_SEQUENCE = "Press the HMI Keys in the following order: ";
            public static string CLICK_ON_THE_PICTURE_REPRESENTING_VUT_DISPLAY = "Click the picture representing validator screen";
            public static string DROP_A_1_EURO_COIN = "Please, insert a 1 Euro coin";
            public static string DROP_A_2_EURO_COIN = "Please, insert a 2 Euro coin";
            public static string DROP_A_10_CENT_EURO_COIN = "Please, insert a 10 cent Euro coin";
            public static string DROP_FAIL_INSERT_AGAIN = "Coin Fail. Please, insert it again";
            public static string INSERT_FIVE_CHF = "Please, insert a 5 Fr coin";
           
            //Info used in the Test Session Information Area
            public static string ATS_MODE_TESTING = "ATS Mode Testing";
            public static string DEBUG_MODE_TESTING = "Debug Mode Testing";
            public static string NO_HMI_DISPLAY_PRESENT = "No HMI Interface Present";
            public static string HMI_DISPLAY_PRESENT = "HMI With Display Present";
            public static string HMI_ONLY_KEYS_PRESENT = "HMI Only Keys Present";
            public static string SAVE_LOG_ACTIVE = "Save Log To File Active";
            public static string RIM_SENSOR_PRESENT = "RIM Sensor Available";

            //
            public static string TEST_IN_PROGRESS = "Test in Progress";

            //info shown in the Fail button used in case the Vallidator does not responde to the user accion
            public static string TEST_FAIL_CAUSE_VUT_DOESNOT_RESPOND = @"Click 'Fail' if Validator does not respond to your actions!";
        }

    }
}
