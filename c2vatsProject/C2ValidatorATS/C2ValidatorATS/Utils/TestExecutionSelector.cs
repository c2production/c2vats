﻿/**
 * Class: TestExecutionSelector
 * This class provides the test to be executed to the TestManager out of the test name. 
 * It is used to retreiive the test cases and the debug cases 
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.TestCases;
using C2ValidatorATS.DbgCases;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.Utils
{
    class TestExecutionSelector
    {
        public ITestCase GetNextTestToExecute(string tcName, int testMode, ValidatorUnderTest CVut, TestManager CtestManager)
        {
            if (testMode == 0)
            {
                switch (tcName)
                {
                    case TestProvider.LIGHT_SENSOR_TEST:
                        {
                            TCLightSensorsTest CTCLightSensor1Test = new TCLightSensorsTest(CVut, CtestManager);
                            return CTCLightSensor1Test;
                        }
                    case TestProvider.COIN_POSITION_SENSOR_TEST:
                        {
                            TCCoinPositionSensorTest CTCCoinPositionSensorTest = new TCCoinPositionSensorTest(CVut, CtestManager);
                            return CTCCoinPositionSensorTest;
                        }
                    case TestProvider.MATERIAL_SENSOR_NO_PULSE_TEST:
                        {
                            TCMaterialSensorNoPulseTest CTCMaterialSensorTest = new TCMaterialSensorNoPulseTest(CVut, CtestManager);
                            return CTCMaterialSensorTest;
                        }
                   
                    case TestProvider.LEVER_STATUS_TEST:
                        {
                            TCLeverStatusTest CTCLeverStatusTest = new TCLeverStatusTest(CVut, CtestManager);
                            return CTCLeverStatusTest;
                        }
                    case TestProvider.SORTER_DOOR_TEST:
                        {
                            TCSorterDoorOpenCloseTest CTCSorterDoorOpenCloseTest = new TCSorterDoorOpenCloseTest(CVut, CtestManager);
                            return CTCSorterDoorOpenCloseTest;
                        }
                    case TestProvider.VALIDATOR_DOOR_TEST:
                        {
                            TCValidatorDoorOpenCloseTest CTCValidatorDoorOpenCloseTest = new TCValidatorDoorOpenCloseTest(CVut, CtestManager);
                            return CTCValidatorDoorOpenCloseTest;
                        }
                    case TestProvider.SORTER_GATE_TEST:
                        {
                            TCSorterGateTest CTCSorterGateTest = new TCSorterGateTest(CVut, CtestManager);
                            return CTCSorterGateTest;
                        }
                    case TestProvider.HMI_KEY_TEST:
                        {
                            TCHmiKeyTest CTCHmiKeyTest = new TCHmiKeyTest(CVut, CtestManager);
                            return CTCHmiKeyTest;
                        }
                    case TestProvider.HMI_DISPLAY_TEST:
                        {
                            TCHmiDisplayTest CTCHmiDisplayTest = new TCHmiDisplayTest(CVut, CtestManager);
                            return CTCHmiDisplayTest;
                        }
                    case TestProvider.MATERIAL_SENSOR_WITH_PULSE_TEST:
                        {
                            TCMaterialSensorWithPulseTest CTCMaterialSensorWithPulseTest = new TCMaterialSensorWithPulseTest(CVut, CtestManager);
                            return CTCMaterialSensorWithPulseTest;
                        }
                    case TestProvider.COIN_INSERT_ONE_EUR_TEST:
                        {
                            TCCoinInsertion1EurTest CTCCoinInsertion1EurTest = new TCCoinInsertion1EurTest(CVut, CtestManager);
                            return CTCCoinInsertion1EurTest;
                        }
                    case TestProvider.RIM_SENSOR_TEST:
                        {
                            TCRimSensorTest CTCRimSensorTest = new TCRimSensorTest(CVut, CtestManager);
                            return CTCRimSensorTest;
                        }
                }
            }
            else
            {
                switch (tcName)
                {
                    case TestProvider.PING_DEVICE_TEST:
                        {
                            DCPingDUTTest CDCPingDUTTest = new DCPingDUTTest(CVut, CtestManager);
                            return CDCPingDUTTest;
                        }
                    case TestProvider.LIGHT_SENSOR_LS1_TEST:
                        {
                            DCLightSensorLS1Test CDCLightSensorLS1Test = new DCLightSensorLS1Test(CVut, CtestManager);
                            return CDCLightSensorLS1Test;
                        }
                    case TestProvider.LIGHT_SENSOR_LS2_TEST:
                        {
                            DCLightSensorLS2Test CDCLightSensorLS2Test = new DCLightSensorLS2Test(CVut, CtestManager);
                            return CDCLightSensorLS2Test;
                        }
                    case TestProvider.LIGHT_SENSOR_LS3_TEST:
                        {
                            DCLightSensorLS3Test CDCLightSensorLS3Test = new DCLightSensorLS3Test(CVut, CtestManager);
                            return CDCLightSensorLS3Test;
                        }
                    case TestProvider.COIN_POSITION_SENSOR_CP3S_TEST:
                        {
                            DCCoinPositionCP3sSensorTest CDCCoinPositionCP3sSensorTest = new DCCoinPositionCP3sSensorTest(CVut, CtestManager);
                            return CDCCoinPositionCP3sSensorTest;
                        }
                    case TestProvider.COIN_POSITION_SENSOR_CP4L_TEST:
                        {
                            DCCoinPositionCP4lSensorTest CDCCoinPositionCP4lSensorTest = new DCCoinPositionCP4lSensorTest(CVut, CtestManager);
                            return CDCCoinPositionCP4lSensorTest;
                        }
                    case TestProvider.COIN_POSITION_SENSOR_CP4R_TEST:
                        {
                            DCCoinPositionCP4rSensorTest CDCCoinPositionCP4rSensorTest = new DCCoinPositionCP4rSensorTest(CVut, CtestManager);
                            return CDCCoinPositionCP4rSensorTest;
                        }
                    case TestProvider.MATERIAL_SENSOR_TRANSMISSIVE_MEAS_NO_PULSE:
                        {
                            DCMaterialSensorTransMeasNoPulse CDCMaterialSensorCoil1NoPulse = new DCMaterialSensorTransMeasNoPulse(CVut, CtestManager);
                            return CDCMaterialSensorCoil1NoPulse;
                        }
                    case TestProvider.MATERIAL_SENSOR_REFLECTIVE_MEAS_NO_PULSE:
                        {
                            DCMaterialSensorReflMeasNoPulse CDCMaterialSensorCoil2NoPulse = new DCMaterialSensorReflMeasNoPulse(CVut, CtestManager);
                            return CDCMaterialSensorCoil2NoPulse;
                        }
                    case TestProvider.MATERIAL_SENSOR_TRANSMISSIVE_MEAS_WITH_PULSE:
                        {
                            DCMaterialSensorTransMeasWithPulse CDCMaterialSensorCoil1WithPulse = new DCMaterialSensorTransMeasWithPulse(CVut, CtestManager);
                            return CDCMaterialSensorCoil1WithPulse;
                        }
                    case TestProvider.MATERIAL_SENSOR_REFLECTIVE_MEAS_WITH_PULSE:
                        {
                            DCMaterialSensorReflMeasWithPulse CDCMaterialSensorCoil2WithPulse = new DCMaterialSensorReflMeasWithPulse(CVut, CtestManager);
                            return CDCMaterialSensorCoil2WithPulse;
                        }
                    case TestProvider.COIN_DROP_EVALUATION_TEST:
                        {
                            DCCoinDropEvaluationTest CDCCoinDropEvaluationTest = new DCCoinDropEvaluationTest(CVut, CtestManager);
                            return CDCCoinDropEvaluationTest;
                        }
                    case TestProvider.COIN_INSERT_TEN_CENT_EUR_TEST:
                        {
                           DCCoinInsertion10centEurTest CDCCoinInsertion10centEurTest = new DCCoinInsertion10centEurTest(CVut, CtestManager);
                            return CDCCoinInsertion10centEurTest;
                        }
                    case TestProvider.COIN_INSERT_ONE_EUR_TEST:
                        {
                            DCCoinInsertion1EurTest CDCCoinInsertion1EurTest = new DCCoinInsertion1EurTest(CVut, CtestManager);
                            return CDCCoinInsertion1EurTest;
                        }
                    case TestProvider.COIN_INSERT_TWO_EUR_TEST:
                        {
                            DCCoinInsertion2EurTest CDCCoinInsertion2EurTest = new DCCoinInsertion2EurTest(CVut, CtestManager);
                            return CDCCoinInsertion2EurTest;
                        }
                    case TestProvider.POLL_DEVICE_TEST:
                        {
                            DCPollingTest CDCPollingTest = new DCPollingTest(CVut, CtestManager);
                            return CDCPollingTest;
                        }
                    case TestProvider.TABLE_TEST:
                        {
                            DCTableTest CDCTableTest = new DCTableTest(CVut, CtestManager);
                            return CDCTableTest;
                        }
                    case TestProvider.POLL_HMI_TEST:
                        {
                            DCPollHMITest CDCPollHMITest = new DCPollHMITest(CVut, CtestManager);
                            return CDCPollHMITest;
                        }
                    case TestProvider.LS1_STABILITY_TEST:
                        {
                            DCLS1StabilityTest CDCLS1StabilityTest = new DCLS1StabilityTest(CVut, CtestManager);
                            return CDCLS1StabilityTest;
                        }
                    case TestProvider.LS2_STABILITY_TEST:
                        {
                            DCLS2StabilityTest CDCLS2StabilityTest = new DCLS2StabilityTest(CVut, CtestManager);
                            return CDCLS2StabilityTest;
                        }
                    case TestProvider.LS3_STABILITY_TEST:
                        {
                            DCLS3StabilityTest CDCLS3StabilityTest = new DCLS3StabilityTest(CVut, CtestManager);
                            return CDCLS3StabilityTest;
                        }
                    case TestProvider.VALIDATOR_MEAS_PERFORMANCE_ANALYSIS_TEST:
                        {
                            DCValidatorMeasPerformanceTest CDCValidatorMeasPerformanceTest = new DCValidatorMeasPerformanceTest(CVut, CtestManager);
                            return CDCValidatorMeasPerformanceTest;
                        }
                    case TestProvider.SOLENOID_TEST:
                        {
                            DCSolenoidsTest CDCSolenoidsTest = new DCSolenoidsTest(CVut, CtestManager);
                            return CDCSolenoidsTest;
                        }
                    case TestProvider.SORTER_GATE_CHECK_TEST:
                        {
                            DCSorterGateCheckTest CDCSorteGateCheckTest = new DCSorterGateCheckTest(CVut, CtestManager);
                            return CDCSorteGateCheckTest;
                        }
                    case TestProvider.MATERIAL_SENSOR_ANALYSIS_TEST:
                        {
                            DCMaterialSensorAnalysis CDCMaterialSensorAnalysis = new DCMaterialSensorAnalysis(CVut, CtestManager);
                            return CDCMaterialSensorAnalysis;
                        }
                }
            }
            return null;
        }
    }
}
