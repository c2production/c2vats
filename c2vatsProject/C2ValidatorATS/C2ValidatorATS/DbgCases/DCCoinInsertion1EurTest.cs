﻿/**
 * Class: DCCoinInsertion1EurTest
 * Debug Case Class. Measure and get verdict when a 1 euro coin is insterted. The data is compared with the limits according to the limits in database.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Properties;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.DbgCases
{
    class DCCoinInsertion1EurTest : TCBaseAbstract
    {
        const int NUMBER_OF_MEASURES = 24;
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        SecurityManager CSecMngr;
        byte[] upperLimits;
        byte[] lowerLimits;
        List<Image> animationInfo;

        public DCCoinInsertion1EurTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            TEST_CASE_NAME = TestProvider.COIN_INSERT_ONE_EUR_TEST;
            PrepareKeyForDecryption();
            upperLimits = CVut.IsRIMPresent ? CoinProvider.EuroCoins.OneEuroUpperLimitsRIM : CoinProvider.EuroCoins.OneEuroUpperLimits;
            lowerLimits = CVut.IsRIMPresent ? CoinProvider.EuroCoins.OneEuroLowerLimitsRIM : CoinProvider.EuroCoins.OneEuroLowerLimits;
            CTestManager.TestExecSuspended = false;
            animationInfo = new List<Image>();
            animationInfo = new List<Image>();
            animationInfo.Add(Resources.coinDrop1euro);
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        private void PrepareKeyForDecryption()
        {
            if (CVut.GetProductionIdData() != null)
                CSecMngr = new SecurityManager((byte[])CVut.GetProductionIdData());
        }
        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME * 10; //30*100 5 min
            int sleepTime = 1000;
            byte[] measureRsp = null;
            byte[] measures = new byte[NUMBER_OF_MEASURES];
            bool coinMeasureFail = false;
            int dropAttempts = 2;

            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg) || CSecMngr == null)
            {
                if (CSecMngr == null)
                    Log.Error(this.GetType().Name, "Error communicating with Validator");
                else
                    Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            CVut.GetMeasuredValues(ref executionResultMsg); //Empty the buffer before tc start
            CTestManager.ShowUserAnimationInfo(animationInfo);
            CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.DROP_A_1_EURO_COIN, Color.DarkBlue);
            while (dropAttempts > 0 & userInteractionTimeOut > 0)
            {
                coinMeasureFail = false;
                if ((measureRsp = CVut.GetMeasuredValues(ref executionResultMsg)) != null && measureRsp.Count() == 28)
                {
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Coin droped and measured");
                    CSecMngr.Decrypt(ref measureRsp, 2); //the second param is the offset i.e. from wherea i want to decrypt/encrypt
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Coin measured values decrypted");	

					for (int i = 0; i < NUMBER_OF_MEASURES; i++)
					{
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Measure M" + (i + 1) + " ------");
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Upper Limit = " + upperLimits[i]);
                        measures[i] = measureRsp[i + 4];
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Measure Value= " + measures[i]);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Lower Limit = " + lowerLimits[i]);
                        if (!(measures[i] <= upperLimits[i] && measures[i] >= lowerLimits[i]))
                        {
                            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " M" + (i + 1) +" Value " + measures[i] + " is OUT OF LIMITS");
                            coinMeasureFail = true;
                            CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.DROP_FAIL_INSERT_AGAIN, Color.Red);
                        }
                        
                    }
                    if (coinMeasureFail)
                        dropAttempts -= 1;
                    else
                    {
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Execution End with Pass");
                        return true;
                    }
                }
                System.Threading.Thread.Sleep(sleepTime);
                userInteractionTimeOut = userInteractionTimeOut - sleepTime;
            }
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " Execution End. Some values are Out of Limits ");
            return false;
        }
    }
}
