﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.DbgCases
{
    class DCSolenoidsTest  : TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;

        public DCSolenoidsTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            TEST_CASE_NAME = TestProvider.SOLENOID_TEST;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            
            for (int i = 1; i < 5; i++)
            {
                int movingAttempts = 0;
                System.Threading.Thread.Sleep(300);
                if (i == 4) i = 7;
                while (!CVut.MoveSorterGates((byte)i, ref executionResultMsg) && movingAttempts < 3)
                {
                    movingAttempts++;
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Solenoid " + ((i==7) ? 4: i) + " Fail. Attempt " + movingAttempts);
                    System.Threading.Thread.Sleep(200);
                }
                if (movingAttempts == 3)
                {
                    Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + "Fail to send command to Validator. Solenoid " + ((i==7) ? 4: i) + " Fail afte 3 atempts");
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return false;
                }
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Solenoid " + ((i == 7) ? 4 : i) + " OK");
            }
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return true;
        }
    }
}
