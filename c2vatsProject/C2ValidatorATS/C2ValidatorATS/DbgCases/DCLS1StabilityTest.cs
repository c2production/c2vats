﻿/**
 * Class: DCLS1StabilityTest
 * Debug Case Class, this class repeat the test of LS1 several times to check the stability of the sensor.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.DbgCases
{
    class DCLS1StabilityTest : TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;

        public DCLS1StabilityTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            TEST_CASE_NAME = TestProvider.LS1_STABILITY_TEST;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            byte sensorValueResult = 0;
            byte[] lightSensorsStatustResp = null;
            int testRepetition = 30;

            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);
            lightSensorsStatustResp = CVut.GetLightSensorsStatus(ref executionResultMsg);
            //If no DBLOCK modify byte 2 bit 4 in Function Setup 
            if ((lightSensorsStatustResp[0] & lightSensorsStatustResp[1] & lightSensorsStatustResp[2]) == 0xFE) //No datblock in it
            {
                byte[] dataTableFuncSetup = CVut.CVutTableOps.GetDataFromFunctionSetupTbl();
                if (!((dataTableFuncSetup.Count() != 0) && CVut.CVutTableOps.WriteDataToFunctionSetupTbl(1, new byte[] { (byte)(dataTableFuncSetup[1] & 0xDF) })))
                {
                    Log.Error(this.GetType().Name, " NO able To Measure the Light Sensors. Modification of datablock to meka meassurment available Fail!");
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return false;
                }
            }

            sensorValueResult = CVut.GetLightSensorLs1Status(ref executionResultMsg);
            Log.Info(this.GetType().Name, executionResultMsg);
            if ((sensorValueResult != 0xff) && (executionResultMsg == Msgs.Info.RESULT_OK))
            {
                while (testRepetition > 0)
                {
                    sensorValueResult = CVut.GetLightSensorLs1Status(ref executionResultMsg);                 
                    Log.Info(this.GetType().Name, "Sensor LS1 Measured value: " + sensorValueResult + " =  ~ " + sensorValueResult * 3 + "µs");
                    System.Threading.Thread.Sleep(50);
                    testRepetition--;
                }
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                return true;
            }
           executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
           Log.Error(this.GetType().Name, executionResultMsg);
            return false;
        }
    }
}
