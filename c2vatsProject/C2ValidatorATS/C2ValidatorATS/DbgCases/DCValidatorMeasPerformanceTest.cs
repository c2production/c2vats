﻿/**
 * Class: DCValidatorMeasPerformanceTest
 * Debug Case Class. The performance of the 1 or more validators are analyzed by measuring dropping coins and drawing in a graphic the result;
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.DbgCases
{
    class DCValidatorMeasPerformanceTest : TCBaseAbstract
    {
        const int NUMBER_OF_MEASURES = 24;
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        SecurityManager CSecMngr;
        byte[] upperLimits;
        byte[] lowerLimits;

        public DCValidatorMeasPerformanceTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            TEST_CASE_NAME = TestProvider.VALIDATOR_MEAS_PERFORMANCE_ANALYSIS_TEST;
            PrepareKeyForDecryption();
            CTestManager.ShowCoinStatisticsUI();
            CTestManager.TestExecSuspended = false;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        private void PrepareKeyForDecryption()
        {
            if (CVut.GetProductionIdData() != null)
                CSecMngr = new SecurityManager((byte[])CVut.GetProductionIdData());
        }
        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME;
            int sleepTime = 1000;
            List<int> meaureRemarks = new List<int>(); //0,2,4...position, specify the index of the Measure with Remarks. 1,3,4..position specify the problem(0 = touching the limit, 1= out of limit)
            byte[] measureRsp = null;
            byte[] measures = new byte[NUMBER_OF_MEASURES];
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg) || CSecMngr == null)
            {
                if (CSecMngr == null)
                    Log.Error(this.GetType().Name, "Error communicating with Validator");
                else
                    Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            CVut.GetMeasuredValues(ref executionResultMsg); //Empty the buffer before tc start
            while (!CTestManager.TestExecSuspended) 
            {
                if(CTestManager.NewCoinSelected)
                {

                    upperLimits = CoinProvider.GetCoinUpperLimits(CTestManager.CoinSelected, CTestManager.ForceTestWithRIMSensor);
                    lowerLimits = CoinProvider.GetCoinLowerLimits(CTestManager.CoinSelected, CTestManager.ForceTestWithRIMSensor);
                    CTestManager.ShowLimitsInTable(this, upperLimits, lowerLimits);
                    CTestManager.NewCoinSelected = false;
                }
                meaureRemarks.Clear(); //Delete information(FAIL or touch limits) of last measurement
                if ((upperLimits != null && lowerLimits != null) && (measureRsp = CVut.GetMeasuredValues(ref executionResultMsg)) != null && measureRsp.Count() == 28)
                {

                    for (int i = 3; i < 25; i++)
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + "measureRsp no encrypted " + i + "= " + measureRsp[i] + "  Hexa --> " + measureRsp[i].ToString("X"));
                    CSecMngr.Decrypt(ref measureRsp, 2); //the second param is the offset i.e. from wherea i want to decrypt/encrypt
						
					for (int i = 0; i < NUMBER_OF_MEASURES; i++)
					{
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + "measureRsp encrypted " + i + "= " + measureRsp[i + 4]);
                        measures[i] = measureRsp[i + 4];
                        if (!(measures[i] <= upperLimits[i] && measures[i] >= lowerLimits[i]))
                        {
                            meaureRemarks.Add(i); //indicates the index of the faulty measure
                            meaureRemarks.Add(1); //indicate the the measure is out of limit
                        }
                        else if(measures[i] == upperLimits[i] || measures[i] == lowerLimits[i])//We check if it is touching the limit
                        {
                            meaureRemarks.Add(i); //indicates the index of the faulty measure
                            meaureRemarks.Add(0); //indicate the the measure is touching the limits
                        }
                    }

                    CTestManager.ShowMeasuredValuesToPlotGraphic(measures);
                }
                System.Threading.Thread.Sleep(sleepTime);
                userInteractionTimeOut = userInteractionTimeOut - sleepTime;
            }
            CTestManager.DestroyMeasuresTable();
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return true;
        }
    }
}
