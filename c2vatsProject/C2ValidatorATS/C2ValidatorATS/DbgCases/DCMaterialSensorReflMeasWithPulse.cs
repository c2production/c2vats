﻿/**
 * Class: DCMaterialSensorCoil2WithPulse
 * Debug Case Class. It checks values of the Material sensor. the Test is with pulse.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.DbgCases
{
    class DCMaterialSensorReflMeasWithPulse: TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        bool withPulse = true;
        byte[] materialSensorStatusRes;


        public DCMaterialSensorReflMeasWithPulse(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            TEST_CASE_NAME = TestProvider.MATERIAL_SENSOR_REFLECTIVE_MEAS_WITH_PULSE;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            materialSensorStatusRes = null;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        private string GetPinName(int pinNr)
        {
            switch (pinNr)
            {
                case 6: return "RA1";
                case 7: return "RA3";
                case 8: return "RA9";
                case 9: return "RB1";
                case 10: return "RB3";
                case 11: return "RB9";
                default: return "";
            }
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                return false;
            }

            if ((materialSensorStatusRes = CVut.GetMaterialSensorStatus(withPulse, ref executionResultMsg)) != null)
            {
                for (int i = 12; i < materialSensorStatusRes.Count(); i++) //byte coilPinInfo in materialSensorStatusRes)
                {

                    if ((i % 2 == 0) && !IsCoilPinInfoInsideTolerance(Concatenate(materialSensorStatusRes[i + 1], materialSensorStatusRes[i]), i))
                    {
                        executionResultMsg = Msgs.Error.MATERIAL_SENSORS_VALUE_OUT_OF_TOLERANCE;
                        Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
                        return false;
                    }
                }
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                return true;
            }
            else
            {
                executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                return false;
            }
        }

        private bool IsCoilPinInfoInsideTolerance(int coilInfo, int byteNr)
        {
            //24 bytes. 2 for each coil pin information
            //
            
            int coilPinNr = byteNr / 2;

            if (coilInfo >= 400 && coilInfo <= 650)
            {
                Log.Info(this.GetType().Name, "Coil pin " + GetPinName(coilPinNr) + " Value is: " + coilInfo);
                return true;
            }

            Log.Error(this.GetType().Name, "Coil pin " + GetPinName(coilPinNr) + " Value is: " + coilInfo);
            return false;
        }

        private int Concatenate(byte b1, byte b2)
        {
            return b1 << 8 | b2;

        }
    }
}

