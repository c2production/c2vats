﻿/**
 * Class: DCLightSensorLS1Test
 * Debug Case Class. It checks the status of the light sensor Ls3
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;


namespace C2ValidatorATS.DbgCases
{
    class DCLightSensorLS3Test : TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;

        public DCLightSensorLS3Test(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            TEST_CASE_NAME = TestProvider.LIGHT_SENSOR_LS2_TEST;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            byte sensorValueResult = 0;
            byte[] lightSensorsStatustResp = null;
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);
            lightSensorsStatustResp = CVut.GetLightSensorsStatus(ref executionResultMsg);
            //If no DBLOCK modify byte 2 bit 4 in Function Setup 
            if ((lightSensorsStatustResp[0] & lightSensorsStatustResp[1] & lightSensorsStatustResp[2]) == 0xFE) //No datblock in it
            {
                byte[] dataTableFuncSetup = CVut.CVutTableOps.GetDataFromFunctionSetupTbl();
                if (!((dataTableFuncSetup.Count() != 0) && CVut.CVutTableOps.WriteDataToFunctionSetupTbl(1, new byte[] { (byte)(dataTableFuncSetup[1] & 0xDF) })))
                {
                    Log.Error(this.GetType().Name, " NO able To Measure the Light Sensors. Modification of datablock to meka meassurment available Fail!");
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return false;
                }
            }
            sensorValueResult = CVut.GetLightSensorLs3Status(ref executionResultMsg);

            if ((sensorValueResult != 0xff) && (executionResultMsg == Msgs.Info.RESULT_OK))
            {
                //Valid value 1 to 3
                if (sensorValueResult >= 1 && sensorValueResult <= 3)
                {
                    Log.Info(this.GetType().Name, executionResultMsg);
                    Log.Info(this.GetType().Name, "Sensor LS3 Measured value: " + sensorValueResult + " =  ~ " + sensorValueResult * 3 + "µs");
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return true;
                }//According to the spec max value is 8 
                else if (sensorValueResult <= 8)
                {
                    executionResultMsg = Msgs.Warning.LS_SENSORS_VALUE_IN_TOLERANCE;
                    Log.Warning(this.GetType().Name, executionResultMsg);
                    Log.Info(this.GetType().Name, "Sensor LS3 Measured value: " + sensorValueResult + " =  ~ " + sensorValueResult * 3 + "µs");
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return true;
                }
                else
                {
                    executionResultMsg = Msgs.Error.LS_SENSORS_VALUE_OUT_OF_TOLERANCE;
                    Log.Info(this.GetType().Name, "Sensor LS3 Measured value: " + sensorValueResult + " =  ~ " + sensorValueResult * 3 + "µs");
                    Log.Error(this.GetType().Name, executionResultMsg);
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return false;
                }
            }
            executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
            Log.Error(this.GetType().Name, executionResultMsg);
            return false;
        }
    }
}

