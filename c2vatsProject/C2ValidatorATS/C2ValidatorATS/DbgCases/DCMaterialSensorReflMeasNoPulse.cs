﻿/**
 * Class: DCMaterialSensorCoil2NoPulse
 * Debug Case Class. It checks values of the Material sensor .
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.DbgCases
{
    class DCMaterialSensorReflMeasNoPulse : TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        bool withPulse = false;
        byte[] materialSensorStatusRes;


        public DCMaterialSensorReflMeasNoPulse(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            TEST_CASE_NAME = TestProvider.MATERIAL_SENSOR_REFLECTIVE_MEAS_NO_PULSE;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            materialSensorStatusRes = null;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        private string GetPinName(int pinNr)
        {
            switch (pinNr)
            {
                case 6: return "RA1";
                case 7: return "RA3";
                case 8: return "RA9";
                case 9: return "RB1";
                case 10: return "RB3";
                case 11: return "RB9";
                default: return "";
            }
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                return false;
            }

            if ((materialSensorStatusRes = CVut.GetMaterialSensorStatus(withPulse, ref executionResultMsg)) != null)
            {
                for (int i = 12; i < materialSensorStatusRes.Count(); i++) //byte coilPinInfo in materialSensorStatusRes)
                {
                    if ((i % 2 == 0) && !IsCoilPinInfoInsideTolerance(materialSensorStatusRes[i], i))
                    {
                        executionResultMsg = Msgs.Error.MATERIAL_SENSORS_VALUE_OUT_OF_TOLERANCE;
                        return false;
                    }
                }
                return true;
            }
            else
            {
                executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                return false;
            }
        }

        private bool IsCoilPinInfoInsideTolerance(byte coilInfo, int byteNr)
        {
            //24 bytes. 2 for each coil pin information
            int coilPinNr = byteNr / 2;

            if (coilInfo > 20 && coilInfo < 100)
            {
                Log.Info(this.GetType().Name, "Coil pin " + GetPinName(coilPinNr) + " Value is: " + coilInfo);
                return true;
            }

            Log.Error(this.GetType().Name, "Coil pin " + GetPinName(coilPinNr) + " Value is: " + coilInfo);
            return false;
        }
    }
}
