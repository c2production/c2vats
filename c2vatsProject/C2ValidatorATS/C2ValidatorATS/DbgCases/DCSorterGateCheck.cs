﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Properties;
using C2ValidatorATS.Vut;
using System.Drawing;
using System.Windows.Forms;

namespace C2ValidatorATS.DbgCases
{
    
        //Test Case: Sorter Gate Funcionality Test
    class DCSorterGateCheckTest : TCBaseAbstract
    {

        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        Random rndSorterGate;
        int[] sorterGatesStatus;
        List<Image> animationInfo;
        bool stopButtomPressed;

        public DCSorterGateCheckTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            TEST_CASE_NAME = TestProvider.SORTER_GATE_CHECK_TEST;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            rndSorterGate = new Random();
            sorterGatesStatus = new int[] { 0, 0, 0, 0 };
            animationInfo = new List<Image>();
            CTestManager.TestExecSuspended = false;
            stopButtomPressed = false;
            CTestManager.UserImageClicked(0,0);
        }

        //protected void btn_stop_click(Object sender, EventArgs e)
        //{
        //    stopButtomPressed = true;
        //}

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        public int GetUserSelectedSorterGate()
        {
            return TranslateCoordinatesToSorterGate(CTestManager.GetUserImageClickCoordinates());
        }

        public int TranslateCoordinatesToSorterGate(int[,] coordinates)
        {
            if ((coordinates[0, 0] > 340) && (coordinates[0, 0] < 475) && (coordinates[0, 1] > 104) && (coordinates[0, 1] < 150))
            {
                return 7; // SorterGate 0 is the number 7 which is the Acceptance gate
            }
            else if ((coordinates[0, 0] > 345) && (coordinates[0, 0] < 475) && (coordinates[0, 1] > 165) && (coordinates[0, 1] < 200))
            {
                return 1;
            }
            else if ((coordinates[0, 0] > 220) && (coordinates[0, 0] < 360) && (coordinates[0, 1] > 205) && (coordinates[0, 1] < 265))
            {
                return 2;
            }
            else if ((coordinates[0, 0] > 140) && (coordinates[0, 0] < 350) && (coordinates[0, 1] > 270) && (coordinates[0, 1] < 330))
            {
                return 3;
            }
            else if ((coordinates[0, 0] > 72) && (coordinates[0, 0] < 185) && (coordinates[0, 1] > 125) && (coordinates[0, 1] < 200))
            {
                stopButtomPressed = true;
                return 0;
            }
            return -1;
        }

        private string GetSorterGateName(int sgNr)
        {
            switch (sgNr)
            {
                case 7: return "Acceptance Gate";
                case 1: return "Gate Tube/Cashbox";
                case 2: return "Gate Left/Right";
                case 3: return "Gate Side/Mid";
                default: return "No Gate Activated";
            }
        }

        public override bool RunTestCase(ref string executionResultMsg)
        {
            stopButtomPressed = false;
            int sorterGateClickedByUser = -1; //none sortergate
            CTestManager.ShowInfoInUserIndicatorLabel(Msgs.Uii.ACTIVATE_SG_CLICKING_IN_COLORED_AREA, Color.DarkBlue);
            animationInfo.Clear();
            animationInfo.Add(Resources.SGwithStop);
            CTestManager.ShowUserAnimationInfo(animationInfo);

            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");
            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);
         
            
            while (!stopButtomPressed && !CTestManager.TestExecSuspended)
            {
                sorterGateClickedByUser = GetUserSelectedSorterGate();
                if (sorterGateClickedByUser != -1 && sorterGateClickedByUser != 0)
                {
                    int movingAttempts = 0;
                    while (!CVut.MoveSorterGates((byte)sorterGateClickedByUser, ref executionResultMsg) & movingAttempts < 3)
                    {
                        movingAttempts++;
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Fail to move the gate. Attempt " + movingAttempts);
                        System.Threading.Thread.Sleep(200);
                    }
                    if (movingAttempts == 3)
                    {
                        CTestManager.DestroyFailPnlVutDoesntRespond();
                        CTestManager.StopShowingInfoInUserIndicationLabel();
                        CTestManager.StopUserAnimationInfo();
                        executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                        Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                        return false;
                    }
                    CTestManager.UserImageClicked(0, 0);
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Sorter Gate Active " + GetSorterGateName(sorterGateClickedByUser));
                    System.Threading.Thread.Sleep(500);           
                }
                
            }
            
            CTestManager.DestroyFailPnlVutDoesntRespond();
            CTestManager.StopShowingInfoInUserIndicationLabel();
            CTestManager.StopUserAnimationInfo();
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return true;
        }
    }

}
