﻿/**
 * Class: DCMaterialSensorCoil2NoPulse
 * Debug Case Class. It checks values of the Material sensor .
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.DbgCases
{
    class DCMaterialSensorAnalysis : TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        bool withPulse = false;
        byte[] materialSensorStatusRes;


        public DCMaterialSensorAnalysis(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            TEST_CASE_NAME = TestProvider.MATERIAL_SENSOR_ANALYSIS_TEST;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            materialSensorStatusRes = null;
            CTestManager.TestExecSuspended = false;
            CTestManager.ShowMaterialSensorPanel();
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        private string GetPinName(int pinNr)
        {
            switch (pinNr)
            {
                case 0: return "TC1";
                case 1: return "TC3";
                case 2: return "TC9";
                case 3: return "TA1";
                case 4: return "TA3";
                case 5: return "TA9";
                case 6: return "RA1";
                case 7: return "RA3";
                case 8: return "RA9";
                case 9: return "RB1";
                case 10: return "RB3";
                case 11: return "RB9";
                default: return "";
            }
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            int[] measureResultAndRemarks = new int[24]; //Store the result and in next byte info about whether it fail=1 or not=0
            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            Log.Info(this.GetType().Name, Msgs.Info.TC_INITIAL_CONDITIONS_OK);
            while (!CTestManager.TestExecSuspended) //userInteractionTimeOut > 0) & CTestManager.TestExecSuspended)
            {
                
                if (CTestManager.doMaterialSensorNoPulseTest)
                {
                    if ((materialSensorStatusRes = CVut.GetMaterialSensorStatus(false, ref executionResultMsg)) != null)
                    {
                        for (int i = 0; i < materialSensorStatusRes.Count(); i++) //byte coilPinInfo in materialSensorStatusRes)
                        {   //16bits, the less significant byte is always 0. Therefore select the even bytes in the array
                            if ((i % 2 == 0))
                            {
                                if (!IsNoPulseResultInsideTolerance(materialSensorStatusRes[i], i))
                                {
                                    executionResultMsg = Msgs.Error.MATERIAL_SENSORS_VALUE_OUT_OF_TOLERANCE;
                                    Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
                                    measureResultAndRemarks[i] = materialSensorStatusRes[i];
                                    measureResultAndRemarks[i + 1] = 1;
                                    //Fail, if chbx stop on fail checked, stop measuring
                                    if (CTestManager.stopOnFailure)
                                    {
                                        CTestManager.runContinuously = false;
                                        CTestManager.StopOnFailTriggered();
                                    }
                                }
                                else
                                {
                                    measureResultAndRemarks[i] = materialSensorStatusRes[i];
                                    measureResultAndRemarks[i + 1] = 0;
                                } 
                            }
                        }
                        CTestManager.ShowMaterialSensorMeasuredDataInTbl(measureResultAndRemarks);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                        CTestManager.doMaterialSensorNoPulseTest = CTestManager.runContinuously ? true : false;
                       
                    }
                    else
                    {
                        executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                        Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    }
                }
                else if (CTestManager.doMaterialSensorWithPulseTest)
                {
                    if ((materialSensorStatusRes = CVut.GetMaterialSensorStatus(true, ref executionResultMsg)) != null)
                    {
                        for (int i = 0; i < materialSensorStatusRes.Count(); i++) //byte coilPinInfo in materialSensorStatusRes)
                        {

                            if (i % 2 == 0)
                            {
                                if (!IsWithPulseResultInsideTolerance(Concatenate(materialSensorStatusRes[i + 1], materialSensorStatusRes[i]), i))
                                {
                                    executionResultMsg = Msgs.Error.MATERIAL_SENSORS_VALUE_OUT_OF_TOLERANCE;
                                    Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + executionResultMsg);
                                    measureResultAndRemarks[i] = Concatenate(materialSensorStatusRes[i + 1], materialSensorStatusRes[i]);
                                    measureResultAndRemarks[i + 1] = 1;
                                    //Fail, if chbx stop on fail checked, stop measuring
                                    if (CTestManager.stopOnFailure)
                                    {
                                        CTestManager.runContinuously = false;
                                        CTestManager.StopOnFailTriggered();
                                    }
                                }
                                else
                                {
                                    measureResultAndRemarks[i] = Concatenate(materialSensorStatusRes[i + 1], materialSensorStatusRes[i]);
                                    measureResultAndRemarks[i + 1] = 0;                                  
                                }
                            }
                               
                        }
                        CTestManager.ShowMaterialSensorMeasuredDataInTbl(measureResultAndRemarks);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                        CTestManager.doMaterialSensorWithPulseTest = CTestManager.runContinuously ? true : false;
                    }
                    else
                    {
                        executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR;
                        Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " " + executionResultMsg);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    }
                }
                System.Threading.Thread.Sleep(CTestManager.runContinuously ? 1000 : 300);
            }
            CTestManager.DestroyMaterialSensorAnalysis();
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return true;
        }

        //Check limits
        private bool IsNoPulseResultInsideTolerance(byte pinMeasResult, int byteNr)
        {
            //24 bytes. 2 for each coil pin information
            int coilPinNr = byteNr / 2;
            if(!withPulse)
            if (coilPinNr < 6)
            {
                if (pinMeasResult >= 80 && pinMeasResult <= 200)
                {
                    Log.Info(this.GetType().Name, "Coil pin " + GetPinName(coilPinNr) + " Value is: " + pinMeasResult);
                    return true;
                }
            }
            else
            {
                if (pinMeasResult >= 20 && pinMeasResult <= 100)
                {
                    Log.Info(this.GetType().Name, "Coil pin " + GetPinName(coilPinNr) + " Value is: " + pinMeasResult);
                    return true;
                }
            }
            Log.Error(this.GetType().Name, "Coil pin " + GetPinName(coilPinNr) + " Value is: " + pinMeasResult);
            return false;
        }

        private bool IsWithPulseResultInsideTolerance(int pinMeasResult, int byteNr)
        {
            //24 bytes. 2 for each coil pin information
            int coilPinNr = byteNr / 2;
            if (coilPinNr < 6)
            {
                if (pinMeasResult >= 800 && pinMeasResult <= 1000)
                {
                    Log.Info(this.GetType().Name, "Coil pin " + GetPinName(coilPinNr) + " Value is: " + pinMeasResult);
                    return true;
                }
            }
            else
            {
                if (pinMeasResult >= 400 && pinMeasResult <= 650)
                {
                    Log.Info(this.GetType().Name, "Coil pin " + GetPinName(coilPinNr) + " Value is: " + pinMeasResult);
                    return true;
                }
            }
            Log.Error(this.GetType().Name, "Coil pin " + GetPinName(coilPinNr) + " Value is: " + pinMeasResult);
            return false;
        }

        private int Concatenate(byte b1, byte b2)
        {
            return b1 << 8 | b2;
        }

    }
}
