﻿/**
 * Class: DCCoinDropEvaluationTest
 * Debug Case Class. Retreive measured data when a coin is dropped and compare it with coins limits
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;
using C2ValidatorATS.Vut.VutDBlocksInfo;

namespace C2ValidatorATS.DbgCases
{
    class DCCoinDropEvaluationTest : TCBaseAbstract
    {
        
        const int NUMBER_OF_MEASURES = 24;
        const byte RIM_SENSOR_MASK = 0x01;
        string testCaseStatus;
        string currentISO;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        SecurityManager CSecMngr;
        byte[] upperLimits;
        byte[] lowerLimits;
        byte[] originalFunctionSetupContent;
        byte[] originalProdIdContent;
        bool restoreOriginalData;
 

        public DCCoinDropEvaluationTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            TEST_CASE_NAME = TestProvider.COIN_DROP_EVALUATION_TEST;
            PrepareKeyForDecryption();
            AEDInvestigation.Instance.statisticsActivated = false;
            AEDInvestigation.Instance.fakeCoinStatistics = false;
            CTestManager.ShowCoinMeasuresTable();
            CTestManager.TestExecSuspended = false;
            restoreOriginalData = false;
            currentISO = "";
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        private void PrepareKeyForDecryption()
        {
            if (CVut.GetProductionIdData() != null)
                CSecMngr = new SecurityManager((byte[])CVut.GetProductionIdData());

        }

        private void FlashFormulaIntoValidator(string currentIso, ref string executionResultMsg)
        {
            switch (currentIso)
            {
                case "PLN": FlashFormula(0x04, ref executionResultMsg);
                    break;
                case "CHF":
                    {
                        if (CTestManager.ForceTestWithRIMSensor)
                        {
                            FlashFormula(0x08, ref executionResultMsg);
                            FlashPresenceOfRIMSensor(ref executionResultMsg);
                        }
                        else FlashFormula(0x00, ref executionResultMsg);
                        break;
                    }
                 case "AED": FlashFormula(0x0D, ref executionResultMsg);
                   break;
                default: FlashFormula(0x00, ref executionResultMsg);
                    break;
            }           
        }

        private void FlashPresenceOfRIMSensor(ref string executionResultMsg)
        {
            byte[] rimSensor = new byte[] { 0x01 };
            if (((originalProdIdContent = CVut.GetProductionIdData()) == null) || (originalProdIdContent.Count() != 27))
            {
                executionResultMsg = Msgs.Error.FAIL_TO_READ_DATA_FROM_TABLE;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg + "  Production Id Table");
                return;
            }
            if ((rimSensor[0] != (originalProdIdContent[22] & RIM_SENSOR_MASK))) //Flash it!
            {
                restoreOriginalData = true;
                if (CVut.WriteDataToProdIdTable(22, rimSensor))
                    executionResultMsg = Msgs.Info.RESULT_OK;
                else executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_WRITETABLE_TO_VALIDATOR;
            }
        }
        private void FlashFormula(byte formulaNr, ref string executionResultMsg)
        {
            byte[] formulaToActivate = new byte[] { formulaNr }; //Byte 30 
            //Function setup changes
            if (((originalFunctionSetupContent = CVut.GetFunctionSetupData()) == null) || (originalFunctionSetupContent.Count() != 32))
            {
                executionResultMsg = Msgs.Error.FAIL_TO_READ_DATA_FROM_TABLE;
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + executionResultMsg + "  Function Setup Table");
                return;
            }
            if ( (formulaNr != originalFunctionSetupContent[29])) //Flash it!
            {
                restoreOriginalData = true;
                if (CVut.WriteToFunctionSetupTable(29, formulaToActivate)) 
                    executionResultMsg = Msgs.Info.RESULT_OK;
                else executionResultMsg = Msgs.Error.FAIL_TO_SEND_CMD_WRITETABLE_TO_VALIDATOR;
            }
            else executionResultMsg = Msgs.Info.RESULT_OK;
        }

        private bool RestoreValidatorOriginalData()
        {

            if ((originalFunctionSetupContent != null) && restoreOriginalData && !CVut.WriteToFunctionSetupTable(0, originalFunctionSetupContent))
            {
                Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + "\" " + " No able to restore date in Validator. Original data corrupted");
                restoreOriginalData = false;
                return false;
            }
            restoreOriginalData = false;
            return true;
        }
        
        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME;
            int sleepTime = 1000;
            List<int> meaureRemarks = new List<int>(); //0,2,4...position, specify the index of the Measure with Remarks. 1,3,4..position specify the problem(0 = touching the limit, 1= out of limit)
            byte[] measureRsp = null;
            byte[] measures = new byte[NUMBER_OF_MEASURES];
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg) || CSecMngr == null)
            {
                if (CSecMngr == null)
                    Log.Error(this.GetType().Name, "Error communicating with Validator");
                else
                  Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }
            CVut.GetMeasuredValues(ref executionResultMsg); //Empty the buffer before tc start
            while (!CTestManager.TestExecSuspended) //userInteractionTimeOut > 0) & CTestManager.TestExecSuspended)
            {
                if(CTestManager.NewCoinSelected && CTestManager.NewCoinSrc == CTestManager.FROM_HARD_CODED)
                {
                    if (CTestManager.CurrentISO != this.currentISO)
                    {
                        if(restoreOriginalData )RestoreValidatorOriginalData(); //In case there were data to restore then do it
                        FlashFormulaIntoValidator(CTestManager.CurrentISO, ref executionResultMsg);//Flash formula if existing
                        if (executionResultMsg != Msgs.Info.RESULT_OK) continue;  //Repite la operacion
                        this.currentISO = CTestManager.CurrentISO;
                    }
                    upperLimits = CoinProvider.GetCoinUpperLimits(CTestManager.CoinSelected, CTestManager.ForceTestWithRIMSensor);
                    lowerLimits = CoinProvider.GetCoinLowerLimits(CTestManager.CoinSelected, CTestManager.ForceTestWithRIMSensor);
                    CTestManager.ShowLimitsInTable(this, upperLimits, lowerLimits);
                    CTestManager.NewCoinSelected = false;
                    CVut.GetMeasuredValues(ref executionResultMsg); //Empty the buffer before tc start
                }
                else if (CTestManager.NewCoinSelected && CTestManager.NewCoinSrc == CTestManager.FROM_DB)
                {
                    // TODO
                }
                else if (CTestManager.NewCoinSelected && CTestManager.NewCoinSrc == CTestManager.FROM_VALIDATOR)
                {
                    TblChannelIds CTblChannelIds =  CVut.GetTblChannelIds();
                    TblCoinLimits CTblCoinLimits = CVut.GetTblCoinLimits();
                   if( !CTblChannelIds.RetreiveAllData() || !CTblCoinLimits.RetreiveAllData())
                       continue;
                    
                    for (int i = 0; i < CTblChannelIds.CCoinIdChannels.Count(); i++)
                    {
                        if ((CTblChannelIds.CCoinIdChannels[i].coinID.ToString() == CTestManager.CoinSelected.Split(',')[0].Split(' ')[1]) && (CTblChannelIds.CCoinIdChannels[i].revision == CTestManager.CoinSelected.Split(',')[1].Split(' ')[2])
                        && (CTblChannelIds.CCoinIdChannels[i].precision.ToString() == CTestManager.CoinSelected.Split(',')[2].Split(' ')[2]))
                        {
                            upperLimits = new byte[24];
                            lowerLimits = new byte[24];
                            byte[] allData = CTblCoinLimits.GetTable(i + 1); //Just sending the real table number( i + 1) Get table adjust it to a real index
                            Array.Copy(allData, 0, upperLimits, 0, 24);
                            Array.Copy(allData, 24, lowerLimits, 0, 24);
                            CTestManager.ShowLimitsInTable(this, upperLimits, lowerLimits);
                            //Save In case statistics will be activated later                          
                            AEDInvestigation.Instance.storeLimits(lowerLimits, upperLimits);
                            CVut.GetMeasuredValues(ref executionResultMsg); //Empty the buffer before tc start
                            break;
                        }
                    }
                    CTestManager.NewCoinSelected = false;
                }

                meaureRemarks.Clear(); //Delete information(FAIL or touch limits) of last measurement
                if ((upperLimits != null && lowerLimits != null) && (measureRsp = CVut.GetMeasuredValues(ref executionResultMsg)) != null && measureRsp.Count() == 28)
                {
                    bool someMeasureFail = false;
                    for (int i = 3; i < 25; i++)
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + "measureRsp no encrypted " + i + "= " + measureRsp[i] + "  Hexa --> " + measureRsp[i].ToString("X"));
                    CSecMngr.Decrypt(ref measureRsp, 2); //the second param is the offset i.e. from wherea i want to decrypt/encrypt
						
					for (int i = 0; i < NUMBER_OF_MEASURES; i++)
                    {
                        
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + "measureRsp encrypted " + i + "= " + measureRsp[i + 4]);
                        measures[i] = measureRsp[i + 4];
                        //###### Testing the AED stuff ########
                        //if (i == 5) //M6 esta formula ayuda 1 aed revC vs orm mag och php1 
                        //{
                        //    measures[i] = (byte)(12 + (29 - measures[i]) * 2);
                        //}
                        //if(i == 16)
                        //{                           
                        //    //M17
                        //    measures[i] = (byte)((34 - measures[5]) * 5 + (measures[9] - 60) * 4 + measures[12]);                           
                        //}
                        //if (i == 22) //M23
                        //{
                        //    measures[i] = (byte)(measures[1] * 5 + measures[5] + 2* measures[17]  + 3 * measures[2] - measures[10]*3 - measures[9]*2 - 500);                           
                        //}
                       
                        //################################# Just for testing #######
                        
                        if (!(measures[i] <= upperLimits[i] && measures[i] >= lowerLimits[i]))
                        {
                            meaureRemarks.Add(i); //indicates the index of the faulty measure
                            meaureRemarks.Add(1); //indicate the the measure is out of limit
                            someMeasureFail = true;
                        }
                        else if(measures[i] == upperLimits[i] || measures[i] == lowerLimits[i])//We check if it is touching the limit
                        {
                            meaureRemarks.Add(i); //indicates the index of the faulty measure
                            meaureRemarks.Add(0); //indicate the the measure is touching the limits
                        }
                    }

                    CTestManager.ShowMeasuredDataInTable(measures, meaureRemarks);
                    if(AEDInvestigation.Instance.statisticsActivated)
                        AEDInvestigation.Instance.StoreMeasuredData(new List<byte>(measures), someMeasureFail, meaureRemarks); //Last parameters false = no out of limit data
                }
                System.Threading.Thread.Sleep(sleepTime);
                userInteractionTimeOut = userInteractionTimeOut - sleepTime;
            }
            CTestManager.DestroyMeasuresTable();
            if (restoreOriginalData) RestoreValidatorOriginalData(); //In case there were data to restore then do it
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return true;
        }
    }
}
