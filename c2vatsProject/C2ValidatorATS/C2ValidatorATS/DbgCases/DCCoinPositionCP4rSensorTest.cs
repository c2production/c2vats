﻿/**
 * Class: DCCoinPositionCP4lSensorTest
 * Debug Case Class. It checks the status of the Coin Position Sensor CP4r.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.DbgCases
{
    class DCCoinPositionCP4rSensorTest : TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;
        byte cPSensorsStatustRes;

        public DCCoinPositionCP4rSensorTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            TEST_CASE_NAME = TestProvider.COIN_POSITION_SENSOR_CP3S_TEST;
            cPSensorsStatustRes = 0xFF;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, Msgs.Error.FAIL_DURING_TEST_CASE_INITIALIZATION + " " + executionResultMsg);
                return false;
            }
            if((cPSensorsStatustRes = CVut.GetCoinPositionSensorCP4rStatus(ref executionResultMsg)) != 0xFF)
            {
                if (cPSensorsStatustRes >= 15)
                {
                    executionResultMsg = Msgs.Info.RESULT_OK;
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + executionResultMsg);
                    Log.Info(this.GetType().Name, "Sensor CP4R Measured value: " + cPSensorsStatustRes);
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return true;
                }
                else
                {
                    executionResultMsg = Msgs.Error.CP_SENSORS_VALUE_OUT_OF_TOLERANCE;
                    Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + executionResultMsg);
                    Log.Error(this.GetType().Name, "Sensor CP4R Measured value: " + cPSensorsStatustRes);
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
                    return false; 
                }
            }
            Log.Error(this.GetType().Name, "\"" + GetTestCaseName() + executionResultMsg);
            Log.Error(this.GetType().Name, "Sensor CP4R Measured value: " + cPSensorsStatustRes);
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return false;
        }
    }
}
