﻿/**
 * Class: DCPollHMITest
 * Debug Case Class. ATTENTION: Dont use it as it can destroy data in data blocks!!!
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.DbgCases
{
    class DCTableTest : TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;

        public DCTableTest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            TEST_CASE_NAME = TestProvider.POLL_DEVICE_TEST;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            //int userInteractionTimeOut = MAX_TC_COMPLETION_TIME * 100;
            //int sleepTime = 1000;
            //byte[] response = null;
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }

            
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Testin Deleting data");
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Data = 14");
            //### Writing to ProdId table
            //byte[] datatable = CVut.CVutTables.GetDataFromProductionIDTbl();
            //if (CVut.WriteDataToProdIdTable(0, new byte[] { 0x04 }))
            //{
            //    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Writing returned ok");
            //}
            //if (CVut.WriteDataToProdIdTable(3, new byte[] { 0x34 }))
            //{
            //    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Writing returned ok");
            //}
            //byte[] datatable1 = CVut.CVutTables.GetDataFromProductionIDTbl();

            ////### Trying with 
            byte[] datatablefd = CVut.CVutTableOps.GetDataFromFunctionSetupTbl();
            CVut.CVutTableOps.WriteDataToFunctionSetupTbl(1, new byte[] { 0xFF });
            System.Threading.Thread.Sleep(2000);
            byte[] datatablefd2 = CVut.CVutTableOps.GetDataFromFunctionSetupTbl();

            return false;
        }
    }
}
