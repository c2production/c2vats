﻿/**
 * Class: DCPollHMITest
 * Debug Case Class. Poll the HMI
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.DbgCases
{
    class DCPollHMITest : TCBaseAbstract
    {
        string testCaseStatus;
        ValidatorUnderTest CVut;
        TestManager CTestManager;
        CommAssistant CCommAssistant;

        public DCPollHMITest(ValidatorUnderTest Vut, TestManager testmngr)
        {
            CVut = Vut;
            CTestManager = testmngr;
            CCommAssistant = CommAssistant.Instance;
            testCaseStatus = Msgs.Uii.TEST_IN_PROGRESS;
            TEST_CASE_NAME = TestProvider.POLL_HMI_TEST;
        }

        public string GetTestCaseResult()
        {
            return testCaseStatus;
        }

        /// <summary>
        /// Method that control the test execution
        /// </summary>
        /// <param name="responseExecutiontMsg">Where the result of the execution is saved</param>
        /// <returns> True if Test Pass</returns>
        public override bool RunTestCase(ref string executionResultMsg)
        {
            int userInteractionTimeOut = MAX_TC_COMPLETION_TIME * 100;
            int sleepTime = 1000;
            byte[] response = null;
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " execution Starts ");

            if (!InitializeTestCase(CVut.ValidatorId, ref executionResultMsg))
            {
                Log.Error(this.GetType().Name, executionResultMsg);
                return false;
            }

            while (userInteractionTimeOut > 0)
            {
                if ((response = CVut.PollHMIAndReadInfo(ref executionResultMsg)) != null && (response.Count() > 0))
                {
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + "byte 1--> " + response[0].ToString("X") + "   byte 2--> " + response[1].ToString("X"));
                }
                else if (response.Count() == 0)
                {
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " No Response bytes from HMI");
                    Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Trying to Reset it");
                    //try to reset the hmi
                    if (CVut.ResetHMI(ref executionResultMsg))
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Device Reseted");
                    else
                    {
                        System.Threading.Thread.Sleep(1000);
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Device No Respond to Reset");
                        Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Trying to Reste the VUT");
                        if (CVut.ResetVut(ref executionResultMsg))
                        {
                            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " VUT Reseted");
                            System.Threading.Thread.Sleep(1000);
                        }
                        else Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " VUT does not resoponse to Reset Command");
                        System.Threading.Thread.Sleep(1000);
                    }
                    
                }
                else Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Unknown Response. Response =" + response);
                System.Threading.Thread.Sleep(sleepTime);
                userInteractionTimeOut = userInteractionTimeOut - sleepTime;
            }
            Log.Info(this.GetType().Name, "\"" + GetTestCaseName() + "\"" + " Execution End ");
            return true;
        }
    }
}

