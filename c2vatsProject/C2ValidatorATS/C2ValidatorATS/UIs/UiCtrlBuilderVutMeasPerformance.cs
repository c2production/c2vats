﻿/**
 * Class: UiCtrlBuilderVutMeasPerformance
 * This class builds the interface used during the debug test "Validator Meausrement Performance Test".
 * To avaid complexity in I did implement som analysis here instead of in the debug case itsself.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using C2ValidatorATS.Utils;

namespace C2ValidatorATS.UIs
{
    class UiCtrlBuilderVutMeasPerformance
    {
        string FROM_HARD_CODED = "Hard Coded";
        string FROM_DB = "DB";
        string FROM_VALIDATOR = "Validator";
        Panel PnlBase;
        Panel pnlWithOptions;
        List<Label> MeasuresAvergValue = new List<Label> { };
        List<Label> UpperLimits = new List<Label> { };
        List<Label> LowerLimits = new List<Label> { };
        List<MeasPlotPoint> PlotPoints = new List<MeasPlotPoint>{};

        int lblVerticalShiftMeasure = 25;
        int lblVerticalShiftLL = 50;
        int lblVerticalShiftUL = 90;
        int lineMeasureSeparation = 30; //lblVerticalShiftMeasure + 5
        int lineLLimitSeparation = 60; //lblVerticalShiftLL + 10;
        int measureNr = 24;
        int execMeasurments = 0;
        int lineNr = 72;
        int limitLinesLength = 500;
        int limitLinesStarPosX = 75;
        int limitLinesStartPosY = 45;
        Point[] lineStartPos;
        Point[] lineStopPos ;
        Point[] lineScrollOffSetStartPos;
        Point[] lineScrollOffSetStopPos;
        ComboBox cboxIsoSelector;
        ComboBox cboxCoinSelector;

        public struct MeasPlotPoint
        {
            public Point location;
            public Color color;
            public int size;
            public bool isOutOfLimit;
            public Point prevPointLocation;
            public byte measValue;
        }
    
       

        public void ShowCoinDropStatisticsUi(C2ATS CC2Mainform)
        {
            CreateMainPanel(CC2Mainform);
            CreatePanelWithOptions(CC2Mainform);
            CreateCoinSelectionCtrlsOnOptionPnl();
            CreateAndDrawLimitAndMeasurelables(new int[] { 20, 45 });
            CreateLimitLines();
            InitLimitValueLables();
        }
        private void CreateMainPanel(C2ATS CC2Mainform)
        {
            PnlBase = new Panel();
            //linePainter = PnlBase.CreateGraphics();
            PnlBase.Location = new Point(2, 90);
            PnlBase.Size = new Size(CC2Mainform.Size.Width - 23, 580);
            PnlBase.BackColor = Color.LightBlue;
            PnlBase.BorderStyle = BorderStyle.Fixed3D;
            PnlBase.AutoScroll = true;
            PnlBase.Parent = CC2Mainform;
            PnlBase.Paint += new System.Windows.Forms.PaintEventHandler(this.DrawLimitLines);
            PnlBase.Scroll += new System.Windows.Forms.ScrollEventHandler(this.RefreshPaintCoordinates);
            PnlBase.BringToFront();
        }
        private void CreatePanelWithOptions(C2ATS CC2Mainform)
        {
            pnlWithOptions = new Panel();
            pnlWithOptions.Location = new Point(2, 48);
            pnlWithOptions.Size = new Size(CC2Mainform.Size.Width - 23, 45);
            pnlWithOptions.BackColor = Color.CadetBlue;
            pnlWithOptions.BorderStyle = BorderStyle.FixedSingle;
            //pnlWithOptions.AutoScroll = true;
            pnlWithOptions.Parent = CC2Mainform;
            pnlWithOptions.BringToFront();
        }
        private void CreateCoinSelectionCtrlsOnOptionPnl()
        {
          
            cboxIsoSelector = new ComboBox();
            cboxIsoSelector.Size = new Size(100, 20);
            cboxIsoSelector.Location = new Point(20, 10);
            cboxIsoSelector.Text = "Select ISO";
            cboxIsoSelector.Items.AddRange(CoinProvider.Iso.GetIsos());
            cboxIsoSelector.SelectedValueChanged += new EventHandler(this.cboxIsoSelector_SelectedValueChanged);
            cboxIsoSelector.Parent = pnlWithOptions;
        
            cboxCoinSelector = new ComboBox();
            cboxCoinSelector.Size = new Size(100, 20);
            cboxCoinSelector.Location = new Point(130, 10);
            cboxCoinSelector.Text = "Select Coin";
            cboxCoinSelector.SelectedValueChanged += new EventHandler(this.cboxCoinSelector_SelectedValueChanged);
            cboxCoinSelector.Parent = pnlWithOptions;
       
        }

        private void CreateLimitLines()
        {
            lineStartPos = new Point[measureNr * 3];  //Array if 72 x,y coordinates
            lineStopPos = new Point[measureNr * 3];
            lineScrollOffSetStartPos = new Point[measureNr * 3];
            lineScrollOffSetStopPos = new Point[measureNr * 3];
            int measure = 0;
            //upperLimits Lines
            for (int i = 0; i < measureNr*3; i+=3)
            {
                    //Upper limit lines
                lineStartPos[i] = new Point(limitLinesStarPosX, limitLinesStartPosY + measure * lblVerticalShiftUL);
                lineScrollOffSetStartPos[i] = lineStartPos[i];
                lineStopPos[i] = new Point(limitLinesStarPosX + limitLinesLength, limitLinesStartPosY + measure * lblVerticalShiftUL);
                lineScrollOffSetStopPos[i] =  lineStopPos[i];
                //Measure lines
                lineStartPos[i + 1] = new Point(limitLinesStarPosX, limitLinesStartPosY + measure * lblVerticalShiftUL + lineMeasureSeparation);
                lineScrollOffSetStartPos[i + 1] = lineStartPos[i + 1];
                lineStopPos[i + 1] = new Point(limitLinesStarPosX + limitLinesLength, limitLinesStartPosY + measure * lblVerticalShiftUL + lineMeasureSeparation);
                lineScrollOffSetStopPos[i + 1] = lineStopPos[i + 1];
                //LLimits Line
                lineStartPos[i + 2] = new Point(limitLinesStarPosX, limitLinesStartPosY + measure * lblVerticalShiftUL + lineLLimitSeparation);
                lineScrollOffSetStartPos[i + 2] = lineStartPos[i + 2];
                lineStopPos[i + 2] = new Point(limitLinesStarPosX + limitLinesLength, limitLinesStartPosY + measure * lblVerticalShiftUL + lineLLimitSeparation);
                lineScrollOffSetStopPos[i + 2] = lineStopPos[i + 2];
                measure++;
            }

          
        }

        private void CreateAndDrawLimitAndMeasurelables(int[] startPos)
        {
            Label lblUpperLimits;// = new Label();
            Label lblLowerLimits;// = new Label();
            Label lblMeasuresNr;// = new Label();

            for (int i = 0; i < measureNr; i++)
            {
                lblUpperLimits = new Label();
                lblUpperLimits.Text = "UL";
                lblUpperLimits.AutoSize = true;
                lblUpperLimits.Font = new Font("Times New Roman", 10, FontStyle.Bold);
                lblUpperLimits.Location = new Point(startPos[0], startPos[1] + i * lblVerticalShiftUL);
                lblUpperLimits.Parent = PnlBase;
                //UpperLimits.Add(lblUpperLimits);
            }
            for (int i = 0; i < measureNr; i++)
            {
                lblMeasuresNr = new Label();
                lblMeasuresNr.Text = "M" + (i + 1);
                lblMeasuresNr.AutoSize = true;
                lblMeasuresNr.Font = new Font("Times New Roman",10 , FontStyle.Bold);
                lblMeasuresNr.ForeColor = Color.BlueViolet;
                lblMeasuresNr.Location = new Point(startPos[0], startPos[1] + i * lblVerticalShiftUL + lblVerticalShiftMeasure);
                lblMeasuresNr.Parent = PnlBase;
                //Measures.Add(lblMeasuresNr);
            }
            for (int i = 0; i < measureNr; i++)
            {
                lblLowerLimits = new Label();
                lblLowerLimits.Text = "LL";
                lblLowerLimits.AutoSize = true;
                lblLowerLimits.Font = new Font("Times New Roman", 10, FontStyle.Bold);
                lblLowerLimits.Location = new Point(startPos[0], startPos[1] + i * lblVerticalShiftUL + lblVerticalShiftLL);
                lblLowerLimits.Parent = PnlBase;
                //LowerLimits.Add(lblLowerLimits);
            }
        }
        private void DrawLimitLines(object sender, PaintEventArgs e)
        {
            Graphics linePainter = e.Graphics;
            int measureLineindicator = 1;  //El primer measureline tiene index 1. esto es para dar un color separado a Measure line
            for (int i = 0; i < lineNr; i++)
            {
                if (i == measureLineindicator)
                {
                    linePainter.DrawLine(new Pen(Color.Cyan, 1), lineScrollOffSetStartPos[i], lineScrollOffSetStopPos[i]);
                    measureLineindicator += 3;
                }
                else
                    linePainter.DrawLine(new Pen(Color.Azure, 1), lineScrollOffSetStartPos[i], lineScrollOffSetStopPos[i]);
                
            }
            //linePainter.Dispose();
            //DrawResultGraphic();
            //Graphics pointPainter = e.Graphics;
            if (PlotPoints != null && PlotPoints.Count() != 0)
            {
                for (int i = 0; i < PlotPoints.Count(); i++)
                {
                    linePainter.DrawLine(new Pen(PlotPoints[i].color, 3), new Point(PlotPoints[i].location.X, PlotPoints[i].location.Y + PnlBase.AutoScrollPosition.Y), new Point(PlotPoints[i].location.X + 3, PlotPoints[i].location.Y + PnlBase.AutoScrollPosition.Y));
                }
                //linePainter.Dispose();
            }
            linePainter.Dispose();
        }
        private void RefreshPaintCoordinates(object sender, ScrollEventArgs e)
        {
            for (int i = 0; i < lineNr; i++)
            {
                lineScrollOffSetStartPos[i] = new Point(lineStartPos[i].X, lineStartPos[i].Y + PnlBase.AutoScrollPosition.Y);
                lineScrollOffSetStopPos[i] = new Point(lineStopPos[i].X, lineStopPos[i].Y + PnlBase.AutoScrollPosition.Y);
            }
            if (PlotPoints != null && PlotPoints.Count() != 0)
            {
                
            }
        }
        protected void cboxIsoSelector_SelectedValueChanged(object sender, EventArgs e)
        {
            cboxCoinSelector.Items.Clear();
            cboxCoinSelector.Text = "Select Coin";
            if ((string)cboxIsoSelector.SelectedItem == "EUR")
            {
                cboxCoinSelector.Items.AddRange(CoinProvider.EuroCoins.GetAllEuroCoins());
            }
            else if ((string)cboxIsoSelector.SelectedItem == "CHF")
            {
                if (MessageBox.Show("Does the Validator have RIM Sensor?", "5 CHF coin Needs RIM Sensor", MessageBoxButtons.YesNo)
                   == DialogResult.Yes)
                {
                    TestManager.Instance.ForceTestWithRIMSensor = true;
                }
                else TestManager.Instance.ForceTestWithRIMSensor = false;
                cboxCoinSelector.Items.AddRange(CoinProvider.SwissFrancCoins.GetAllCHFCoins(TestManager.Instance.ForceTestWithRIMSensor));
            }
            else if ((string)cboxIsoSelector.SelectedItem == "AED")
            {
                cboxCoinSelector.Items.AddRange(CoinProvider.EmiratesCoins.GetAllAEDCoins());
            }
            else cboxCoinSelector.Items.AddRange(new string[] { }); //Empty Coin Selection
        }

        protected void cboxCoinSelector_SelectedValueChanged(object sender, EventArgs e)
        {
            TestManager.Instance.NewCoinSelection((string)cboxCoinSelector.SelectedItem, (string)cboxIsoSelector.SelectedItem, FROM_HARD_CODED);
        }

        private void InitLimitValueLables()
        {
            Label lblUpperLimits;
            Label lblLowerLimits;
            Label lblMeasuresNr;
            int[] startPos = new int[] { 65, 45 };

            for (int i = 0; i < measureNr; i++)
            {
                lblUpperLimits = new Label();
                lblUpperLimits.Text = "";
                lblUpperLimits.AutoSize = true;
                lblUpperLimits.Location = new Point(startPos[0], startPos[1] + i * lblVerticalShiftUL);
                lblUpperLimits.Parent = PnlBase;
                lblUpperLimits.BringToFront();
                UpperLimits.Add(lblUpperLimits);
            }
            for (int i = 0; i < measureNr; i++)
            {
                lblMeasuresNr = new Label();
                lblMeasuresNr.Text = "";
                lblMeasuresNr.AutoSize = true;
                lblMeasuresNr.Font = new Font("Times New Roman", 10, FontStyle.Bold);
                lblMeasuresNr.ForeColor = Color.BlueViolet;
                lblMeasuresNr.Location = new Point(startPos[0], startPos[1] + i * lblVerticalShiftUL + lblVerticalShiftMeasure);
                lblMeasuresNr.Parent = PnlBase;
                lblMeasuresNr.BringToFront();
                MeasuresAvergValue.Add(lblMeasuresNr);
            }
            for (int i = 0; i < measureNr; i++)
            {
                lblLowerLimits = new Label();
                lblLowerLimits.Text = "";
                lblLowerLimits.AutoSize = true;
                lblLowerLimits.Location = new Point(startPos[0], startPos[1] + i * lblVerticalShiftUL + lblVerticalShiftLL);
                lblLowerLimits.Parent = PnlBase;
                lblLowerLimits.BringToFront();
                LowerLimits.Add(lblLowerLimits);
            }
        }

        public void UpdateUpperLowerLimits(byte[] upperLimits, byte[] lowerLimits)
        {
            for (int i = 0; i < measureNr; i++)
            {
                UpperLimits[i].Text = upperLimits[i].ToString();
            }
            for (int i = 0; i < measureNr; i++)
            {
                UpperLimits[i].Text = upperLimits[i].ToString();
            }
            for (int i = 0; i < measureNr; i++)
            {
                MeasuresAvergValue[i].Text = ((upperLimits[i] + lowerLimits[i]) / 2).ToString();
                
            }
            for (int i = 0; i < measureNr; i++)
            {
                LowerLimits[i].Text = lowerLimits[i].ToString();
            }
        }

        private Point GetMeasurePointPosition(byte measureResult, int measIndex, bool isOutOfLimit)
        {
            int lowerToUpperLineSeparation = 2*lineMeasureSeparation;  //Now is 60. Just in case the value will be changed
            int posY = 0;
            int posX = 0;
            //int mitadPercent;
            int MeasValueinPercent;
            int pxlPerPoints;
            if (!isOutOfLimit)
            {
                MeasValueinPercent = (measureResult - (byte)Int32.Parse(LowerLimits[measIndex].Text)) * 100 / ((byte)Int32.Parse(UpperLimits[measIndex].Text) - (byte)Int32.Parse(LowerLimits[measIndex].Text));
                int posYInPixel = (lowerToUpperLineSeparation * MeasValueinPercent) / 100;
                posY = LowerLimits[measIndex].Location.Y - posYInPixel;
            }
            else
            {
                int totalPoints = ((byte)Int32.Parse(UpperLimits[measIndex].Text) - (byte)Int32.Parse(LowerLimits[measIndex].Text));
                if (totalPoints >= lowerToUpperLineSeparation)
                    pxlPerPoints = 1;
                else
                {
                    pxlPerPoints = lowerToUpperLineSeparation/totalPoints;
                }
                if(measureResult > (byte)Int32.Parse(UpperLimits[measIndex].Text))
                {
                    //int testVar = ((measureResult - (byte)Int32.Parse(UpperLimits[measIndex].Text)) * pxlPerPoints) > 12 ? 10 : (measureResult - (byte)Int32.Parse(UpperLimits[measIndex].Text)) * pxlPerPoints;
                    posY = UpperLimits[measIndex].Location.Y - (int)(((measureResult - Int32.Parse(UpperLimits[measIndex].Text)) * pxlPerPoints) > 12 ? 10 : (measureResult - Int32.Parse(UpperLimits[measIndex].Text)) * pxlPerPoints);
                    //int test = 45 - testVar;
                }
                else
                {
                    //posY = LowerLimits[measIndex].Location.Y + ((byte)Int32.Parse(LowerLimits[measIndex].Text) - measureResult) * pxlPerPoints;
                    posY = LowerLimits[measIndex].Location.Y + (int)(((Int32.Parse(LowerLimits[measIndex].Text) - measureResult) * pxlPerPoints) > 10 ? 10 : (Int32.Parse(LowerLimits[measIndex].Text) - measureResult) * pxlPerPoints);
                }
                
            }
            posX = 130 + execMeasurments * 10 + measIndex;// measIndex + 5;
            return new Point(posX, posY);
        }

        public void NewMeasuredValue(byte[] measureResult)
        {
            MeasPlotPoint measPlotPoint;
            
            for(int i = 0; i< 24; i++)
            {
                measPlotPoint = new MeasPlotPoint();
                measPlotPoint.measValue = measureResult[i];
                if ((measureResult[i] > (byte)Int32.Parse(UpperLimits[i].Text)) || (measureResult[i] < (byte)Int32.Parse(LowerLimits[i].Text)))
                {
                    measPlotPoint.isOutOfLimit = true;
                    measPlotPoint.color = Color.Red;
                }
                else
                {
                    measPlotPoint.color = Color.Black;
                    measPlotPoint.isOutOfLimit = false;
                }
                measPlotPoint.location = GetMeasurePointPosition(measureResult[i], i, measPlotPoint.isOutOfLimit);
                measPlotPoint.prevPointLocation = (PlotPoints.Count() >0 && i != 0) ? PlotPoints[i - 1].location : new Point(0, 0);
                PlotPoints.Add(measPlotPoint);
            }//Esto pas cuando no puedo
            execMeasurments++;
            DrawResultGraphic();
        }

        private void DrawResultGraphic()
        {
            Graphics pointPainter = PnlBase.CreateGraphics();
            if (PlotPoints != null && PlotPoints.Count() != 0)
            {
                for (int i = 0; i < PlotPoints.Count(); i++)
                {
                    pointPainter.DrawLine(new Pen(PlotPoints[i].color, 3), new Point(PlotPoints[i].location.X, PlotPoints[i].location.Y + PnlBase.AutoScrollPosition.Y), new Point(PlotPoints[i].location.X + 3, PlotPoints[i].location.Y + PnlBase.AutoScrollPosition.Y));
                }
                pointPainter.Dispose();
            }
            
        }
    }
}
