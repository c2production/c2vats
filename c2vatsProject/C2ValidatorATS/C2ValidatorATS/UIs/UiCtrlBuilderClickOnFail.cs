﻿/**
 * Class: UiCtrlBuilderClickOnFail
 * This class builds the "Fail" form consisting of a button and a test lbl. This is use in all testcases where user interaction is needed.
 * If the Validator does not react or does not correctly behave, then the user click this button to make the test fail and end,
 * otherwise the test will end with timeout
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using C2ValidatorATS.Utils;
namespace C2ValidatorATS.UIs
{
    class UiCtrlBuilderClickOnFail
    {
        Panel pnlClickOnFail;
        Button btnMakeFailTest;
        Label lblShowUserExplanation;

        public void ShowClickOnFailPanel(C2ATS CC2Mainform, Point pnlLocation)
        {
            pnlClickOnFail = new Panel();
            pnlClickOnFail.Location = new Point(pnlLocation.X, pnlLocation.Y);
            pnlClickOnFail.Size = new Size(90, 100);
            pnlClickOnFail.BackColor = Color.Khaki;
            pnlClickOnFail.BorderStyle = BorderStyle.FixedSingle;
            pnlClickOnFail.Parent = CC2Mainform.pnl_UserInfo;
            pnlClickOnFail.BringToFront();

            lblShowUserExplanation = new Label();
            lblShowUserExplanation.Location = new Point(3 , 3);
            lblShowUserExplanation.Size = new Size(pnlClickOnFail.Size.Width, pnlClickOnFail.Size.Height - 35);
            lblShowUserExplanation.Text = Msgs.Uii.TEST_FAIL_CAUSE_VUT_DOESNOT_RESPOND;
            lblShowUserExplanation.Parent = pnlClickOnFail;

            btnMakeFailTest = new Button();
            btnMakeFailTest.Size = new Size(50, 25);
            btnMakeFailTest.Location = new Point(20, pnlClickOnFail.Size.Height - 30);
            btnMakeFailTest.Text = "Fail";
            btnMakeFailTest.ForeColor = Color.Red;
            btnMakeFailTest.Click += new EventHandler(this.btnMakeFailTest_clicked);
            btnMakeFailTest.Parent = pnlClickOnFail;
        }
        public void DestroyPnlpnlClickOnFail()
        {
            if (pnlClickOnFail != null) pnlClickOnFail.Dispose();
        }
        protected void btnMakeFailTest_clicked(object sender, EventArgs e)
        {
            TestManager.Instance.TestExecSuspended = true;
            DestroyPnlpnlClickOnFail();
        }
    }
}
