﻿/**
 * Class: UiCtrlBuilderCoinDrop
 * This class builds the interface used during the debug test "Coin Drop Evaluation Test".
 * Author: Leonardo Garcia, PayComplete
 * 
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut.VutDBlocksInfo;

namespace C2ValidatorATS.UIs
{
    class UiCtrlBuilderCoinDrop
    {
       
         string   FROM_HARD_CODED = "Hard Coded";
         //string   FROM_DB = "DB";
         string  FROM_VALIDATOR = "Validator";
        
        Panel PnlMeasures;
        ListViewItem lVwItemMeasureRow;
        ListView lviewMeasures;
        GroupBox gbxSelectCoin;
        Button btnEndTest;
       // Button btnChangeValidator;
        Button btnSartMeas;
        Button bntnSaveMeas;
        ComboBox cboxIsoSelector;
        ComboBox cboxCoinSelector;
        ComboBox cboxFormulaSelector;
        TextBox tboxFakeCoinName;
        Label lblFakeCoinNameInfo;
        RadioButton rbtnDataHardCoded;
        RadioButton rbtnDataFromDB;
        RadioButton rbtnDataFromValidator;
        //ComboBox cboxCoinNr;
        ComboBox cboxValidatorNr;
        CheckBox chbxUseStatistics;
        CheckBox chbxVsFakeCoins;
        public Label lblCounter;
        GroupBox gbxStatistics;
        Label lblInsertion;
        int insertionCounter = 0;
                   //#### END AED #####
        DBHandler CDBHandler;
        VutIdentificationData CVutIdentificationData;
        TblChannelIds CTblChannelIds;

        public UiCtrlBuilderCoinDrop( )
        {
            CDBHandler = new DBHandler();
        }

        private void CreateTable()
        {
            lviewMeasures = new ListView();

            lviewMeasures.View = View.Details;
            lviewMeasures.BackColor = Color.LightBlue;
            lviewMeasures.Location = new Point(0, 185);
            lviewMeasures.Size = new Size(PnlMeasures.Size.Width, 150);
            lviewMeasures.Columns.Add(" ", -2); //First one empty
            for (int i = 1; i < 25; i++)
            {
                lviewMeasures.Columns.Add("M" + i, -2, HorizontalAlignment.Left);
            }
            
            //UpperLimits
            ListViewItem lVwItemUpperLimits = new ListViewItem("Max", 0);
            for (int i = 0; i < 24; i++)
            {
                lVwItemUpperLimits.SubItems.Add("XXX");
            }
            lviewMeasures.Items.AddRange(new ListViewItem[] { lVwItemUpperLimits });

            MeasureTableEmptyRow(lviewMeasures, 1); //second param = row index
            //Set MeasureRow to empty
            lVwItemMeasureRow = new ListViewItem("Meas", 2);
            for (int i = 0; i < 24; i++)
            {
                lVwItemMeasureRow.SubItems.Add("");              
            }
            lviewMeasures.Items.AddRange(new ListViewItem[] { lVwItemMeasureRow });

            MeasureTableEmptyRow(lviewMeasures, 3); //second param = row index
            //LowerLimits
            ListViewItem lVwItemlowerLimits = new ListViewItem("Min", 4);
            for (int i = 0; i < 24; i++)
            {
                //lVwItemlowerLimits.SubItems.Add(lowerLimits[i].ToString());
                lVwItemlowerLimits.SubItems.Add("XXX");
            }
            lviewMeasures.Items.AddRange(new ListViewItem[] { lVwItemlowerLimits });
            lviewMeasures.Parent = PnlMeasures;
        }

        private void CleanMeasuredValuesRow()
        {
             foreach (ListViewItem item in lviewMeasures.Items)
            {
                if (item.SubItems[0].Text == "Meas")
                {
                    item.SubItems[0].ForeColor = Color.Black;
                    for (int i = 1; i < 25; i++)
                    {
                        item.SubItems[i].Text = "";
                    }
                }
             }
        }
        private void MeasureTableEmptyRow(ListView lviewMeasures, int index)
        {
            ListViewItem emptyRow = new ListViewItem("", index);
            for (int i = 0; i < 25; i++)
            {
                emptyRow.SubItems.Add("");
            }
            lviewMeasures.Items.AddRange(new ListViewItem[] { emptyRow });
        }

        public void ShowCoinMeasuresTable(C2ATS CC2Mainform)
        {
            PnlMeasures = new Panel();
            PnlMeasures.Location = new Point(0, 0);
            PnlMeasures.Size = new Size(CC2Mainform.pnl_UserInfo.Size.Width, 340);
            PnlMeasures.BackColor = Color.LightBlue;
            PnlMeasures.Parent = CC2Mainform.pnl_UserInfo;
            CreateCoinSelectionSession();
            CreateEndTestButton();
            createRadioButtons();
           
            CreateStatisticsGBx();
            CreateUseStatisticsChbx();
            CreateStartMeasButton();//Que no sabe donde esta la cosa
            //CreateSaveOverallResultButton();
            CreateSaveResultMeasButton();
            //CreateCoinTypeSelector();  //Este es el selector the fake coins que los sustituyo por text box
            CreateFakeCoinNameInfoLbl();
            CreateFakeCoinNameInput();
            //CreateCoinNrSelector();
            CreateValidatorNrSelector();
            CreateCounterTxtLbl();
            CreateInsertionTxtLbl();
            CreateVsFakeCoinsChbx();
           
            CreateTable();
            PnlMeasures.BringToFront();
        }

        public void UpdateUpperLowerLimits(byte[] upperLimits, byte[] lowerLimits)
        {
            CleanMeasuredValuesRow();
            foreach (ListViewItem item in lviewMeasures.Items)
            {
                if (item.SubItems[0].Text == "Max")
                {
                    for (int i = 1; i < 25; i++)
                    {
                        item.SubItems[i].Text = upperLimits[i - 1].ToString();
                    }
                    item.UseItemStyleForSubItems = false;
                }

                if (item.SubItems[0].Text == "Min")
                {
                    for (int i = 1; i < 25; i++)
                    {
                        item.SubItems[i].Text = lowerLimits[i - 1].ToString();
                    }
                    item.UseItemStyleForSubItems = false;
                }
            }
        }
        public void UpdateRowWithNewMeasuredData(byte[] measureResult, List<int> measureRemarks)
        {
            foreach (ListViewItem item in lviewMeasures.Items)
            {
                if (item.SubItems[0].Text == "Meas")
                {   //Clean foreground color
                    for (int i = 1; i < 25; i++)
                    {
                        item.SubItems[i].ForeColor = Color.Green;
                    }
                    //Write the measured values
                    for (int i = 1; i < 25; i++)
                    {
                        item.SubItems[i].Text = measureResult[i - 1].ToString();
                    }
                    //Check if there are remarks to change value colors
                    if (measureRemarks.Count() != 0)
                    {
                        for (int i = 0; i < measureRemarks.Count(); i += 2)
                        {
                            if (measureRemarks[i + 1] == 0)
                                item.SubItems[measureRemarks[i] + 1].ForeColor = Color.Yellow;
                            else item.SubItems[measureRemarks[i] + 1].ForeColor = Color.Red;
                        }
                        for (int i = 1; i < measureRemarks.Count(); i += 2)
                        {
                            if (measureRemarks[i] == 1)
                            {
                                item.SubItems[0].ForeColor = Color.Red;
                                break;
                            }
                            else item.SubItems[0].ForeColor = Color.Yellow;
                        }
                    }
                    item.UseItemStyleForSubItems = false;
                    break;
                }
            }
            if (AEDInvestigation.Instance.statisticsActivated)
            {
                insertionCounter++;
                if (!bntnSaveMeas.Enabled)
                    bntnSaveMeas.Enabled = true;
                lblCounter.Text = insertionCounter.ToString();
            }        
        }

        private void CreateCoinSelectionSession()
        {
            gbxSelectCoin = new GroupBox();
            gbxSelectCoin.Text = "Select Coin";
            gbxSelectCoin.Size = new Size(270, 70);
            gbxSelectCoin.Location = new Point(5,100);
            gbxSelectCoin.Parent = PnlMeasures;
            CreateIsoSelector();
            CreateCoinSelector();
            CreateFormulaSelector();

        }
        private void CreateIsoSelector()
        {
            cboxIsoSelector = new ComboBox();
            cboxIsoSelector.Size = new Size(75, 20);
            cboxIsoSelector.Location = new Point(10, 30);
            cboxIsoSelector.Text = "Select ISO";
            cboxIsoSelector.Items.AddRange(CoinProvider.Iso.GetIsos());
            cboxIsoSelector.SelectedValueChanged += new EventHandler(this.cboxIsoSelector_SelectedValueChanged);
            cboxIsoSelector.Parent = gbxSelectCoin;
        }

        private void CreateCoinSelector()
        {
            cboxCoinSelector = new ComboBox();
            cboxCoinSelector.Size = new Size(80, 20);
            cboxCoinSelector.Location = new Point(100, 30);
            cboxCoinSelector.Text = "Select Coin";
            cboxCoinSelector.SelectedValueChanged += new EventHandler(this.cboxCoinSelector_SelectedValueChanged);
            cboxCoinSelector.Parent = gbxSelectCoin;
        }

        private void CreateFormulaSelector()
        {
            cboxFormulaSelector = new ComboBox();
            cboxFormulaSelector.Size = new Size(60, 20);
            cboxFormulaSelector.Location = new Point(196, 30);
            cboxFormulaSelector.Text = "Formula";
            cboxFormulaSelector.SelectedValueChanged += new EventHandler(this.cboxCoinSelector_SelectedValueChanged);
            cboxFormulaSelector.Parent = gbxSelectCoin;
        }

        private void CreateEndTestButton()
        {
            btnEndTest = new Button();
            btnEndTest.Text = "End Coin Test";
            btnEndTest.Size = new Size(100, 30);
            btnEndTest.Location = new Point(220, 10);
            btnEndTest.BackColor = Color.LightSalmon;
            btnEndTest.Click += new EventHandler(this.btnEndTest_Click);
            btnEndTest.Parent = PnlMeasures;
        }

        private void createRadioButtons()
        {
            rbtnDataHardCoded = new RadioButton();
            rbtnDataHardCoded.Location = new Point(10, 25);  //(10,5)
            rbtnDataHardCoded.AutoSize = true;
            rbtnDataHardCoded.Checked = true;
            rbtnDataHardCoded.Text = "Using HardCoded Test Limits";
            rbtnDataHardCoded.CheckedChanged += new EventHandler(this.rbtnChecked_Changed);
            rbtnDataHardCoded.Parent = PnlMeasures;

            rbtnDataFromDB = new RadioButton();
            rbtnDataFromDB.Location = new Point(10, 30);
            rbtnDataFromDB.AutoSize = true;
            rbtnDataFromDB.Text = "Using Test Limits From DB";
            rbtnDataFromDB.CheckedChanged += new EventHandler(this.rbtnChecked_Changed);
            rbtnDataFromDB.Visible = false;
            rbtnDataFromDB.Parent = PnlMeasures;

            rbtnDataFromValidator = new RadioButton();
            rbtnDataFromValidator.Location = new Point(10, 55);
            rbtnDataFromValidator.AutoSize = true;
            rbtnDataFromValidator.Text = "Using Test Limits From Current Validator";
            rbtnDataFromValidator.CheckedChanged += new EventHandler(this.rbtnChecked_Changed);
            rbtnDataFromValidator.Parent = PnlMeasures;
        }
        //###########    AED Related  ##########################
        private void CreateStatisticsGBx()
        {
            gbxStatistics = new GroupBox();
            gbxStatistics.Size = new Size(290, 180);
            gbxStatistics.Location = new Point(330, 3);
            gbxStatistics.Text = "Statistics";
            gbxStatistics.Parent = PnlMeasures;
        }

        private void CreateUseStatisticsChbx()
        {
            chbxUseStatistics = new CheckBox();
            chbxUseStatistics.AutoSize = true;
            chbxUseStatistics.Location = new Point(10, 18);
            chbxUseStatistics.Text = "Use Statistics";
            chbxUseStatistics.CheckedChanged += new EventHandler(this.chbxUseStatistics_changed);
            chbxUseStatistics.Parent = gbxStatistics;
            chbxUseStatistics.Enabled = false;
        }

        private void CreateInsertionTxtLbl()
        {
            lblInsertion = new Label();
            lblInsertion.Size = new Size(30, 30);
            lblInsertion.Location = new Point(20, 60);
            lblInsertion.AutoSize = true;
            lblInsertion.Text = "Insertions";
            lblInsertion.Parent = gbxStatistics;
            lblInsertion.Enabled = false;
        }

        private void CreateCounterTxtLbl()
        {
            lblCounter = new Label();
            lblCounter.AutoSize = true; // = new Size(40, 30);
            lblCounter.Location = new Point(35, 80);
            lblCounter.AutoSize = true;
            lblCounter.Text = "0";
            lblCounter.Font = new Font("Serif", 20, FontStyle.Bold);
            lblCounter.Parent = gbxStatistics;
            lblCounter.Enabled = false;
        }


        private void CreateStartMeasButton()
        {
            btnSartMeas = new Button();
            btnSartMeas.Text = "Start";
            btnSartMeas.Size = new Size(60, 35);
            btnSartMeas.Location = new Point(120, 15);
            btnSartMeas.BackColor = Color.LightGreen;
            btnSartMeas.Click += new EventHandler(this.btn_start_click);
            btnSartMeas.Enabled = true;
            btnSartMeas.Parent = gbxStatistics;
            btnSartMeas.Enabled = false;
        }
        private void CreateSaveResultMeasButton()
        {
            bntnSaveMeas = new Button();
            bntnSaveMeas.Text = "Save";
            bntnSaveMeas.Size = new Size(60, 35);
            bntnSaveMeas.Location = new Point(210, 15);
            bntnSaveMeas.BackColor = Color.LightSalmon;
            bntnSaveMeas.Click += new EventHandler(this.btn_save_click);
            bntnSaveMeas.Enabled = true;
            bntnSaveMeas.Parent = gbxStatistics;
            bntnSaveMeas.Enabled = false;
        }

        private void CreateValidatorNrSelector()
        {
            cboxValidatorNr = new ComboBox();
            cboxValidatorNr.Size = new Size(78, 20);
            cboxValidatorNr.Location = new Point(140, 65);
            cboxValidatorNr.Text = "Validator Nr";
            cboxValidatorNr.Items.AddRange(AEDInvestigation.Instance.GetValNr());
            cboxValidatorNr.TextChanged += new EventHandler(this.cboxValidatorNr_TextChanged);
            cboxValidatorNr.Parent = gbxStatistics;
            cboxValidatorNr.Enabled = false;
        }

        private void CreateVsFakeCoinsChbx()
        {
            chbxVsFakeCoins = new CheckBox();
            chbxVsFakeCoins.AutoSize = true;
            chbxVsFakeCoins.Location = new Point(10, 130);
            chbxVsFakeCoins.Text = "Test Vs Fake Coins";
            chbxVsFakeCoins.CheckedChanged += new EventHandler(this.chbxVsFakeCoins_changed);
            chbxVsFakeCoins.Parent = gbxStatistics;
            chbxVsFakeCoins.Enabled = false;
        }

        //private void CreateCoinTypeSelector()
        //{
        //    cboxFakeCoinNr = new ComboBox();
        //    cboxFakeCoinNr.Size = new Size(78, 20);
        //    cboxFakeCoinNr.Location = new Point(140, 130);
        //    cboxFakeCoinNr.Text = "Fake Coin";
        //    cboxFakeCoinNr.Items.AddRange(AEDInvestigation.Instance.GetFakeCoinNr());
        //    cboxFakeCoinNr.Parent = gbxStatistics;
        //    cboxFakeCoinNr.Enabled = false;
        //}

        private void CreateFakeCoinNameInfoLbl()
        {
            lblFakeCoinNameInfo = new Label();
            lblFakeCoinNameInfo.AutoSize = true;
            lblFakeCoinNameInfo.Location = new Point(138, 115);
            lblFakeCoinNameInfo.Text = "Write Fake Coin Name";
            lblFakeCoinNameInfo.Parent = gbxStatistics;
            lblFakeCoinNameInfo.Enabled = false;
        }
        private void CreateFakeCoinNameInput()
        {
            tboxFakeCoinName = new TextBox();
            tboxFakeCoinName.Size = new Size(78, 20);
            tboxFakeCoinName.Location = new Point(140, 135);
            tboxFakeCoinName.Text = "";
            tboxFakeCoinName.Parent = gbxStatistics;
            tboxFakeCoinName.Enabled = false;
        }
        //private void CreateCoinNrSelector()
        //{
        //    cboxCoinNr = new ComboBox();
        //    cboxCoinNr.Size = new Size(60, 20);
        //    cboxCoinNr.Location = new Point(470, 60);
        //    cboxCoinNr.Text = "Coin Nr";
        //    cboxCoinNr.Items.AddRange(AEDInvestigation.Instance.GetCoinNr());
        //    //cboxCoinNr.SelectedValueChanged += new EventHandler(this.cboxCoinSelector_SelectedValueChanged);
        //    cboxCoinNr.Parent = PnlMeasures;
        //}

        

       
        //#######  END AED Investigaton ###################33nnn#3##########

        protected void cboxIsoSelector_SelectedValueChanged(object sender, EventArgs e)
        {
            cboxCoinSelector.Items.Clear();
            lblCounter.Text = "0";   //In case running statistics
            cboxCoinSelector.Text = "Select Coin";
            if (rbtnDataHardCoded.Checked == true) //Hard coded data will be handled
            {
                if ((string)cboxIsoSelector.SelectedItem == "EUR")
                {
                    cboxCoinSelector.Items.AddRange(CoinProvider.EuroCoins.GetAllEuroCoins());
                }
                else if ((string)cboxIsoSelector.SelectedItem == "CHF")
                {
                    if (MessageBox.Show("Does the Validator have RIM Sensor?", "5 CHF coin Needs RIM Sensor", MessageBoxButtons.YesNo)
                        == DialogResult.Yes)
                    {
                        TestManager.Instance.ForceTestWithRIMSensor = true;
                    }
                    else TestManager.Instance.ForceTestWithRIMSensor = false;
                    cboxCoinSelector.Items.AddRange(CoinProvider.SwissFrancCoins.GetAllCHFCoins(true));//TestManager.Instance.ForceTestWithRIMSensor));
                }
                else if ((string)cboxIsoSelector.SelectedItem == "AED")
                {
                    cboxCoinSelector.Items.AddRange(CoinProvider.EmiratesCoins.GetAllAEDCoins());

                }
                else if ((string)cboxIsoSelector.SelectedItem == "PLN")
                {
                    cboxCoinSelector.Items.AddRange(CoinProvider.PolishCoins.GetAllPLNCoins());
                }
                else cboxCoinSelector.Items.AddRange(new string[] { }); //Empty Coin Selection
            }
            else if(rbtnDataFromDB.Checked == true)
            //Data from DB   #### TODO  #####
            {
                List<List<string>> ISOSs = new List<List<string>>();
                ISOSs = CDBHandler.GetDataFromISO(cboxIsoSelector.SelectedItem.ToString(), new List<string> { "value", "revision" });
            }
            else if (rbtnDataFromValidator.Checked == true)
            {

                CTblChannelIds = new TblChannelIds(TestManager.Instance.GetVutTableOps());
                CTblChannelIds.RetreiveAllData();
                List<string> coins = new List<string> { };
                for (int i = 0; i < CTblChannelIds.CCoinIdChannels.Count(); i++ )
                {
                    if (CTblChannelIds.CCoinIdChannels[i].coinID == 0 ) //&& !CTblChannelIds.CCoinIdChannels[i].iSo.ToString().Contains("TOK"))
                        continue;
                    coins.Add("CoinId " + CTblChannelIds.CCoinIdChannels[i].coinID + ", Rev " + CTblChannelIds.CCoinIdChannels[i].revision + ", Prec. " +CTblChannelIds.CCoinIdChannels[i].precision);
                }
                cboxCoinSelector.Items.AddRange(coins.ToArray());
            }
        }

        protected void cboxCoinSelector_SelectedValueChanged(object sender, EventArgs e)
        {

            if (rbtnDataHardCoded.Checked == true)
            {

                TestManager.Instance.NewCoinSelection((string)cboxCoinSelector.SelectedItem, (string)cboxIsoSelector.SelectedItem, FROM_HARD_CODED);
                //cboxFakeCoinNr.Items.Clear();
                //cboxFakeCoinNr.Items.AddRange(AEDInvestigation.Instance.GetFakeCoinNr());
                chbxUseStatistics.Enabled = true;
            }
            else if(rbtnDataFromDB.Checked == true)
            {
                    //####  TODO  #####
            }
            else if(rbtnDataFromValidator.Checked == true)
            {
                TestManager.Instance.NewCoinSelection((string)cboxCoinSelector.SelectedItem, (string)cboxIsoSelector.SelectedItem, FROM_VALIDATOR);
                chbxUseStatistics.Enabled = true;
            }
        }

        protected void btnEndTest_Click(object sender, EventArgs e)
        {
            TestManager.Instance.TestExecSuspended = true;
            this.DestroyMeasuresPnl();
        }

        protected void rbtnChecked_Changed(object sender, EventArgs e)
        {
            cboxIsoSelector.Text = "Select ISO";
            cboxCoinSelector.Text = "Select Coin";
            cboxFormulaSelector.Text = "Formula";
            lviewMeasures.Controls.Clear();
            lviewMeasures.Dispose();
            CreateTable();
            cboxIsoSelector.Items.Clear();
            if (rbtnDataHardCoded.Checked == true)
            {
                cboxIsoSelector.Items.AddRange(CoinProvider.Iso.GetIsos());
            }
            else if(rbtnDataFromDB.Checked == true)
            {
                List<List<string>> ISOSs = new List<List<string>>();
                ISOSs = CDBHandler.GetISOs();
                if (ISOSs != null)
                {
                    cboxIsoSelector.Items.AddRange(ISOSs[0].Skip(1).ToArray());
                }
            }
            else if(rbtnDataFromValidator.Checked == true)
            {
                cboxFormulaSelector.Visible = false;
                //cboxCoinSelector.Size = new Size(80, 20);  //org
                cboxCoinSelector.Size = new Size(140, 20);
                //Extract info from validator
                CVutIdentificationData = TestManager.Instance.GetValidatorIdData();
                CVutIdentificationData.RetreiveAllData();
                string iso;
                if((iso = CVutIdentificationData.DataBlock.Substring(0,3)) != "")
                {
                    cboxIsoSelector.Items.AddRange(new string[] {iso });
                }
                
            }
        }
        //###############3 AED stuf  #######################
        protected void cboxValidatorNr_TextChanged(Object sender, EventArgs e)
        {
            if (cboxValidatorNr.Text == "Validator Nr")
                return;
            btnSartMeas.Enabled = true;
            chbxVsFakeCoins.Enabled = true;
        }
        protected void chbxUseStatistics_changed(Object sender, EventArgs e)
        {
            if (chbxUseStatistics.Checked)
            {
                EnableAllStatistic();
            }
            else ResetAllStatistic();
        }
        protected void chbxVsFakeCoins_changed(Object sender, EventArgs e)
        {
            if (chbxVsFakeCoins.Checked)
            {
                //cboxFakeCoinNr.Enabled = true;
                lblFakeCoinNameInfo.Enabled = true;
                tboxFakeCoinName.Enabled = true;
                AEDInvestigation.Instance.fakeCoinStatistics = true;
            }
            else
            {
                //cboxFakeCoinNr.Enabled = false;
                //cboxFakeCoinNr.Text = "Fake Coin";
                lblFakeCoinNameInfo.Enabled = false;
                tboxFakeCoinName.Enabled = false;
                tboxFakeCoinName.Text = "";
                AEDInvestigation.Instance.fakeCoinStatistics = false;
            } 
        }

        private void EnableAllStatistic()
        {
            lblInsertion.Enabled = true;
            lblCounter.Enabled = true;
            btnSartMeas.Enabled = false;
            bntnSaveMeas.Enabled = false;
            cboxValidatorNr.Enabled = true;
            lblCounter.Text = "0";
            //chbxVsFakeCoins.Enabled = true;
            //cboxFakeCoinNr.Enabled = false;
        }

        private void ResetAllStatistic()
        {
            lblInsertion.Enabled = false;
            lblCounter.Enabled = false;
            btnSartMeas.Enabled = false;
            bntnSaveMeas.Enabled = false;
            cboxValidatorNr.Enabled = false;
            chbxVsFakeCoins.Enabled = false;
            chbxVsFakeCoins.Checked = false;

            //cboxFakeCoinNr.Enabled = false;
            //Reset All
            lblCounter.Text = "0";
            cboxValidatorNr.Text = "Validator Nr";
           // cboxFakeCoinNr.Text = "Fake Coin";
        }

        protected void btn_start_click(Object sender, EventArgs e)
        {
           
            AEDInvestigation.Instance.coinType = cboxIsoSelector.Text + " " + cboxCoinSelector.Text;           
            AEDInvestigation.Instance.valNr = (string)cboxValidatorNr.SelectedItem;
            if (chbxVsFakeCoins.Checked)
            {
                AEDInvestigation.Instance.fakeCoinNr = tboxFakeCoinName.Text == "" ? "Missing Name" : tboxFakeCoinName.Text;  
            }
                
            AEDInvestigation.Instance.nullifyValues();
            AEDInvestigation.Instance.statisticsActivated = true;
            lblCounter.Text = "0";
            insertionCounter = 0;
            //bntnSaveMeas.Enabled = true;
        }

        protected void btn_save_click(Object sender, EventArgs e)
        {
            //AEDInvestigation.Instance.SaveResultToFile();
            AEDInvestigation.Instance.Save2File();
            AEDInvestigation.Instance.statisticsActivated = false;
            bntnSaveMeas.Enabled = false;
            btnSartMeas.Enabled = true;
        }

        //protected void btn_saveoverall_click(Object sender, EventArgs e)
        //{
        //    AEDInvestigation.Instance.saveTotalResult2File();
        //}
        //############### END AED Stuff #################################
        public void DestroyMeasuresPnl()
        {
            if(PnlMeasures != null) PnlMeasures.Dispose();
        }

    }
}
