﻿/**
 * Class: C2ATS
 * Main class. The GUI
 * Author: Leonardo Garcia, PayComplete
 **/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using C2ValidatorATS.Utils;
using C2ValidatorATS.UIs;

namespace C2ValidatorATS
{
    public partial class C2ATS : Form
    {
        public bool AtsMode { get; set;}
        Graphics figure;
        AboutBoxSuzo aboutBox;
        LoginForm toolMenuLogin;

        public C2ATS()
        {
            InitializeComponent();
            this.Text = Msgs.AppNameAndVersion.APP_C2VATS_NAME + " " + Msgs.AppNameAndVersion.APP_C2VATS_VERSION;
            PrepareComponentsForTesting();
            aboutBox = new AboutBoxSuzo();
            
        }

        private void PrepareComponentsForTesting()
        {
            List<string> tc_List = new List<string>();
            List<bool> tc_Selection_Status = new List<bool>();
            TestManager.Instance.CTestGUI = this; 
            tc_List = TestManager.Instance.GetTestCaseList();
            tc_Selection_Status = TestManager.Instance.GetTestCaseSelectionStatus();

            for(int i = 0; i < tc_List.Count; i++ )
            {
                ListViewItem item = new ListViewItem("",0);
                item.Checked = tc_Selection_Status[i];
                item.SubItems.Add(tc_List[i]);
                item.SubItems.Add("");
                lView_TC_List.Items.AddRange(new ListViewItem[] {item});
            }
            AtsMode = true; // default value
        }

        #region Events
        private void lView_TC_List_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            TestManager.Instance.ChangeTestCaseSelectionStatus(e.Item.Index, e.Item.Checked);
           // CtestManager.RefreshTestCaseList();
        }

        private void runAcceptanceTestToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = (ToolStripMenuItem)sender;

            if (mi == runAcceptanceTestToolStripMenuItem)
            {
                if (mi.Checked == true)
                {
                    runDebugToolStripMenuItem.Checked = false;
                }
                else runDebugToolStripMenuItem.Checked = true;
     
            }
            else
            {
                if (mi.Checked == true)
                {
                    runAcceptanceTestToolStripMenuItem.Checked = false;
                }
                else runAcceptanceTestToolStripMenuItem.Checked = true;
            }
            AtsMode = runAcceptanceTestToolStripMenuItem.Checked;
        }

        private void btn_run_Click(object sender, EventArgs e)
        {
            TestManager.Instance.test();

            // TestManager.Instance.StartTestCaseExecution();
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            TestManager.Instance.StopTestCaseExecution(sender);
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            TestManager.Instance.StopTestCaseExecution(sender);
            Application.Exit();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btn_exit_Click(sender, e);
        }

        #endregion Events

        //public void ShowCoinMeasuresTable()
        //{
        //    (new UiControlProvider()).ShowCoinMeasuresTable(this);
        //}
  
        public void setLablesInTestSessionInfoAreaToDefaultValues()
        {
            lbl_info1.Location = new System.Drawing.Point(73, 36);
            lbl_info1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11);
            lbl_info1.Size = new System.Drawing.Size(150, 18);
            lbl_info1.ForeColor = Color.Black;
            lbl_info1.Text = "";

            lbl_info2.Size = new System.Drawing.Size(150, 18);
            lbl_info2.ForeColor = Color.Black;
            lbl_info2.Text = "";

            lbl_info3.Size = new System.Drawing.Size(150, 18);       
            lbl_info3.ForeColor = Color.Black;
            lbl_info3.Text = "";
        }

        public void paintFigure(Color color, string shape, int x, int y, int width, int height) 
        {
            figure = pbx_user_img.CreateGraphics();
            Pen pen = new Pen(color);
            if (shape == "Rectangle")
            {
                Rectangle r = new Rectangle(x, y, width, height);
                figure.DrawRectangle(pen, r);
            }
            figure.Dispose();
        }
        private void pbx_user_img_MouseClick(object sender, MouseEventArgs e)
        {
            TestManager.Instance.UserImageClicked(e.X, e.Y);
        }

        private void btn_ats_Click(object sender, EventArgs e)
        {
            TestManager.Instance.AtsModeBtnClicked();
        }

        private void btn_debug_Click(object sender, EventArgs e)
        {
            TestManager.Instance.DbgModeBtnClicked();
        }

        private void cbx_select_all_CheckedChanged(object sender, EventArgs e)
        {
            TestManager.Instance.SelectAllCboxChanged(((CheckBox)sender).Checked);
        }

        private void cbx_rim_present_CheckedChanged(object sender, EventArgs e)
        {
            TestManager.Instance.RIMSensorPresentCboxChanged( ((CheckBox)sender).Checked);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            aboutBox.ShowDialog();
        }

        private void C2ATS_Load(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btn_save_log, "Activate or deactivate saving logs to a log file");
            toolTip1.SetToolTip(this.btn_exit, "Exit the application");
            toolTip1.SetToolTip(this.btn_ats, "Acceptance Test Mode Test Case Execution");
            toolTip1.SetToolTip(this.btn_debug, "Debug Mode Test Execution");
            toolTip1.SetToolTip(this.btn_run, "Run Test Session");
            toolTip1.SetToolTip(this.btn_stop, "Stop Test Session");
        }

        private void runDebugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestManager.Instance.DbgModeBtnClicked();
        }

        private void runAcceptanceTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestManager.Instance.AtsModeBtnClicked();
        }

        private void btn_save_log_Click(object sender, EventArgs e)
        {
            TestManager.Instance.SaveLogsBtnClicked();
        }

        private void saveLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestManager.Instance.SaveLogsBtnClicked();
        }

        private void confToolStripTestType_CheckedChanged(object sender, EventArgs e)
        {
            TestManager.Instance.TestTypeToolTripChanged(((ToolStripMenuItem)sender).Checked);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TestManager.Instance.GarageBtnClicked();
        }

        private void flashFWToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog COpenFileDialog = new OpenFileDialog();
  
            COpenFileDialog.Filter = "FW Files|*.cxb";
            COpenFileDialog.Title = "Select the FirmWare File to Flash into Validator";
            if (COpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string fullPath = COpenFileDialog.FileName;
                
                if (fullPath != "")
                {
                    TestManager.Instance.FlashingInProgress = true;
                    TestManager.Instance.FwPath = fullPath;
                }

            }
        }

        private void getValidatorInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestManager.Instance.GarageBtnClicked();
        }

        private void decodeDataToolStripMenuItem_Click(object sender, EventArgs e)
        {

            GuiHandler.Instance.DecodeDataClicked(this);
        }

        public void toolsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if((TestManager.Instance.toolPass == "") || ( Properties.Settings.Default.ToolPass != TestManager.Instance.toolPass))
            {
                toolMenuLogin = new LoginForm();
                toolMenuLogin.Location = new Point(this.Location.X + 400, this.Location.Y + 300);
                toolMenuLogin.ShowDialog();
                        
            }
           
            
            
        
        }
    }
}
