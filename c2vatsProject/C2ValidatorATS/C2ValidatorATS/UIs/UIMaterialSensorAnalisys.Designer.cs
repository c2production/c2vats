﻿namespace C2ValidatorATS.UIs
{
    partial class UIMaterialSensorAnalisys
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.HdTC1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HdTC3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HdTC9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HdTA1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HdTA3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HdTA9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HdRA1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HdRA3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HdRA9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HdRB1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HdRB3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HdRB9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.HdTC1,
            this.HdTC3,
            this.HdTC9,
            this.HdTA1,
            this.HdTA3,
            this.HdTA9,
            this.HdRA1,
            this.HdRA3,
            this.HdRA9,
            this.HdRB1,
            this.HdRB3,
            this.HdRB9});
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(33, 41);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(340, 49);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // HdTC1
            // 
            this.HdTC1.Text = "TC1";
            // 
            // HdTC3
            // 
            this.HdTC3.Text = "TC3";
            // 
            // HdTC9
            // 
            this.HdTC9.Text = "TC9";
            // 
            // HdTA1
            // 
            this.HdTA1.Text = "TA1";
            // 
            // HdTA3
            // 
            this.HdTA3.Text = "TA3";
            // 
            // HdTA9
            // 
            this.HdTA9.Text = "TA9";
            // 
            // HdRA1
            // 
            this.HdRA1.Text = "RA1";
            // 
            // HdRA3
            // 
            this.HdRA3.Text = "RA3";
            // 
            // HdRA9
            // 
            this.HdRA9.Text = "RA9";
            // 
            // HdRB1
            // 
            this.HdRB1.Text = "RB1";
            // 
            // HdRB3
            // 
            this.HdRB3.Text = "RB3";
            // 
            // HdRB9
            // 
            this.HdRB9.Text = "RB9";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(111, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // UIMaterialSensorAnalisys
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listView1);
            this.Name = "UIMaterialSensorAnalisys";
            this.Size = new System.Drawing.Size(535, 164);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader HdTC1;
        private System.Windows.Forms.ColumnHeader HdTC3;
        private System.Windows.Forms.ColumnHeader HdTC9;
        private System.Windows.Forms.ColumnHeader HdTA1;
        private System.Windows.Forms.ColumnHeader HdTA3;
        private System.Windows.Forms.ColumnHeader HdTA9;
        private System.Windows.Forms.ColumnHeader HdRA1;
        private System.Windows.Forms.ColumnHeader HdRA3;
        private System.Windows.Forms.ColumnHeader HdRA9;
        private System.Windows.Forms.ColumnHeader HdRB1;
        private System.Windows.Forms.ColumnHeader HdRB3;
        private System.Windows.Forms.ColumnHeader HdRB9;
        private System.Windows.Forms.Label label1;
    }
}
