﻿/**
 * Class: LoginForm
 * Used to login into the menu Tools
 * Author: Leonardo Garcia, PayComplete
 **/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace C2ValidatorATS.UIs
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (TestManager.Instance.IsToolPassValid(txtbxPass.Text))
            {
                this.Dispose();
            }
            else lblWrongPass.Visible = true;
        }

        private void txtbxPass_KeyDown(object sender, KeyEventArgs e)
        {
           
            if(e.KeyCode==Keys.Enter)
                btnOk_Click(sender, e);

        }
        
    }
}
