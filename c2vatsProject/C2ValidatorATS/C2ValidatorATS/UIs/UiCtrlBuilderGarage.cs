﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut;

namespace C2ValidatorATS.UIs
{
    class UiCtrlBuilderGarage
    {
        Panel pnlGarage;
        Panel pnlDBlockInformation;
        Panel pnlChannelIdsformation;
        Panel pnlCoinLimtsInformation;

        Button btnShowIdentificationInfo;
        Button btnShowFunctionSetupInfo;
        Button btnShowChanneSetupInfo;
        Button btnShowChanneIDsInfo;
        Button btnShowCoinLimitsInfo;
        Button btnExit;
        int subPanelsStartPos;
        bool functionSetupPnlActive;
        bool productIdentificationPnlActive;
        bool channelSetupActive;
        bool channelIDsActive;
        bool coinLimitsActive;
        TextBox tbxSrNumber; //global because of editing feature
        string originalSrNr;
        //GroupBox gBxProductId;
        //Label lblSerieNumber;
        ValidatorUnderTest CVut;

        List<Control> idInfoCtrlListHdl;

        public UiCtrlBuilderGarage()
        {
            subPanelsStartPos = 50;
            functionSetupPnlActive = false;
            productIdentificationPnlActive = false;
            channelSetupActive = false;
            channelIDsActive = false;
            coinLimitsActive = false;
            CVut = new ValidatorUnderTest();
        }

        public void ShowGaragePanel(C2ATS CC2Mainform)
        {
            pnlGarage = new Panel();
            pnlGarage.Location = new Point(0, 50);
            pnlGarage.Size = new Size(CC2Mainform.Size.Width - 20, CC2Mainform.Size.Height - 90);
            pnlGarage.BackColor = Color.Beige;
            pnlGarage.BorderStyle = BorderStyle.FixedSingle;
            pnlGarage.AutoScroll = true;
            pnlGarage.Parent = CC2Mainform;
            pnlGarage.BringToFront();

            //Buttons
            btnShowIdentificationInfo = new Button();
            btnShowIdentificationInfo.Size = new Size(100, 35);
            btnShowIdentificationInfo.Location = new Point(20, 5);
            btnShowIdentificationInfo.Text = "ID Data";
            btnShowIdentificationInfo.ForeColor = Color.Black;
            btnShowIdentificationInfo.Click += new EventHandler(this.btnGetIdentificationInfo_clicked);
            btnShowIdentificationInfo.Parent = pnlGarage;

            btnShowFunctionSetupInfo = new Button();
            btnShowFunctionSetupInfo.Size = new Size(100, 35);
            btnShowFunctionSetupInfo.Location = new Point(130, 5);
            btnShowFunctionSetupInfo.Text = "Function Setup";
            btnShowFunctionSetupInfo.ForeColor = Color.Black;
            btnShowFunctionSetupInfo.Click += new EventHandler(this.btnGetFunctionSetupInfo_clicked);
            btnShowFunctionSetupInfo.Parent = pnlGarage;

            btnShowChanneSetupInfo = new Button();
            btnShowChanneSetupInfo.Size = new Size(100, 35);
            btnShowChanneSetupInfo.Location = new Point(240, 5);
            btnShowChanneSetupInfo.Text = "Channel Info";
            btnShowChanneSetupInfo.ForeColor = Color.Black;
            btnShowChanneSetupInfo.Click += new EventHandler(this.btnGetChannelSetupInfo_clicked);
            btnShowChanneSetupInfo.Parent = pnlGarage;

            btnShowChanneIDsInfo = new Button();
            btnShowChanneIDsInfo.Size = new Size(100, 35);
            btnShowChanneIDsInfo.Location = new Point(350, 5);
            btnShowChanneIDsInfo.Text = "Channel Ids";
            btnShowChanneIDsInfo.ForeColor = Color.Black;
            btnShowChanneIDsInfo.Click += new EventHandler(this.btnGetChannelIdsInfo_clicked);
            btnShowChanneIDsInfo.Parent = pnlGarage;

            btnShowCoinLimitsInfo = new Button();
            btnShowCoinLimitsInfo.Size = new Size(100, 35);
            btnShowCoinLimitsInfo.Location = new Point(460, 5);
            btnShowCoinLimitsInfo.Text = "Coin Limits";
            btnShowCoinLimitsInfo.ForeColor = Color.Black;
            btnShowCoinLimitsInfo.Click += new EventHandler(this.btnGetCoinLimitsInfo_clicked);
            btnShowCoinLimitsInfo.Parent = pnlGarage;

            //Exit button
            btnExit = new Button();
            btnExit.Size = new Size(50, 35);
            btnExit.Location = new Point(590, 5);
            btnExit.BackColor = Color.Coral;
            btnExit.Text = "Exit";
            btnExit.ForeColor = Color.Black;
            btnExit.Click += new EventHandler(this.btnExit_clicked);
            btnExit.Parent = pnlGarage;

        }
        //public void DestroyPnlpnlClickOnFail()
        //{
        //    if (pnlGarage != null) pnlGarage.Dispose();
        //}
        protected void btnGetIdentificationInfo_clicked(object sender, EventArgs e)
        {
            if (!productIdentificationPnlActive)
                TestManager.Instance.ShowInformationInGarage(0, CreateProdIdPanel(pnlGarage));
            productIdentificationPnlActive = true;
        }

        protected void btnGetFunctionSetupInfo_clicked(object sender, EventArgs e)
        {
            if (!functionSetupPnlActive)
                TestManager.Instance.ShowInformationInGarage(1, CreateFunctionSetupPanel(pnlGarage));
            functionSetupPnlActive = true;
        }

        protected void btnGetChannelSetupInfo_clicked(object sender, EventArgs e)
        {
            if (!channelSetupActive)
                TestManager.Instance.ShowInformationInGarage(2, CreateChannelSetupPanel(pnlGarage));
            channelSetupActive = true;
        }

        protected void btnGetChannelIdsInfo_clicked(object sender, EventArgs e)
        {
            if (!channelIDsActive)
                TestManager.Instance.ShowInformationInGarage(3, CreateChannelIDsPanel(pnlGarage));
            channelIDsActive = true;
        }

        protected void btnGetCoinLimitsInfo_clicked(object sender, EventArgs e)
        {
            if (!coinLimitsActive)
                TestManager.Instance.ShowInformationInGarage(4, CreateCoinLimitsPanel(pnlGarage));
            coinLimitsActive = true;
        }

        public void btnExit_clicked(object sender, EventArgs e)
        {
            subPanelsStartPos = 50;
            functionSetupPnlActive = false;
            productIdentificationPnlActive = false;
            channelSetupActive = false;
            channelIDsActive = false;
            coinLimitsActive = false;
            pnlGarage.Dispose();
        }

        protected void btnEditSrNr_clicked(object sender, EventArgs e)
        {
            tbxSrNumber.ReadOnly = false;
            originalSrNr = tbxSrNumber.Text;
        }

        protected void btnSaveSrNr_clicked(object sender, EventArgs e)
        {
            tbxSrNumber.ReadOnly = true;
            bool result = true;
            string SerialNumber = "";
            byte[] SrNrInBCD = new byte[9];

            SerialNumber = tbxSrNumber.Text.Replace("-", "");

            if (SerialNumber == "")
                return;
            SrNrInBCD = Convert2Bytes(SerialNumber, ref result);
            if (result)
                TestManager.Instance.SaveInfoIntoProductIdTbl(3, SrNrInBCD);
            else tbxSrNumber.Text = originalSrNr;
        }

        private byte[] Convert2Bytes(string srNrString, ref bool result)
        {

            byte[] converted = new byte[srNrString.Length / 2];
            int j = 0;
            for (int i = 0; i < srNrString.Length; i += 2)
            {
                try
                {
                    int convertedToInt = Convert.ToInt32(srNrString.Substring(i, 2));
                    if (convertedToInt == 0)
                    {
                        converted[j++] = 0;
                        continue;
                    }
                    //firs nr in first 4 bits second nr shifted 4 bits
                    converted[j++] = (byte)(convertedToInt % 10 + convertedToInt / 10 * 16);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Only numbers are permited in Validator Serial Numbers. " + e.Message);
                    result = false;
                    break;
                }
            }

            return converted;
        }

        private List<Control> CreateProdIdPanel(Panel pnlGarage)
        {
            idInfoCtrlListHdl = new List<Control>();
            pnlDBlockInformation = new Panel();
            pnlDBlockInformation.Location = new Point(10, subPanelsStartPos);
            pnlDBlockInformation.Size = new Size(pnlGarage.Size.Width - 50, 105);
            pnlDBlockInformation.BackColor = Color.LightBlue;
            pnlDBlockInformation.BorderStyle = BorderStyle.Fixed3D;
            pnlDBlockInformation.Parent = pnlGarage;
            subPanelsStartPos += 110; //For next panel to open

            GroupBox gBxProductId = new GroupBox();
            gBxProductId.Text = "Function Id Data";
            gBxProductId.Location = new Point(0, 0);
            gBxProductId.Size = new Size(pnlDBlockInformation.Size.Width - 5, pnlDBlockInformation.Size.Height - 5);
            gBxProductId.Parent = pnlDBlockInformation;

            //Labels info type
            Label lblSrNumber = new Label();
            lblSrNumber.Text = "Sr/Order Number:";
            lblSrNumber.Location = new Point(10, 40);
            lblSrNumber.AutoSize = true;
            lblSrNumber.Parent = gBxProductId;
            //####


            Label lblDBlockNr = new Label();
            lblDBlockNr.Text = "Data Block:";
            lblDBlockNr.Location = new Point(270, 40);
            lblDBlockNr.AutoSize = true;
            lblDBlockNr.Parent = gBxProductId;

            Label lblProdDate = new Label();
            lblProdDate.Text = "Production Date;";
            lblProdDate.Location = new Point(476, 40);
            lblProdDate.AutoSize = true;
            lblProdDate.Parent = gBxProductId;

            Label lblFWVersion = new Label();
            lblFWVersion.Text = "FW Ver:";
            lblFWVersion.Location = new Point(660, 40);
            lblFWVersion.AutoSize = true;
            lblFWVersion.Parent = gBxProductId;

            Label lblPCBId = new Label();
            lblPCBId.Text = "PCB Id:";
            lblPCBId.Location = new Point(785, 40);
            lblPCBId.AutoSize = true;
            lblPCBId.Parent = gBxProductId;

            Label lblRIMSensor = new Label();
            lblRIMSensor.Text = "RIM Sensor:";
            lblRIMSensor.Location = new Point(10, 70);
            lblRIMSensor.AutoSize = true;
            lblRIMSensor.Parent = gBxProductId;

            Button btnEditSrNr = new Button();
            btnEditSrNr.Image = Properties.Resources.edit;
            btnEditSrNr.Size = new Size(25, 25);
            btnEditSrNr.Location = new Point(lblSrNumber.Location.X + 105, 10);
            btnEditSrNr.Click += new EventHandler(this.btnEditSrNr_clicked);
            btnEditSrNr.Parent = gBxProductId;

            Button btnSaveSrNr = new Button();
            btnSaveSrNr.Image = Properties.Resources.save;
            btnSaveSrNr.Size = new Size(25, 25);
            btnSaveSrNr.Location = new Point(lblSrNumber.Location.X + 140, 10);
            btnSaveSrNr.Click += new EventHandler(this.btnSaveSrNr_clicked);
            btnSaveSrNr.Parent = gBxProductId;

            //Where to show the information

            tbxSrNumber = new TextBox();
            //tbxSrNumber.Text = "Sr/Order Number:";
            tbxSrNumber.Location = new Point(lblSrNumber.Location.X + 105, 40);
            tbxSrNumber.ReadOnly = true;
            tbxSrNumber.Size = new Size(120, lblSrNumber.Size.Height);
            tbxSrNumber.BorderStyle = BorderStyle.FixedSingle;
            tbxSrNumber.BackColor = Color.LightBlue;
            tbxSrNumber.Parent = gBxProductId;
            idInfoCtrlListHdl.Add(tbxSrNumber);

            Label lblIndicatorDBlock = new Label();
            lblIndicatorDBlock.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorDBlock.Location = new Point(lblDBlockNr.Location.X + 70, 40);
            lblIndicatorDBlock.AutoSize = true;
            lblIndicatorDBlock.Parent = gBxProductId;
            idInfoCtrlListHdl.Add(lblIndicatorDBlock);

            Label lblIndicatorProdDate = new Label();
            lblIndicatorProdDate.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorProdDate.Location = new Point(lblProdDate.Location.X + 100, 40);
            lblIndicatorProdDate.AutoSize = true;
            lblIndicatorProdDate.Parent = gBxProductId;
            idInfoCtrlListHdl.Add(lblIndicatorProdDate);

            Label lblIndicatorFW = new Label();
            lblIndicatorFW.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorFW.Location = new Point(lblFWVersion.Location.X + 50, 40);
            lblIndicatorFW.AutoSize = true;
            lblIndicatorFW.Parent = gBxProductId;
            idInfoCtrlListHdl.Add(lblIndicatorFW);

            Label lblIndicatorPCBId = new Label();
            lblIndicatorPCBId.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorPCBId.Location = new Point(lblPCBId.Location.X + 47, 40);
            lblIndicatorPCBId.AutoSize = true;
            lblIndicatorPCBId.Parent = gBxProductId;
            idInfoCtrlListHdl.Add(lblIndicatorPCBId);

            Label lblIndicatorRIMSensor = new Label();
            lblIndicatorRIMSensor.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorRIMSensor.Location = new Point(lblRIMSensor.Location.X + 80, 70);
            lblIndicatorRIMSensor.AutoSize = true;
            lblIndicatorRIMSensor.Parent = gBxProductId;
            idInfoCtrlListHdl.Add(lblIndicatorRIMSensor);


            return new List<Control>(idInfoCtrlListHdl);
        }

        private List<Control> CreateFunctionSetupPanel(Panel pnlGarage)
        {
            idInfoCtrlListHdl = new List<Control>();
            pnlDBlockInformation = new Panel();
            pnlDBlockInformation.Location = new Point(10, subPanelsStartPos);
            pnlDBlockInformation.Size = new Size(pnlGarage.Size.Width - 50, 100);
            pnlDBlockInformation.BackColor = Color.LightBlue;
            pnlDBlockInformation.BorderStyle = BorderStyle.Fixed3D;
            pnlDBlockInformation.Parent = pnlGarage;
            subPanelsStartPos += 110;

            GroupBox gBxSetupData = new GroupBox();
            gBxSetupData.Text = "Setup Data";
            gBxSetupData.Location = new Point(0, 0);
            gBxSetupData.Size = new Size(pnlDBlockInformation.Size.Width - 5, pnlDBlockInformation.Size.Height - 5);
            gBxSetupData.Parent = pnlDBlockInformation;

            //Labels info type
            Label lblCurrencyCode = new Label();
            lblCurrencyCode.Text = "Currency Code:";
            lblCurrencyCode.Location = new Point(10, 40);
            lblCurrencyCode.AutoSize = true;
            lblCurrencyCode.Parent = gBxSetupData;

            Label lblFormula = new Label();
            lblFormula.Text = "Formula:";
            lblFormula.Location = new Point(200, 40);
            lblFormula.AutoSize = true;
            lblFormula.Parent = gBxSetupData;

            Label lblMMV = new Label();
            lblMMV.Text = "MMV:";
            lblMMV.Location = new Point(300, 40);
            lblMMV.AutoSize = true;
            lblMMV.Parent = gBxSetupData;

            Label lblMMVmin = new Label();
            lblMMVmin.Text = "MMV min:";
            lblMMVmin.Location = new Point(380, 40);
            lblMMVmin.AutoSize = true;
            lblMMVmin.Parent = gBxSetupData;

            Label lblMMVmax = new Label();
            lblMMVmax.Text = "MMV max:";
            lblMMVmax.Location = new Point(480, 40);
            lblMMVmax.AutoSize = true;
            lblMMVmax.Parent = gBxSetupData;

            Label lblDecimalPlace = new Label();
            lblDecimalPlace.Text = "Decimal Place;";
            lblDecimalPlace.Location = new Point(576, 40);
            lblDecimalPlace.AutoSize = true;
            lblDecimalPlace.Parent = gBxSetupData;

            //Labels where to show the information
            Label lblIndicatorCurrencyCode = new Label();
            lblIndicatorCurrencyCode.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorCurrencyCode.Location = new Point(lblCurrencyCode.Location.X + 95, 40);
            lblIndicatorCurrencyCode.AutoSize = true;
            lblIndicatorCurrencyCode.Parent = gBxSetupData;
            idInfoCtrlListHdl.Add(lblIndicatorCurrencyCode);

            Label lblIndicatorFormula = new Label();
            lblIndicatorFormula.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorFormula.Location = new Point(lblFormula.Location.X + 60, 40);
            lblIndicatorFormula.AutoSize = true;
            lblIndicatorFormula.Parent = gBxSetupData;
            idInfoCtrlListHdl.Add(lblIndicatorFormula);

            Label lblIndicatorMMV = new Label();
            lblIndicatorMMV.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorMMV.Location = new Point(lblMMV.Location.X + 45, 40);
            lblIndicatorMMV.AutoSize = true;
            lblIndicatorMMV.Parent = gBxSetupData;
            idInfoCtrlListHdl.Add(lblIndicatorMMV);

            Label lblIndicatorMMVmin = new Label();
            lblIndicatorMMVmin.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorMMVmin.Location = new Point(lblMMVmin.Location.X + 70, 40);
            lblIndicatorMMVmin.AutoSize = true;
            lblIndicatorMMVmin.Parent = gBxSetupData;
            idInfoCtrlListHdl.Add(lblIndicatorMMVmin);

            Label lblIndicatorMMVmax = new Label();
            lblIndicatorMMVmax.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorMMVmax.Location = new Point(lblMMVmax.Location.X + 70, 40);
            lblIndicatorMMVmax.AutoSize = true;
            lblIndicatorMMVmax.Parent = gBxSetupData;
            idInfoCtrlListHdl.Add(lblIndicatorMMVmax);

            Label lblIndicatorDecimalPlace = new Label();
            lblIndicatorDecimalPlace.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorDecimalPlace.Location = new Point(lblDecimalPlace.Location.X + 100, 40);
            lblIndicatorDecimalPlace.AutoSize = true;
            lblIndicatorDecimalPlace.Parent = gBxSetupData;
            idInfoCtrlListHdl.Add(lblIndicatorDecimalPlace);

            return new List<Control>(idInfoCtrlListHdl);
        }

        private List<Control> CreateChannelSetupPanel(Panel pnlGarage)
        {
            idInfoCtrlListHdl = new List<Control>();



            pnlDBlockInformation = new Panel();
            pnlDBlockInformation.Location = new Point(10, subPanelsStartPos);
            pnlDBlockInformation.Size = new Size(pnlGarage.Size.Width - 50, 240);
            pnlDBlockInformation.BackColor = Color.LightBlue;
            pnlDBlockInformation.BorderStyle = BorderStyle.Fixed3D;
            pnlDBlockInformation.Parent = pnlGarage;
            subPanelsStartPos += 250;

            GroupBox gBxChannelSetup = new GroupBox();
            gBxChannelSetup.Text = "Channel Setup";
            gBxChannelSetup.Location = new Point(0, 0);
            gBxChannelSetup.Size = new Size(pnlDBlockInformation.Size.Width - 5, pnlDBlockInformation.Size.Height - 5);
            gBxChannelSetup.Parent = pnlDBlockInformation;

            Label lblBlkA = new Label();
            lblBlkA.Text = "Block A";
            lblBlkA.AutoSize = true;
            lblBlkA.Location = new Point(200, 10);
            lblBlkA.Parent = gBxChannelSetup;

            Label lblBlkB = new Label();
            lblBlkB.Text = "Block B";
            lblBlkB.AutoSize = true;
            lblBlkB.Location = new Point(640, 10);
            lblBlkB.Parent = gBxChannelSetup;

            ListView lViewChannelSeupBlockA = new ListView();
            //lViewChannelSeupBlockA.BackColor = Color.LightBlue;
            lViewChannelSeupBlockA.View = View.Details;
            lViewChannelSeupBlockA.Location = new Point(30, 30);
            lViewChannelSeupBlockA.Size = new Size(380, 200);
            lViewChannelSeupBlockA.Parent = gBxChannelSetup;

            idInfoCtrlListHdl.Add(lViewChannelSeupBlockA);

            ListView lViewChannelSeupBlockB = new ListView();
            //lViewChannelSeupBlockB.BackColor = Color.LightBlue;
            lViewChannelSeupBlockB.View = View.Details;
            lViewChannelSeupBlockB.Location = new Point(480, 30);
            lViewChannelSeupBlockB.Size = new Size(380, 200);
            lViewChannelSeupBlockB.Parent = gBxChannelSetup;

            idInfoCtrlListHdl.Add(lViewChannelSeupBlockB);

            return idInfoCtrlListHdl;

        }

        //Chnnel Stup Table

        private List<Control> CreateChannelIDsPanel(Panel pnlGarage)
        {
            idInfoCtrlListHdl = new List<Control>();

            pnlChannelIdsformation = new Panel();
            pnlChannelIdsformation.Location = new Point(10, subPanelsStartPos);
            pnlChannelIdsformation.Size = new Size(pnlGarage.Size.Width - 50, 260);
            pnlChannelIdsformation.BackColor = Color.LightBlue;
            pnlChannelIdsformation.BorderStyle = BorderStyle.Fixed3D;
            pnlChannelIdsformation.Parent = pnlGarage;
            subPanelsStartPos += 270;

            ListView lViewChannelIDs = new ListView();
            //lViewChannelSeupBlockA.BackColor = Color.LightBlue;
            lViewChannelIDs.View = View.Details;
            lViewChannelIDs.Location = new Point(30, 30);
            lViewChannelIDs.Size = new Size(500, 200);
            lViewChannelIDs.Parent = pnlChannelIdsformation;
            idInfoCtrlListHdl.Add(lViewChannelIDs);
            return idInfoCtrlListHdl;
        }

        //Coin Limits Pannel
        private List<Control> CreateCoinLimitsPanel(Panel pnlGarage)
        {
            idInfoCtrlListHdl = new List<Control>();

            pnlCoinLimtsInformation = new Panel();
            pnlCoinLimtsInformation.Location = new Point(10, subPanelsStartPos);
            pnlCoinLimtsInformation.Size = new Size(pnlGarage.Size.Width - 50, 360);
            pnlCoinLimtsInformation.BackColor = Color.LightBlue;
            pnlCoinLimtsInformation.BorderStyle = BorderStyle.Fixed3D;
            pnlCoinLimtsInformation.Parent = pnlGarage;
            subPanelsStartPos += 370;

            ListView lViewCoinLimits = new ListView();
            //lViewChannelSeupBlockA.BackColor = Color.LightBlue;
            lViewCoinLimits.View = View.Details;
            lViewCoinLimits.Location = new Point(10, 10);
            lViewCoinLimits.Size = new Size(890, 340);
            lViewCoinLimits.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            lViewCoinLimits.Parent = pnlCoinLimtsInformation;
            idInfoCtrlListHdl.Add(lViewCoinLimits);
            return idInfoCtrlListHdl;

        }
    }
}
