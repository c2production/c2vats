﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using C2ValidatorATS.Utils;
using C2ValidatorATS.Vut.VutDBlocksInfo;
namespace C2ValidatorATS.UIs
{

    class UICtrlBuilderProdInfo
    {
        Panel pnlProdInfo;
        VutIdentificationData CVutIdentificationData;
        Label lblSrNumber, lblDBlockNr, lblFWVersion, lblProdDate;
        C2ATS CmainForm;
        ToolTip toolTip1; 
        public void ShowProdInfoPanel(C2ATS CC2Mainform)
        {
            toolTip1 = new System.Windows.Forms.ToolTip();
            CmainForm = CC2Mainform;
            pnlProdInfo = new Panel();
            pnlProdInfo.Location = new Point(265, 57);
            pnlProdInfo.Size = new Size(355, 125);
            pnlProdInfo.BackColor = Color.Bisque;
            pnlProdInfo.BorderStyle = BorderStyle.FixedSingle;
            pnlProdInfo.Parent = CC2Mainform;
            pnlProdInfo.BringToFront();

            //Labels info type
            lblSrNumber = new Label();
            lblSrNumber.Text = "Sr/Order Number:";
            lblSrNumber.Font  = new Font(lblSrNumber.Font, FontStyle.Bold);
            lblSrNumber.Location = new Point(5, 5);
            lblSrNumber.AutoSize = true;
            lblSrNumber.Parent = pnlProdInfo;

            lblDBlockNr = new Label();
            lblDBlockNr.Text = "Data Block:";
            lblDBlockNr.Font = new Font(lblDBlockNr.Font, FontStyle.Bold);
            lblDBlockNr.Location = new Point(165, 5);
            lblDBlockNr.AutoSize = true;
            lblDBlockNr.Parent = pnlProdInfo;

            lblFWVersion = new Label();
            lblFWVersion.Text = "FW Ver:";
            lblFWVersion.Font = new Font(lblFWVersion.Font, FontStyle.Bold);
            lblFWVersion.Location = new Point(5 , 60);
            lblFWVersion.AutoSize = true;
            lblFWVersion.Parent = pnlProdInfo;

            lblProdDate = new Label();
            lblProdDate.Text = "Production Date;";
            lblProdDate.Font = new Font(lblProdDate.Font, FontStyle.Bold);
            lblProdDate.Location = new Point(165, 60);
            lblProdDate.AutoSize = true;
            lblProdDate.Parent = pnlProdInfo;

            Button btnUpdate = new Button();
            btnUpdate.Location = new Point(300, 5);
            btnUpdate.Size = new Size(49, 40); ;
            Image updateImg = Properties.Resources.update11;
            btnUpdate.Image = updateImg;
            btnUpdate.FlatStyle = FlatStyle.Flat;
            btnUpdate.FlatAppearance.BorderSize = 0;
            btnUpdate.Parent = pnlProdInfo;
            btnUpdate.Click += new EventHandler(this.btnUpdate_clicked);
            //Label lblPCBId = new Label();
            //lblPCBId.Text = "PCB Id:";
            //lblPCBId.Location = new Point(785, 40);
            //lblPCBId.AutoSize = true;
            //lblPCBId.Parent = pnlProdInfo;
            toolTip1.SetToolTip(btnUpdate, "Update Information");
            ExtractAndShowInfo();
          
        }

        public void ExtractAndShowInfo()
        {
            CVutIdentificationData = new VutIdentificationData();
            CVutIdentificationData.RetreiveAllData();
            //Labels Where to show th information
            Label lblIndicatorSrNumber = new Label();
            lblIndicatorSrNumber.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorSrNumber.Location = new Point(lblSrNumber.Location.X, lblSrNumber.Location.Y + 25);
            lblIndicatorSrNumber.AutoSize = true;
            lblIndicatorSrNumber.Parent = pnlProdInfo;
            lblIndicatorSrNumber.Click += new EventHandler(this.lblIndicator_clicked);
            lblIndicatorSrNumber.DoubleClick += new EventHandler(this.lblIndicatorSrNumber_doubleClicked);
            lblIndicatorSrNumber.Text = (CVutIdentificationData.SerialNumber == null || CVutIdentificationData.SerialNumber.Contains("FFF"))
                ? "NONE" : CVutIdentificationData.SerialNumber;

            Label lblIndicatorDBlock = new Label();
            lblIndicatorDBlock.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorDBlock.Location = new Point(lblDBlockNr.Location.X, lblDBlockNr.Location.Y + 25);
            lblIndicatorDBlock.AutoSize = true;
            lblIndicatorDBlock.Parent = pnlProdInfo;
            lblIndicatorDBlock.Click += new EventHandler(this.lblIndicator_clicked);
            lblIndicatorDBlock.Text = (CVutIdentificationData.DataBlock == null || CVutIdentificationData.DataBlock == "")
                ? "NONE" : CVutIdentificationData.DataBlock;

            Label lblIndicatorFW = new Label();
            lblIndicatorFW.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorFW.Location = new Point(lblFWVersion.Location.X, lblFWVersion.Location.Y + 25);
            lblIndicatorFW.AutoSize = true;
            lblIndicatorFW.Parent = pnlProdInfo;
            lblIndicatorFW.Click += new EventHandler(this.lblIndicator_clicked);
            lblIndicatorFW.Text = (CVutIdentificationData.FirmwareVersion == null || CVutIdentificationData.FirmwareVersion.Contains("FFF"))
                ? "NONE" : CVutIdentificationData.FirmwareVersion;

            Label lblIndicatorProdDate = new Label();
            lblIndicatorProdDate.BorderStyle = BorderStyle.FixedSingle;
            lblIndicatorProdDate.Location = new Point(lblProdDate.Location.X, lblProdDate.Location.Y + 25);
            lblIndicatorProdDate.AutoSize = true;
            lblIndicatorProdDate.Parent = pnlProdInfo;
            lblIndicatorProdDate.Click += new EventHandler(this.lblIndicator_clicked);
            lblIndicatorProdDate.Text = (CVutIdentificationData.ProductionDate == null || CVutIdentificationData.ProductionDate.Contains("FF"))
                ? "NONE" : CVutIdentificationData.ProductionDate;

            toolTip1.SetToolTip(lblIndicatorSrNumber, "Click on it to Copy SrNumber or Double Click to Copy SrNr and Date of Production ");
            toolTip1.SetToolTip(lblIndicatorDBlock, "Click on it to Copy Data Block Nr");
            toolTip1.SetToolTip(lblIndicatorFW, "Click on it to Copy FirmWare version");
            toolTip1.SetToolTip(lblIndicatorProdDate, "Click on it to Copy Date of Production");

        }
        public void btnUpdate_clicked(object sender, EventArgs e)
        {
            pnlProdInfo.Controls.Clear();
            pnlProdInfo.Dispose();

            ShowProdInfoPanel(CmainForm);
        }
        public void lblIndicator_clicked(object sender, EventArgs e)
        {
            Clipboard.SetText((sender as Label).Text);
        }
        public void lblIndicatorSrNumber_doubleClicked(object sender, EventArgs e)
        {
            Clipboard.SetText((sender as Label).Text + "  " + '\t' + " " + ((CVutIdentificationData.ProductionDate == null || CVutIdentificationData.ProductionDate.Contains("FF")) ? "NONE" : CVutIdentificationData.ProductionDate));
        }
        public void DestroyPnlProductInfo()
        {

            if (pnlProdInfo != null)
            {
                //List<Control> ctrls = new List<Control>(pnlProdInfo.Controls);
                pnlProdInfo.Controls.Clear();
                pnlProdInfo.Visible = false;
                pnlProdInfo.Dispose();
                //foreach (Control c in ctrls)
                //    c.Dispose();
                //pnlProdInfo.Visible = false;
                //pnlProdInfo.
                ////pnlProdInfo.Dispose();
                //foreach (var control in pnlProdInfo.Controls)
                //{
                //    control.Dispose();
                //}
            }
        }
        
    }
}
