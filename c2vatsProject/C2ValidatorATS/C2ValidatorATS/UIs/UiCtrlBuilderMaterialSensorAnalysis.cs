﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace C2ValidatorATS.UIs
{
    class UiCtrlBuilderMaterialSensorAnalysis
    {
        Panel PnlMaterialSensor;
        ListView lVwMaterialSensors;
        ListViewItem lVwItemMaxLimits;
        ListViewItem lVwItemMinLimits;
        ListViewItem lVwItemMeasuredValues;
        Button btnTestSensors;
        Button btnContinuousTest;
        Button btnEndTest;
        RadioButton rBtnWithPulse;
        RadioButton rBtnNoPulse;
        CheckBox chBxStopOnFailure;

        int MAX_PIN = 12;
        //No Pulse Limits
        string  MAX_TRANS_NO_PULSE = "200";
        string  MIN_TRANS_NO_PULSE = "80";
        string  MAX_REFLX_NO_PULSE = "100";
        string  MIN_REFLX_NO_PULSE = "20";
        //With pulse Limits
        string MAX_TRANS_WITH_PULSE = "1000";
        string MIN_TRANS_WITH_PULSE = "800";
        string MAX_REFLX_WITH_PULSE = "650";
        string MIN_REFLX_WITH_PULSE = "400";

        public UiCtrlBuilderMaterialSensorAnalysis()
        {
            //Set all to initial state
            TestManager.Instance.runContinuously = false;
            TestManager.Instance.stopOnFailure = false;
        }
        private string GetPinName(int pinNr)
        {
            switch (pinNr)
            {
                case 0: return "TC1";
                case 1: return "TC3";
                case 2: return "TC9";
                case 3: return "TA1";
                case 4: return "TA3";
                case 5: return "TA9";
                case 6: return "RA1";
                case 7: return "RA3";
                case 8: return "RA9";
                case 9: return "RB1";
                case 10: return "RB3";
                case 11: return "RB9";
                default: return "";
            }
        }

        private void CreateTable()
        {
            lVwMaterialSensors = new ListView();

            lVwMaterialSensors.View = View.Details;
            lVwMaterialSensors.BackColor = Color.LightBlue;
            lVwMaterialSensors.Location = new Point(20, 120);
            lVwMaterialSensors.Size = new Size(460, 120);
            lVwMaterialSensors.Columns.Add(" ", -2); //First one empty
            for (int i = 0; i < MAX_PIN; i++)
            {
                lVwMaterialSensors.Columns.Add(GetPinName(i), -2, HorizontalAlignment.Left);
            }
            //Add "Max" in second row as the title of max values
            lVwItemMaxLimits = new ListViewItem("Max", 0);
            //Add empty characters to the limit values
            ResetRow(lVwItemMaxLimits);
            //Add an empty row just to make visibility better
            AddEmptyRow(1); //index 1
            //Add Result/Measured Value row. Row index 2
            lVwItemMeasuredValues = new ListViewItem("Result", 2);
            //Add empty characters to the result values
            ResetRow(lVwItemMeasuredValues);
            //Add an empty row just to make visibility better
            AddEmptyRow(3); //index 3
            //Add min row it is the 4th row
            lVwItemMinLimits = new ListViewItem("Min", 4);
            //Add empty characters to the limit values
            ResetRow(lVwItemMinLimits);
            //Add Limits according to default test selection
            SetLimits(true); //With pulse is the default selection
            lVwMaterialSensors.Parent = PnlMaterialSensor;
        }

        //Add empty row in the row with index = param index
        private void AddEmptyRow(int index)
        {
            ListViewItem emptyRow = new ListViewItem("", index);
            for (int i = 0; i < MAX_PIN; i++)
            {
                emptyRow.SubItems.Add("");
            }
            lVwMaterialSensors.Items.AddRange(new ListViewItem[] { emptyRow });
        }

        private void ResetRow(ListViewItem lVwItem)
        {
            for (int i = 0; i < MAX_PIN; i++)
            {
                lVwItem.SubItems.Add(" --");
            }
            lVwMaterialSensors.Items.AddRange(new ListViewItem[] { lVwItem });
        }

        private void ClearMeasuredValueRow()
        {
            foreach (ListViewItem item in lVwMaterialSensors.Items)
            {
                if (item.SubItems[0].Text == "Result")
                {   //Clean foreground color
                    for (int i = 1; i < MAX_PIN + 1; i++)
                    {
                        item.SubItems[i].ForeColor = Color.Black;
                        item.SubItems[i].Text = " --";
                    }
                    //Write the measured values
                   
                    item.UseItemStyleForSubItems = false;
                    break;
                }
            }
        }
        private void SetLimits(bool withPulse)
        {
            //CleanMeasuredValuesRow();
            foreach (ListViewItem item in lVwMaterialSensors.Items)
            {
                if (item.SubItems[0].Text == "Max")
                {
                    for (int i = 0; i < MAX_PIN; i++)
                    {
                        if (i < 6)
                        {
                            item.SubItems[i + 1].Text =  withPulse ? MAX_TRANS_WITH_PULSE : MAX_TRANS_NO_PULSE;
                            lVwItemMinLimits.SubItems.Add(withPulse ? MIN_TRANS_WITH_PULSE : MIN_TRANS_NO_PULSE);
                        }
                        else
                        {
                            item.SubItems[i + 1].Text = withPulse ? MAX_REFLX_WITH_PULSE : MAX_REFLX_NO_PULSE;
                            lVwItemMinLimits.SubItems.Add(withPulse ? MIN_REFLX_WITH_PULSE : MIN_REFLX_NO_PULSE);
                        }
                    }
                    item.UseItemStyleForSubItems = false;
                }

                if (item.SubItems[0].Text == "Min")
                {
                    for (int i = 0; i < MAX_PIN; i++)
                    {
                        if (i < 6)
                        {
                            item.SubItems[i + 1].Text = withPulse ? MIN_TRANS_WITH_PULSE : MIN_TRANS_NO_PULSE;      
                        }
                        else
                        {
                            item.SubItems[i + 1].Text = withPulse ? MIN_REFLX_WITH_PULSE : MIN_REFLX_NO_PULSE;
                        }
                    }
                    item.UseItemStyleForSubItems = false;
                }
            }
        }


        public void ShowMaterialSensorTable(C2ATS CC2Mainform)
        {
            PnlMaterialSensor = new Panel();
            PnlMaterialSensor.Location = new Point(0, 0);
            PnlMaterialSensor.Size = new Size(CC2Mainform.pnl_UserInfo.Size.Width, 320);
            PnlMaterialSensor.BackColor = Color.LightBlue;
            PnlMaterialSensor.Parent = CC2Mainform.pnl_UserInfo;
            CreateTable();
            BuildTestSensorBtn();
            BuildContinuousTestBtn();
            BuildStopOnFailureChBox();
            BuildTestSelectionRadioBtn();
            BuildEndTestBtn();
            PnlMaterialSensor.BringToFront();
        }

        public void ShowMaterialSensorResults(int[] measureResult)
        {
            foreach (ListViewItem item in lVwMaterialSensors.Items)
            {
                if (item.SubItems[0].Text == "Result")
                {   //Clean foreground color
                    item.SubItems[0].ForeColor = Color.Black;
                    for (int i = 1; i < MAX_PIN + 1; i++)
                    {
                        item.SubItems[i].ForeColor = Color.Green;
                    }
                    //Write the measured values
                    for (int i = 0; i < MAX_PIN; i++)
                    {
                        item.SubItems[i + 1].Text = measureResult[i*2].ToString();
                        //If result is out of limits. Byte right after the measured value indicates measurement fail = 1  
                        if (measureResult[i * 2 + 1] == 1) 
                        {
                            item.SubItems[i + 1].ForeColor = Color.Red;
                            item.SubItems[0].ForeColor = Color.Red;
                        }
                    }
                    item.UseItemStyleForSubItems = false;
                    break;
                }
            }
        }

        public void StopOnFailTriggered()
        {
            btnContinuousTest.Text = "Continuous Test";
        }
        public void DestroyThisPnl()
        {
            if (PnlMaterialSensor != null)
            {
                TestManager.Instance.TestExecSuspended = true;
                TestManager.Instance.doMaterialSensorWithPulseTest = false;
                TestManager.Instance.doMaterialSensorNoPulseTest = false;
                TestManager.Instance.runContinuously = false;
                PnlMaterialSensor.Dispose();
            }
        }

        ///Other Controls and Events/////////////////////////////////
        ///
        private void BuildTestSensorBtn()
        {
            btnTestSensors = new Button();
            btnTestSensors.Text = "Test Sensors";
            btnTestSensors.Size = new Size(100, 30);
            btnTestSensors.Location = new Point(100, 60);
            btnTestSensors.BackColor = Color.LightGreen;
            btnTestSensors.Click += new EventHandler(this.btnTestSensor_Click);
            btnTestSensors.Enabled = true;
            btnTestSensors.Parent = PnlMaterialSensor;
        }

        private void BuildTestSelectionRadioBtn()
        {
            rBtnWithPulse = new RadioButton();
            rBtnWithPulse.Location = new Point(20, 5);
            rBtnWithPulse.AutoSize = true;
            rBtnWithPulse.Checked = true;
            rBtnWithPulse.Text = "Test Material Sensor With Pulse";
            rBtnWithPulse.CheckedChanged += new EventHandler(this.rbtnChecked_Changed);
            rBtnWithPulse.Parent = PnlMaterialSensor;

            rBtnNoPulse = new RadioButton();
            rBtnNoPulse.Location = new Point(20, 30);
            rBtnNoPulse.AutoSize = true;
            rBtnNoPulse.Text = "Test Material Sensor No Pulse";
            rBtnNoPulse.CheckedChanged += new EventHandler(this.rbtnChecked_Changed);
            rBtnNoPulse.Parent = PnlMaterialSensor;
        }
        private void BuildEndTestBtn()
        {
            btnEndTest = new Button();
            btnEndTest.Text = "End SensorTest";
            btnEndTest.Size = new Size(130, 30);
            btnEndTest.Location = new Point(300, 10);
            btnEndTest.BackColor = Color.LightSalmon;
            btnEndTest.Click += new EventHandler(this.btnEndTest_Click);
            btnEndTest.Parent = PnlMaterialSensor;
        }
        private void BuildContinuousTestBtn()
        {
            btnContinuousTest = new Button();
            btnContinuousTest.Text = "Continuous Test";
            btnContinuousTest.Size = new Size(130, 30);
            btnContinuousTest.Location = new Point(250, 60);
            btnContinuousTest.BackColor = Color.LightGreen;
            btnContinuousTest.Click += new EventHandler(this.btnContinuousTest_Click);
            btnContinuousTest.Parent = PnlMaterialSensor;
        }
        private void BuildStopOnFailureChBox()
        {
            chBxStopOnFailure = new CheckBox();
            chBxStopOnFailure.Text = "Stop on failure";
            chBxStopOnFailure.Location = new Point(260, 92);
            chBxStopOnFailure.Size = new Size(130, 30);
            chBxStopOnFailure.CheckedChanged += new EventHandler(this.chBxStopOnFailure_checkedChanged);
            chBxStopOnFailure.Parent = PnlMaterialSensor;
        }
        //Events
        protected void btnTestSensor_Click(Object sender, EventArgs e)
        {
            if(rBtnWithPulse.Checked ) 
            {
                TestManager.Instance.doMaterialSensorWithPulseTest = true;
                TestManager.Instance.doMaterialSensorNoPulseTest = false;
            }
            else
            {
                TestManager.Instance.doMaterialSensorNoPulseTest = true;
                TestManager.Instance.doMaterialSensorWithPulseTest = false;
            }
        }
        protected void btnContinuousTest_Click(Object sender, EventArgs e)
        {
            if (TestManager.Instance.runContinuously)
            {
                TestManager.Instance.runContinuously = false;
                (sender as Button).Text = "Continuous Test";
            }
            else
            {
                TestManager.Instance.runContinuously = true;
                (sender as Button).Text = "Stop Test";
                btnTestSensor_Click(sender, e);
            }
        }
        protected void chBxStopOnFailure_checkedChanged(object sender, EventArgs e)
        {
            if (chBxStopOnFailure.Checked)
                TestManager.Instance.stopOnFailure = true;
            else
                TestManager.Instance.stopOnFailure = false;
        }
        protected void rbtnChecked_Changed(object sender, EventArgs e)
        {
            if (rBtnNoPulse.Checked)
            {
                SetLimits(false); //False is to set limits for No Pulse
            }
            else SetLimits(true);
            ClearMeasuredValueRow();
            //In case continues test is On, then update the measurement type
            if (TestManager.Instance.runContinuously) 
                btnTestSensor_Click(null, null);

        }

        protected void btnEndTest_Click(object sender, EventArgs e)
        {
            this.DestroyThisPnl();
        }

        ///Helper methods
        ///
 
    }
}
