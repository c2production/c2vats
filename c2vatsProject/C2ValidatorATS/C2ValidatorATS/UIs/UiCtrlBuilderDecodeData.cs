﻿/**
 * Class: UiCtrlBuilderDecodeData
 * Class that execute the decoding of the Addition data
 * Author: Leonardo Garcia, PayComplete
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using C2ValidatorATS.Utils;

namespace C2ValidatorATS.UIs
{
    class UiCtrlBuilderDecodeData
    {
        Panel pnlDecodeData;
        Label lblInfo, lblInfo1, lblUpper, lblLower, lblUpperNr, lblLowerNr;
        TextBox tbxUpperLimitsDecodingArea, tbxLowerLimitsDecodingArea;
        Button btnExit;
        Button btnDecode;
        SecurityManager CSecMngr;
        List<ushort> decodedData;
        //#####Exeprimenting algorithm
       // private static ushort[] sKey;
        //private static ushort[] sInitKey;
        //#########

        public void ShowFlashInProgressPanel(C2ATS CC2Mainform)
        {
            pnlDecodeData = new Panel();
            pnlDecodeData.Location = new Point(350, 200);
            pnlDecodeData.Size = new Size(550, 250);
            pnlDecodeData.BackColor = Color.LightGray;
            pnlDecodeData.BorderStyle = BorderStyle.Fixed3D;
            pnlDecodeData.Parent = CC2Mainform;
            pnlDecodeData.BringToFront();

            lblInfo = new Label();
            lblInfo.Text = "Copy and Paste Offset data(Addition) you would like to Decode";
            lblInfo.Font = new Font(lblInfo.Font, FontStyle.Bold);
            lblInfo.ForeColor = Color.Red;
            lblInfo.AutoSize = true;
            lblInfo.Location = new Point(30, 10);
            lblInfo.Parent = pnlDecodeData;

            lblInfo1 = new Label();
            lblInfo1.Text = "Data has to be consistent(pair), i.e. Upper and Lower and same position";
            lblInfo1.Font = new Font(lblInfo1.Font, FontStyle.Bold);
            lblInfo1.ForeColor = Color.Red;
            lblInfo1.AutoSize = true;
            lblInfo1.Location = new Point(30, 40);
            lblInfo1.Parent = pnlDecodeData;

            lblUpper = new Label();
            lblUpper.Text = "Upper Offsets";
            lblUpper.AutoSize = true;
            lblUpper.Location = new Point(1, 90);
            lblUpper.Parent = pnlDecodeData;

            lblLower = new Label();
            lblLower.Text = "Lower Offsets";
            lblLower.AutoSize = true;
            lblLower.Location = new Point(1, 150);
            lblLower.Parent = pnlDecodeData;

            lblUpperNr = new Label();
            lblUpperNr.Text = "";
            lblUpperNr.AutoSize = true;
            lblUpperNr.Location = new Point(87, 75);
            lblUpperNr.Parent = pnlDecodeData;

            lblLowerNr = new Label();
            lblLowerNr.Text = "";
            lblLowerNr.AutoSize = true;
            lblLowerNr.Location = new Point(87, 135);
            lblLowerNr.Parent = pnlDecodeData;

            //The box where to past in limits information
            tbxUpperLimitsDecodingArea = new TextBox();
            tbxUpperLimitsDecodingArea.Size = new Size(450, 45);
            tbxUpperLimitsDecodingArea.Location = new Point(90, 90);
            tbxUpperLimitsDecodingArea.AcceptsReturn = true;
            tbxUpperLimitsDecodingArea.AcceptsTab = true;
            tbxUpperLimitsDecodingArea.Multiline = true;
            tbxUpperLimitsDecodingArea.ScrollBars = ScrollBars.Horizontal;
            tbxUpperLimitsDecodingArea.WordWrap = false;
            tbxUpperLimitsDecodingArea.AllowDrop = true;
            tbxUpperLimitsDecodingArea.Parent = pnlDecodeData;

            tbxLowerLimitsDecodingArea = new TextBox();
            tbxLowerLimitsDecodingArea.Size = new Size(450, 45);
            tbxLowerLimitsDecodingArea.Location = new Point(90, 150);
            tbxLowerLimitsDecodingArea.AcceptsReturn = true;
            tbxLowerLimitsDecodingArea.AcceptsTab = true;
            tbxLowerLimitsDecodingArea.Multiline = true;
            tbxLowerLimitsDecodingArea.ScrollBars = ScrollBars.Horizontal;
            tbxLowerLimitsDecodingArea.WordWrap = false;
            tbxLowerLimitsDecodingArea.AllowDrop = true;
            tbxLowerLimitsDecodingArea.Parent = pnlDecodeData;

            //Exit button
            btnExit = new Button();
            btnExit.Size = new Size(50, 35);
            btnExit.Location = new Point(430, 5);
            btnExit.BackColor = Color.Coral;
            btnExit.Text = "Exit";
            btnExit.ForeColor = Color.Black;
            btnExit.Click += new EventHandler(this.btnExit_clicked);
            btnExit.Parent = pnlDecodeData;

            btnDecode = new Button();
            btnDecode.Size = new Size(60, 35);
            btnDecode.Location = new Point(250, 210);
            btnDecode.BackColor = Color.LightGreen;
            btnDecode.Text = "Decode";
            btnDecode.ForeColor = Color.Black;
            btnDecode.Click += new EventHandler(this.btnDecode_clicked);
            btnDecode.Parent = pnlDecodeData;

        }

        public void btnDecode_clicked(object sender, EventArgs e)
        {
            string[] resultString;
            ushort[] numbersToDecode;
            List<int> decodedData;
            List<string> resultDecoded = new List<string> { };
            char[] separator = new char[]{' ', '\t', ',', '\'', '\"', '\n', '\r'};
            if (tbxUpperLimitsDecodingArea.Text != "")// && tbxLowerLimitsDecodingArea.Text != "")
            {
                if ((CSecMngr = new SecurityManager(Encoding.ASCII.GetBytes("NRI D.Jankowski"))) == null)
                    return;
                //####### Experimenting
               // InitHash((byte[])(Encoding.ASCII.GetBytes("NRI D.Jankowski")));
                //######### END
                string allData = tbxUpperLimitsDecodingArea.Text + " " + tbxLowerLimitsDecodingArea.Text;
                //resultString = tbxUpperLimitsDecodingArea.Text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                resultString = allData.Split(separator, StringSplitOptions.RemoveEmptyEntries);

                numbersToDecode = new ushort[resultString.Count()];
                for (int i = 0; i < resultString.Count(); i++ )
                {
                    try
                    {
                        numbersToDecode[i] = Convert.ToUInt16(resultString[i]);
                    }
                    catch(Exception)
                    {
                        return;
                    }
                }
                if (numbersToDecode.Count() != 0)
                {
                  decodedData = DecodeData(numbersToDecode);
                  ShowDecodedData(decodedData);
                }                
            }
        }

        private List<int> DecodeData(ushort[] rawData)
        {
            byte[] dataSplitInTwoBytes = new byte[rawData.Count() * 2];
            List<int> dataDecoded = new List<int> { };

            //######EXPERIMENT PART
            int idx = 0;
            for (int i = 0; i < rawData.Count()/2; i++)
            {
                dataSplitInTwoBytes[(i * 4)] = (byte)(rawData[idx] & 0xFF);
                dataSplitInTwoBytes[(i * 4) + 1] = (byte)(rawData[idx] >> 8);
                idx++;
            }
            for (int i = 0; i < rawData.Count() / 2; i++)
            {              
                dataSplitInTwoBytes[(i * 4) + 2] = (byte)(rawData[idx] & 0xFF);
                dataSplitInTwoBytes[(i * 4) + 3] = (byte)(rawData[idx] >> 8);
                idx++;
            }

            CSecMngr.Decrypt(ref dataSplitInTwoBytes, 0);
            //Start();
            //Decrypt(dataSplitInTwoBytes, dataSplitInTwoBytes.Count());
            //############END
            for (int i = 0; i < rawData.Count() / 2; i++)
            {
                dataDecoded.Add((ushort)((dataSplitInTwoBytes[i * 4 + 1] << 8) + dataSplitInTwoBytes[i * 4]));
            }
            for (int i = 0; i < rawData.Count() / 2; i++)
            {
                dataDecoded.Add((ushort)((dataSplitInTwoBytes[(i * 4) + 3] << 8) + dataSplitInTwoBytes[(i * 4) + 2]));
            }
             return dataDecoded;
        }

        private void ShowDecodedData(List<int> decodedNumbers)
        {
            lblUpperNr.Text = "";
            lblLowerNr.Text = "";

            string upperDecodedData = "", lowerDecodedData = "";
            int i;
            for ( i = 0; i < decodedNumbers.Count(); i++)
            {
                if (i < decodedNumbers.Count() / 2)
                {
                    upperDecodedData += "Up" + (i + 1) + ": " + decodedNumbers[i] + "   ";
                   
                    //lblUpperNr.Text += i + 1 + GetSpaceToNextNr((decodedNumbers[i]).ToString().Length);
                }
                else
                {
                    lowerDecodedData += "Lo" + (i + 1 - decodedNumbers.Count() / 2) + ":  -" + decodedNumbers[i] + "   ";
                    //lblLowerNr.Text += (i + 1 - decodedNumbers.Count() / 2) + (((i - decodedNumbers.Count() / 2 )< 10) ? "   " : "  ");
                }
                
            }
            tbxUpperLimitsDecodingArea.Text = upperDecodedData;
            tbxLowerLimitsDecodingArea.Text = lowerDecodedData;
        }


        public void btnExit_clicked(object sender, EventArgs e)
        {
            pnlDecodeData.Dispose();
        }

        //######## Testing other alfgorithm

 

        //###### End experimenting #####
    }
}
