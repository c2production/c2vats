﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace C2ValidatorATS.UIs
{
    class UiCtrlBuilderFlashInProgress
    {
        Panel pnlFlashInProgress;
        Label lblInfo;
        ProgressBar pgrBarFlashProgress;
        byte actualPercentage = 0;

        public void ShowFlashInProgressPanel(C2ATS CC2Mainform)
        {
            pnlFlashInProgress = new Panel();
            pnlFlashInProgress.Location = new Point(450, 200);
            pnlFlashInProgress.Size = new Size(300, 100);
            pnlFlashInProgress.BackColor = Color.LightGray;
            pnlFlashInProgress.BorderStyle = BorderStyle.FixedSingle;
            pnlFlashInProgress.Parent = CC2Mainform;
            pnlFlashInProgress.BringToFront();

            lblInfo = new Label();
            lblInfo.Text = "FW Falshing in Progress";
            lblInfo.ForeColor = Color.Red;
            lblInfo.AutoSize = true;
            lblInfo.Location = new Point(75, 5);
            lblInfo.Parent = pnlFlashInProgress;

            pgrBarFlashProgress = new ProgressBar();
            pgrBarFlashProgress.Location = new Point(20, 45);
            pgrBarFlashProgress.Size = new Size(255, 20);
           
            pgrBarFlashProgress.Parent = pnlFlashInProgress;
        }

        public void FlashProgressChanged(byte progressInPercentage)
        {
            if (actualPercentage < progressInPercentage)
            {

                pgrBarFlashProgress.Step = progressInPercentage - actualPercentage;
                pgrBarFlashProgress.PerformStep();
                actualPercentage = progressInPercentage;
            }
            
        }

        public void ExitFlashProgressPnl()
        {
            actualPercentage = 0;
            pnlFlashInProgress.Dispose();
        }

        
    }
}
