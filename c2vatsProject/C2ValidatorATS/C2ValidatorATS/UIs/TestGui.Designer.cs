﻿namespace C2ValidatorATS
{
    partial class C2ATS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(C2ATS));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aTSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runAcceptanceTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runDebugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.confToolStripTestType = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getValidatorInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flashFWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decodeDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExecutionGroupBox = new System.Windows.Forms.GroupBox();
            this.pbx_running = new System.Windows.Forms.PictureBox();
            this.btn_stop = new System.Windows.Forms.Button();
            this.btn_run = new System.Windows.Forms.Button();
            this.test_verdict_gbx = new System.Windows.Forms.GroupBox();
            this.lbl_Session_Verdict = new System.Windows.Forms.Label();
            this.test_info_gbox = new System.Windows.Forms.GroupBox();
            this.lbl_info3 = new System.Windows.Forms.Label();
            this.lbl_info2 = new System.Windows.Forms.Label();
            this.lbl_info1 = new System.Windows.Forms.Label();
            this.lView_TC_List = new System.Windows.Forms.ListView();
            this.Hd_Tc_Select = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Hd_Tc_Name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Hd_Tc_Satus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lbl_user_indication = new System.Windows.Forms.Label();
            this.lbx_debug_info = new System.Windows.Forms.ListBox();
            this.pnl_quick_buttons = new System.Windows.Forms.Panel();
            this.cbx_retryTestOnFail = new System.Windows.Forms.CheckBox();
            this.cbx_retryOntestInc = new System.Windows.Forms.CheckBox();
            this.btn_save_log = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.btn_debug = new System.Windows.Forms.Button();
            this.btn_ats = new System.Windows.Forms.Button();
            this.cbx_select_all = new System.Windows.Forms.CheckBox();
            this.lbl_aux_user_info = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnl_UserInfo = new System.Windows.Forms.Panel();
            this.pbx_user_img = new System.Windows.Forms.PictureBox();
            this.cbx_rim_present = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.ExecutionGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_running)).BeginInit();
            this.test_verdict_gbx.SuspendLayout();
            this.test_info_gbox.SuspendLayout();
            this.pnl_quick_buttons.SuspendLayout();
            this.pnl_UserInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_user_img)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aTSToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStrip1.Size = new System.Drawing.Size(964, 24);
            this.menuStrip1.Stretch = false;
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aTSToolStripMenuItem
            // 
            this.aTSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runAcceptanceTestToolStripMenuItem,
            this.runDebugToolStripMenuItem,
            this.toolStripSeparator1,
            this.saveLogFileToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.aTSToolStripMenuItem.Name = "aTSToolStripMenuItem";
            this.aTSToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.aTSToolStripMenuItem.Text = "ATS";
            // 
            // runAcceptanceTestToolStripMenuItem
            // 
            this.runAcceptanceTestToolStripMenuItem.Checked = true;
            this.runAcceptanceTestToolStripMenuItem.CheckOnClick = true;
            this.runAcceptanceTestToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.runAcceptanceTestToolStripMenuItem.Name = "runAcceptanceTestToolStripMenuItem";
            this.runAcceptanceTestToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.runAcceptanceTestToolStripMenuItem.Text = "Run in Acceptance Test Mode";
            this.runAcceptanceTestToolStripMenuItem.CheckedChanged += new System.EventHandler(this.runAcceptanceTestToolStripMenuItem_CheckedChanged);
            this.runAcceptanceTestToolStripMenuItem.Click += new System.EventHandler(this.runAcceptanceTestToolStripMenuItem_Click);
            // 
            // runDebugToolStripMenuItem
            // 
            this.runDebugToolStripMenuItem.CheckOnClick = true;
            this.runDebugToolStripMenuItem.Name = "runDebugToolStripMenuItem";
            this.runDebugToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.runDebugToolStripMenuItem.Text = "Run in Debug Mode";
            this.runDebugToolStripMenuItem.CheckedChanged += new System.EventHandler(this.runAcceptanceTestToolStripMenuItem_CheckedChanged);
            this.runDebugToolStripMenuItem.Click += new System.EventHandler(this.runDebugToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(229, 6);
            // 
            // saveLogFileToolStripMenuItem
            // 
            this.saveLogFileToolStripMenuItem.CheckOnClick = true;
            this.saveLogFileToolStripMenuItem.Name = "saveLogFileToolStripMenuItem";
            this.saveLogFileToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.saveLogFileToolStripMenuItem.Text = "Save Log File";
            this.saveLogFileToolStripMenuItem.Click += new System.EventHandler(this.saveLogFileToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.confToolStripTestType});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.configurationToolStripMenuItem.Text = "Configuration";
            // 
            // confToolStripTestType
            // 
            this.confToolStripTestType.Checked = global::C2ValidatorATS.Properties.Settings.Default.DoorOnlyTest;
            this.confToolStripTestType.CheckOnClick = true;
            this.confToolStripTestType.Name = "confToolStripTestType";
            this.confToolStripTestType.Size = new System.Drawing.Size(179, 22);
            this.confToolStripTestType.Text = "HMI Door Only Test";
            this.confToolStripTestType.CheckedChanged += new System.EventHandler(this.confToolStripTestType_CheckedChanged);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.getValidatorInfoToolStripMenuItem,
            this.flashFWToolStripMenuItem,
            this.decodeDataToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            this.toolsToolStripMenuItem.Click += new System.EventHandler(this.toolsToolStripMenuItem_Click);
            // 
            // getValidatorInfoToolStripMenuItem
            // 
            this.getValidatorInfoToolStripMenuItem.Name = "getValidatorInfoToolStripMenuItem";
            this.getValidatorInfoToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.getValidatorInfoToolStripMenuItem.Text = "Get Validator Info";
            this.getValidatorInfoToolStripMenuItem.Click += new System.EventHandler(this.getValidatorInfoToolStripMenuItem_Click);
            // 
            // flashFWToolStripMenuItem
            // 
            this.flashFWToolStripMenuItem.Image = global::C2ValidatorATS.Properties.Resources.playsmallest;
            this.flashFWToolStripMenuItem.Name = "flashFWToolStripMenuItem";
            this.flashFWToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.flashFWToolStripMenuItem.Text = "Flash FW";
            this.flashFWToolStripMenuItem.Click += new System.EventHandler(this.flashFWToolStripMenuItem_Click);
            // 
            // decodeDataToolStripMenuItem
            // 
            this.decodeDataToolStripMenuItem.Name = "decodeDataToolStripMenuItem";
            this.decodeDataToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.decodeDataToolStripMenuItem.Text = "Decode Data";
            this.decodeDataToolStripMenuItem.Click += new System.EventHandler(this.decodeDataToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // ExecutionGroupBox
            // 
            this.ExecutionGroupBox.Controls.Add(this.pbx_running);
            this.ExecutionGroupBox.Controls.Add(this.btn_stop);
            this.ExecutionGroupBox.Controls.Add(this.btn_run);
            this.ExecutionGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExecutionGroupBox.Location = new System.Drawing.Point(21, 55);
            this.ExecutionGroupBox.Name = "ExecutionGroupBox";
            this.ExecutionGroupBox.Size = new System.Drawing.Size(237, 120);
            this.ExecutionGroupBox.TabIndex = 1;
            this.ExecutionGroupBox.TabStop = false;
            this.ExecutionGroupBox.Text = "Test Execution";
            // 
            // pbx_running
            // 
            this.pbx_running.InitialImage = null;
            this.pbx_running.Location = new System.Drawing.Point(233, 17);
            this.pbx_running.Name = "pbx_running";
            this.pbx_running.Size = new System.Drawing.Size(4, 97);
            this.pbx_running.TabIndex = 2;
            this.pbx_running.TabStop = false;
            this.pbx_running.Tag = "0";
            // 
            // btn_stop
            // 
            this.btn_stop.Enabled = false;
            this.btn_stop.Image = ((System.Drawing.Image)(resources.GetObject("btn_stop.Image")));
            this.btn_stop.Location = new System.Drawing.Point(135, 31);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(70, 70);
            this.btn_stop.TabIndex = 1;
            this.btn_stop.Text = "STOP";
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // btn_run
            // 
            this.btn_run.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_run.Enabled = false;
            this.btn_run.Image = ((System.Drawing.Image)(resources.GetObject("btn_run.Image")));
            this.btn_run.Location = new System.Drawing.Point(31, 31);
            this.btn_run.Name = "btn_run";
            this.btn_run.Size = new System.Drawing.Size(68, 70);
            this.btn_run.TabIndex = 0;
            this.btn_run.UseVisualStyleBackColor = true;
            this.btn_run.Click += new System.EventHandler(this.btn_run_Click);
            // 
            // test_verdict_gbx
            // 
            this.test_verdict_gbx.Controls.Add(this.lbl_Session_Verdict);
            this.test_verdict_gbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.test_verdict_gbx.Location = new System.Drawing.Point(630, 55);
            this.test_verdict_gbx.Name = "test_verdict_gbx";
            this.test_verdict_gbx.Size = new System.Drawing.Size(287, 120);
            this.test_verdict_gbx.TabIndex = 2;
            this.test_verdict_gbx.TabStop = false;
            this.test_verdict_gbx.Text = "Test Session Verdict";
            // 
            // lbl_Session_Verdict
            // 
            this.lbl_Session_Verdict.AutoSize = true;
            this.lbl_Session_Verdict.Font = new System.Drawing.Font("Segoe UI Semibold", 27F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Session_Verdict.Location = new System.Drawing.Point(20, 38);
            this.lbl_Session_Verdict.Name = "lbl_Session_Verdict";
            this.lbl_Session_Verdict.Size = new System.Drawing.Size(0, 48);
            this.lbl_Session_Verdict.TabIndex = 1;
            // 
            // test_info_gbox
            // 
            this.test_info_gbox.Controls.Add(this.lbl_info3);
            this.test_info_gbox.Controls.Add(this.lbl_info2);
            this.test_info_gbox.Controls.Add(this.lbl_info1);
            this.test_info_gbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.test_info_gbox.Location = new System.Drawing.Point(264, 55);
            this.test_info_gbox.Name = "test_info_gbox";
            this.test_info_gbox.Size = new System.Drawing.Size(347, 120);
            this.test_info_gbox.TabIndex = 3;
            this.test_info_gbox.TabStop = false;
            this.test_info_gbox.Text = "Test Session Information";
            // 
            // lbl_info3
            // 
            this.lbl_info3.AutoSize = true;
            this.lbl_info3.Location = new System.Drawing.Point(73, 88);
            this.lbl_info3.Name = "lbl_info3";
            this.lbl_info3.Size = new System.Drawing.Size(0, 18);
            this.lbl_info3.TabIndex = 2;
            // 
            // lbl_info2
            // 
            this.lbl_info2.AutoSize = true;
            this.lbl_info2.Location = new System.Drawing.Point(73, 62);
            this.lbl_info2.Name = "lbl_info2";
            this.lbl_info2.Size = new System.Drawing.Size(0, 18);
            this.lbl_info2.TabIndex = 1;
            // 
            // lbl_info1
            // 
            this.lbl_info1.AutoSize = true;
            this.lbl_info1.Location = new System.Drawing.Point(73, 36);
            this.lbl_info1.Name = "lbl_info1";
            this.lbl_info1.Size = new System.Drawing.Size(0, 18);
            this.lbl_info1.TabIndex = 0;
            // 
            // lView_TC_List
            // 
            this.lView_TC_List.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lView_TC_List.CheckBoxes = true;
            this.lView_TC_List.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Hd_Tc_Select,
            this.Hd_Tc_Name,
            this.Hd_Tc_Satus});
            this.lView_TC_List.FullRowSelect = true;
            this.lView_TC_List.GridLines = true;
            this.lView_TC_List.Location = new System.Drawing.Point(12, 213);
            this.lView_TC_List.Name = "lView_TC_List";
            this.lView_TC_List.Size = new System.Drawing.Size(324, 359);
            this.lView_TC_List.TabIndex = 11;
            this.lView_TC_List.UseCompatibleStateImageBehavior = false;
            this.lView_TC_List.View = System.Windows.Forms.View.Details;
            this.lView_TC_List.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lView_TC_List_ItemChecked);
            // 
            // Hd_Tc_Select
            // 
            this.Hd_Tc_Select.Text = "Select";
            this.Hd_Tc_Select.Width = 42;
            // 
            // Hd_Tc_Name
            // 
            this.Hd_Tc_Name.Text = "Test Case Name";
            this.Hd_Tc_Name.Width = 187;
            // 
            // Hd_Tc_Satus
            // 
            this.Hd_Tc_Satus.Text = "Status";
            this.Hd_Tc_Satus.Width = 91;
            // 
            // lbl_user_indication
            // 
            this.lbl_user_indication.AutoSize = true;
            this.lbl_user_indication.Location = new System.Drawing.Point(492, 195);
            this.lbl_user_indication.Name = "lbl_user_indication";
            this.lbl_user_indication.Size = new System.Drawing.Size(0, 15);
            this.lbl_user_indication.TabIndex = 16;
            // 
            // lbx_debug_info
            // 
            this.lbx_debug_info.FormattingEnabled = true;
            this.lbx_debug_info.Location = new System.Drawing.Point(12, 581);
            this.lbx_debug_info.Name = "lbx_debug_info";
            this.lbx_debug_info.Size = new System.Drawing.Size(940, 160);
            this.lbx_debug_info.TabIndex = 19;
            // 
            // pnl_quick_buttons
            // 
            this.pnl_quick_buttons.Controls.Add(this.cbx_retryTestOnFail);
            this.pnl_quick_buttons.Controls.Add(this.cbx_retryOntestInc);
            this.pnl_quick_buttons.Controls.Add(this.btn_save_log);
            this.pnl_quick_buttons.Controls.Add(this.btn_exit);
            this.pnl_quick_buttons.Controls.Add(this.btn_debug);
            this.pnl_quick_buttons.Controls.Add(this.btn_ats);
            this.pnl_quick_buttons.Location = new System.Drawing.Point(254, 0);
            this.pnl_quick_buttons.Name = "pnl_quick_buttons";
            this.pnl_quick_buttons.Size = new System.Drawing.Size(710, 49);
            this.pnl_quick_buttons.TabIndex = 20;
            // 
            // cbx_retryTestOnFail
            // 
            this.cbx_retryTestOnFail.AutoSize = true;
            this.cbx_retryTestOnFail.Enabled = false;
            this.cbx_retryTestOnFail.Location = new System.Drawing.Point(197, 5);
            this.cbx_retryTestOnFail.Name = "cbx_retryTestOnFail";
            this.cbx_retryTestOnFail.Size = new System.Drawing.Size(126, 19);
            this.cbx_retryTestOnFail.TabIndex = 19;
            this.cbx_retryTestOnFail.Text = "Retry Test On FAIL";
            this.cbx_retryTestOnFail.UseVisualStyleBackColor = true;
            // 
            // cbx_retryOntestInc
            // 
            this.cbx_retryOntestInc.AutoSize = true;
            this.cbx_retryOntestInc.Enabled = false;
            this.cbx_retryOntestInc.Location = new System.Drawing.Point(197, 27);
            this.cbx_retryOntestInc.Name = "cbx_retryOntestInc";
            this.cbx_retryOntestInc.Size = new System.Drawing.Size(190, 19);
            this.cbx_retryOntestInc.TabIndex = 18;
            this.cbx_retryOntestInc.Text = "Retry Test On INCONCLUSIVE";
            this.cbx_retryOntestInc.UseVisualStyleBackColor = true;
            // 
            // btn_save_log
            // 
            this.btn_save_log.Image = ((System.Drawing.Image)(resources.GetObject("btn_save_log.Image")));
            this.btn_save_log.Location = new System.Drawing.Point(124, 0);
            this.btn_save_log.Name = "btn_save_log";
            this.btn_save_log.Size = new System.Drawing.Size(51, 45);
            this.btn_save_log.TabIndex = 14;
            this.btn_save_log.UseVisualStyleBackColor = true;
            this.btn_save_log.Click += new System.EventHandler(this.btn_save_log_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Image = ((System.Drawing.Image)(resources.GetObject("btn_exit.Image")));
            this.btn_exit.Location = new System.Drawing.Point(651, 1);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(56, 48);
            this.btn_exit.TabIndex = 17;
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // btn_debug
            // 
            this.btn_debug.Enabled = false;
            this.btn_debug.Image = global::C2ValidatorATS.Properties.Resources.btnDbg;
            this.btn_debug.Location = new System.Drawing.Point(67, 0);
            this.btn_debug.Name = "btn_debug";
            this.btn_debug.Size = new System.Drawing.Size(51, 45);
            this.btn_debug.TabIndex = 13;
            this.btn_debug.UseVisualStyleBackColor = true;
            this.btn_debug.Click += new System.EventHandler(this.btn_debug_Click);
            // 
            // btn_ats
            // 
            this.btn_ats.Enabled = false;
            this.btn_ats.Image = global::C2ValidatorATS.Properties.Resources.btnAts;
            this.btn_ats.Location = new System.Drawing.Point(10, 0);
            this.btn_ats.Name = "btn_ats";
            this.btn_ats.Size = new System.Drawing.Size(51, 45);
            this.btn_ats.TabIndex = 12;
            this.btn_ats.UseVisualStyleBackColor = true;
            this.btn_ats.Click += new System.EventHandler(this.btn_ats_Click);
            // 
            // cbx_select_all
            // 
            this.cbx_select_all.AutoSize = true;
            this.cbx_select_all.Location = new System.Drawing.Point(27, 193);
            this.cbx_select_all.Name = "cbx_select_all";
            this.cbx_select_all.Size = new System.Drawing.Size(76, 19);
            this.cbx_select_all.TabIndex = 21;
            this.cbx_select_all.Text = "Select All";
            this.cbx_select_all.UseVisualStyleBackColor = true;
            this.cbx_select_all.Visible = false;
            this.cbx_select_all.CheckedChanged += new System.EventHandler(this.cbx_select_all_CheckedChanged);
            // 
            // lbl_aux_user_info
            // 
            this.lbl_aux_user_info.AutoSize = true;
            this.lbl_aux_user_info.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lbl_aux_user_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_aux_user_info.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lbl_aux_user_info.Location = new System.Drawing.Point(112, 32);
            this.lbl_aux_user_info.Name = "lbl_aux_user_info";
            this.lbl_aux_user_info.Size = new System.Drawing.Size(0, 37);
            this.lbl_aux_user_info.TabIndex = 22;
            // 
            // pnl_UserInfo
            // 
            this.pnl_UserInfo.Controls.Add(this.pbx_user_img);
            this.pnl_UserInfo.Controls.Add(this.lbl_aux_user_info);
            this.pnl_UserInfo.Location = new System.Drawing.Point(340, 229);
            this.pnl_UserInfo.Name = "pnl_UserInfo";
            this.pnl_UserInfo.Size = new System.Drawing.Size(621, 346);
            this.pnl_UserInfo.TabIndex = 23;
            // 
            // pbx_user_img
            // 
            this.pbx_user_img.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbx_user_img.InitialImage = null;
            this.pbx_user_img.Location = new System.Drawing.Point(38, 3);
            this.pbx_user_img.Name = "pbx_user_img";
            this.pbx_user_img.Size = new System.Drawing.Size(534, 336);
            this.pbx_user_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbx_user_img.TabIndex = 18;
            this.pbx_user_img.TabStop = false;
            this.pbx_user_img.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbx_user_img_MouseClick);
            // 
            // cbx_rim_present
            // 
            this.cbx_rim_present.AutoSize = true;
            this.cbx_rim_present.Location = new System.Drawing.Point(52, 191);
            this.cbx_rim_present.Name = "cbx_rim_present";
            this.cbx_rim_present.Size = new System.Drawing.Size(136, 19);
            this.cbx_rim_present.TabIndex = 24;
            this.cbx_rim_present.Text = "RIM Sensor Present";
            this.cbx_rim_present.UseVisualStyleBackColor = true;
            this.cbx_rim_present.CheckedChanged += new System.EventHandler(this.cbx_rim_present_CheckedChanged);
            // 
            // C2ATS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 744);
            this.Controls.Add(this.cbx_rim_present);
            this.Controls.Add(this.pnl_UserInfo);
            this.Controls.Add(this.cbx_select_all);
            this.Controls.Add(this.lbx_debug_info);
            this.Controls.Add(this.lbl_user_indication);
            this.Controls.Add(this.lView_TC_List);
            this.Controls.Add(this.test_info_gbox);
            this.Controls.Add(this.test_verdict_gbx);
            this.Controls.Add(this.ExecutionGroupBox);
            this.Controls.Add(this.pnl_quick_buttons);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "C2ATS";
            this.Text = "C2 Validator Acceptance Test System. Ver.";
            this.Load += new System.EventHandler(this.C2ATS_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ExecutionGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_running)).EndInit();
            this.test_verdict_gbx.ResumeLayout(false);
            this.test_verdict_gbx.PerformLayout();
            this.test_info_gbox.ResumeLayout(false);
            this.test_info_gbox.PerformLayout();
            this.pnl_quick_buttons.ResumeLayout(false);
            this.pnl_quick_buttons.PerformLayout();
            this.pnl_UserInfo.ResumeLayout(false);
            this.pnl_UserInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_user_img)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aTSToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem runAcceptanceTestToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem saveLogFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.GroupBox ExecutionGroupBox;
        public  System.Windows.Forms.Button btn_stop;
        public System.Windows.Forms.Button btn_run;
        private System.Windows.Forms.GroupBox test_verdict_gbx;
        private System.Windows.Forms.GroupBox test_info_gbox;
        public System.Windows.Forms.ListView lView_TC_List;
        private System.Windows.Forms.ColumnHeader Hd_Tc_Select;
        public System.Windows.Forms.ColumnHeader Hd_Tc_Name;
        private System.Windows.Forms.ToolStripMenuItem confToolStripTestType;
        public System.Windows.Forms.ToolStripMenuItem runDebugToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public System.Windows.Forms.Button btn_ats;
        public System.Windows.Forms.Button btn_debug;
        public System.Windows.Forms.Button btn_save_log;
        public System.Windows.Forms.Label lbl_info1;
        public System.Windows.Forms.Label lbl_info3;
        public System.Windows.Forms.Label lbl_info2;
        public System.Windows.Forms.Label lbl_user_indication;
        public System.Windows.Forms.ColumnHeader Hd_Tc_Satus;
        public System.Windows.Forms.Label lbl_Session_Verdict;
        public System.Windows.Forms.PictureBox pbx_running;
        private System.Windows.Forms.Button btn_exit;
        public System.Windows.Forms.PictureBox pbx_user_img;
        public System.Windows.Forms.ListBox lbx_debug_info;
        private System.Windows.Forms.Panel pnl_quick_buttons;
        private System.Windows.Forms.CheckBox cbx_retryTestOnFail;
        private System.Windows.Forms.CheckBox cbx_retryOntestInc;
        public System.Windows.Forms.CheckBox cbx_select_all;
        public System.Windows.Forms.Label lbl_aux_user_info;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Panel pnl_UserInfo;
        public System.Windows.Forms.CheckBox cbx_rim_present;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flashFWToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getValidatorInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decodeDataToolStripMenuItem;
    }
}

