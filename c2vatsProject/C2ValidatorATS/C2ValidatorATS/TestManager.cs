﻿/**
 * Class: TestManager
 * Class control the interation between the test GUI and the Validator under test. 
 * It receive orders from the GUI and fetch information from the VUT or the Tests.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using C2ValidatorATS.Utils;
using C2ValidatorATS.TestCases;
using C2ValidatorATS.Interfaces;
using C2ValidatorATS.Vut;
using C2ValidatorATS.Vut.VutDBlocksInfo;
using C2ValidatorATS.RDProcedures;

namespace C2ValidatorATS
{
    class TestManager
    {
        const int TIMER_INTERVAL_TEST_RUNNING = 300;
        const int USER_INFO_INDICATION_INTERVAL = 4 * TIMER_INTERVAL_TEST_RUNNING;
        const int USER_ANIMATION_INTERVAL = 3 * TIMER_INTERVAL_TEST_RUNNING;
        //Test Mode
        const int ACCEPTANCE_TEST_MODE =0;
        const int DEBUG_TEST_MODE = 1;


        public string FROM_HARD_CODED = "Hard Coded";
        public string FROM_DB = "DB";
        public string FROM_VALIDATOR = "Validator";
       

 
        private enum TestSessionVerdict
        {
            INACTIVE,
            IN_PROGRESS,
            INCONCLUSIVE,
            FAIL,
            PASS,
            IRRELEVANT
        }
        private enum TestManagerProcessState
        {
            REDY_FOR_TESTING = 0,
            VUT_DISCONNECTED,
            VUT_CONNECTED,
            RUNNING_TC,
            EXECUTION_STOPPED,
            FW_FALSHING
        }
       
        private int delayExecutionTimer;
        Thread tcThread;
        private List<bool> testCaseSelectionStatus = new List<bool>();
        ITestCase CCurrentTestCase;
        private List<string> testCaseList = new List<string>();
        private List<Image> animationImages = new List<Image>();
        public List<string> debugTarget = new List<string>();
        private List<bool> LatestDbgCasesUsed = new List<bool>();
        private ValidatorUnderTest CVut;
        private TestExecutionSelector CTestXSelector;
        private FirmWareFlash CFirmWareFlash;
        private string responseExecutionStatusMsg;
        private int currentTCIndex;
        private string userActionIndicationMsg = "";
        private Color userActionIndicationMsgColor = Color.DarkBlue;
        private TestSessionVerdict tSessionVerdict;
        private TestManagerProcessState tMgrProcessState;
        private bool doorOnlyTest { get; set; }
        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private bool ShowUsersAnimationInfo { get; set; }
        private bool ShowUserIndicationInfo { get; set; }
        private bool IsCommObjectRepresentationCreated { get; set; }
        private int[,] UserImageClickCoordinates;
        public bool TestExecSuspended { get; set; }
        public string NewCoinSrc { get; set; }
        public bool NewCoinSelected { get; set; }
        public bool WaitingForNewConnectedValidator { get; set; } //Variable used during the coin drop analysis debug test
        public string CoinSelected { get; set; }
        public string CurrentISO { get; set; }
        public bool ForceTestWithRIMSensor { get; set; }
        //public bool VutNoresponding { get; set; }
        private Leogger Log;
        private int testMode;
        public C2ATS CTestGUI { get; set; }
        public GuiHandler CGuiHandler{get; set;}
        public bool ActiveThread { get; set; }
        public bool SaveLogs { get; set; }
        private string DefaultLogPath;
        public bool FlashingInProgress{get; set;}
        public string FwPath { get; set; }
        public string toolPass;
        public bool doMaterialSensorNoPulseTest { get; set; }
        public bool doMaterialSensorWithPulseTest { get; set; }
        public bool runContinuously { get; set; }
        public bool stopOnFailure { get; set; }
        
        #region Singletone implementation
        //thread-safe Singletone class implementation
        static readonly TestManager _instance = new TestManager();
        public static TestManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public TestManager( )
        {
            InitAll();
            InitTimer();
        }
        #endregion

        private void InitAll()
        {
            CVut = new ValidatorUnderTest();
            CGuiHandler = GuiHandler.Instance;
            testCaseList = TestProvider.getAllTCs();
            for (int i = 0; i < TestProvider.getAllDebuggCases().Count(); i++)
                LatestDbgCasesUsed.Add(false);
            CTestXSelector = new TestExecutionSelector();
            Logger.SetLogLevel( Logger.LLevel.INFO);
            SetAllTestCasesAsSelected();
            tSessionVerdict = TestSessionVerdict.INACTIVE;
            ShowUserIndicationInfo = false;
            ShowUsersAnimationInfo = false;
            WaitingForNewConnectedValidator = false;
            ForceTestWithRIMSensor = false;
            FlashingInProgress = false;
            delayExecutionTimer = 0;
            UserImageClickCoordinates = new int[,] { { 0, 0 } };
            Log = Logger.instance;
            testMode = ACCEPTANCE_TEST_MODE; //Default Mode
            SaveLogs = false;
            if (Properties.Settings.Default.LogPath != "")
                DefaultLogPath = Properties.Settings.Default.LogPath;
            else //DefaultLogPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\VatsLogs\\";
                DefaultLogPath = "c:\\PayComplete\\C2VATS\\Logs\\";
            Directory.CreateDirectory(DefaultLogPath);//Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + ;
            doorOnlyTest = Properties.Settings.Default.DoorOnlyTest;
            tMgrProcessState = TestManagerProcessState.VUT_DISCONNECTED;
            IsCommObjectRepresentationCreated = CommAssistant.Instance.CreateCommunicationObjectRepresentation();
            toolPass = "";
        }

        private void InitTimer()
        {
            timer.Tick += new EventHandler(TimerEventHandler); 
            timer.Interval = 1000;
            timer.Enabled = true;                       
            timer.Start();  
        }
        private void stopTimer()
        {
            timer.Stop();
        }
        private void changeTimer(int interval)
        {
            timer.Interval = interval;
        }

        private void TimerEventHandler(Object sender, EventArgs e)
        {
            if (!CheckPresenceOfConnectedVut())
            {
                tMgrProcessState = TestManagerProcessState.VUT_DISCONNECTED;
            }
            switch(tMgrProcessState)
            {
                case TestManagerProcessState.VUT_DISCONNECTED:
                    DeviceDisconnected();                   
                   break;
                case TestManagerProcessState.VUT_CONNECTED:
                    DeviceConnected();
                    break;
                case TestManagerProcessState.RUNNING_TC:
                    UpdateInformation();                
                    break;
                case TestManagerProcessState.EXECUTION_STOPPED:
                    TestExecutionStopped();
                    break;
                case TestManagerProcessState.REDY_FOR_TESTING:
                    SetSystemRedyForTest();
                    break;
                case TestManagerProcessState.FW_FALSHING:
                    FWFalshingInProgress();
                    break;
                default: break;
            }
            if ((Logger.DisplayText.GetTextToDisplay().Count() != 0))
            {
                CGuiHandler.ShowDebugInformation(CTestGUI, Logger.DisplayText.GetTextToDisplay());
                Logger.DisplayText.GetTextToDisplay().Clear();
            }
        }

        //Set the Selection status to Selected! This list will change when a test has been selected or unselected
        private void SetAllTestCasesAsSelected()
        {
            testCaseSelectionStatus = null;
            testCaseSelectionStatus = new List<bool>();
            for (int i = 0; i < testCaseList.Count; i++)
            {
                testCaseSelectionStatus.Add(true);
            }
        }

        private void SetAllTestCasesAsUnSelected(bool unselectAll) //Except the debug cas that has been used latest
        {
            testCaseSelectionStatus = null;
            testCaseSelectionStatus = new List<bool>();
            for (int i = 0; i < testCaseList.Count; i++)
            {
                if (unselectAll || LatestDbgCasesUsed[i] != true) //if unselectAll = true unselect all 
                  testCaseSelectionStatus.Add(false);
                else testCaseSelectionStatus.Add(true);
            }
        }
        //If the HDMI is not present the TCs related to HMI will be suppressed
        private void RefreshTestCaseList()
        {
            if (doorOnlyTest && testMode == ACCEPTANCE_TEST_MODE)
            {
                testCaseList = TestProvider.GetDoorOnlyTest();
                if (CVut.IsHMIKeysOnlyPresent)  //No dipslay only keyboard
                {
                    testCaseList.RemoveAt(testCaseList.Count() - 1);
                }else if (!CVut.IsHMIDisplayPresent) //no diplsay 
                {
                    testCaseList.RemoveAt(testCaseList.Count() - 1);
                    testCaseList.RemoveAt(testCaseList.Count() - 1);
                }
                SetAllTestCasesAsSelected();

            }else if (testMode == ACCEPTANCE_TEST_MODE)
            {
                if (CVut.IsHMIDisplayPresent)
                {
                    testCaseList = TestProvider.getAllTCs();
                }
                else if(CVut.IsHMIKeysOnlyPresent)
                {
                    testCaseList = TestProvider.getHMIKeyOnlyTCs();
                }
                else if (!CVut.IsSorterDoorPresent)
                {
                    testCaseList = TestProvider.getSorterDoorNoMountedTCs();
                }
                else
                {
                    testCaseList = TestProvider.getNoHmiTCs();
                }
                if (!ForceTestWithRIMSensor) //take rim sensor test out of the list 
                    testCaseList.RemoveAt(testCaseList.Count() - 1);
                SetAllTestCasesAsSelected();
            }
            else
            {
                testCaseList = TestProvider.getAllDebuggCases();
            }
            
        }

        private void DeviceDisconnected()
        {   
            Log.Info(this.GetType().Name, Msgs.Info.NO_DEVICE_CONNECTED);
            if (ActiveThread)
            {
                ActiveThread = false;
                tcThread.Abort();
                //Sleep.thread(500);  probar a ver si no cracsha TODO
                CGuiHandler.ShowTestCaseList(CTestGUI, GetTestCaseList(), testCaseSelectionStatus);

            }
            changeTimer(USER_INFO_INDICATION_INTERVAL);
            tMgrProcessState = TestManagerProcessState.VUT_DISCONNECTED;
            CVut.NullifyValues();
            //CVut = null;  //Para probar el problema de que no se refresca si tiene dispay
            //CommAssistant;
            //InitAll();  //Lo ismo de arriba No resuelve esto :(
            CGuiHandler.EnableDisableRunBtn(CTestGUI, false);
            CGuiHandler.EnableDisableStopBtn(CTestGUI, false);
            CGuiHandler.EnableDisableAtsBtn(CTestGUI, false);
            CGuiHandler.EnableDisableDbgBtn(CTestGUI, false);
            //CGuiHandler.ShowDeviceNoConnected(CTestGUI);//Test
            CGuiHandler.ShowMsgInUserIndicatorLabel(CTestGUI, Msgs.Uii.CONNECT_A_NEW_VALIDATOR, Color.Red);
            CGuiHandler.CleanInfoInAuxUserIndicationLbl(CTestGUI);
            //CGuiHandler.CleanMsgInUserIndicatorLabel(CTestGUI);
            CGuiHandler.CleanPictureInImageInfoArea(CTestGUI);
            CGuiHandler.CleanInfoInSessionVerdictSatatus(CTestGUI);
            CGuiHandler.DestroyMeasuresTable(CTestGUI);
            CGuiHandler.DestroyMaterialSensorAnalysis(CTestGUI);
             if (!IsCommObjectRepresentationCreated)
            {
                //CGuiHandler.ShowMsgInUserIndicatorLabel(CTestGUI, Msgs.Uii.CONNECT_A_NEW_VALIDATOR, Color.Red);
                IsCommObjectRepresentationCreated = CommAssistant.Instance.CreateCommunicationObjectRepresentation();
            }
            //else
            //{
            //    Log.Info(this.GetType().Name, Msgs.Info.DEVICE_UNDER_TEST_CONNECTED);
            //    tMgrProcessState = TestManagerProcessState.VUT_CONNECTED;
            //}
        }

        private void DeviceConnected()
        {
            List<string> lablesInfo = new List<string>();
            if (tMgrProcessState != TestManagerProcessState.RUNNING_TC && tMgrProcessState != TestManagerProcessState.REDY_FOR_TESTING)
            {
                Log.Info(this.GetType().Name, Msgs.Info.DEVICE_UNDER_TEST_CONNECTED + " 1");
                CGuiHandler.CleanMsgInUserIndicatorLabel(CTestGUI);
                CVut.InitializeValues();
                lablesInfo.Add(CTestGUI.AtsMode ? Msgs.Uii.ATS_MODE_TESTING : Msgs.Uii.DEBUG_MODE_TESTING);
                lablesInfo.Add(CVut.IsHMIDisplayPresent ? Msgs.Uii.HMI_DISPLAY_PRESENT : (CVut.IsHMIKeysOnlyPresent ? Msgs.Uii.HMI_ONLY_KEYS_PRESENT : Msgs.Uii.NO_HMI_DISPLAY_PRESENT));
                //lablesInfo.Add(CVut.IsRIMPresent ? Msgs.Uii.RIM_SENSOR_PRESENT : "");
                //Display characteristic of the test Session
                CGuiHandler.ShowMsgInLabelsInTestSessionInfoArea(CTestGUI, lablesInfo);
                CGuiHandler.ShowTestCaseList(CTestGUI, GetTestCaseList(), testCaseSelectionStatus);
                CGuiHandler.EnableDisableRunBtn(CTestGUI, true);
                CGuiHandler.EnableDisableStopBtn(CTestGUI, false);
                if (testMode == ACCEPTANCE_TEST_MODE)
                {
                    AtsModeBtnClicked();
                }
                else DbgModeBtnClicked();
                tMgrProcessState = TestManagerProcessState.REDY_FOR_TESTING;
                Log.Info(this.GetType().Name, Msgs.Info.REDY_TO_START_TEST_SESSION);
                
            }
        }

        private void TestExecutionStopped()
        {
            Log.Info(this.GetType().Name, Msgs.Info.EXECUTION_STOPPED_BY_USER);
            CGuiHandler.EnableDisableRunBtn(CTestGUI, true);
            CGuiHandler.EnableDisableStopBtn(CTestGUI, false);
            CGuiHandler.EnableDisableMenu(CTestGUI, true);
            CGuiHandler.EnableDisableSaveLogBtn(CTestGUI, true);
            if (testMode == ACCEPTANCE_TEST_MODE)
            {
                CGuiHandler.EnableDisableAtsBtn(CTestGUI, false);
                CGuiHandler.EnableDisableDbgBtn(CTestGUI, true);
            }
            else
            {
                CGuiHandler.EnableDisableAtsBtn(CTestGUI, true);
                CGuiHandler.EnableDisableDbgBtn(CTestGUI, false);
            }
            StopShowingInfoInUserIndicationLabel();
            StopUserAnimationInfo();
            CGuiHandler.DestroyMeasuresTable(CTestGUI);
            CGuiHandler.DestroyMaterialSensorAnalysis(CTestGUI);
            tMgrProcessState = TestManagerProcessState.REDY_FOR_TESTING;

        }

        private void UpdateInformation()
        {
            MoveRunningBar();
            if (ShowUserIndicationInfo && (delayExecutionTimer % USER_INFO_INDICATION_INTERVAL == 0))
            {
                CGuiHandler.ShowMsgInUserIndicatorLabel(CTestGUI, userActionIndicationMsg, userActionIndicationMsgColor);
            }

            if (ShowUsersAnimationInfo && (delayExecutionTimer % USER_ANIMATION_INTERVAL == 0))
            {
                CGuiHandler.ShowAnimationInUserImageInfoArea(CTestGUI, animationImages);
            } 
             delayExecutionTimer += TIMER_INTERVAL_TEST_RUNNING;
        }

        private void FWFalshingInProgress()
        {
            byte progressInPercentage = 0;
            if (FlashingInProgress && FwPath != "")
            {
                if (CFirmWareFlash == null)
                {
                    CGuiHandler.ShowFWFlashProgressUI(CTestGUI);
                    CFirmWareFlash = new FirmWareFlash(FwPath);
                    
                    if (CFirmWareFlash.FlashFirmWare())
                    {
                        Log.Info(this.GetType().Name, "Flashing in Progress");
                    }
                    else
                    {
                        Log.Info(this.GetType().Name, "Error during flashing");
                        FlashingInProgress = false;
                        CFirmWareFlash = null;
                        tMgrProcessState = TestManagerProcessState.REDY_FOR_TESTING;
                        return;
                    }
                }
                if (CFirmWareFlash.CheckFirmwareProgress(1, ref progressInPercentage))
                {
                    CGuiHandler.ProgressBarChange(progressInPercentage);
                    Log.Info(this.GetType().Name, "Progress in percentage = " + progressInPercentage + "%");
                }
                else
                {
                    if(CFirmWareFlash.EndFlashingProcess(1))
                    {
                        Log.Info(this.GetType().Name, "Validator FW Flash Completed");
                        FlashingInProgress = false;
                        CFirmWareFlash = null;
                        System.GC.Collect();
                        System.GC.WaitForPendingFinalizers();
                        CGuiHandler.CloseFWFlashingProgressBar();
                        tMgrProcessState = TestManagerProcessState.REDY_FOR_TESTING;
                        return;
                    }
                    else
                    {
                        Log.Info(this.GetType().Name, "Error Ending the flash Process");
                        FlashingInProgress = false;
                        CFirmWareFlash = null;
                        tMgrProcessState = TestManagerProcessState.REDY_FOR_TESTING;
                    }
                }

            }
        }
        private void MoveRunningBar()
        {
            CGuiHandler.RunningBarMove(CTestGUI);
        }
       /// <summary>
       /// Check if VUt is connected and show information in the "TestSession Inormation Area" accordenly
       /// </summary>
        private bool CheckPresenceOfConnectedVut()
        {
            bool isValidatorConnected = CommAssistant.Instance.IsValidatorConnected(CVut.ValidatorId, ref responseExecutionStatusMsg);
            if (isValidatorConnected)
            {
                if (tMgrProcessState == TestManagerProcessState.VUT_DISCONNECTED) 
                    tMgrProcessState = tMgrProcessState = TestManagerProcessState.VUT_CONNECTED;
                WaitingForNewConnectedValidator = false; //Used for tests that needs to connect more than one vut during same test run
                return true;
            }
            else if (!isValidatorConnected && WaitingForNewConnectedValidator)
                return true; //Validator disconnected and the system is waiting for the user to connected a new validator. This is only valid during Drop Coin Debug test
            else
            {
                IsCommObjectRepresentationCreated = !CommAssistant.Instance.DestroyCommunicationObjectRepresentation();
                return false;

            }
   
        }

        private string GetTcNoPassVerdict()
        {
            if ((responseExecutionStatusMsg == Msgs.Error.NO_ABLE_TO_PING_THE_VALIDATOR) || (responseExecutionStatusMsg == Msgs.Error.NO_COMPORT_USB_TO_SERIAL_FOUND)
                || (responseExecutionStatusMsg == Msgs.Error.FAIL_TO_CREATE_OBJECT_REPRESENTATION_FOR_THIS_VUT) ||  (responseExecutionStatusMsg == Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR))
            {
                if (tSessionVerdict != TestSessionVerdict.FAIL) tSessionVerdict = TestSessionVerdict.INCONCLUSIVE;
                return Msgs.Verdict.TC_NONE;
            }
            if((responseExecutionStatusMsg == Msgs.Error.FAIL_TO_RECEIVE_CORRECT_RESPONSE_AFTER_SENDING_GET_VALUE) || (responseExecutionStatusMsg == Msgs.Error.TIME_OUT_OCURRED)
                || (responseExecutionStatusMsg == Msgs.Error.DOOR_INITIAL_STATE_CLOSED_FAIL) || (responseExecutionStatusMsg == Msgs.Error.FAIL_TO_SEND_CMD_EXECUTE_TO_VALIDATOR)
                || (responseExecutionStatusMsg == Msgs.Error.FAIL_TO_SEND_CMD_GET_VALUE_TO_VALIDATOR))
            {
                if (tSessionVerdict != TestSessionVerdict.FAIL) tSessionVerdict = TestSessionVerdict.INCONCLUSIVE;
                return Msgs.Verdict.TC_INCONCLUSIVE;
            }
            if (responseExecutionStatusMsg == Msgs.Info.EXECUTION_STOPPED_BY_USER)
            {
                if (tSessionVerdict != TestSessionVerdict.FAIL) tSessionVerdict = TestSessionVerdict.INCONCLUSIVE;
                return Msgs.Verdict.TC_EXECUTION_STOPPED;
            }
            tSessionVerdict = TestSessionVerdict.FAIL;
            return Msgs.Verdict.TC_FAIL;
        }

        private void ResetAllBeforeTestCaseStart()
        {
            responseExecutionStatusMsg = "";
            userActionIndicationMsg = "";
            StopShowingInfoInUserIndicationLabel();
            StopUserAnimationInfo();
        }

        private void SetSystemRedyForTest()
        {
            bool testcaseSelected = false;
            ResetAllBeforeTestCaseStart();
            CGuiHandler.RunningBarStop(CTestGUI);
           
            for (int i = 0; i < testCaseSelectionStatus.Count(); i++)
            {
                if(testCaseSelectionStatus[i] == true)
                {
                    testcaseSelected = true;
                    break;
                }
            }
            CGuiHandler.EnableDisableRunBtn(CTestGUI, testcaseSelected);
            CGuiHandler.EnableDisableStopBtn(CTestGUI, false);
            CGuiHandler.EnableDisableMenu(CTestGUI, true);
            CGuiHandler.EnableDisableSaveLogBtn(CTestGUI, true);

            if (testMode == ACCEPTANCE_TEST_MODE)
            {
                CGuiHandler.EnableDisableAtsBtn(CTestGUI, false);
                CGuiHandler.EnableDisableDbgBtn(CTestGUI, true);
            }
            else
            {
                CGuiHandler.EnableDisableAtsBtn(CTestGUI, true);
                CGuiHandler.EnableDisableDbgBtn(CTestGUI, false);
            }
            if (FlashingInProgress)
            {
                tMgrProcessState = TestManagerProcessState.FW_FALSHING;
            }
        }

        private string GetTSessionVerdict(TestSessionVerdict verdict)
        {
            switch (verdict)
            {
                case TestSessionVerdict.PASS:           return "PASS";
                case TestSessionVerdict.INCONCLUSIVE:   return "INCONCLUSIVE";
                case TestSessionVerdict.FAIL:           return "FAIL";
                case TestSessionVerdict.IN_PROGRESS: return "IN PROGRESS";
                case TestSessionVerdict.IRRELEVANT: return "IRRELEVANT";
                default: return "";
            }
        }
        public void ShowInfoInUserIndicatorLabel(string info, Color color)
        {
            ShowUserIndicationInfo = true;
            userActionIndicationMsg = info;
            userActionIndicationMsgColor = color;
        }

        public void StopShowingInfoInUserIndicationLabel()
        {
            ShowUserIndicationInfo = false;
            userActionIndicationMsg = "";
            CGuiHandler.CleanMsgInUserIndicatorLabel(CTestGUI);
        }

        public void ShowUserAnimationInfo(List<Image> Pictures)
        {
            ShowUsersAnimationInfo = true;
            animationImages = Pictures;
        }

        public void StopUserAnimationInfo()
        {
            ShowUsersAnimationInfo = false;
            animationImages.Clear();
            CGuiHandler.CleanAnimationInUserImageInfoArea(CTestGUI);
        }

        public void ShowInfoInAuxUserIndicationLabel(string auxInfo)
        {
            CGuiHandler.ShowInfoInAuxUserIndicationLbl(CTestGUI, auxInfo);
        }

        public void CleanInfoInAuxUserIndicationLbl()
        {
            CGuiHandler.CleanInfoInAuxUserIndicationLbl(CTestGUI);
        }

        public void UserImageClicked(int x, int y)
        {
            UserImageClickCoordinates[0, 0] = x;
            UserImageClickCoordinates[0, 1] = y;
           // CVut.SetUserSelectedSorterGate(UserImageClickCoordinates);
        }

        public int[,] GetUserImageClickCoordinates()
        {
            return UserImageClickCoordinates; 
        }

        public void paintFigure(Color color, string shape, int x, int y, int width, int height )
        {
            CGuiHandler.PaintFigure(CTestGUI, color, shape, x, y, width, height );
        }

        public List<string> GetTestCaseList()
        {
            RefreshTestCaseList();
            return testCaseList;
        }

        //Retuen the List containing information if the TC is Selected or Not
        public List<bool> GetTestCaseSelectionStatus()
        {
            return testCaseSelectionStatus;
        }
        //It is called when the user select or unselect a TC. 
        public void ChangeTestCaseSelectionStatus(int TcIndex, bool status)
        {
            testCaseSelectionStatus[TcIndex] = status;
            if (testMode == DEBUG_TEST_MODE) LatestDbgCasesUsed = testCaseSelectionStatus;
        }

        /// <summary>
        /// The user clicked button Stop. It Stops the test case execution
        /// </summary>
        public void StopTestCaseExecution(Object sender)
        {
            responseExecutionStatusMsg = Msgs.Info.EXECUTION_STOPPED_BY_USER; 
            Log.Info(this.GetType().Name, "\"" + testCaseList[currentTCIndex] + "\" " + responseExecutionStatusMsg);
            CGuiHandler.EnableDisableStopBtn(CTestGUI, false);
            CGuiHandler.EnableDisableSaveLogBtn(CTestGUI, true);
            ShowUserIndicationInfo = false;
            CleanInfoInAuxUserIndicationLbl();
            //Remake the line below. the TC is done and this run before it jumps to the next TC the the Verdict is Soped instead of Pass or Fail etc
            CGuiHandler.SetTestCaseVerdict(CTestGUI, GetTcNoPassVerdict(), currentTCIndex, testCaseList);
            CGuiHandler.DestroyFailPnlIfNoResponseFromVut(CTestGUI);
            if (testMode == DEBUG_TEST_MODE)
                CGuiHandler.ShowSessionVerdictStatus(CTestGUI, GetTSessionVerdict(TestSessionVerdict.IRRELEVANT)); 
            else
                CGuiHandler.ShowSessionVerdictStatus(CTestGUI, GetTSessionVerdict(TestSessionVerdict.INCONCLUSIVE));

            if (ActiveThread)
                tcThread.Abort();
            ActiveThread = false;
            CGuiHandler.RunningBarStop(CTestGUI);
            if (SaveLogs && (sender as Button) != null && (sender as Button).Name == "btn_stop") SaveLogsToFile();
            tMgrProcessState = TestManagerProcessState.EXECUTION_STOPPED;    
        }

        public void AtsModeBtnClicked()
        {
            CGuiHandler.EnableDisableAtsBtn(CTestGUI, false);
            CGuiHandler.EnableDisableDbgBtn(CTestGUI, true);
            CGuiHandler.ChangeInfoInLblsInfo(CTestGUI, Msgs.Uii.ATS_MODE_TESTING, 1, Color.Black);
            CGuiHandler.ShowSessionVerdictStatus(CTestGUI, GetTSessionVerdict(TestSessionVerdict.INACTIVE));
            CGuiHandler.ShowSelectAll(CTestGUI, false);
            CGuiHandler.ShowRIMSensorPresent(CTestGUI, true, ForceTestWithRIMSensor);
            CGuiHandler.DestroyPanelProductInfo();
            testMode = ACCEPTANCE_TEST_MODE;
            RefreshTestCaseList();
            SetAllTestCasesAsSelected();
            CGuiHandler.ShowTestCaseList(CTestGUI, GetTestCaseList(), testCaseSelectionStatus);
        }

        public void DbgModeBtnClicked()
        {
            CGuiHandler.EnableDisableAtsBtn(CTestGUI, true);
            CGuiHandler.EnableDisableDbgBtn(CTestGUI, false);
            CGuiHandler.ChangeInfoInLblsInfo(CTestGUI, Msgs.Uii.DEBUG_MODE_TESTING, 1, Color.Red);
            CGuiHandler.ShowSessionVerdictStatus(CTestGUI, GetTSessionVerdict(TestSessionVerdict.INACTIVE));
            CGuiHandler.ShowSelectAll(CTestGUI, true);
            CGuiHandler.ShowRIMSensorPresent(CTestGUI, false, ForceTestWithRIMSensor);
            CGuiHandler.ShowProductInfoPnl(CTestGUI);
            testMode = DEBUG_TEST_MODE;
            RefreshTestCaseList();
            SetAllTestCasesAsUnSelected(false);  //False = let the used latest used Debug cases selected
            CGuiHandler.ShowTestCaseList(CTestGUI, GetTestCaseList(), testCaseSelectionStatus);
        }

        public void SaveLogsBtnClicked()
        {
            SaveLogs = true ? !SaveLogs : false;
            CGuiHandler.ChangePictureBtnSaveLog(CTestGUI, SaveLogs);
            if (SaveLogs) 
                CGuiHandler.ChangeInfoInLblsInfo(CTestGUI, Msgs.Uii.SAVE_LOG_ACTIVE, 3, Color.Black);
            else CGuiHandler.ChangeInfoInLblsInfo(CTestGUI, "", 3, Color.Black);
        }

        public void SelectAllCboxChanged(bool isChecked)
        {
            
            if (isChecked)
            {
                SetAllTestCasesAsSelected();
            }
            else
            {
                SetAllTestCasesAsUnSelected(true); //True = unselect All
            }
            CGuiHandler.ShowTestCaseList(CTestGUI, GetTestCaseList(), testCaseSelectionStatus);
        }
        public void RIMSensorPresentCboxChanged(bool isChecked)
        {
            ForceTestWithRIMSensor = isChecked;
            RefreshTestCaseList();
            CGuiHandler.ShowTestCaseList(CTestGUI, GetTestCaseList(), testCaseSelectionStatus);
        }

        public void TestTypeToolTripChanged(bool isDoorOnlyTest)
        {
            Properties.Settings.Default.DoorOnlyTest = doorOnlyTest = isDoorOnlyTest;
            Properties.Settings.Default.Save();
            RefreshTestCaseList();
            
            CGuiHandler.ShowTestCaseList(CTestGUI, GetTestCaseList(), testCaseSelectionStatus);
        }
        public void test()
        {
            Log.Info(this.GetType().Name, Msgs.Info.TEST_SESSION_STARTED);
            CGuiHandler.ResetTCVerdict(CTestGUI);
            tMgrProcessState = TestManagerProcessState.RUNNING_TC;
            tSessionVerdict = TestSessionVerdict.IN_PROGRESS;
            timer.Interval = TIMER_INTERVAL_TEST_RUNNING;
            ActiveThread = true;
            tcThread = new Thread(StartTestCaseExecution1);
            CGuiHandler.EnableDisableRunBtn(CTestGUI, false);
            CGuiHandler.EnableDisableStopBtn(CTestGUI, true);
            CGuiHandler.EnableDisableAtsBtn(CTestGUI, false);
            CGuiHandler.EnableDisableDbgBtn(CTestGUI, false);
            CGuiHandler.EnableDisableMenu(CTestGUI, false);
            CGuiHandler.EnableDisableSaveLogBtn(CTestGUI, false);
            CGuiHandler.ShowSessionVerdictStatus(CTestGUI, GetTSessionVerdict(tSessionVerdict));
            tcThread.Start();
        }
       
        /// <summary>
        /// Start the TC execution according to the testcase list order
        /// </summary>
        public void StartTestCaseExecution1()
        {   
            ITestCase TestCaseToBeExecuted;
           
            for (int i = 0; i < testCaseSelectionStatus.Count; i++)
            {
                currentTCIndex = i; //a global memeber of the class used in case the execution is stopped
                if (testCaseSelectionStatus[i] == true)
                {
                    ResetAllBeforeTestCaseStart();
                    if ((TestCaseToBeExecuted = CTestXSelector.GetNextTestToExecute(testCaseList[i], testMode, CVut, this)) != null)
                    {
                        CCurrentTestCase = TestCaseToBeExecuted;
                        Log.Info(this.GetType().Name, "\"" + testCaseList[currentTCIndex] + "\" " + " execution started");
                        CGuiHandler.SetTestCaseVerdict(CTestGUI, Msgs.Verdict.TC_RUNNING, i, testCaseList);
                        responseExecutionStatusMsg = "";
                        if (TestCaseToBeExecuted.RunTestCase(ref responseExecutionStatusMsg))
                        {
                            Log.Info(this.GetType().Name, "\"" + testCaseList[currentTCIndex] + "\" " + " PASS");
                            CGuiHandler.SetTestCaseVerdict(CTestGUI, Msgs.Verdict.TC_PASS, i, testCaseList);
                            if ((tSessionVerdict != TestSessionVerdict.FAIL) && (tSessionVerdict != TestSessionVerdict.INCONCLUSIVE)) 
                                tSessionVerdict = TestSessionVerdict.PASS;
                        }
                        else
                        {
                            Log.Info(this.GetType().Name, "\"" + testCaseList[currentTCIndex] + "\" " + " " + GetTcNoPassVerdict()
                                + " " + " Execution response: " + responseExecutionStatusMsg);
                            CGuiHandler.SetTestCaseVerdict(CTestGUI, GetTcNoPassVerdict(), i, testCaseList);
                        }
                    }
                    else
                    {
                        Log.Info(this.GetType().Name, "\"" + testCaseList[currentTCIndex] + "\" " + " " + Msgs.Verdict.TC_NONE
                            + " Test Case does not exist or has not been implemented yet");
                        CGuiHandler.SetTestCaseVerdict(CTestGUI,Msgs.Verdict.TC_NONE, i, testCaseList);
                        if (tSessionVerdict != TestSessionVerdict.FAIL) tSessionVerdict = TestSessionVerdict.INCONCLUSIVE;
                        
                    }
                    Thread.Sleep(500);
                }
            }
            CCurrentTestCase = null;
            ActiveThread = false;
            if (testMode == DEBUG_TEST_MODE) tSessionVerdict = TestSessionVerdict.IRRELEVANT; 
            CGuiHandler.ShowSessionVerdictStatus(CTestGUI, GetTSessionVerdict(tSessionVerdict));           
            if (SaveLogs) SaveLogsToFile();          
            tMgrProcessState = TestManagerProcessState.REDY_FOR_TESTING;
        }

        private void SaveLogsToFile()
        {
            Logger.SaveLogToFile(DefaultLogPath + GetLogFileName());
            Logger.EmptyLogBuffer();
        }

        private string GetLogFileName()
        {
           
            if (testMode == ACCEPTANCE_TEST_MODE)
                return DateTime.Now.ToString("yy-MM-dd_HH-mm-ss_") + "TS_Log_" + Msgs.AppNameAndVersion.APP_C2VATS_VERSION.Replace(' ', '_').Replace('.', '_') + ".txt";
            else
                return DateTime.Now.ToString("yy-MM-dd_HH-mm-ss_") + "DS_Log_" + Msgs.AppNameAndVersion.APP_C2VATS_VERSION.Replace(' ', '_').Replace('.', '_') + ".txt";
        }
        //Related to Coin Drop Evaluation Test -----------
        public void ShowCoinMeasuresTable()
        {
            CGuiHandler.ShowCoinMeasuresTable(CTestGUI);
        }
        public void ShowMeasuredDataInTable(byte[] measures, List<int> meaureRemarks)
        {
            CGuiHandler.ShowMeasuredDataInTable(CTestGUI, measures, meaureRemarks);
        }
        public void ShowLimitsInTable(ITestCase CurrentTestCase, byte[] upperLimits, byte[] lowerLimits)
        {
            CGuiHandler.UpdateUpperLowerLimitsInTable(CurrentTestCase.GetTestCaseName(), CTestGUI, upperLimits, lowerLimits);
        }
        public void DestroyMeasuresTable()
        {
            CGuiHandler.DestroyMeasuresTable(CTestGUI);
        }
        public void NewCoinSelection(string coinSelected, string currentIso, string newCoinSource)
        {
            this.NewCoinSelected = true;
            this.NewCoinSrc = newCoinSource;
            CurrentISO = currentIso;
            CoinSelected = coinSelected;
        }
        public void WaitForNewConnectedValidator()
        {
            this.WaitingForNewConnectedValidator = true;
        }
        //End

        //Releated to CoinDrop Statistics and Comparison ---------
        public void ShowCoinStatisticsUI()
        {
            CGuiHandler.ShowCoinStatisticsAndComparisonUI(CTestGUI);
            
        }

        public void ShowMeasuredValuesToPlotGraphic(byte[] measures)
        {
            CGuiHandler.ShowNewMeasuredValuesToPlotGraphic(CTestGUI, measures);
        }

        public void ShowFailPnlVutDoesntRespond(Point location)
        {
            CGuiHandler.ShowFailPnlIfNoResponseFromVut(CTestGUI, location);
        }
        public void DestroyFailPnlVutDoesntRespond()
        {
            CGuiHandler.DestroyFailPnlIfNoResponseFromVut(CTestGUI);
        }
        //END

        public void GarageBtnClicked()
        {
            CGuiHandler.ShowGarageUI(CTestGUI);
        }
        public void ShowInformationInGarage(int infoType, List<Control> ctrlToShowInfo)
        {
            switch (infoType)
            {
                case 0: new RDShowProductId(CVut, ctrlToShowInfo);
                    break; 
                case 1: new RDShowFunctionSetup(CVut, ctrlToShowInfo);
                    break;
                case 2: new RDShowChannelSetup(CVut, ctrlToShowInfo);
                    break;
                case 3: new RDShowChannelIDs(CVut, ctrlToShowInfo);
                    break;
                case 4: new RDShowCoinLimits(CVut, ctrlToShowInfo);
                    break;
            }
        }
        

        //Related to Material sensor Analysis----------
        public void ShowMaterialSensorPanel()
        {
            CGuiHandler.ShowMaterialSensorPanel(CTestGUI);
        }
        public void ShowMaterialSensorMeasuredDataInTbl(int[] ReaultsAndRemarks)
        {
            CGuiHandler.ShowMaterialResultInTbl(CTestGUI, ReaultsAndRemarks);
        }
        public void StopOnFailTriggered()
        {
            CGuiHandler.StopOnFailTriggered(CTestGUI);
        }
        public void DestroyMaterialSensorAnalysis()
        {
            CGuiHandler.DestroyMaterialSensorAnalysis(CTestGUI);
        }
        //END

        //F0 --------
        public void SaveInfoIntoProductIdTbl(int fromBytePos, byte[] content)
        {
            CVut.CVutTableOps.WriteDataToProductIdTbl(fromBytePos, content);
        }

        public VutIdentificationData GetValidatorIdData()
        {
           return  CVut.GetVutIdentificationData();
        }

        public VutTableOps GetVutTableOps()
        {
            return CVut.CVutTableOps; 
        }
        public bool IsToolPassValid(string pass)
        {
            if (Properties.Settings.Default.ToolPass != pass)
                return false;
            else toolPass = pass;

            return true;
        }
       // public void DecodeDataClicked()
    }
}
