﻿/**
 * AbstractClass: TableOperations
 * This is an abstract classs is a representation of some of the common operations to the tables in the validator.
 * 
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Utils;

namespace C2ValidatorATS.Interfaces
{
    abstract class TableOperations
    {
        public static int DATA_BLOCK_SIZE = 28;
        public static int PRODUCTION_ID_TBL_ID = 0xF0;
        public static int PRODUCTION_ID_TBL_SIZE = 27;
        public static byte FUNCTION_SETUP_TBL_ID = 0x82;
        public static int FUNCTION_SETUP_TBL_SIZE = 32;  //According to de doc the table is 32 bytes size but I just read out 30 - 2 bytesrequest command ?
        public static byte CHANNEL_SETUP_TBL_ID = 0x81;
        public static int CHANNEL_SETUP_TBL_SIZE = 28;
        public static byte CHANNEL_IDS_TBL_ID = 0x80;
        public static int CHANNEL_IDS_TBL_SIZE = 196;
        public static int COIN_LIMITS_TBL_SIZE = 52;
        public static byte VALIDATOR_TYPE_ID = 1;
        //PollResult 
        public static byte FLASH_SUCCESSEFULLY_DONE = 0xFE; //byte1 0xFE || byte2 0xF0 = FE
        public static byte FLASH_PROCESS_ERROR = 0xFF; //byte1 0xFE || byte2 0xF1 = FF
        public static byte POLL_NOTHING_TO_REPORT = 0x00; //byte1 0x00 || byte2 0x00 = 00
        //Access Level All
        public static byte[] ACCESS_LEVEL_PASSWORD = new byte[] { 0x0D, 0x42, 0x18, 0xA8, 0x24, 0xFF, 0xB3, 0xE7, 0xF7, 0xCC, 0x39, 0x44, 0xDB, 0x12, 0x04 };
        
        public static Leogger Log = Logger.instance;
        public bool operationResultOK;

        //Will temporary give full access to all tables
        public static bool ChangeTableAccessLevelToAll()
        {
            byte[] rspData = null;
            if (CX2ProtAdaptLayer.SetAccessLevel(VALIDATOR_TYPE_ID, ACCESS_LEVEL_PASSWORD, out rspData))
            {
                if ((CX2ProtAdaptLayer.GetAccessLevel(VALIDATOR_TYPE_ID, out rspData)) && (rspData != null) && (rspData.Count() == 1) && (rspData[0] == 13))
                {
                    Log.Info("Abstract Class TableOperations-->", "  Table Access granted. Access Level = " + rspData[0].ToString("X"));
                    return true;
                }
                else
                {
                    Log.Error("Abstract Class TableOperations-->", "  Table Access No granted. Response returned = " + (rspData != null ? rspData[0] : 0xFF));
                    return false;
                }
            }
            Log.Error("Abstract Class TableOperations-->", " Fail to Grant Access Level");
            return false;
            
        }
        public static bool DeleteTable(byte table)
        {     //2 = Production Info table , 3 = Ref Data table, 8 = Firmware

            byte[] requestCmd = new byte[] { 0xF0, table };
            byte[] responseData;
            if (CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, requestCmd, out responseData))
            {
                Log.Info("Abstract Class TableOperations-->", "  Table Successfully deleted");
                return true;
            }
            return false;
        }
        //Flash Prepare Datablock Update. Must be called before any new table is written to the device
        public static bool PrepareForUpdate()
        {
            byte[] requestCmd = new byte[] { 0x02, 0x00 };
            byte[] responseData;
            if (CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, requestCmd, out responseData))
            {
                Log.Info("Abstract Class TableOperations-->", " Preparing for flashing of new table successfully done");
                return true;
            }
            Log.Error("Abstract Class TableOperations-->", " Preparing for flashing FAIL");
            return false;
        }
        //Must be called when all new tables have been written to the device. Stores changed tables to flash
        //Device will answer immediately with ACK. After that the programming process runs and the device will not answer for about 1s
        //second. The subsequent Poll comes up with a Resultevent: 0xFEF0 = successful, 0xFEF1 = failed, details are stored in Value 0x16
        public static bool FlasWrittenTable()
        {
            byte[] requestCmd = new byte[] { 0x03, 0x00 };
            byte[] responseData;
            if (CX2ProtAdaptLayer.ExecFunction(VALIDATOR_TYPE_ID, requestCmd, out responseData))
            {
                ///######## Log here
                if (IsFlashProcessSuccessfullyExecuted())
                {
                    Log.Debug("Abstract Class TableOperations-->", " Flash Successfully execute");
                    return true;
                }
                Log.Error("Abstract Class TableOperations-->", " Flashing Process Fail");
            }
            Log.Error("Abstract Class TableOperations-->", " Writing operation Did not Success");
            return false;
        }
        //Return alwasy true. The fact is that polling process may return error msgs when datablock is not present or Sortergate is not mounted.
        //So this method is usefull to give sometie afte execution command and for debug propouse to check error codes
        public static bool IsDeviceRedyToBeFlashed()
        {
            byte[] responseData = new byte[2];
            int timeout = 1500;
            int sleepTime = 100;
            while (timeout > 0)
            {
                System.Threading.Thread.Sleep(sleepTime);
                if (CX2ProtAdaptLayer.PollDevice(VALIDATOR_TYPE_ID, out responseData) && (responseData.Count() == 2))
                {
                    Log.Debug("Abstract Class TableOperations-->", " Poll result: byte 1 = " + responseData[0].ToString("X") + " byte 2 = " + responseData[1].ToString("X"));
                    if ((responseData[0] | responseData[1]) == 0)
                    {
                        Log.Debug("Abstract Class TableOperations-->", " Poll result: Device Redy To Be Flashed");
                        return true;
                    }
                }
                timeout -= sleepTime;
            }
            Log.Debug("Abstract Class TableOperations-->", " Time Out. Polling shows some errors most probably, not affecting the Falshing preparation process");
            Log.Debug("Abstract Class TableOperations-->", " Poll Error Message: byte 1 = " + responseData[0].ToString("X") + " byte 2 = " + responseData[1].ToString("X"));
            return true;           
        }

        public static bool IsFlashProcessSuccessfullyExecuted()
        {
            byte[] responseData = new byte[2];
            bool nothingToReport = false;
            bool flashSuccessfullyDone = false;
            int timeout = 1500;
            int sleepTime = 100;
            while ((timeout > 0) && (!nothingToReport || !flashSuccessfullyDone))
            {
                System.Threading.Thread.Sleep(sleepTime);
                if (CX2ProtAdaptLayer.PollDevice(VALIDATOR_TYPE_ID, out responseData) && (responseData.Count() == 2))
                {
                    Log.Debug("Abstract Class TableOperations-->", " Poll result: byte 1 = " + responseData[0].ToString("X") + " byte 2 = " + responseData[1].ToString("X"));
                    if ((responseData[0] | responseData[1]) == POLL_NOTHING_TO_REPORT)
                    {
                        nothingToReport = true;
                    }
                    else if((responseData[0] | responseData[1]) == FLASH_SUCCESSEFULLY_DONE)
                    {
                        flashSuccessfullyDone = true;
                    }
                    else if ((responseData[0] | responseData[1]) == FLASH_PROCESS_ERROR)
                    {
                        Log.Error("Abstract Class TableOperations-->", " Flash process error reported! Poll result: byte 1 = " + responseData[0].ToString("X") + " byte 2 = " + responseData[1].ToString("X"));
                        CX2ProtAdaptLayer.GetValue(VALIDATOR_TYPE_ID, new byte[]{0x16}, out responseData);
                        Log.Error("Abstract Class TableOperations-->", " GetValue, Flash process Details: byte 1 = " + responseData[0].ToString("X") + " byte 2 = " + responseData[1].ToString("X"));

                        return false;
                    }
                }
                timeout -= sleepTime;
            }
            if (timeout <= 0)
            {
                Log.Debug("Abstract Class TableOperations-->", " Time Out. Polling shows some errors most probably, not affecting the Falshing process");
                Log.Debug("Abstract Class TableOperations-->", " Poll Error Message: byte 1 = " + responseData[0].ToString("X") + " byte 2 = " + responseData[1].ToString("X"));
            }          
            return true;
        }

        public void AddCheckSumToData(int antalBytes, ref byte[] tableData)
        {
	        int result = 0;
            for (int i = 0; i < antalBytes - 1; i++)
            {
                result += tableData[i] & 0xFF;
            }
            result = 0x100 - (result & 0xFF);
            tableData[antalBytes - 1] = (byte)result;
        }
    }
}
