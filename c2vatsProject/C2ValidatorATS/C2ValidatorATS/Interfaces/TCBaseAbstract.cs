﻿/**
 * Abstract Class: TCBaseAbstract
 * This calls inherit from interface ITestCase. TCBaseAbstract define some variables and methods used by all test and debug cases.
 * Author: Leonardo Garcia, PayComplete
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Utils;

namespace C2ValidatorATS.Interfaces
{
    abstract class TCBaseAbstract: ITestCase
    {
        public string TEST_CASE_NAME = "";
        public Leogger Log = Logger.instance;
        public const int MAX_TC_COMPLETION_TIME = 30000;

        public string GetTestCaseName()
        {
            return TEST_CASE_NAME;
        }

        /// <summary>
        /// Pre-test initialization. It checks the the VUT is properly connected and it is reachable.
        /// </summary>
        /// <returns></returns>
        public bool InitializeTestCase(byte validatorId, ref string executionResultMsg)
        {
            if (!CommAssistant.Instance.IsValidatorConnected(validatorId, ref executionResultMsg))
            {
                return false;
            }
            return true;
        }

        public abstract bool RunTestCase(ref string resultMsg);
        
    }
}
