﻿/**
 * Class: Leogger
 * This is an abstract classs that serves as base for a logger implementation. 
 * Member class DisplayText can be used optionally to keep track of all kind of loggs. Its textToDisplay member
 * may be used as "target" when calling membr functions like "Debug()". This is implemented to make information 
 * available from any class.
 * Please override in your project the methods defined here.
 * Author: Leonardo Garcia
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace C2ValidatorATS.Interfaces
{
    abstract class Leogger
    {
        public enum LLevel
        {
            ALL,  //Include all type of logs
            DEBUG,   //Include DEBUG and all below type of logs
            INFO,  //Include INFO and all below type of logs
            WARNING, //Include WARNING and all below type of logs
            ERROR,  //Include ERROR only
        }
        public static LLevel LogLevel {get; set;}

        public static bool debug = false;
        public static bool info = false;
        public static bool warning = false;
        public static bool error = false;

        public static class DisplayText
        {
            static List<string> logType = new List<string>();
            static List<string> textToDisplay = new List<string>();
            static List<string> logBuffer = new List<string>();

            public static List<string> GetTextToDisplay()
            {
                return textToDisplay;
            }

            public static List<string> GetLogBuffer()
            {
                return logBuffer;
            }
            public static List<string> GetLogType()
            {
                return logType;
            }

            public static void SetTextToDisplay(string text)
            {
                textToDisplay.Add(text);
                logBuffer.Add(text);
                //SetLogType(type);
            }

            static void SetLogType(string type)
            {
                logType.Add(type);
            } 
        }
        //Dynamic change of the loglevel
        public static void SetLogLevel(LLevel level)
        {
            LogLevel = level;
            debug = true ? (LogLevel == LLevel.DEBUG || LogLevel == LLevel.ALL) : false;
            info = true ? (LogLevel == LLevel.INFO || LogLevel == LLevel.DEBUG || LogLevel == LLevel.ALL) : false;
            warning = true ? (LogLevel == LLevel.WARNING || LogLevel == LLevel.INFO || LogLevel == LLevel.DEBUG || LogLevel == LLevel.ALL) : false;
            error = true ? (LogLevel == LLevel.ERROR || LogLevel == LLevel.WARNING || LogLevel == LLevel.INFO || LogLevel == LLevel.DEBUG || LogLevel == LLevel.ALL) : false; 
        }
        //Write the content in a text file
        public static void SaveLogToFile(string logPath)
        {
            if (DisplayText.GetLogBuffer() != null)
            {
                File.WriteAllLines(logPath, DisplayText.GetLogBuffer());
            }
        }

        public static void EmptyLogBuffer()
        {
            DisplayText.GetLogBuffer().Clear();
        }
        public virtual void Debug(String className, string logMsg)
        {

        }

        public virtual void Debug(String className, string logMsg, ref List<string> target)
        {
            
        }

        public virtual void Debug(String className, string logMsg, ref List<string> target, bool saveToFile)
        {

        }

        public virtual void Info(String className, string logMsg)
        {

        }

        public virtual void Info(String className, string logMsg, ref List<string> target)
        {          
            
        }

        public virtual void Info(String className, string logMsg, ref List<string> target, bool saveToFile)
        {

        }

        public virtual void Warning(String className, string logMsg)
        {

        }

        public virtual void Warning(String className, string logMsg, ref List<string> target)
        {

        }

        public virtual void Warning(String className, string logMsg, ref List<string> target, bool saveToFile)
        {

        }

        public virtual void Error(String className, string logMsg)
        {

        }

        public virtual void Error(String className, string logMsg, ref List<string> target)
        {

        }

        public virtual void Error(String className, string logMsg, ref List<string> target, bool saveToFile)
        {

        }

    }
}
