﻿/**
 * Interface: ITestCase
 * It shows base functinality of a testcase. This interface have to be inherit by all TC classes
 * Author: Leonardo Garcia, Suzuhapp
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C2ValidatorATS.Utils;

namespace C2ValidatorATS.Interfaces
{
    interface ITestCase
    {
        string GetTestCaseName();
        bool InitializeTestCase(byte valId, ref string executionResultMsg);
        bool RunTestCase(ref string executionResultMsg);
    }
}
